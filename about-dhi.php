
<!doctype html>
<html lang="en">

  <body>
<?php include 'header.php';?>

<section class="bg-col-1 commmon-padd">
<div class="container">
 <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-12 col-md-9">
          <p class="float-right d-md-none">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="row">
            <div class="col-sm-6">
              <img src="image/doctors.png" class="img-fluid" alt="">
              <h1 class="text-center mt-3"><a href="contact-us.php"  class="btn btn-common my-2 my-sm-0 ">Contact Us</a></h1> 
            </div>
            <div class="col-sm-6">
              <p>DHI Medical Group is the world’s largest chain of hair restoration clinics with 60 clinics in 36 countries.</p>
              <p>After having pioneered FUE in 2003 DHI innovated and launched the Direct Hair Implantation procedure in 2005. DHI is now considered the gold standard in hair restoration with the strongest research advisory board and world class facilities and US and EU patents. DHI continues to launch new treatments for various stages of hair loss.</p>
              <p>At DHI you are not just a client, but a patient whose best interest is in mind of our medical teams.</p>
              <p> DHI international performed thousand procedures including those of celebrities like Bollywood actors, Sportsmen, Politicians and Business Tycoons.</p>

            </div>
          </div>
          <!--/row-->
        </div><!--/span-->

        <div class="col-6 col-md-3 sidebar-offcanvas about-sidebar" id="sidebar">
          <div class="list-group">
             <a href="about-dhi.php" class="list-group-item active"> About DHI</a>
             <a href="about-us.php" class="list-group-item"> Milestone</a>
           <!--  <a href="about-scientific.php" class="list-group-item">Scientific Board</a> -->
            
            
          </div>
        </div><!--/span-->
      </div><!--/row-->

      
  
</div>


</section>

<section class="bg-col-1 commmon-padd">
  <div class="container">
    <div class="row ">
      
      <div class="col-sm-12 text-center">
        <h2 class="pb-5">The Most Experienced Medical Team in the World</h2>
        <img src="image/group.jpg" alt="" class="img-fluid">
<!-- <h1 ><a href="hair-transplant-restore.php"  class="btn btn-common my-2 my-sm-0 ">read more</a></h1>  -->
      </div>
     
    </div>
    
  </div>
</section>
  <section class="bg-col-1 commmon-padd">
    <div class="container">
      <div class="row">
      <div class="col-sm-4">
        <img src="image/Quality-Standardization.png" alt="" class="img-fluid">
      </div>
      <div class="col-sm-8">
        <h3>Quality Standards</h3>
        <p>For the first time in the history of hair restoration, DHI Global Medical Group introduced standard operating procedures (SOPs). These apply to all processes in order to guarantee safety, quality and great results every time. At every DHI clinic, each doctor and member of t staff follows these procedures and in extra ordinary cases, the scientific board advises.</p>
        <p>In recognition of its unique excellence, DHI has been awarded various certifications including CQC – UK, TUV – Austria, ACHS- Australia and ISO.</p>
      </div>
    </div>
    </div>
    
  </section>   
<div class="clearfix"></div>
<section class="commmon-padd">
  <div class="container">
    <h2 class="text-center">Featured In</h2>
    <div class="row pt-4 pb-4 text-center">
      <div class="col-sm-3"><a href="javascript:void(0);"><img src="image/news/bbc.png" alt="" class="img-fluid" width="150"></a></div>
      <div class="col-sm-3"><a href="http://www.thehindubusinessline.com/news/india-a-potential-market-for-hair-restoration-services-survey/article5500996.ece" target="_blank"><img src="image/news/business.png" alt="" class="img-fluid" width="150"></a></div>
      <div class="col-sm-3"><a href="http://www.dailymail.co.uk/sport/football/article-2328797/The-Footballers-Football-Column--Richard-Lee-Play-pain-hard-Brentford-bounce-back.html#ixzz2Ua0pXSAZ" target="_blank"><img src="image/news/Daily_mail_642.png" alt="" class="img-fluid" width="150"></a></div>
       <div class="col-sm-3"><a href="http://www.financialexpress.com/industry/dhi-launches-direct-hair-fusion-the-latest-technique-in-their-range-of-hair-restoration-solutions/138843/" target="_blank"><img src="image/news/fe-new-logo-rtl.png" alt="" class="img-fluid" width="150"></a></div>

       <div class="col-sm-3"><a href="http://indiatoday.intoday.in/story/hair-brained-cheap-transplants-can-damage-scalp/1/154651.html" target="_blank"><img src="image/news/tech-logo.png" alt="" class="img-fluid" width="150"></a></div>

        <div class="col-sm-3"><a href="http://businesswireindia.com/news/news-details/dhi-launches-direct-hair-fusion-latest-technique-their-range-hair-restoration-solutions/45336" target="_blank"><img src="image/news/bwlogo_extreme.png" alt="" class="img-fluid" width="150"></a></div>

<div class="col-sm-3"><a href="http://www.ptinews.com/pressrelease/13849_press-subRevolutionary-New-Technology-by-DHI-Medical-Group-Now-Let-s-You-Create-a-Dense-Scalp-of-Hair-Without-Surgery" target="_blank"><img src="image/news/presstrust.png" alt="" class="img-fluid" width="150"></a></div>

<div class="col-sm-3"><a href="http://www.indiaprwire.com/pressrelease/health-care/2011020176511.htm" target="_blank"><img src="image/news/india.png" alt="" class="img-fluid" width="150"></a></div>

    </div>
  </div>
</section>
     
<section class="bottm_sec">
  <h1>Increase your hair density naturally</h1>
   <a href="book-appoint.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>


      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


   

  </body>
</html>
