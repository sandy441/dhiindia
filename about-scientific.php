
<!doctype html>
<html lang="en">
  
  <body>
<?php include 'header.php';?>

<section class="bg-col-1 commmon-padd">
<div class="container">
 <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-12 col-md-9">
          <p class="float-right d-md-none">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="jumbotron">
            <h4>WORLD RENOWNED EXPERTS, UNIVERSITY PROFESSORS AND MEDICAL SPECIALISTS</h4>
            
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/team1.jpg" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
      <div class="float-left">
        <h5 class="mt-0">Konstantinos P. Giotis</h5>
    <p>DHI Medical Group Founder</p>
      </div>
      <div class="float-right">
        <button type="button" class="btn btn-outline-secondary" data-toggle="collapse" data-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample1">Show Cv <span class="oi oi-arrow-right"></span></button>
      </div>
    </div>
    

  </div>

</div>
    <div class="collapse pb-5" id="collapseExample1">
  <div class="card card-body">
  <p>Konstantinos P. Giotis established the DHI Global Medical Group in 1970 in Palo Alto California. His sole aim was to offer a pain less, scar free, minimally invasive hair restoration procedure, available to all and most important at an affordable cost.</p> 

  <p>DHI Global Medical Group has grown to become the world’s largest Hair Restoration provider performing unique techniques such as ‘Direct Hair Implantation’, ‘Directin’, ‘Direct Micropigmentation’, and ‘Direct PRP’ treatments, which are all part of the DHI ‘Total Care System’, in more than 50 locations around the World.</p>

 <p>DHI is certified with ISO, registered with CQC UK and approved by the World Health Association and 5 Public Universities in France, Italy, Greece and Czech Republic.</p>

<p>K.P.Giotis has also published various papers in the Hair Transplant forum such as “Hair Restoration Surgery Is Not the Airlines Industry” and many more.</p>

<p>Dr. Walter Unger, in the Journal of Dermatology, quoted K. P. Giotis “As one of the pioneers of the field worldwide”.</p>

<p>As a result of K. P. Giotis life time devotion to hair loss, he was awarded diplomas by the University of South Lyon in France, after successfully teaching and contributing to the courses and annual lectures of “Restorative Scalp Surgery” and ”baldness courses” for Prof. Christian Hospitalier in Lyon Sud, during the last 4 years.</p>
<p>In 2013, K.P. Giotis started collaboration with a research group of Harokopio University in Athens, in the field of Hair Biology.</p>
<p>In March 2013, he has presented the ‘DHI Total Care System’, the world’s first quality standardization in hair restoration, to a full auditorium of medical doctors, at Syngros Athens University and received a honorary certificate of recognition and appreciation.</p>
<p>K. P. Giotis is invited to lecture in a series of University courses in Lyon, Rome, Athens and Prague. His presentation at the ISHRS meeting in Amsterdam, on July 2009 entitled “The Psychological Aspects of Hair Loss” was well received by an audience of over 600 doctors from around the world.</p>
<p>His latest invention “Direct Hair Implantation” was presented at the ISHRS 2011 meeting in Alaska while the ”DHI Total Care System” was presented at ISHRS 2013 in San Fransisco and Imcas 2013 in Shanghai China.</p>

  <p>
In 2012, K. P. Giotis has established the DHI Training Academy in UK, dedicated to teach and train people on hair restoration methods and practices.</p>
<p>
The sole aim of the DHI Academy is to set the benchmarks of hair restoration and to provide R&D experts and specialists in Direct Hair Implantation and Directin techniques, as well as in Direct micropigmentation SMP and Direct PRP treatments.</p>

<p>The DHI Academy offers both online and hands on training to all interested parties involved in clinical operations, from the initial consultation and diagnostics, to performing Direct hair implantation, Direct PRP treatments and Direct micro pigmentation, throughout the year.</p>
  
<p>K. P. Giotis contributes to DHI Live Webinar, an interactive exchange on hair loss options and solutions, presenting the DHI ‘Total Care System’ to potential and existing clients.</p>

<p>K. P. Giotis has published a book in three different languages called “Hair Restoration Options”. This book has gained recognition by the leaders within the hair restoration field and also serves as an educational and informative account for hair loss sufferers.</p>


<p>
He is the president of the Academy Masters Meeting (www.dhiamm.com) and holds several patents on hair restoration methods, tools and instruments.</p>


  </div>
</div>
<div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Georgia-Ligda-MD.JPG" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
      <div class="float-left">
        <h5 class="mt-0">Georgia Ligda MD</h5>
    <p>Plastic Surgeon, PhD,
DHI Global Medical Director</p>
      </div>
      <div class="float-right">
        <button type="button" class="btn btn-outline-secondary" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2">Show Cv <span class="oi oi-arrow-right"></span></button>
      </div>
    </div>
    
  
  </div>
</div>
  <div class="collapse pb-5" id="collapseExample2">
  <div class="card card-body">
   <p> Georgia Lidga MD, is a plastic surgeon specialized in aesthetic plastic surgery.</p>
<p>She had her PhD from the University of Athens and she is a member of HESPRAS since 2002.</p>
<p>She is a certified DHI Doctor for Hair Restoration.</p>
<p>She has attended several congresses related to her specialty, given speeches about various medical themes in Rotary meetings and she is member of ‘Who is Who’ publication for the last 5 years.</p>
<p>She is also an expert plastic surgeon in the list of Athens City Court.</p>
  </div>
</div>

    
            </div>
             

          </div><!--/row-->
          
        </div><!--/span-->

        <div class="col-6 col-md-3 sidebar-offcanvas about-sidebar" id="sidebar">
          <div class="list-group">
             <a href="about-dhi.php" class="list-group-item "> About DHI</a>
             <a href="about-us.php" class="list-group-item"> Milestone</a>
         <!--    <a href="about-scientific.php" class="list-group-item active">Scientific Board</a> -->
            
            
          </div>
        </div><!--/span-->
      </div><!--/row-->

  <div class="row">
            <div class="col-sm-12 pt-3">
               <h4>DHI MEDICAL ADVISORY & RESEARCH BOARD</h4>
                 <hr>
            </div>
       
              
<div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/John-Curran-MD.JPG" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">John Curran MD</h5>
    <p>DHI Channel Islands Clinic Director</p>
      <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample3" aria-expanded="false" aria-controls="collapseExample3">Show Cv <span class="oi oi-arrow-right"></span></button>
      
      
    </div>
    
  
  </div>
</div>
  <div class="collapse pb-3" id="collapseExample3">
  <div class="card card-body">
    <p>John Curran MD is the Lead Physician and Chief Executive at the Aesthetic Skin Clinics in the UK Channel Islands and Ireland.</p>
    <p>A respected senior member of the medical community, Dr. Curran specialises in cosmetic dermatological, medical and surgical procedures and personally oversees the addition of all treatments that are offered by Skinstation. He is known professionally for this role in the education of cosmetic medical practitioners.
Dr. Curran and is an active Council member, Chief Examiner and the current President for the British Association of Cosmetic Doctors (BACD).</p>
<p>He also lectures regularly on aesthetic medicine, is published in leading Cosmetic Medicine Journals and is involved in monitoring and setting industry standards through the Committee. His experience of cosmetic procedures span over 15 years. Dr. Curran has special interests in skin ageing, radio frequency, laser treatments and dermatological surgery. He is popular with peers and patients alike.</p>
<p>Dr. Curran has extensive experience in performing the full range of cosmetic dermatological and medical treatments including, Advanced use of Botulinum Toxin, Hyperhidrosis management, Dermal Hydration Therapy/Fillers, Microsclerotherapy/Vein Treatments, Minor Surgery, Lipotherapy, Fraxel, Thermage, Skin Rejuvenation, Skin Care, and deep medical Peels.</p>
<p>Dr. Curran is the Chief Executive of the Aesthetic Skin Clinic and mentors the medical team at all clinics</p>

  </div>
</div>
</div>
<div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Ryan-Welter-MD,-PhD.JPG" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">Ryan Welter MD, PhD</h5>
    <p>Hair Transplant Surgeon</p>
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample4" aria-expanded="false" aria-controls="collapseExample4">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample4">
  <div class="card card-body">
    <p>Dr. Welter obtained his doctorate in Biochemistry and Molecular Biology from Oklahoma State University and his MD from the University of Oklahoma. He completed his residency at Brown University of Medicine where he remains a clinical associate professor. Research continues to be very important to Dr. Welter, and he continues to be active in clinical trials. In 2005, Dr Welter founded the New England Center for Hair Restoration and NEhair.com having built a state of the art operating suite designed exclusively for accommodating hair transplant surgery and stem cell research. Dr. Welter continues work as a pioneer in the field of hair transplant surgery, working to develop new techniques using stem cells to achieve unparalleled results in hair restoration density.
</p>
<p>Post Residency Hair Restoration and Transplant Surgery Training Areas of Emphasis Complete hair restoration FUE Micro-grafting techniques Research Practice Management Post Graduate University of Oklahoma College of Medicine, Oklahoma City, OK Doctor of Medicine May, 1999 Oklahoma State University, Stillwater, OK Doctor of Philosophy December, 1996 Major: Biochemistry and Molecular Biology Skills: Biotechnology, molecular genetics, protein biochemistry, bioengineering, bioenergetics Designed and implemented novel research projects, lead research teams, research budgets Thesis: Studies on electron transport molecules, binding sites, structural analysis, and chemical modification Undergraduate Oklahoma State University Bachelor of Science May, 1993 Major: Biochemistry Faculty Appointment (2005 – present) Associate Clinical Professor, Brown University School of Medicine Hospital Medical Staff Appointments 2002-present Attending Staff Physician Morton Hospital, Taunton, MA 2010- present Attending Staff Physician, Norwood Hospital, Norwood, MA 2001-2003 St. Anne’s Hospital, Fall River, MA House Staff Officer Routine Admissions, Codes, Urgent care consults. Covering ED, Tele, Floors, ICU 2000-2003 Mediplex Rehab Hospital, New Bedford, MA Night Coverage Physician Routine after hours in house coverage for rehab patients Floors covered include: neuropsychological unit, pulmonary/vented cardiac, and general rehab patients</p>


<p>Professional Societies</p>
<p>International Society of Hair Restoration Surgery, member</p>
<p>American Medical Association, member (1996-present)</p>
<p>American Academy of Family Physicians, member (1996-present)</p>
<p>Massachusetts State Medical Association (1999-present)</p>



  </div>
</div>
</div>
<div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Georgios-Dedoussis.jpg" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">Georgios Dedoussis</h5>
    <p>Associate Professor of Biology</p>
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample5" aria-expanded="false" aria-controls="collapseExample5">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample5">
  <div class="card card-body">
    <h4>Studies</h4>
<p>Degree in Biology, Physics, University of Patras (1988)<br />D.E.A. (Diplome d &lsquo;etudes approfondies) Enzymes, Biotransformation and Microbiology, Technological University of Compiegne, France (1990)<br />PhD thesis, Department of Microbiology, University of Athens Medical Sxolis (1995)<br />Areas of Research Interest: Clinical Genetics, Human Molecular Genetics</p>
<p><strong>Publications:</strong><br />1. Dupuis et al. New genetic loci implicated in fasting glucose homeostasis and their impact on type 2 diabetes risk. Nat Genet Nov 2009<br />2. Dedoussis GV, Kapiri A, Samara A, et al. Visfatin: the link between inflammation and childhood obesity. Diabetes Care. Jun 2009;32(6):e71.<br />3. Kanoni S, Dedoussis GV, Herbein G, et al. Assessment of gene-nutrient interactions on inflammatory status of the elderly with the use of a zinc diet score &ndash; ZINCAGE study. J Nutr Biochem. May 14 2009.<br />4. Ntalla I, Dedoussis G, Yannakoulia M, et al. ADIPOQ gene polymorphism rs1501299 interacts with fibre intake to affect adiponectin concentration in children: the GENe-Diet Attica Investigation on childhood obesity. Eur J Nutr. Jun 19 2009.<br />5. Smart MC, Dedoussis G, Louizou E, et al. APOE, CETP and LPL genes show strong association with lipid levels in Greek children. Nutr Metab Cardiovasc Dis. Apr 28 2009.<br />6. Lasky-Su J, Lyon HN, Emilsson V, et al. On the replication of genetic associations: timing can be everything! Am J Hum Genet. Apr 2008;82(4):849-858.<br />7. Mocchegiani E, Giacconi R, Costarelli L, et al. Zinc deficiency and IL-6 -174G/C polymorphism in old people from different European countries: Effect of zinc supplementation. ZINCAGE study. Exp Gerontol. 2008;43(5):433-444.<br />8. Dedoussis GV, Panagiotakos DB, Louizou E, et al. Cholesteryl ester-transfer protein (CETP) polymorphism and the association of acute coronary syndromes by obesity status in Greek subjects: the CARDIO2000-GENE study. Hum Hered. 2007;63(3-4):155-161<br />9. Dedoussis GV, Theodoraki EV, Manios Y, et al. The Pro12Ala polymorphism in PPARgamma2 gene affects lipid parameters in Greek primary school children: A case of gene-to-gender interaction. Am J Med Sci. Jan 2007;333(1):10-15.<br />10. Dedoussis GV. Apolipoprotein polymorphisms and familial hypercholesterolemia. Pharmacogenomics J. 2007;8(9):1179-1189.</p>
  </div>
</div>
</div>
     <div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Christian-Dubreuil.jpg" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">Christian Dubreuil</h5>
    <p>Professor of Ear Nose Throat</p>
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample6" aria-expanded="false" aria-controls="collapseExample6">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample6">
  <div class="card card-body">
    <p>Centre Hospitalier Lyon Sud Head and Neck Surgery &ndash; Oto Neuro Surgery Department 69495 Pierre Benite Cedex FRANCE<br />Director of the Department of ENT &ndash; Oto Neuro Surgery for 20 years (1986)<br />Professor of Oto Rhino Laryngology (exceptional Grade) at Lyon 1 University (1996)<br />Head and Neck Surgeon for Cancerology, Cervical and Plastic Surgery, 1977 &ndash; 1995.<br />Oto-neuro Surgeon Since 1987; more than 1,200 Acoustic Neuromas and Meningiomas of the Cerebello Pontine angle operated. Surgery of the Skull Base ; Paragangliomas, tumours of the Petrous Bone.<br />Surgery of the Facial Nerve : Decompression : Anastomosis, Facial Reparation Using Muscle transfers.<br />Surgery of Vertigo : Vestibular Neurotomy.<br />Middle Ear Surgery : Otosclerosis (150 &ndash; 180/ year), Cholesteatomas (120 &ndash; 180 / year), Tympanoplasties (150 &ndash; 200 / Year).<br />Middle Ear Implants : Med-El Symphonix, Middle Ear Transducer for Sensorineural Hearing Loss and Conductive Hearing Loss.<br />Many publications on Oto &ndash; Neurotology and Facial Nerve Surgery.</p>
  </div>
</div>
</div> 

<div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Jana-Hercogova-MD,-PhD.JPG" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">Jana Hercogova MD, PhD</h5>
    <p>Professor of Dermatology</p>
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample7" aria-expanded="false" aria-controls="collapseExample7">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample7">
  <div class="card card-body">
    <p>Chairwoman of the Prague Dermatology Centre<br />(Chairwoman of the Dermatology Dept., Charles University Prague)<br />(President of the European Academy of Dermatology and Venearology &ndash; EADV)</p>
<p><strong>Professional qualifications</strong><br />&ndash; MUDr., Medical School Qualification degree with honours, 2nd Medical School, Charles Univ., Prague (Czech equivalent to M.D.) &ndash; 1984<br />&ndash; Specialist on Dermatology and Venereology 1st degree, 2nd degree, Inst. for Postgrad. Education, Prague (Czech equivalent to Certification of the American Board of Dermatology) &ndash; 1988, 1993<br />&ndash; CSc., Thesis &acirc;&euro;žCutaneous Manifestations of Lyme Borreliosis&ldquo; (Czech equivalent to Ph.D.) &ndash; 1990<br />&ndash; Doc., Second doctorate on &acirc;&euro;žLong-term Follow-up of the Patients with Cutaneous Manifestations of Lyme Borreliosis&ldquo; (Czech equivalent to Associate Professor) &ndash; 1994<br />&ndash; Prof., professor&acute;s lecture on &acirc;&euro;žBorreliosis Lymensis Cutis&ldquo; (Czech equivalent to Professor) &ndash; 2004</p>
<p><strong>Present position</strong><br />&ndash; Chairwoman Dept. Dermatology, 2nd Medical School, Charles Univ. Prague: since 1995<br />&ndash; Board Member Czech Society of Dermatology and Venereology:since 1998<br />&ndash; Scientific Board Member 2nd Medical School, Charles University since 1999<br />&ndash; Scientific Board Member Czech Medical Chamber since 1999<br />&ndash; Board Member European Academy of Dermatology and Venereology 2000-2004<br />&ndash; Board Member European Society for Cosmetic and Aesthetic Dermatology since 2000<br />&ndash; Professor of Dermatology, Venereology, Cell Biology and Psychocutaneous Medicine, Int. Faculty of the &acirc;&euro;žCentro Interuniversitario di Dermatologia Biologica e Psicosomatica&ldquo;, Florence, Siena, Milan, Italy since 2001<br />&ndash; Visiting Professor University of Siena, Italy since 2003<br />&ndash; Board Member Czech Society of Laser and Aesthetic Medicine since 2003<br />&ndash; President Czech Academy of Dermatology and Venereology since 2003<br />&ndash; Vice-President Central-Eastern European Dermatovenereological Association since 2003<br />&ndash; Congress</p>
  </div>
</div>
</div>  


<div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Torello-Lotti-MD.JPG" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">Torello Lotti MD</h5>
    <p>Professor of Dermatology and 
Director of Dermatology & 
Venereology Dpt of the University 
of Guglielmo Marconi, Rome, Italy</p>
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample8" aria-expanded="false" aria-controls="collapseExample8">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample8">
  <div class="card card-body">
    <p>The main fields of his scientific researches are focused on the study of neuropeptides in numerous skin diseases, plasminogen activators in autoimmune dermatosis and lichen planus , and the clinical aspects and treatment of psoriasis with particular attention to new therapies with biological agents.<br />Areas of interest:<br />Acne, Eczema, Allergy, Trichology, Cosmetology Dermatology.<br />Of particular relevance for research on pathogenesis and novel treatments for vitiligo and psoriasis.<br />Finally it is worth mentioning his research on the prevention and therapy in dermatology-oncology melanoma and non-melanoma skin cancer.</p>
  </div>
</div>
</div>

<div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Bernd-Michael-Loeffler-MD.JPG" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">Bernd-Michael Loeffler MD</h5>
   
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample9" aria-expanded="false" aria-controls="collapseExample9">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample9">
  <div class="card card-body">
    <p>Beruflicher Werdegang<br />1975 &ndash; 1985 Studium der Humanmedizin, Biologie, Chemie und Physik an der Georg-August-Universit&Atilde;&curren;t G&Atilde;&para;ttingen<br />1981 Diplom in Biologie, Schwerpunkt Biochemie mit Summa cum Laude<br />1986 Promotion in Medizin mit Summa cum Laude<br />1978 &ndash; 1980 Wissenschaftlicher Assistent am Institut f&Atilde;&frac14;r Pflanzen Biochemie der Universit&Atilde;&curren;t G&Atilde;&para;ttingen Stellvertretender Leiter PD Dr. Harnischfeger<br />1980 &ndash; 1984 Wissenschaftlicher Assistent am Max-Planck-Institut f&Atilde;&frac14;r experimentelle Medizin, Arbeitsgruppe Prof. Dr. H. Kunze, Abteilung Prof. Dr. W. Vogt<br />1985 &ndash; 1989 Wissenschaftlicher Mitarbeiter am Max-Planck-Institut f&Atilde;&frac14;r experimentelle Medizin, Arbeitsgruppe Prof. Dr. H. Kunze, Abteilung Prof. Dr. W. Vogt<br />1988 Gastprofessur an der University of Edmonton, Canada, Arbeitsgruppe Prof. Dr. D.N. Brindley<br />1989 &ndash; 2005 Pr&Atilde;&curren;klinische Forschung F. Hoffmann-La Roche (Basel) seit 2002 in der Funktion eines Vizedirektors<br />2003 &ndash; 2004 Ausbildung in M&Atilde;&curren;nnermedizin bei HOMMAGE<br />Seit 2004 Fortlaufend Weiterbildung in Orthomolekular und Pr&Atilde;&curren;ventiv Medizin<br />April 2005 Gr&Atilde;&frac14;ndung der L&Atilde;&para;ffler Consulting in Zug (Schweiz), wissenschaftliche Beratung von forschenden pharmazeutischen Unternehmen (z,B. F.Hoffmannn-La Roche, Santhera, Biovitrum, )<br />Mai 2005 Gr&Atilde;&frac14;ndung der IPAM (Institut for Prevention and Antiaging Medicine) Entwicklung medizinischer Produkte, Vortr&Atilde;&curren;ge, pr&Atilde;&curren;ventivmedizinische Aufkl&Atilde;&curren;rung<br />Dezember 2006 Berufung in das Scientific Advisory Board von Santhera Pharmaceuticals AG<br />Februar 2007 Private Praxis f&Atilde;&frac14;r Pr&Atilde;&curren;ventivmedizin (vormals Platz vor dem Neuen Tor 5, 10115 Berlin, Vergr&Atilde;&para;&Atilde;&Yuml;erung der Praxisr&Atilde;&curren;ume ab 01.02.2007 in der Knesebeckstrasse 68/69, 10623 Berlin)<br />Seit Dez. 2006 Mitglied des Scientific Advisory Board der Santhera Pharmaceuticals</p>
  </div>
</div>
</div>

<div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Mary-Petroliagki-MD.JPG" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">Mary Petroliagki MD</h5>
   <p>Registrat Endocrinologist, General Hospital of Athens</p>
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample10" aria-expanded="false" aria-controls="collapseExample10">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample10">
  <div class="card card-body">
    <p>Mary Petroliagki MD, was born in Athens in 1976.<br />Graduated from Medical School of Constanta in 2001.<br />Has been working as Observer attachment at the Department of Intensive Care Unit, Internal Medicine and Assessment Unit at the &ldquo;PSYCHIKOU&rdquo; Medical Clinic, Senior doctor at the department of nephrology at the &ldquo;Daphni&rdquo; Medical Clinic, Senior doctor at the Department of Internal Medicine at the General Hospital of Argos and Senior doctor at BIOIATRIKI Laboratories.</p>
<p>In 2009 Mary Petroliagki MD, became part of the DHI Global Medical Group as Master Doctor, and since 2012 she is Registrat Endocrinologist in the Endocrinology Department at the General Hospital of Athens.</p>
  </div>
</div>
</div>

<div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Professor-J.C.-Kim,-MD,-PhD.JPG" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">Professor J.C. Kim, MD, PhD</h5>
   <p>DHI Group Research Director</p>
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample11" aria-expanded="false" aria-controls="collapseExample11">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample11">
  <div class="card card-body">
    <p>Professor J.C. Kim, MD, PhD is affiliated with The Hair Laboratory and Immunology Department, at Kyungpook National University in Seoul, Korea.<br />He is the leading international scientist in the research of hair follicle cloning and DNA testing.<br />His work focuses on identifying the specific genes responsible for hair loss.</p>
  </div>
</div>
</div>
<div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Egor-Egorov-MD.JPG" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">Egor Egorov MD</h5>
   <p>Specialist in Preventing Aesthetic Medicine</p>
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample12" aria-expanded="false" aria-controls="collapseExample12">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample12">
  <div class="card card-body">
    <p>Beruflicher Werdegang<br />1979-1985 Studium der Humanmedizin an der Medizinischen Hochschule Vladivostok, Russland.<br />1985 Diplom mit Summa cum Laude<br />1985-1990 Klinische Vollassistenz am Lehrstuhl f&Atilde;&frac14;r Innere Medizin Uniklinik<br />1990 Promotion in Medizin mit Summa cum Laude<br />1995 Deutscher Facharzt f&Atilde;&frac14;r An&Atilde;&curren;sthesie und Intensivmedizin.<br />1991-2001 T&Atilde;&curren;tigkeit als Arzt und Facharzt f&Atilde;&frac14;r An&Atilde;&curren;sthesie am Reinhard-Nieter-Krankenhaus, Wilhelmshaven (Uniklinik G&Atilde;&para;ttingen).<br />Seit 2001 niedergelassen als Arzt f&Atilde;&frac14;r An&Atilde;&curren;sthesiologie.<br />1993-1995 Ausbildung in Manueller Medizin bei der Gesellschaft f&Atilde;&frac14;r Manuelle Medizin Boppard-Hamm<br />2000-2001 Ausbildung in TCM (Akupunktur) an der Uni St.Petersburg<br />2003-2004 Ausbildung in M&Atilde;&curren;nnermedizin bei HOMMAGE<br />2004-2005 Praxis f&Atilde;&frac14;r M&Atilde;&curren;nnermedizin, Wilhelmshaven<br />Seit 2004 Fortlaufend Weiterbildung in Orthomolekular und Pr&Atilde;&curren;ventiv Medizin<br />Mai 2005 Gr&Atilde;&frac14;ndung der IPAM<br />Entwicklung medizinischer Produkte, Vortr&Atilde;&curren;ge, pr&Atilde;&curren;ventivmedizinische Aufkl&Atilde;&curren;rung<br />Februar 2006 Private Praxis f&Atilde;&frac14;r Pr&Atilde;&curren;ventivmedizin (vormals Platz vor dem Neuen Tor 5, 10115 Berlin,Vergr&Atilde;&para;&Atilde;&Yuml;erung der Praxisr&Atilde;&curren;ume ab 01.02.2007 in der Knesebeckstrasse 68/69, 10623 Berlin</p>
  </div>
</div>
</div>


<div class="col-sm-12 pt-3">
               <h4>DHI Medical Directors</h4>
                 <hr>
            </div>


  <div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Jarir-Al-Khaledi-MD-Plastic.jpg" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">Jarir Al Khaledi MD Plastic Surgeon</h5>
   <p>DHI Jordan Medical Director</p>
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample13" aria-expanded="false" aria-controls="collapseExample13">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample13">
  <div class="card card-body">
  <p><strong>WORKING EXPERIENCE</strong><br /><strong>May 2008 to Present</strong><br />Consultant of Plastic &amp; Reconstructive Surgery, Section Head of Plastic Surgery &amp; Dermatology Center of Excellence at the International Medical Center &ndash; Jeddah<br /><strong>November 2007 to April 2008</strong>&nbsp;Consultant of Plastic &amp; Reconstructive Surgery in the Department of Surgery at King Hussein Cancer Center, with an academic rank of instructor &ndash; Amman<br /><strong>May 2007 to August 2007&nbsp;</strong>Preceptorship with Dr. Ghaith Shubailat, Amman Surgical Hospital &ndash; Amman<br /><strong>May 2005 to May 2007</strong>&nbsp;Plastic and Reconstructive Surgery Training, King Hussein Medical Center, Royal Medical Services.</p>
<p>EDUCATION<br />September 2007 Jordanian Board in Plastic &amp; Reconstructive Surgery. Amman<br />September 2006 Passed Medical Council of Canada Evaluating Exam (MCCEE). Abu Dhabi<br />September 2003 Jordanian Board in General Surgery. Amman</p>
<p><strong>TRAINING COURSES</strong><br />November 2011 &ndash; ISAPS Course, Body Contouring Conference &amp; Live Surgery, Beirut<br />October 2011 &ndash; 2nd International Conference of the Jordanian Society for Plastic and Reconstructive Surgeons, Amman<br />June 2011 &ndash; 3rd International Aesthetic Surgery Course, Istanbul<br />June 2011 &ndash; Istanbul International Rhinoplasty Course, Istanbul<br />April 2011 &ndash; Dubai Derma, Dubai June 2010 &ndash; Macrolane Workshop, Beirut<br />May 2010 &ndash; Aegean Masters Meeting on Hair Restoration, Greece<br />May 2010 &ndash; 5th International Conference of the Royal Medical Services, Amman<br />March 2010 &ndash; 4th Jeddah Dermatology &amp; Cosmetics Conference, Jeddah<br />April 2009 &ndash; 39th Annual Symposium on Aesthetic Plastic Surgery, Toronto</p>
<p><strong>PROFESSIONAL AFFILIATIONS<br /></strong>&ndash; Jordanian Medical Association.<br />&ndash; Jordanian Surgical Society.<br />&ndash; Jordanian Society of Plastic &amp; Reconstructive Surgeons (JSPRS).<br />&ndash; Member of the International Society of Aesthetic Plastic Surgery (ISAPS).<br />&ndash; Member of AO CMF.</p>
<p>&ndash; International Member of the American Society of Plastic Surgeons (ASPS).</p>
  </div>
</div>
</div> 
<div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Viral-Desai-MD.JPG" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">Viral Desai MD</h5>
   <p>DHI India Clinic Director</p>
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample14" aria-expanded="false" aria-controls="collapseExample14">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample14">
  <div class="card card-body">
    <p>He is the pioneer of DHI in India and also the Medical Director for DHI India. He is board certified Surgeon and has degree of M. Ch. [Masters- Superspeciality degree] and D. N. B. [Diplomat National Board] in Cosmetic and Plastic Surgery; also recognized by all Common wealth countries.<br />He is member of All India Association of Plastic Surgeons, All India Association of Aesthetic Surgeon, Maharashtra Association of Plastic Surgeons, Consultants Association of India, Indian Medical Council [IMA], Santacruz Medical Association, General Practitioners Association.</p>
  </div>
</div>
</div>
<div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/George-Gounaris-MD.JPG" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">George Gounaris MD</h5>
   <p>Plastic Surgeon – DHI France Medical Director</p>
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample15" aria-expanded="false" aria-controls="collapseExample15">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample15">
  <div class="card card-body">
    <p><strong>TRAINING-EXPERIENCE</strong></p>
<p>09.2011 Plastic Surgeon.</p>
<p>10.2004 &ndash;TODAY</p>
<p>D.H.I Certified Master Specialist.</p>
<p>07.2007 &ndash; 07.2011 Registration in Plastic Surgery, Athens Naval Hospital.</p>
<p>11.2009-07.2010 participate in the programme of the Plastic Surgery Department of Agia Sofia Children&rsquo;s Hospital.</p>
<p>07.2010-01.2011 participate in the programme of the Surgery Department of Agia Sofia Children&rsquo;s Hospital.</p>
<p>01.2011-07.2011 participate in the programme of the Orthopaedic Surgery Department of Agia Sofia Children&rsquo;s Hospital.</p>
<p>05.2010-10.2010 participate in the Emergency Department (Plastic Surgery) of Evaggelismos General Hospital Athens.</p>
<p>08.2002 &ndash; 07.2004</p>
<p>06.2002 &ndash; 07.2002 Registration in General Surgery<br />Surgical Department of Sismanoglio General Hospital Athens.</p>
<p>Registration in General Surgery<br />Surgical Department of Larnaca&rsquo;s General Hospital Cyprus.</p>
<p>10.1995 &ndash; 02.2002</p>
<p>Medical School of Athens, University of Athens.</p>
<p><strong>CONGRESSES</strong></p>
<p>03.2011 5th Congress in Wound &amp; Ulcers Healing, Athens-Greece.</p>
<p>10.2010 8th Congress of Mediterranean Association of Pediatric Surgery, Cyprus.</p>
<p>02.2010 13th Congress of Athens Naval Hospital (9 CME), Athens-Greece.</p>
<p>05.2009 International Meeting on Aesthetic &amp; Reconstructive Facial Surgery (21 CME), Myconos-Greece.</p>
<p>04.2009 4th Congress in Wound &amp; Ulcers Healing, Athens-Greece.</p>
<p>02.2009 12th Congress of Athens Naval Hospital (9 CME), Athens-Greece.</p>
<p>11.2008 26th Congress of General Surgery (24 CME), Athens-Greece.</p>
<p>12.2007 10th Congress of Surgical Infections (15 CME), Athens-Greece.</p>
<p>11.2004 Common Annual Congress of Hellenic Society of Hand Surgery &amp;Hellenic Society of Reconstructive Microsurgery, Athens.</p>
<p>04.2004 2nd Congress of Hernia Reconstruction Athens-Greece.</p>
<p>11.2003 1st Congress of Hellenic Society of intestinal Surgery Athens-Greece.</p>
<p>11.1999 3rd Congress in Plastic &amp; Reconstructive Surgery Athens-Greece.</p>
<p><strong>SEMINARS</strong></p>
<p>03.2010 &lsquo;&rsquo; Care of superficial wounds&rsquo;&rsquo;&rsquo;<br />5th Congress in Wound &amp; Ulcers Healing, Athens-Greece.</p>
<p>10.2010 8th Seminar of Surgical Anatomy of the Hand , Metsovo-Greece.</p>
<p>10.2010 &lsquo;&rsquo; Reconstruction of defects &amp; ulcers of lower limbs&rsquo;&rsquo;, Salonica-Greece.</p>
<p>09.2010 Practice in Microsurgery (12 weeks, KAT Hospital Athens-Greece)</p>
<p>04.2010 2nd training seminar &lsquo;&rsquo;Rhinoplasty Evolutions&rsquo;&rsquo;, Athens-Greece.</p>
<p>05.2009 Instructional Course &lsquo;&rsquo;Injectable Rejuvenation with Botox and Restylane&rsquo;&rsquo;, International Meeting<br />On Aesthetic and Reconstructive Facial Surgery, Myconos-Greece.</p>
<p>05.2009 Instructional Course &lsquo;&rsquo;Face Lift&rsquo;&rsquo;, International Meeting On Aesthetic and Reconstructive Facial<br />Surgery, Myconos-Greece.</p>
<p>05.2009 Instructional Course &lsquo;&rsquo;Complications of blepharoplasty&rsquo;&rsquo;, International Meeting On Aesthetic<br />and Reconstructive Facial Surgery, Myconos-Greece.</p>
<p>05.2009 Diagnosis &amp; Treatment of Craniofacial Anomalies, Greek Craniofacial Center, Athens-Greece.</p>
<p>05.2009 &lsquo;&rsquo;The contribution of Pediatrician in Diagnosis &amp; reconstruction of clefts &amp; other craniofacial anomalies&rsquo;&rsquo; ,Athens-Greece.</p>
<p>04.2009 &lsquo;&rsquo;Nutrition support of the burn victim&rsquo;&rsquo;,<br />4th Congress in Wound &amp; Ulcers Healing, Athens-Greece.</p>
<p>04.2009 &lsquo;&rsquo;Skin grafts &amp; Skin subtitutes&rsquo;&rsquo;,<br />4th Congress in Wound &amp; Ulcers Healing, Athens-Greece.</p>
<p>04.2009 &lsquo;&rsquo; Laser applications&rsquo;&rsquo;,<br />4th Congress in Wound &amp; Ulcers Healing, Athens-Greece.</p>
<p>04.2009 Workshop in Botox &amp; Hyalorounic acid, Juvederm Ultra,<br />Athens Naval Hospital, Athens-Greece.</p>
<p>02.2009 3rd International Course of Experimental Vascularised Flaps in Living Tissue with Clinical<br />Applications in Limps Reconstruction, Experimental &amp; Resurge Center of the ELPEN Pharma,<br />(18 CME) Attica-Greece.</p>
<p><strong>SEMINARS</strong></p>
<p>01.2009 9TH Panhellenic Seminar in Rhinology &amp; 3rd workshop in endoscopic rhinoplasty, (12 CME), Athens-Greece.</p>
<p>02.2008 5th Instructural Seminar &lsquo;&rsquo;Electrolyte &amp; acid-base impalance&rsquo;&rsquo;, (3 CME), Athens-Greece.</p>
<p>01.2008 Instructural Seminar in &lsquo;&rsquo;Breast Reduction&rsquo;&rsquo; Hellenic Society of Plastic &amp; Reconstructive Surgery, Athens-Greece.</p>
<p>11.2005 BLS Course, Greek Resuscitation Council, Athens-Greece.</p>
<p>11.2004 BLS Instructor Course, European Resuscitation Council, Athens-Greece.</p>
<p>06.2004 &lsquo;&rsquo;Dealing with emergencies due to Nuclear-Bio-Chemical threat, natural disasters &amp; massive<br />accidents&rsquo;&rsquo; Athens-Greece.</p>
<p>04.2004 BLS/AED Provider Course, European Resuscitation Council, Athens-Greece.</p>
<p>04.2004 BLS Instructor Course, Athens-Greece.</p>
<p><strong>PUBLICATIONS-ABSTRACTS</strong></p>
<p>. P10.&rsquo;&rsquo; Gluteal lift after massive weight loss&rsquo;&rsquo;.<br />7th Congress of the Surgical Treatment of Obesity ( poster) Athens-Greece 05.2011.</p>
<p>. P11. &lsquo;&rsquo;Thigh lift after massive weight loss&rsquo;&rsquo;.<br />7th Congress of the Surgical Treatment of Obesity ( poster) Athens-Greece 05.2011.</p>
<p>. P12. &lsquo;&rsquo;Plastic surgeries after massive weight loss&rsquo;&rsquo;.<br />7th Congress of the Surgical Treatment of Obesity ( poster) Athens-Greece 05.2011.</p>
<p>. Abdominal wall and peritoneum defects closure by components separeation technique and synthetic mesh covered by the deepithelialised skin of the exomphalos sac. (e-poster)<br />16th Congress of the International Confederation for Plastic Reconstructive and Aesthetic Surgery. Vancouver, 05.2011.</p>
<p>. &lsquo;&rsquo;The use of INEGRA in covering skin &amp; soft tissue defects&rsquo;&rsquo;.<br />5th Congress in Wound &amp; Ulcers Healing, Athens-Greece, 03.2011</p>
<p>. &lsquo;&rsquo;The use of INTEGRA in children&rsquo;&rsquo;.<br />5th Congress in Wound &amp; Ulcers Healing, Athens-Greece, 03.2011</p>
<p>. &lsquo;&rsquo;Reconstruction of pressure ulcers in children with the use of local flaps&rsquo;&rsquo;.<br />5th Congress in Wound &amp; Ulcers Healing, Athens-Greece, 03.2011</p>
<p>. &lsquo;&rsquo;Coverage of postmastectomy defects with the use of abdominal flap&rsquo;&rsquo;.<br />5th Congress in Wound &amp; Ulcers Healing, Athens-Greece, 03.2011</p>
<p>. &lsquo;&rsquo;Reconstruction of scalp defects in children&rsquo;&rsquo;.<br />5th Congress in Wound &amp; Ulcers Healing, Athens-Greece, 03.2011</p>
<p>. P.10. Aesthetic Interventions on Children<br />8th Congress of Mediterranean Association of Pediatric Surgery, Cyprus 10 . 2010</p>
<p><strong>PUBLICATIONS-ABSTRACTS</strong></p>
<p>. &lsquo;&rsquo;Local flaps for lower limb defcts&rsquo;&rsquo;.<br />26th Panhellenic Surgical Congress Athens-Greece 11.2008</p>
<p>. &lsquo;&rsquo;Combination of Plastic Surgeries after massive weight loss&rsquo;&rsquo;<br />26th Panhellenic Surgical Congress Athens-Greece 11.2008</p>
<p>. &lsquo;&rsquo;Gynecomastia reconstruction with the use of Tumescence anesthesia &lsquo;&rsquo;<br />26th Panhellenic Surgical Congress Athens-Greece 11.2008</p>
<p>. &lsquo;&rsquo;Reconstruction of lower limb defects with the use of peroneal muscle flap&rsquo;&rsquo;.<br />26th Panhellenic Surgical Congress Athens-Greece 11.2008</p>
<p>. &lsquo;&rsquo;Hydatid Disease in the Cervical Region&rsquo;&rsquo;.<br />5th European Congress of Oto-Rhino-Laryngology Head and Neck Surgery, Rhodos-Kos Hellas 2004</p>
<p>. &lsquo;&rsquo; SMEAD-JONES technique for postoperative abdominal hernia&rsquo;&rsquo;<br />2nd Panhellenic Hernia Congress, Athens-Greece 2004.</p>
<p>. &lsquo;&rsquo;Surgical treatment of Colon Cancer&rsquo;&rsquo;.<br />1st Panhellenic Congress of the Hellenic Society of Colon Surgery in Collaboration with ST&rsquo; Mark&rsquo;s Hospital London UK, Athens-Greece 2003</p>
<p><strong>CONGRESSES-SPEECHES</strong></p>
<p>. &lsquo;&rsquo;The use of INEGRA in covering skin &amp; soft tissue defects&rsquo;&rsquo;.<br />5th Congress in Wound &amp; Ulcers Healing, Athens-Greece, 03.2011</p>
<p>. &lsquo;&rsquo;The use of INTEGRA in children&rsquo;&rsquo;.<br />5th Congress in Wound &amp; Ulcers Healing, Athens-Greece, 03.2011</p>
<p>. &lsquo;&rsquo;Reconstruction of pressure ulcers in children with the use of local flaps&rsquo;&rsquo;.<br />5th Congress in Wound &amp; Ulcers Healing, Athens-Greece, 03.2011</p>
<p>. The use of Integra in Children<br />VIIIth Congress of Mediterranean Association of Pediatric Surgery, Cyprus 10 . 2010</p>
<p>. The contribution of tissue expansion in skin defects of the scalp<br />VIIIth Congress of Mediterranean Association of Pediatric Surgery, Cyprus 10 . 2010</p>
<p>. &lsquo;&rsquo;Gynecomastia reconstruction with the use of Tumescence anesthesia &lsquo;&rsquo;<br />26th Panhellenic Surgical Congress Athens-Greece 11.2008</p>
<p>. &lsquo;&rsquo;Reconstruction of lower limb defects with the use of peroneal muscle flap&rsquo;&rsquo;.<br />26th Panhellenic Surgical Congress Athens-Greece 11.2008</p>
  </div>
</div>
</div>    

<div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Dimitrios-Ziakas-MD.JPG" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">Dimitrios Ziakas MD</h5>
   <p>DHI Dubai Medical Director</p>
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample16" aria-expanded="false" aria-controls="collapseExample16">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample16">
  <div class="card card-body">
    <p>Dimitrios Ziakas MD, was born in Athens in 1974.<br />Graduated from medical university Varna in 1999, continued his medical training in General Surgery and from 2003 is a part of DHI family.<br />As a member of different worldwide hair organisations, Dr. Ziakas has published a great number of publications about hair loss.<br />Dimitrios Ziakas MD was appointed as DHI Dubai Medical Director in 2010 after gaining experience as a Master Hair Surgeon, in order to expand the DHI vision in the Gcc countries.</p>
  </div>
</div>
</div>   

<div class="col-sm-6">
  <div class="media pb-5">
  <img class="align-self-start mr-3" src="image/team/Eleftherios-Papanikolaou-MD.JPG" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <div class="clearfix">
     
        <h5 class="mt-0">Eleftherios Papanikolaou MD</h5>
   <p>DHI Greece Medical Director</p>
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="collapse" data-target="#collapseExample17" aria-expanded="false" aria-controls="collapseExample17">Show Cv <span class="oi oi-arrow-right"></span></button>
     
      
    </div>
    
    
  </div>
</div>
<div class="collapse pb-3" id="collapseExample17">
  <div class="card card-body">
   Eleftherios Papanikolaou MD is Medical Director in DHI Athens, with many years of experience in hair restoration.
He is 37 years old, married and father of 2 kids.
In the past he has worked at the London DHI Clinic and he is a regular member of the General Medical Council.
He has taken part in DHI missions worldwide, having performed hundreds of successful sessions on VIPs and everyday people.
He is responsible for the best possible scientific training of the medical stuff and the nurses, according to the standards and protocols of DHI.
Great supporter of the DHI vision and DHI ACADEMY.
  </div>
</div>
</div>  
          </div>
</div>


</section>


     
<div class="clearfix"></div>

     
<section class="bottm_sec">
  <h1>Increase your hair density naturally</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>


      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


   

  </body>
</html>
