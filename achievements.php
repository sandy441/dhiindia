
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="DHI Awards & Achievements - DHI Ranked no-1 Hair Restoration Company in Customer satisfaction by IMRB Survey. DHI is Gobal Leader in Hair Transplant Since 1970.">
    <meta name="author" content="">
      <title>Achievements & Awards - Your Hair Transplant Specialist - DHI India</title>
<?php include 'header.php';?>
    </head>
  <body>


<section class="bg-col-1 commmon-padd">
<div class="container">
 <div class="row ">

        <div class="col-12 col-md-12">
          <!-- <p class="float-right d-md-none">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="offcanvas">Toggle nav</button>
          </p> -->
          <div class="jumbotron">
            <h1>LEADING HAIR RESTORATION SINCE 1970</h1>
            <p>DHI Global Medical Group has been dedicated solely to the education research, diagnosis and treatment of hair and scalp disorders for 47 years, and has helped more than 250,000+ delighted patients across 65 locations worldwide .</p>
            <p>During these 47 years, DHI Global Medical Group has been a leader in the evolution of the hair restoration industry by setting new standards and introducing innovative techniques and medical treatments.</p>
          </div>
          <div class="row">
             <ul class="timeline">
                <li class="w100 "><div class="tldate">1970</div></li>
        <li class="w100">
         
          <div class="timeline-panel ">
            <div class="timeline-heading">
             
              <h5>Mr. Kostas P. Giotis founded DHI Global Medical Group</h5>
            </div>
            <div class="timeline-body">
               <img class="img-fluid" src="image/first-dhi-clinic.png"  />
              
              <p>The first DHI clinic in Palo Alto, CA.</p>
            </div>
           
          </div>
        </li>
        <li class="w100"><div class="tldate">1986-1999</div></li>
         <li>
          <div class="timeline-badge primary"><span class="oi oi-target"></span></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
            <h5>1993</h5>
          </div>
            <div class="timeline-body">
              <p>DHI introduce micro grafting, one of the first microsurgery based hair replacement techniques.</p>
              
            </div>
           
          </div>
        </li>
        <li  class="timeline-inverted">
          <div class="timeline-badge primary"><span class="oi oi-target"></span></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
            <h5>1986</h5>
          </div>
            <div class="timeline-body">
                <img class="img-fluid" src="image/first-dhi-athens-clinic.png"  />
              <p>DHI opens it's first clinic in Europe.</p>
              
            </div>
           
          </div>
        </li>
       
        

        
        
        <li >
          <div class="timeline-badge primary"><span class="oi oi-target"></span></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h5>1999</h5>
           
            </div>
            <div class="timeline-body">
               <img class="img-fluid" src="image/norwood-DHI.png" width="300" /> 
              <p>Dr. O’Tar Norwood at the 4th DHI Hair WorkShop</p>
              
            </div>
           
          </div>
        </li>
       
        <li class="w100 "><div class="tldate">2001-2015</div></li>
        
        
         <li class="w100">
         
          <div class="timeline-panel ">
            <div class="timeline-heading">
             
              <h5>2001</h5>
            </div>
            <div class="timeline-body">
            
              <p>DHI Medical Group develops FUE (Follicular Unit Extraction) in conjunction with some of the finest doctors in the Hair Restoration industry.</p>
            </div>
           
          </div>
        </li>
        <li >
          <div class="timeline-badge primary"><span class="oi oi-target"></span></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h5>2004</h5>
           
            </div>
            <div class="timeline-body">
              
              <p>Direct IN Technique was developed in response to surgical innovations in hair replacement. In this procedure, up to 2,000 follicles were implanted in an out-patient session of up to 6 hours.</p>
              
            </div>
           
          </div>
        </li>
        <li  class="timeline-inverted">

          <div class="timeline-badge primary"><span class="oi oi-target"></span></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h5>2008</h5>
           
              
            </div>
            <div class="timeline-body">
              
              <p>DHI launches in India with flagship clinic in Delhi.</p>
              
            </div>
           
          </div>
        </li>

        <li >
          <div class="timeline-badge primary"><span class="oi oi-target"></span></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h5>2012</h5>
           
              
            </div>
            <div class="timeline-body">
              
              <p>DHI opens 16 clinics across India.</p>
              
            </div>
           
          </div>
        </li>
    
         
        <li  class="timeline-inverted">
          <div class="timeline-badge primary"><span class="oi oi-target"></span></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h5>2013</h5>
           
              
            </div>
            <div class="timeline-body">
              
              <p>DHI India takes over the international operations</p>
              
            </div>
           
          </div>
        </li> 
        
        

      


         <li class="timeline-inverted">
          <div class="timeline-badge primary"><span class="oi oi-target"></span></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h5>2014</h5>
           
            </div>
            <div class="timeline-body">
              
            <p>A new PRP protocol was developed and Direct-MPG was introduced with great success.</p>
              
            </div>
           
          </div>
        </li>

         <li>
       <div class="timeline-badge primary"><span class="oi oi-target"></span></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h5>2014</h5>
           
              
            </div>
            <div class="timeline-body">
              <img class="img-fluid" src="image/Patent-dhi.jpg" /> 
              <p>DHI receives European and US patents for the new Direct Hair Implantation instruments.</p>
              
            </div>
           
          </div>
        </li>

<li  class="timeline-inverted">
          <div class="timeline-badge primary"><span class="oi oi-target"></span></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h5>2015</h5>
           
              
            </div>
            <div class="timeline-body">
              
              <p>DHI International starts training academy in New Delhi and trains doctors from all over the world.</p>
              
            </div>
           
          </div>
        </li>
<li class="w100 "><div class="tldate">2016-2017</div></li>

<li>
          <div class="timeline-badge primary"><span class="oi oi-target"></span></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h5>2016</h5>
           
              
            </div>
            <div class="timeline-body">
                 
              <p>DHI International operates 35 clinics all over the world.</p>
              
            </div>
           
          </div>
        </li>
        <li class="timeline-inverted">
          <div class="timeline-badge primary"><span class="oi oi-target"></span></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h5>2017</h5>
           
              
            </div>
            <div class="timeline-body">
                 <img class="img-fluid" src="image/imp.jpg" /> 
              <p>New records were achieved by the DHI International by implanting 11,126 hairs in one session!</p>
              
            </div>
           
          </div>
        </li>


        <li class="clearfix" style="float: none;"></li>
    </ul>
          
          </div><!--/row-->
        </div><!--/span-->

     <!--    <div class="col-6 col-md-3 sidebar-offcanvas about-sidebar" id="sidebar">
          <div class="list-group">
             <a href="about.php" class="list-group-item "> About DHI</a>
             <a href="achievements.php" class="list-group-item active"> Milestone</a>
          
            
            
          </div>
        </div> -->
      </div><!--/row-->

      <hr>
  
</div>


</section>


     
<div class="clearfix"></div>

     
<section class="bottm_sec">
  <h1>Ready To Regain Your Hair & Confidence</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>


      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


   

  </body>
</html>
