
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="know about types of Alopecia &amp; treatment by DHI - Androgenetic Alopecia, Alopecia Areata, Traction, Trichotillomania, scarring, Triangular, Telogen Effluvium &amp; Loose Anagen Syndrome">
    <meta name="author" content="">
      <title>Alopecia Types | Articles - DHI India</title>
<?php include 'header.php';?>
    </head>
  <body>


<section class="bg-col-1">
  <nav aria-label="breadcrumb" role="navigation">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="./">DHI International</a></li>
    <li class="breadcrumb-item active" aria-current="page">Alopecia Types</li>
  </ol>
</nav>
<div class="container hair_loss">
 <div class="content">
    <div class="card bt-border">
  
  <div class="card-body ">
<h2 class="text-center">Types of Alopecia</h2>
           
  
   <div class="row">
     <div class="col-sm-8">
  <h3>Androgenetic Alopecia</h3>
  <p>Androgenetic alopecia is an extremely common disorder that affects roughly 50% of men and perhaps as many women above the age of 40 years. Reportedly, as many as 13% of 
  premenopausal women experience androgenetic alopecia. However, the incidence of androgenetic alopecia increases greatly in women following menopause, and, according to a source, it may affect 75% of women older than 65 years.</p>
  <p>A community-based study of androgenetic alopecia in six Chinese cities indicated that the prevalence of androgenetic alopecia in both Chinese males and females was lower than that seen in Caucasians, but of comparable incidence to their Korean counterparts.</p>
</div>
<div class="col-sm-4 p-5">
  <img src="image/androgenetic-alopecia.png" alt="Types-of-Alopecia" class="img-fluid"> 
</div>
   </div>      
        
    <hr>     
   <div class="row">
    <div class="col-sm-4 p-5">
  <img src="image/alopecia-areata.png" alt="alopecia-areata" class="img-fluid"> 
</div>
     <div class="col-sm-8">
  <h3>Alopecia Areata</h3>
  <p>The most common type of Alopecia Areata presents as round or oval patches of hair loss, most obvious on the scalp or in the eybrows. Those who develop round or oval areas of hair loss can progress to toatal scalp hair loss. (Alopecia Totalis). </p>
  <p>The cause of Alopecia Areata is unknown, but commonly thought to be an autoimmune disorder.  The most common treatment is with steroids (cortisone is one form), either topically or by injection.</p>
  
</div>

   </div>         
        <hr>
<div class="row">
     <div class="col-sm-8">
  <h3>Traction Alopecia</h3>
  <p>Traction Alopecia is caused by chronic traction (pulling) on the hair follicle and is seen most commonly amongst African- American females associated with tight braiding or cornrow hair styles. In most cases, this form of alopecia manifests itself along the hairline.</p>
  <p>Men who attach hairpieces to their existing hair can also experience this type of permanent hair loss if the hairpiece is attached in the same location over a long period of time</p>
</div>
<div class="col-sm-4 p-5">
  <img src="image/Traction-alopecia.png" alt="Traction-alopecia" class="img-fluid"> 
</div>
   </div>
   <hr>
<div class="row">
    <div class="col-sm-4 p-5">
  <img src="image/Trichotillomania.png" alt="alopecia-areata" class="img-fluid"> 
</div>
     <div class="col-sm-8">
  <h3>Trichotillomania</h3>
  <p>Trichotillomania is a traction related form of alopecia. Trichotillomania is the name given to the habitual and compulsive plucking of hair from the scalp and/or other hair-bearing areas of the body. Long-term trichotillomania can result in permanent irrevocable damage. It remains unknown whether trichotillomania should be classified as a habit or as obsessive-compulsive behavior. In its mildest form, trichotillomania is a habitual plucking of hair while a person reads or watches television. In its more severe forms, trichotillomaniahas a ritualistic pattern and the hair-plucking may be conducted in front of a mirror. The person with trichotillomania often has guilt feelings about his or her "odd" behavior and will attempt to conceal it. Hair loss due to trichotillomaniais typically patchy, as compulsive hair pullers tend to concentrate the pulling in selected areas. Hair loss in this case cannot be treated effectively until the psychological or emotional reasons for trichotillomaniahave been addressed effectively. </p>
  
  
</div>

   </div>
   <hr>
   <div class="row">
   
     <div class="col-sm-8">
  <h3>Scarring Alopecia</h3>
  <p>Hair loss due to scarring of the scalp is called scarring alopecia. Scarring can be due to a variety of causes. Traction alopecia over a period of time may lead to scarring and permanent hair loss. Similarly, trichotillomania(compulsive hair-plucking) can cause permanent scalp scarring over time. </p>
  <p>Injury to the scalp caused by physical trauma or burns may leave permanent scars and permanent hair loss. Diseases that may cause permanent hair loss due to scalp scarring include (1) the autoimmune conditions lupus erythematosusand scleroderma, and (2) bacterial infections such as folliculitis, fungal infections, and viral infections such as shingles (Herpes Zoster). A form of scarring alopecia also may occur in post-menopausal women, associated with inflammation of hair follicles and subsequent scarring.</p>
  
</div>
 <div class="col-sm-4 p-5">
  <img src="image/Scarring-Alopecia.png" alt="Scarring-Alopecia" class="img-fluid"> 
</div>
   </div>

 <hr>
 <div class="row">
   <div class="col-sm-4 p-5">
  <img src="image/triangular-alopecia.png" alt="triangular-alopecia" class="img-fluid"> 
</div>
     <div class="col-sm-8">
  <h3>Triangular Alopecia</h3>
  <p>Triangular alopecia is loss of hair in the temporal areas that sometimes begins in childhood. Hair loss may be complete, or a few fine, thin diameter hairs may remain. The cause of triangular alopecia is not known, but the condition can be treated medically or surgically. </p>
  
  
</div>
 
   </div>  

<hr>

<div class="row">

     <div class="col-sm-8">
  <h3>Telogen Effluvium</h3>
  <p>Telogen effluvium is a common type of hair loss caused when a large percentage of scalp hairs suddenly going into "shedding" phase. The causes of telogen effluvium may be hormonal, nutritional, drug-associated, or stress associated.</p>
  
  
</div>
    <div class="col-sm-4 p-5">
  <img src="image/Telogen-Effluvium.png" alt="Telogen-Effluvium" class="img-fluid"> 
</div>
   </div>
   <hr>

<div class="row">
   <div class="col-sm-4 p-5">
  <img src="image/Loose-Anagen-syndrome.png" alt="Loose-Anagen-syndrome" class="img-fluid"> 
</div>
     <div class="col-sm-8">
  <h3>Loose Anagen Syndrome</h3>
  <p>Loose Anagen Syndrome typically affects fair-haired persons,and happens when scalp hairs sit loosely in their hair follicles so they can easily be extracted by combing or pulling. It appears in childhood but usually improves as the child ages.</p>
  
  
</div>
 
   </div>
            </div>
  </div><!--/row-->

  
</div>


</section>


     
<div class="clearfix"></div>

<section class="bottm_sec">
 <h1>Ready to regain your hair & confidence</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>

      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


   

  </body>
</html>
