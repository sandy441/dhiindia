<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 $cityList =  $userinfo->getcity();
?>
  <form class="form-padd" id="appointmentForm" method="post" action="appointmentformsubmit.php">
                            
                            <h3 class="text-center pb-4">Book Your Consultation</h3>
                            <!-- <div class="off_circle">
                                 <p class="off">65% off</p>
                                 <p>on online Booking</p>
                             </div> -->
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    
                                    <input type="text" name="name" class="form-control"  placeholder="Enter Name">
                                </div>
                                <div class="form-group col-md-6">
                                    
                                    <input type="text" name="mobile" maxlength="10"class="form-control" placeholder="Enter Mobile Number">
                                </div>
                            </div>
                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    
                                    <input required="" type="text" name="email" class="form-control" placeholder="Email Id">
                                </div>
                                <div class="form-group col-md-6">
                                    
                                    <select name="appointmentcity" class="form-control" required="" id="sel-city">
        <?php foreach ($cityList as $val):?>
                <option value="<?php echo $val['id'];?>"><?php echo $val['city'];?></option> 
        <?php endforeach;?>     
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control datepicker" name="appointmentdate" placeholder="Choose Preferred Date" id="appointment_inputnamedate">
                                </div>
                                <div class="form-group col-md-6">
                                    
                                    <select required="" name="timeform" id="timeForm" class="form-control">
                                        <option value=""></option>
                                        <option style="color:#555;font-weight:bold" value="10:00:00">10:00-10:30</option>
                                        <option style="color:#555;font-weight:bold" value="10:30:00">10:30-11:00</option>
                                        <option style="color:#555;font-weight:bold" value="11:00:00">11:00-11:30</option>
                                        <option style="color:#555;font-weight:bold" value="11:30:00">11:30-12:00</option>
                                        <option style="color:#555;font-weight:bold" value="12:00:00">12:00-12:30</option>
                                        <option style="color:#555;font-weight:bold" value="12:30:00">12:30-13:00</option>
                                        <option style="color:#555;font-weight:bold" value="13:00:00">13:00-13:30</option>
                                        <option style="color:#555;font-weight:bold" value="13:30:00">13:30-14:00</option>
                                        <option style="color:#555;font-weight:bold" value="14:00:00">14:00-14:30</option>
                                        <option style="color:#555;font-weight:bold" value="14:30:00">14:30-15:00</option>
                                        <option style="color:#555;font-weight:bold" value="15:00:00">15:00-15:30</option>
                                        <option style="color:#555;font-weight:bold" value="15:30:00">15:30-16:00</option>
                                        <option style="color:#555;font-weight:bold" value="16:00:00">16:00-16:30</option>
                                        <option style="color:#555;font-weight:bold" value="16:30:00">16:30-17:00</option>
                                        <option style="color:#555;font-weight:bold" value="17:00:00">17:00-17:30</option>
                                        <option style="color:#555;font-weight:bold" value="17:30:00">17:30-18:00</option>
                                        <option style="color:#555;font-weight:bold" value="18:00:00">18:00-18:30</option>
                                        <option style="color:#555;font-weight:bold" value="18:30:00">18:30-19:00</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" data-formtype="" type="radio" name="paymentType" id="" value="Online" checked>
                                            Pay Now <!--& <a href="#" class="primary-link" >Get 65% off</a> on Consultation -->
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" data-formtype="" type="radio" name="paymentType" id="" value="AtClinic" >
                                            Pay At Clinic
                                        </label>
                                    </div>
                                </div>
                                <div class=" paymentgatewaty" data-paymentdiv="Parent">
                                    <!--        <div class="radio" id="div-paymentgatewaypayu" data-paymentdiv="chaild" >
                                                <label>
                                                        <input class="cons_pay" required checked="true" type="radio" data-ref="" name="paymentOnlineType" value="payU" data-element="payU"/>
                                                        Pay by PayU
                                                </label>
                                            </div>-->
                                    <!--        <div class="radio" id="div-paymentgatewaypaytm" data-paymentdiv="chaild" data-paymenttype="payTm">
                                                    <label>
                                                            <input class="cons_pay" required type="radio" data-ref="" name="paymentOnlineType" value="payTm" data-element="payTm" />
                                                            Pay by Paytm
                                                    </label>
                                            </div>-->
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-dark ">Book Now</button>  
                            </div>
                            
                        </form>
                        
