
<!doctype html>
<html lang="en">
 <head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Looking for Beard hair restoration? DHI™ provides beard & facial hair transplant by advanced techniques & procedure by Trained Doctors with no Pain, no Scar & 100% Safe.
">
    <meta name="author" content="">
      <title>Beard Restoration Surgery, Facial Hair Transplant - DHI India

</title>
<?php include 'header.php';?>
    </head>
  <body>

<section class="bg-col-1">
   <div class="clearfix">
           
            	<video  preload="auto" width="100%" loop autoplay>
                        <source type="video/webm" src="video/beard-restoration-dhi-delhi-clinic.mp4">
                        <source type="video/mp4" src="video/beard-restoration-dhi-delhi-clinic.mp4">
                        <source type="video/webm" src="video/beard-restoration-dhi-delhi-clinic.ogg">
                        <source type="video/ogg" src="video/beard-restoration-dhi-delhi-clinic.ogg">
                    </video>
           
        </div>
        
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card mar-minus">
            <div class="card-body"> 
               <div class="row ">
     
    <div class="col-sm-6"><h2> Beard Restoration</h2>
      <p>With Direct Hair Implantation it is now possible to transplant hair to any part of the body and the face.</p>
      <p>Transplanted hair follicles are picked one by one to match facial hair; and thanks to DHI custom designed implanting tool, there is complete control on the depth and the direction of the implanted hair, for a nice and natural looking result.</p>
      <h4>How Does Beard Restoration Work?</h4>
      <p>Beard restoration with Direct Hair Implantation restores the natural hair on the beard and the moustache.</p>
    </div>
    <div class="col-sm-6">
      <div class="table-box">
        <div class="table-img">
          <img  class="img-fluid" src="image/beard-restoration.png" alt="beard restoration before after
" />
        </div>
      </div>
      
    </div>
   
  </div>
            </div>
          </div>
           
            </div>
<div class="content key_features">
    <div class="card bt-border">
  
  <div class="card-body ">
  <div class="row ">
    <div class="col-sm-6 pl-5 pr-4 pt-5 pb-5">
  
    <img src="image/hairline.jpg" alt="dhi beard transplant of client
" class="img-fluid">
           <div class=" bg-col-w pb-4">
             <h4 class="card-title">Hairline Designing</h4>
    <p class="card-text">The doctor thoroughly designs the beard area according to the needs of the patient, and the defined symmetry.</p>
           </div>
    
        
    </div>
    <div class="col-sm-6 pl-4 pr-5 pt-5 pb-5 ">
      
   
             
        <img src="image/implant.jpg" alt="" class="img-fluid">
           <div class=" bg-col-w pb-5">
             <h4 class="card-title">Extraction</h4>
    <p class="card-text">The required hair follicles are extracted from the donor area (usually the back of the head)</p>
           </div>   
    
         
     
        
    </div>
    
  </div>
   <div class="row  ">
     <div class="col-sm-6 pl-5 pr-4  pb-5">
      
         <img src="image/file.jpg" alt="" class="img-fluid">
      <div class=" bg-col-w pb-4">
             <h4 class="card-title">Follicle Counting</h4>
    <p class="card-text">The number of the required hairs and the kind of follicle grafts required for natural result and density are counted.</p>
      </div>     
               
    
         
       
    </div>
     <div class="col-sm-6 pl-4 pr-5  pb-5 ">
      
    
         <img src="image/extraction.jpg" alt="" class="img-fluid">    
          <div class=" bg-col-w pb-5">
               <h4 class="card-title">Implantation</h4>
    <p class="card-text">The extracted hair follicles are placed into the beard area as per the predetermined design.</p>
    
          </div>
            
         
         
       
    </div>
    <div class="clearfix pl-5 pr-5 pb-5 ">
      <img src="image/natural.jpg" class="img-fluid" alt="dhi natural hair results
">
      <div class=" bg-col-w pb-4">
     <h2 class="text-center">Natural Growth</h2>
    <p class="text-center">The implanted hair will grow throughout  a person’s lifetime.</p>
</div>
                
    </div>
   </div>
  </div>
</div>
            
	
 
                 	</div>


</section>


     
<div class="clearfix"></div>
<section class="bottm_sec">
 <h1>Ready to regain your beard & confidence</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>


      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


  

  </body>
</html>
