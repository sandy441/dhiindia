<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test_blog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Mmzv7UDUn`=* A1bs(yZ+<qUF52gWl),^AWOa.!3&*$orB9#{GLgG}3*]j^zvJ$;');
define('SECURE_AUTH_KEY',  '-:{#U9NcW[MyBFmB9D]iUsc.1am93z .oM5+R`>soy0%1H&v&RQ6iIw`UFqQeqyJ');
define('LOGGED_IN_KEY',    '(n(Nx9%r<RuJ|et7lHl=,~^j~n2cU1X5|2tGrgRjTIzCSyyfg{Ahr1FS+rOvF~{o');
define('NONCE_KEY',        'N<)X{6;),2.RcnrRs,x0x(14XDWF50$~J0^et-=tA.7GF~w<h+18@Y<Ld?kzy/N~');
define('AUTH_SALT',        'o>|#9>;5QIZu1N]b*!`.}SZzI.@:-AHsedh=#Qxw|>_00f~9~Q:BwYp2+#NG[NHM');
define('SECURE_AUTH_SALT', '/i|yct(]f- 9by Em{r0TLSQJKyM*3/45BLq4uK3l/;Ho5L$]yP.<Rfx[8;Zh-=8');
define('LOGGED_IN_SALT',   '-ugO4Ldo8-G$` KTS=LXDZlYSlfw,t_il5j<hkJz;QYDjE{2#BXn=plKK(E/6DY>');
define('NONCE_SALT',       'T~b.O5l)GgYJ?$x4.h!VB[Omd2v_XSRZl}:HnWw5 0$#sy,9is_O%[s@B@f6JA5(');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
