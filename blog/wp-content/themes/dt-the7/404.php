<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package presscore
 * @since presscore 0.1
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

presscore_config_base_init();

get_header(); ?>
</div>
</div>
</div>
			<!-- Content -->
			<div id="content" class="content" role="main" style="min-height: 500px; text-align:center;    margin: 0 auto;">

				<article id="post-0" class="post error404 not-found">

					<h1 class="entry-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'the7mk2' ); ?></h1>

					<p><?php _e( 'It looks like nothing was found at this location. Maybe try to use a search?', 'the7mk2' ); ?></p>

					<?php get_search_form(); ?>

				</article><!-- #post-0 .post .error404 .not-found -->

			</div><!-- #content .site-content -->
<?php
$config = presscore_get_config();
$footer_sidebar = apply_filters( 'presscore_default_footer_sidebar', 'sidebar_2' );
 $show_sidebar = $config->get( 'footer_show' ) && is_active_sidebar( $footer_sidebar );
$show_bottom_bar = apply_filters( 'presscore_show_bottom_bar', true );
$sidebar_layout = presscore_get_sidebar_layout_parser( $config->get( 'template.footer.layout' ) );
			$sidebar_layout->add_sidebar_columns();
 ?>
<footer id="footer" <?php echo presscore_footer_html_class(); ?>>
	<div class="wf-wrap">
				<div class="wf-container-footer">
					<div class="wf-container">
			<?php 
			do_action('presscore_after_content'); 
			dynamic_sidebar( 'sidebar_2' );
			?>		
			</div>
			</div>
			</div>
</footer>
<?php get_footer(); ?>