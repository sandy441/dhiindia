
<?php
/* 
Template Name: Appointment Online DHI 
*/
$config = Presscore_Config::get_instance();
$config->set('template', 'page');
presscore_config_base_init();
get_header(); 
global $wpdb;
$city_detail = "select * from wp_ba_city where status=1 order by id=1 asc, city asc"; 
$city_detail = $wpdb->get_results($city_detail);
 ?>
</div>
</div>
</div>
<style>
* {
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
}
*:before,
*:after {
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
}
</style>
	<section id="appointment-mid-section" style="background-image:url(<?php echo get_template_directory_uri().'/images/udsa2.png'; ?>)">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="appointment-right-div">
					<!-- <h1 class="appointment-discount-heading" >Get <span>50%</span> off on online booking</h1> -->
					<img class="form-banner-img" src="<?php echo get_template_directory_uri();?>/images/offer.png">
					<div class="appointment-book-div">
						<h2>Book Your Consultation</h2>
						<div class="appointment-form-div">
						<form class="appointment-form" id="appointment-form" method="POST" action=<?php echo get_template_directory_uri().'/app-submit.php';?>>
					
								<div class="appointment-form-field-div">
									<div class="form-group">
										<label>Name</label>
										<input required name="nameForm" value="<?php if(isset($_REQUEST['Fullname']))echo $_REQUEST['Fullname']; ?>"  type="text" class="form-control inputbox" id="appointment-inputname" pattern="[a-zA-Z. ]{3,150}$" />
										<span class="appointment-validate-check-right"><i class="fa fa-check"></i></span>
									</div>
									<div class="form-group">
										<label>10 Digit Mobile Number</label>
										<input required name="mobileForm" value="<?php if(isset($_REQUEST['phone']))echo $_REQUEST['phone']; ?>" type="text" class="form-control inputbox" maxlength="10" id="appointment-inputmobile" pattern="([0-9]{10,14})|(\+[0-9]{10,14})|(\+[1-9]{1,3}-[0-9]{10,14})|(\+[1-9]{1,3}-[0-9]{3,4}-[0-9]{3,4}-[0-9]{3,4})$" />
										<span class="appointment-validate-check-right"><i class="fa fa-check"></i></span>
									</div>
									<div class="form-group">
										<label>Email</label>
										<input required name="emailForm" value="<?php if(isset($_REQUEST['email'])) echo $_REQUEST['email']; ?>" type="email" class="form-control inputbox" id="appointment-inputemail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"  />
										<span class="appointment-validate-check-right"><i class="fa fa-check"></i></span>
									</div>
									<div class="form-group">
										<label class="scity">Select City</label>
										<select required name="cityForm" id="sel-city" class="sel_city form-control">
											<option value=""></option>
											<?php foreach($city_detail as $row) {?>
											<option value="<?php echo $row->id; ?>" <?php echo ((isset($_REQUEST['city']) && strtolower($_REQUEST['city']) == strtolower($row->city))?'selected':'')?>><?php echo $row->city; ?></option>
											<?php } ?>
										</select>
										
										<span class="appointment-validate-check-right"><i class="fa fa-check"></i></span>
									</div>
									<div class="form-group">
										<label>Preferred Date</label>
										<input required name="dateForm" type="text" class="form-control inputdate" id="appointment_inputnamedate" pattern=".+$" readonly="readonly"/> 
										<span class="appointment-date-calender"><i class="fa fa-calendar" aria-hidden="true"></i></span>
										<span class="appointment-validate-check-right"><i class="fa fa-check"></i></span>
									</div>
									<div class="form-group">
										<label class="sel_time">Select Time</label>
										<select required  name="timeForm" id="timeForm"  class="form-control">
											<option></option>
										</select>
										<span class="appointment-validate-check-right"><i class="fa fa-check"></i></span>
									</div>
									<div class="appointment-radio-div">
										<div class="radio" id="div-appointment-paynow">
											<label>
												<input class="cons_pay" required type="radio" data-ref="" name="payForm" value="1" id="appointment-paynow" />
												Pay Now & <b>Get 65% Off</b> on consultation
											</label>
										</div>
										<div class="radio" id="div-appointment-skype">
											<label>
												<input class="cons_pay" required type="radio" data-ref="" name="payForm" value="2" id="appointment-skype" />
												Skype Consultation
											</label>
										</div>
										<div class="radio" id="div-appointment-paylater">
											<label>
												<input class="cons_pay" required type="radio" data-ref="" name="payForm" value="3" id="appointment-paylater" />
												Pay At Clinic
											</label>
										</div>
									</div> 
								</div>
								<div class="message-skype"> We currently do not have a clinic
								at your location, however, we offer <br /><span class="skype-consultation-title">Skype consultation with our hair expert:</span> <span class="skype-price"><span class="fee-consi-skype">INR 200</span><span class="skype-price-cut" style="margin-left:5px;">400</span> <span class="skype-consultation-offer-price">(50%)</span></span>
								</div>
								<div id="appointment-consultation-fee">
									<h5>Consultation Fee: <span class="consultation-fee-inr"><span class="fee-consi">INR 200 </span><span class="appointment-fee-cut">400</span> <span class="appointment-fee-offer">(-65%)</span></span> </h5>
								</div>
								<div id="appointment-consultation-fee1">
									<h5>Consultation Fee Payable at Clinic: <span class="consultation-fee-inr consultation-fee-inr12"></span> </h5>
									<p><span class="payable-clinic-text">Choose Pay Now option & get 65% discount on Consultation Fee</span></p>
								</div>
								<!-- <div id="appointment-consultation-address">
									<h5>Consultation Address:</h5>
									<p>
										B – 5/15 Safdarjung<br>
										Enclave Opposite DLTA & Deer Park<br>
										Delhi 110 029
									</p>
								</div>-->
								<input type="hidden" name="consultationForm" value="" id="consultationForm" />
								<input type="hidden" name="ref_id" value="<?php echo $_REQUEST['ref_id']; ?>" id="ref_id" />
								<input type="hidden" name="source" value="<?php echo $_REQUEST['source']; ?>" id="source" />
								<input type="hidden" name="campaign" value="<?php echo $_REQUEST['campaign']; ?>" id="campaign" />
								<div class="paymentgatewaty">
									<div class="radio" id="div-paymentgatewaypayu">
										<label>
											<input class="cons_pay" required checked="true" type="radio" data-ref="" name="payType" value="1" id="payment-payu" />
											Pay by PayU
										</label>
									</div>
									<div class="radio" id="div-paymentgatewaypaytm">
										<label>
											<input class="cons_pay" required type="radio" data-ref="" name="payType" value="2" id="payment-payTM" />
											Pay by Paytm
										</label>
									</div>
								</div>
								<div class="appointment-div-btn">
									<button onclick="ga('send', 'event', 'button', 'click', 'submit')" type="submit" id="app_submit" class="btn btn-default appointment-btn">Book Now</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="appointment-left-div col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
			<h4>The correct diagnosis is the <b>Best Treatment</b></h4>
		</div>
		<section class="appointment-usp-section" style="background-image:url(<?php echo get_template_directory_uri().'/images/pattern.png'; ?>)">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 appointment-usp-div">
					<p class="appointment-usp-number ">
						<span class="appointment-usp1-no">47</span>
					</p>
					<p class="appointment-usp-heading">
						Years in Hair Restoration
					</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 appointment-usp-div">
					<p class="appointment-usp-number">
						<span class="appointment-usp2-no">60</span>
					</p>
					<p class="appointment-usp-heading">
						Clinics Worldwide
					</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 appointment-usp-div">
					<p class="appointment-usp-number">
						<span class="appointment-usp3-no">200,000</span><span>+</span>
					</p>
					<p class="appointment-usp-heading">
						Delighted Clients
					</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 appointment-usp-div">
					<p class="appointment-usp-number">
						<span class="appointment-usp4-no">120</span><span>+</span>
					</p>
					<p class="appointment-usp-heading">
						Procedures Daily
					</p>
				</div>
			</div>
		</section>
<style type="text/css"> 
input[type="text"]:focus,input[type="select"]:focus,input[type="email"]:focus{outline:none;color:#555!important;font-size:14px!important;}	
</style>	
	
<?php get_footer(); ?>
	
