<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the <div class="wf-container wf-clearfix"> and all content after
 *
 * @package vogue
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }
 include '../footer.php';
//$config = Presscore_Config::get_instance();
?>
<!-- /Chrome video fullscreen -->
</body>
</html>