<?php
/**
 * Vogue theme.
 *
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since 1.0.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1200; /* pixels */
}

/**
 * Initialize theme.
 *
 * @since 1.0.0
 */
require( trailingslashit( get_template_directory() ) . 'inc/init.php' );

require( trailingslashit( get_template_directory() ) . 'inc/init.php' );

add_filter('wpseo_title', 'vg_wpseo_title');
function vg_wpseo_title($title) {

	global $post;
    	
	if( is_singular( 'post' ) ) {
		$thetitle = get_the_title();
		$terms = get_the_terms( $post->ID, 'category' );
		$sname = "DHI India";
		
		$term = get_term_by( 'id', $terms[0]->parent, 'category' );
		
		if( $term != '' || $term != null || $term != '0' ) {
			$title = $thetitle . " | " . $term->name . " - " . $sname;
		} else {
			$title = $thetitle . " | " . $terms[0]->name . " - " . $sname;
		}
	}
    
	return $title;
}

/* appointment */
function twentysixteen_scripts() {
 wp_enqueue_script( 'booking_custom_js', get_template_directory_uri() . '/js/book_custom.js', array( 'jquery' ), '1.0.0', false );
 wp_enqueue_script( 'mainscript_number_js', get_template_directory_uri() . '/js/custom-app-script.js', array( 'jquery' ), '1.0.0', false ); 
 wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '1.0.0', false );
  wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/css/bootstrap.css' );
   wp_enqueue_style( 'app_module_css', get_template_directory_uri() . '/css/custom-app-style.css' );
    wp_enqueue_style( 'responsive_css', get_template_directory_uri() . '/css/responsive.css' );
    
	if(is_page('new-hair-implant-india'))
		{	
			wp_enqueue_script( 'owl_carousel_js', get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), '1.0.0', false );
			wp_enqueue_script( 'app_js', get_template_directory_uri() . '/js/app.js', array( 'jquery' ), '1.0.0', false );
			wp_enqueue_style( 'landingpage_css', get_template_directory_uri() . '/css/landingpage.css' );
			wp_enqueue_style( 'owl_theme', get_template_directory_uri() . '/css/owl.theme.css' );
			wp_enqueue_style( 'owl_carousel', get_template_directory_uri() . '/css/owl.carousel.css' );
			wp_enqueue_style( 'owl_transition', get_template_directory_uri() . '/css/owl.transitions.css' );
			wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.css' );
			wp_enqueue_style( 'muli-font', 'https://fonts.googleapis.com/css?family=Muli' );
		}
	
}
add_action( 'wp_enqueue_scripts', 'twentysixteen_scripts' );

/* appointment */
/* Hair implant landing page */

/* Hair implant landing page */
/*SMS*/
function callurl($url){
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$output = curl_exec($ch);
					curl_close($ch);
					
					return $output;
				}
/*SMS*/

/* e-mail */
				
				function callApi($api_type='', $api_activity='', $api_input='') {
        $data = array();
		$result = http_post_form("http://api.falconide.com/falconapi/web.send.rest", $api_input);
        return $result;
}
  
function http_post_form($url,$data,$timeout=20) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url); 
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_RANGE,"1-2000000");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
        curl_setopt($ch, CURLOPT_REFERER, @$_SERVER['REQUEST_URI']);
        $result = curl_exec($ch); 
        $result = curl_error($ch) ? curl_error($ch) : $result;
        curl_close($ch);
        return $result;
} 
/* e-mail */

function load_contactform7_on_specific_page(){
   if( is_front_page() ) { 
      wp_dequeue_script('contact-form-7');
      wp_dequeue_style('contact-form-7');
   }
}
 
add_action( 'wp_enqueue_scripts', 'load_contactform7_on_specific_page' );


add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );

function dequeue_jquery_migrate( &$scripts){
	if(!is_admin()){
		$scripts->remove( 'jquery');
		$scripts->add( 'jquery', false, array( 'jquery-core' ), '1.4.1' );
	}
}
/* Get Location By ip */
add_filter( 'gform_field_value_locationbyip', 'populate_iplocation' );
function populate_iplocation( $value ) {
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
require_once('geoplugin.class.php');
$geoplugin = new geoPlugin();
$geoplugin->locate($ip);
return $geoplugin->city;
}
/* Get Location By ip */
/*  Phone number validation */
add_filter( 'gform_field_validation', 'validate_phone', 10, 4 );
function validate_phone( $result, $value, $form, $field ) {
    $pattern = "/^([0|\+[0-9]{1,5})?([1-9][0-9]{7})$/";
    if ( $field->type == 'phone' && ! preg_match( $pattern, $value ) ) {
        $result['is_valid'] = false;
        $result['message']  = 'Please enter a valid phone number';
    }
    return $result;
}
/*  Phone number validation */
/* Gravity form email using falconide api, Gravity form sending data to observlabs */
add_filter( 'gform_pre_send_email', 'before_email' );
function before_email( $email, $message_format) {
	 $api_key = "d1942cc8edb1bbd654af61c9c6ece16f";
	$from = "enquiry@dhiindia.com";
	$fromname = "DHI India"; 
    $data=array();
	$data['subject']= $email['subject'];                                                                       
	$data['fromname']= $fromname;                                                             
	$data['api_key'] = $api_key;
	$data['from'] = $from;
	$data['content']= '<html>' . $email['message'] . '</html>';
	$data['recipients']= $email['to'];
	callApi(@$api_type,@$action,$data);
    return $email;  
}
add_action( 'gform_after_submission', 'post_to_third_party', 10, 2 );
function post_to_third_party( $entry, $form ) {
	if($entry['form_id']==4)
	{
		$_POST['formname']='Request a call back';
		$_POST['name']=$entry['1'];
		$_POST['phone']=$entry['2'];
		$_POST['email']='-';
		?>
			<script>
				ga('send', 'event', 'form', 'submit', 'RACB');
			</script>
			<!--Offer Conversion:  -->
			<img src="http://shoogloomobile.go2affise.com/success.php?afgoal=lead" height="1" width="1" alt="" />
			<!-- End Offer Conversion -->
		<?php
	}
	else if($entry['form_id']==5)
	{
		$_POST['formname']='Leave a message';
		$_POST['name']=$entry['1'];
		$_POST['email']=$entry['2'];
		$_POST['phone']=$entry['3'];
		$_POST['comment']=$entry['4'];
		?>
			<script>
				ga('send', 'event', 'form', 'submit', 'popup');
			</script>
			<!--Offer Conversion:  -->
			<img src="http://shoogloomobile.go2affise.com/success.php?afgoal=lead" height="1" width="1" alt="" />
			<!-- End Offer Conversion -->
		<?php
			
	}
	else if($entry['form_id']==2)
	{
		$_POST['formname']='Enquiry';
		$_POST['name']=$entry['1'];
		$_POST['email']=$entry['2'];
		$_POST['phone']=$entry['3'];
		$_POST['city']=$entry['4'];
		$_POST['city']=$_POST['city'].'/'.$entry['5'];
		?>
			<script>
				ga('send', 'event', 'form', 'submit', 'specialist');
			</script>
			<!--Offer Conversion:  -->
			<img src="http://shoogloomobile.go2affise.com/success.php?afgoal=lead" height="1" width="1" alt="" />
			<!-- End Offer Conversion -->
		<?php
	}
	else if($entry['form_id']==3)
	{
		$_POST['formname']='Contact Us';
		$_POST['name']=$entry['1'];
		$_POST['email']=$entry['2'];
		$_POST['phone']=$entry['3'];
		$_POST['city']=$entry['4'];
		$_POST['city']=$_POST['city'].'/'.$entry['5'];
		$_POST['comment']=$entry['6'];
		?>
			<script>
				ga('send', 'event', 'form', 'submit', 'contact_us');
			</script>
			<!--Offer Conversion:  -->
			<img src="http://shoogloomobile.go2affise.com/success.php?afgoal=lead" height="1" width="1" alt="" />
			<!-- End Offer Conversion -->
		<?php
	}
	else if($entry['form_id']==1)
	{
		$_POST['formname']='All CMS Page';
		$_POST['name']=$entry['1'];
		$_POST['email']=$entry['2'];
		$_POST['phone']=$entry['3'];
		$_POST['city']=$entry['4'];
		$_POST['city']=$_POST['city'].'/'.$entry['5'];
		?>
			<script>
				ga('send', 'event', 'form', 'submit', 'personal_information');
			</script>
			<!--Offer Conversion:  -->
			<img src="http://shoogloomobile.go2affise.com/success.php?afgoal=lead" height="1" width="1" alt="" />
			<!-- End Offer Conversion -->
		<?php
	}
 	$curlHandler = curl_init();
    $curlOpts = array(
	CURLOPT_URL => "http://observelabs.com/dhi_website/submit.php/",
	CURLOPT_FRESH_CONNECT => true,
	CURLOPT_HEADER => false,
	CURLOPT_POSTFIELDS => http_build_query($_POST, "", "&"),
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_USERAGENT => "MyScript/0.1"
	 );
	curl_setopt_array($curlHandler, $curlOpts);
	 if (false===($responseString=curl_exec($curlHandler))) {
	   exit("0 - No connection to server.");
	 }
}
/* / Gravity form email using falconide api, Gravity form sending data to observlabs */
/* Wordpress hide version - security */
function wpbeginner_remove_version() {
return '';
}
add_filter('the_generator', 'wpbeginner_remove_version');
add_theme_support( 'post-thumbnails' );
/* / Wordpress hide version - security */
