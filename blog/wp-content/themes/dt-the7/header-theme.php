<?php
    /*Just for your server-side code*/
    header('Content-Type: text/html; charset=ISO-8859-1');
    $host = $_SERVER['HTTP_HOST'];
?><link rel="icon" href="http://<?php echo $host;?>/image/favicon.png">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<!--  <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
<!-- Custom styles for this template -->
<link href="http://<?php echo $host;?>/css/carousel.css" rel="stylesheet">
        <link href="http://<?php echo $host;?>/css/jquery-ui.css" rel="stylesheet">
<!-- <link href="css/slick.min.css" rel="stylesheet">
<link href="css/slick-theme.css" rel="stylesheet"> -->
<link href="http://<?php echo $host;?>/css/wpcustom.css" rel="stylesheet">
        <link rel="stylesheet" href="http://<?php echo $host;?>/css/open-iconic-bootstrap.css" >
<div class="custom_nav">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a  href="./"><img alt="direct hair implantation 
                           " src="http://<?php echo $host;?>/image/dhi-small.png" width="110"></a>
        <form class="form-inline text-right mt-md-0 d-block d-sm-none">            
            <a href="http://<?php echo $host;?>/book-an-appointment.php" class="btn btn-outline-info my-sm-0 ">Book an Appointment</a>
        </form>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto ">
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Hair Loss
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        
                        <a class="dropdown-item" href="hair-anatomy.php">Hair Anatomy</a>  
                        <a class="dropdown-item" href="alopecia-types.php">Type of Alopecia</a>
                        <a class="dropdown-item" href="hair-loss-causes.php">Causes of Hair Loss</a>
                        <a class="dropdown-item" href="hair-loss-treatment.php">Hair Loss Treatment</a>
                        
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        About Us
                    </a>
                    
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="http://<?php echo $host;?>/about.php">About DHI</a>
                        <a class="dropdown-item" href="http://<?php echo $host;?>/achievements.php">Milestone</a>
                        <a class="dropdown-item" href="http://<?php echo $host;?>/clinics.php">Our Clinics</a>
                        <a class="dropdown-item" href="http://<?php echo $host;?>/hair-transplant-training.php"> Training Academy</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Treatments
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="http://<?php echo $host;?>/hair-loss-diagnosis.php">Alopecia Diagnostic Test</a>
                        <a class="dropdown-item" href="http://<?php echo $host;?>/direct-hair-implantation.php">Direct Hair Implantation</a>
                        <a class="dropdown-item" href="http://<?php echo $host;?>/direct-hair-fusion.php">Hair Prosthetics</a>
                        <a class="dropdown-item" href="http://<?php echo $host;?>/prp.php">Platelet Rich Plasma</a>
                        
                        <!-- <a class="dropdown-item" href="laser-anageny.php">Laser Anagen</a> -->
                        <a  class="dropdown-item"  href="http://<?php echo $host;?>/scalp-micro-pigmentation.php">Scalp Micropigmentation</a>
                        
                        
                        <a class="dropdown-item" href="http://<?php echo $host;?>/eyebrow-restoration.php">Eyebrow Restoration</a>
                        <a class="dropdown-item"  href="http://<?php echo $host;?>/beard-restoration.php">Beard Restoration</a>
                        <a class="dropdown-item"  href="http://<?php echo $host;?>/scar-repair.php">Scar Repair</a>
                        <a class="dropdown-item"  href="http://<?php echo $host;?>/e-shop.php">Hair Care Products</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="http://<?php echo $host;?>/results.php">results</a>
                </li>
                <li class="nav-item">
                    <a href="http://<?php echo $host;?>/client-feedback.php">Client's Feedback</a>
                </li>
                
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Resource
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="http://<?php echo $host;?>//blog">Blog</a>
                        <a class="dropdown-item" href="http://<?php echo $host;?>/questions-you-must-ask.php">FAQ</a>
                        <a class="dropdown-item" href="http://<?php echo $host;?>/career_index.php">Work with Us</a>
                        <a class="dropdown-item" href="http://<?php echo $host;?>/contact-us.php">Contact Us</a>
                </li>
                
            </ul>
            
            <form class="form-inline mt-2 mt-md-0 d-none d-sm-block clinic_name">
                <a href="tel:18001039300" class="colw my-2 my-sm-0 " style="font-size: 12px;font-weight: bold;">Toll Free :1800 103 9300</a>
                <a href="http://<?php echo $host;?>/book-an-appointment.php" class="btn btn-outline-info my-2 my-sm-0 ">Book an Appointment</a>
            </form>
        </div>
    </nav>
</div> 
