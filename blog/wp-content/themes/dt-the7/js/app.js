jQuery(document).ready(function(){


	/************************************
	*********OWL CAROUSEL BUILDER********
	************************************/

	jQuery('#owl-carousel1').owlCarousel({
		loop:true,
		margin:10,
		items: 3,
		transitionStyle: "fade",
		singleItem: true,
		autoPlay: 5000,
		autoPlayHoverPause:true,
		lazyLoad: true,
		navigation:true,
		navigationText: [
			'<i class="fa fa-chevron-left"></i>',
			'<i class="fa fa-chevron-right"></i>'
		]
	})
})

jQuery(document).on('click', 'a', function(event){
	event.preventDefault();

	$('html, body').animate({
		scrollTop: $( $.attr(this, 'href') ).offset().top
	}, 500);
});
jQuery(document).ready(function() {
	jQuery('#contact_form').bootstrapValidator({
		// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
		// feedbackIcons: {
		// 	valid: 'glyphicon glyphicon-ok',
		// 	invalid: 'glyphicon glyphicon-remove',
		// 	validating: 'glyphicon glyphicon-refresh'
		// },
		fields: {
			name: {
				validators: {
					stringLength: {
						min: 2,
					},
					notEmpty: {
						message: 'Please supply your first name'
					}
				}
			},
			email: {
				validators: {
					notEmpty: {
						message: 'Please supply your email address'
					},
					emailAddress: {
						message: 'Please supply a valid email address'
					}
				}
			},
			phone: {
				validators: {
					notEmpty: {
						message: 'Please supply your phone number'
					},
					phone: {
						country: 'US',
						message: 'Please supply a vaild phone number with area code'
					}
				}
			},
			age: {
				validators: {
					stringLength: {
						min: 1,
					},
					notEmpty: {
						message: 'Please supply your age'
					}
				}
			},
			city: {
				validators: {
					stringLength: {
						min: 2,
					},
					notEmpty: {
						message: 'Please supply your city'
					}
				}
			},
			dropdown: {
				validators: {
					notEmpty: {
						message: 'Please select your state'
					}
				}
			}
		}
	})
	.on('success.form.bv', function(e) {
		jQuery('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
			jQuery('#contact_form').data('bootstrapValidator').resetForm();

		// Prevent form submission
		e.preventDefault();

		// Get the form instance
		var $form = jQuery(e.target);

		// Get the BootstrapValidator instance
		var bv = $form.data('bootstrapValidator');

		// Use Ajax to submit form data
		jQuery.post($form.attr('action'), $form.serialize(), function(result) {
			console.log(result);
		}, 'json');
	});
});
