// appointment animate number
( function( $ ) {
  $(document).ready(function() {
	/*  if ( $(window).width() > 845 ) { 
	   $(".rightsidebarfixed").slideUp();
	   setTimeout(function(){ $(".rightsidebarfixed").slideDown() }, 10000);
       $(".rightsidebarfixed .upperimage, .rightsidebarfixed .smallimage").click(function(){
			$(".imagetoogle").slideToggle();
	  	
    });
   } */  
  $('.appointment-date-calender').on('click',function(){
			$(this).parent().children('input').trigger('focus');
			$(this).parent().children('select').trigger('focus');
			$('.appointment-date-calender').hide();
		});
		$('.appointment-form .form-group .inputdate').blur(function(){
			setTimeout(function(){
				
				if(!$(".inputdate").val()){
					$('.appointment-date-calender').show();
				}
			},500)
		});
$("#sel-city").change(function() {
		if($("#sel-city").val() == '1') {
		$("#div-appointment-paynow").slideUp();
		$("#div-appointment-paylater").slideUp();
		$("#appointment-skype").prop("checked", true);
		$("#appointment-consultation-fee").slideUp();
		$("#appointment-consultation-fee1").slideUp();
  		$("#appointment-consultation-address").slideUp();
		
		$("#div-appointment-skype").slideDown();
		$(".message-skype").slideDown();
		} else {
		$("#div-appointment-skype").slideUp();
		$(".message-skype").slideUp();
		$("#appointment-skype").prop("checked", false);
		
		$("#div-appointment-paynow").slideDown();
		$("#div-appointment-paylater").slideDown();
		}
  	}).trigger("change");
	
  	$("#appointment-paynow, #appointment-skype, #appointment-paylater, #sel-city").change(function () {
		if ($("#appointment-paynow").prop("checked") == true) {
			$("#app_submit").html('Pay Now');
		} else if ($("#appointment-paylater").prop("checked") == true) {
			$("#app_submit").html('Book Now');
		} else if ($("#appointment-skype").prop("checked") == true) {
			$("#app_submit").html('Pay Now');
		} else {
			$("#app_submit").html('Book Now');
		}
  		if ($("#appointment-paynow").prop("checked") == true && $("#sel-city").val() != '') {
  			$("#appointment-consultation-fee1").slideUp();
			
			$("#appointment-consultation-fee").slideDown();
  			$("#appointment-consultation-address").slideDown();
  		} else if ($("#appointment-paylater").prop("checked") == true && $("#sel-city").val() != '') {
  			$("#appointment-consultation-fee").slideUp();
			
  			$("#appointment-consultation-fee1").slideDown();
  			$("#appointment-consultation-address").slideDown();
  		} else {
  			$("#appointment-consultation-fee").slideUp();
  			$("#appointment-consultation-fee1").slideUp();
  			$("#appointment-consultation-address").slideUp();
  		}
  		
  		
  	})
  	
  	
});
$(window).load(function() {
		
		// form label animation
		$('.appointment-form .form-group label').on('click',function(){
			$(this).parent().children('input').trigger('focus');
		});
		$('.scity').click(function(){
			var element = document.getElementById("sel-city");
			var event = document.createEvent('MouseEvents');
			event.initMouseEvent('mousedown', true, true, window);
			element.dispatchEvent(event);
		})
		$('.sel_time').click(function(){
			var element = document.getElementById("timeForm");
			var event = document.createEvent('MouseEvents');
			event.initMouseEvent('mousedown', true, true, window);
			element.dispatchEvent(event);
		})
		$('.scity').on('click',function(){
			$('#sel-city').trigger('focus');
		});
		
		
		$('.appointment-form .form-group select').blur(function(){

				if(!$(this).val()){
					$(this).parent().find('label').css('top','5px');
					$(this).parent().find('label').css('left','35px');
					$(this).parent().find('label').css('color','#585858');
				}
		});
		
		
		// form input validation
		/* $('.appointment-form .form-group input').focus(function(){
			$(this).parent().addClass('dirty');
		}); */
		
		$('.appointment-form .form-group input').blur(function(){
			var appointmentval = $(this).val();
			appointmentval = $.trim(appointmentval);
			var appointmentpattern = $(this).attr('pattern');
			var appointmentregex = new RegExp(appointmentpattern);
			
			try {
				var appointmentcheck = appointmentregex.test(appointmentval);
				// console.log(appointmentcheck);
			}
			catch (e){
				console.log(e);
			}


			if (appointmentcheck){
				$(this).parent().find('.appointment-validate-check-right').show();
				$(this).parent().find('.appointment-validate-check-cross').hide();
			} else {
				$(this).parent().find('.appointment-validate-check-right').hide();
				$(this).parent().find('.appointment-validate-check-cross').show();
			}
		});
		$('.appointment-form .form-group select').blur(function(){
			var appointmentval = $(this).val();
			appointmentval = $.trim(appointmentval);
			if (appointmentval !== ""){
				$(this).parent().find('.appointment-validate-check-right').show();
				$(this).parent().find('.appointment-validate-check-cross').hide();
			} else {
				$(this).parent().find('.appointment-validate-check-right').hide();
				$(this).parent().find('.appointment-validate-check-cross').show();
			}
		});
		
		$('#appointment_inputnamedate').change(function(){
			var appointmentval = $(this).val();
			appointmentval = $.trim(appointmentval);
			if (appointmentval !== ""){
				$(this).parent().find('.appointment-validate-check-right').show();
				$(this).parent().find('.appointment-validate-check-cross').hide();
			} else {
				$(this).parent().find('.appointment-validate-check-right').hide();
				$(this).parent().find('.appointment-validate-check-cross').show();
			}
		});
		 $('#appointment_inputnamedate').datepicker({
      dateFormat: 'dd-mm-yy' 
 
 });
});
 } )( jQuery );
 
	/*  Cookies */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as anonymous module.
		define(['jquery'], factory);
	} else {
		// Browser globals.
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		if (value !== undefined && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setTime(+t + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {};

		// To prevent the for loop in the first place assign an empty array
		// in case there are no cookies at all. Also prevents odd result when
		// calling $.cookie().
		var cookies = document.cookie ? document.cookie.split('; ') : [];

		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = parts.join('=');

			if (key && key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) === undefined) {
			return false;
		}

		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));

 
	/*  Cookies */
/*  Popup window only on home page */
 jQuery(document).ready(function(){  
  jQuery('#popup-container a.close').click(function(){
      jQuery('#popup-container').fadeOut();
      jQuery('#active-popup').fadeOut();
  });
  
  if(jQuery('#popup-container').length != 0) {
	  var visits = jQuery.cookie('my_visits') || 0;
	visits++;
	  
	  	  jQuery.cookie('my_visits', visits, { expires: 100, path: '/' });
		
	  //console.debug(jQuery.cookie('my_visits'));
		
	  if ( jQuery.cookie('my_visits') > 1000 ) {
		jQuery('#active-popup').hide();
		jQuery('#popup-container').hide();
	  } else {
		  var pageHeight = jQuery(document).height();
		  centerContent();
		
		  jQuery('#active-popup').css("height", pageHeight);
		  jQuery('#popup-container').show();
	  }

  }
}); 

jQuery(document).mouseup(function(e){
  var container = jQuery('#popup-container');
  
  if( !container.is(e.target)&& container.has(e.target).length === 0)
  {
    container.fadeOut();
    jQuery('#active-popup').fadeOut();
  }

});


function centerContent()
{
	var container = jQuery(window);
	var content = jQuery('#popup-container');
	content.css("left", (container.width()-content.width())/4);
	content.css("top", (container.height()-content.height())/4);
}
 /*  Popup window only on home page */