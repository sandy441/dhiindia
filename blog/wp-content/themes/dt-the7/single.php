<?php
/**
 * The Template for displaying all single posts.
 *
 * @package presscore
 * @since presscore 0.1
 */
     
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }
    
$config = Presscore_Config::get_instance();
presscore_config_base_init();
    
get_header( 'theme' ); ?>
    
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    

                    
<?php if ( presscore_is_content_visible() ): ?>
                    
			<?php do_action( 'presscore_before_loop' ); ?>
<section class="bg-col-1 commmon-padd">
    <div class="container">
        <div class="row blog">
            <div class="col-12 col-md-9">
                <!-- !- Content -->
                <article>
                    <h3 class="mt-0 "><?php echo the_title();?></h3>
                        <?php get_template_part( 'content-single', str_replace( 'dt_', '', get_post_type() ) ); ?>
                        <?php comments_template( '', true ); ?>
                 </article>
                <!--/row-->
            </div><!--/span-->
            
            <div class="col-6 col-md-3 sidebar-offcanvas about-sidebar" id="sidebar">
            
            </div><!--/span-->
        </div><!--/row-->
    </div>
</section>
                  <?php do_action('presscore_after_content'); ?>
                      
		<?php endif; // content is visible ?>
                    
<?php endwhile; endif; // end of the loop. ?>
    
<?php get_footer('theme'); ?>