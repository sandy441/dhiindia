<?php
/**
 * Blog post content with media, even layout
 *
 * @package vogue
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

// remove presscore_the_excerpt() filter
remove_filter( 'presscore_post_details_link', 'presscore_return_empty_string', 15 );

$config = Presscore_Config::get_instance();

$article_content_layout = presscore_get_template_image_layout( $config->get( 'layout' ), $config->get( 'post.query.var.current_post' ) );
$post_id = get_the_ID();
$image_attributes = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),'full');
//$post_thumbnail_id = get_post_thumbnail_id( $post_id ); 
//echo '<pre>';print_r($image_attributes[0]);die;
?>

<?php do_action('presscore_before_post'); ?>

<div class="media box_w">
    <img class="align-self-start mr-3 img-fluid" src="<?php echo $image_attributes[0];?>" alt="Generic placeholder image" width="250">
	<?php
	$post_format = get_post_format();
        
	if ( 'odd' == $article_content_layout || 'wide' == $config->get( 'post.preview.width' ) ) {
		// media
//		if ( presscore_post_format_supports_media_content( $post_format ) ) {
//			dt_get_template_part( 'blog/list/blog-list-post-media', $post_format );
//		}
		// content
		dt_get_template_part( 'blog/list/blog-list-post-content', $post_format );

	} else {

		// content
		dt_get_template_part( 'blog/list/blog-list-post-content', $post_format );

		// media
		if ( presscore_post_format_supports_media_content( $post_format ) ) {
			dt_get_template_part( 'blog/list/blog-list-post-media', $post_format );
		}

	}
	?>

</div><!-- #post-<?php the_ID(); ?> -->

<?php do_action('presscore_after_post'); ?>