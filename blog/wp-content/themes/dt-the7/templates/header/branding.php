<?php
/**
 * Description here.
 *
 */

// File Security Check

if ( ! defined( 'ABSPATH' ) ) { exit; }
?>
				<!-- !- Branding -->
				<!-- <div id="branding" class="wf-td"> -->
				<script>
				jQuery(document).ready(function($){
$( ".preload-me" ).attr({
 itemprop:"logo"
});
});

</script>		<div class="header_top_logo">
				<div id="branding" class="wf-td" itemscope itemtype="http://schema.org/Organization">

					<?php

					// header logo
					$logo = presscore_get_logo_image( presscore_get_header_logos_meta() );

					// modile logo
					$logo .= presscore_get_logo_image( presscore_get_mobile_logos_meta(), 'mobile-logo' );

					if ( $logo ) {

						$config = Presscore_Config::get_instance();

						if ( 'microsite' == $config->get('template') ) {
							$logo_target_link = get_post_meta( $post->ID, '_dt_microsite_logo_link', true );

							if ( $logo_target_link ) {
								echo sprintf('<a itemprop="url" href="%s">%s</a>', esc_url( $logo_target_link ), $logo);
								
							} else {
								echo $logo;
							}

						} else {
							echo sprintf('<a itemprop="url" href="%s">%s</a>', esc_url( home_url( '/' ) ), $logo);

						}

						$site_title_class = 'assistive-text';

					} else {  
						$site_title_class = 'h3-size site-title';

					} 
					?>
					  
					<div id="site-title" class="<?php echo $site_title_class; ?>"><?php bloginfo( 'name' ); ?></div>
					<div id="site-description" class="assistive-text"><?php bloginfo( 'description' ); ?></div>
				</div>
			
				<div class="yearscelebration"><img src=<?php echo home_url().'/wp-content/uploads/2017/09/47-years.png'; ?>></div>
				</div>
			<div class="header_top_logo">
				<div class="sociallinks">
					<div class="tollfreenumber headtop"><a href="tel:18001039300" onclick="ga('send','event','button','click','18001039300')"><i class="fa fa-phone"></i><p>Call Toll Free</p>1800 103 9300</a></div>
					<div class="scheduleapp headtop"><a href=<?php echo home_url()."/book-an-appointment/";?>><i class="fa fa-calendar"></i><br>Schedule<br> Appointment</a></div>
					<div class="leavemsg headtop"><a href="#" data-toggle="modal" data-target="#popup"><i class="fa fa-envelope"></i><br>Leave a <br>Message</a></div>
					
				</div>
			</div>  
			<div class="footer-fixed-3col">
				<div class="footer3col tapcall"><a href="tel:18001039300"><i class="fa fa-phone" style="transform: rotate(40deg);"></i><div class="clt">Tap to Call</div></a></div>
				<div class="footer3col schdapp"><a href="<?php echo home_url()."/book-an-appointment/";?>"><p><span class="blink">Get 65% off</span></p><i class="fa fa-calendar"></i><div class="clt">Schedule Appointment</div></a></div>
				<div class="footer3col leavmsg"><a href="#" data-toggle="modal" data-target="#popup"><i class="fa fa-envelope"></i><div class="clt">Leave a Message</div></a></div>
			</div>