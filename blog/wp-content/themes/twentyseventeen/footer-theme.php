  <?php 
 $host = $_SERVER['HTTP_HOST'];

?>
<footer>
      <div class="container">
      <div class="row">
      <div class="small-logo"><img src="http://<?php echo $host;?>/image/dhi-small.png" alt="Dhi" width="110"></div>
      </div>
        <div class="row">
        	
          <div class="col-md-3 col-sm-6 col-xs-6 col">
            <h4>About us</h4>
            <ul>
            	<li><a href="http://<?php echo $host;?>/about-us.php">Introduction</a></li>
                <li><a href="http://<?php echo $host;?>/about-scientific.php">Our Team</a></li>
                <li><a href="http://<?php echo $host;?>/result.php">Our results</a></li>
                <li><a href="http://<?php echo $host;?>/testimonials.php">Client Feedback</a></li>
                <li><a href="http://<?php echo $host;?>/our-clinic-page.php">Our Clinics</a></li>
                <li><a href="http://<?php echo $host;?>/book-appoint.php">Book a consultation</a></li>
                <li><a href="http://<?php echo $host;?>/contact-us.php">Contact us</a></li>
            </ul>
            
          </div>
          <div class="col-md-3 col-sm-6 col-xs-6 col">
            <h4>Our services</h4>
            <ul>
            	<li><a href="http://<?php echo $host;?>/direct-hair.php">Direct Hair Implantation</a></li>
                <li><a href="http://<?php echo $host;?>/sclapmicro.php">Scalp Micro-Pigmentation (MPG)</a></li>
                <li><a href="http://<?php echo $host;?>/prp.php">PRP Growth Factors</a></li>
              <!--   <li><a href="laser-anageny.php">Anagen Treatment</a></li> -->
                <li><a href="http://<?php echo $host;?>/eyebrow.php">Eyebrow Restoration</a></li>
                <li><a href="http://<?php echo $host;?>/beard-restoration.php">Beard Restoration</a></li>
                <li><a href="http://<?php echo $host;?>/dhf.php">Direct Hair Fusion</a></li>
            </ul>
            
          </div>
          <div class="col-md-3 col-sm-6 col-xs-6 col">
            <h4>Treatments</h4>
            <ul>
            	<li><a href="http://<?php echo $host;?>/hair-loss.php">Hair Loss</a></li>
                <li><a href="http://<?php echo $host;?>/hair-transplant-restore.php">Hair Transplant & Restoration</a></li>
                <li><a href="http://<?php echo $host;?>/hair-regrowth.php">Hair Regrowth</a></li>
                <li><a href="http://<?php echo $host;?>/hair-baldness.php">Baldness Treatment in India</a></li>
                <li><a href="http://<?php echo $host;?>/eyebrow-reconstruction.php">Eyebrow Reconstruction</a></li>
                <li><a href="http://<?php echo $host;?>/treatment-costing.php">Treatment Costing</a></li>
                <li><a href="http://<?php echo $host;?>/female-hairloss.php">Female Hair Loss</a></li>
            </ul>
            
          </div>
          <div class="col-md-3 col-sm-6 col-xs-6 col">
          	
            <ul >
              <li >
              	<span><img src="http://<?php echo $host;?>/image/phone-icon.png" alt="phone" ></span><span class="font-weight-bold ">18001039300</span> 
              </li>
              <li>
               	<img class="logo-47" src="http://<?php echo $host;?>/image/footerlogo3.png" alt="" class="img-fluid" width="150">
              </li>
              <li >
              	<img class="logo-iso" src="http://<?php echo $host;?>/image/footerlogo2.png" alt="" class="img-fluid" width="40">
              </li>
              <li>
              	<img class="logo-q" src="http://<?php echo $host;?>/image/footerlogo1.png" alt="" class="img-fluid" width="100">
              </li>
              <li>
              	<span>US Patent #8,801,743 B2 <br />
                EU Patent #EU 2234550</span>
                
              </li>
            </ul>
          </div>
        </div>
      </div>
      
    </footer>

<section class="footer_btm">
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
              <p>&copy; 2017. All rights reserved by dhiindia.com.</p>
            </div>
            <div class="col-sm-6 text-right">
              <ul>
                <li><a href="http://<?php echo $host;?>/terms.php">Terms & Conditions</a></li>
                <li><a href="http://<?php echo $host;?>/privacy.php">Privacy Policy</a></li>
                <li><a href="http://<?php echo $host;?>/contact-us.php">Contact Us</a></li>
              </ul>
            </div>
          </div>
        </div>
      </section>
       <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://<?php echo $host;?>/js/jquery-3.2.1.slim.min.js" type="text/javascript"></script>
    <script src="http://<?php echo $host;?>/js/popper.min.js" type="text/javascript"></script>
    <script src="http://<?php echo $host;?>/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="http://<?php echo $host;?>/js/custom.js"></script>
    <!-- <script src="js/slick.min.js"></script> -->
  
<!-- <script>
 $(document).ready(function() {
   $('.single-item').slick({
     centerMode: true,
     centerPadding: '300px',
     slidesToShow: 1,
     speed: 1500,
     index: 2,
     focusOnSelect:true,
     responsive: [{
       breakpoint: 768,
       settings: {
         arrows: true,
         centerMode: true,
         centerPadding: '40px',
         slidesToShow: 1
       }
     }, {
       breakpoint: 480,
       settings: {
         arrows: false,
         centerMode: true,
         centerPadding: '40px',
         slidesToShow: 1
       }
     }]
   });
   $('single-item video').trigger('pause');
   $('.slick-center video').trigger('play');
   $('.card').click(function()
   {
     //var $this = $(this);
     $('.single-item video').trigger('pause');
     var vname='#'+$(this)[0].children[0].id;
     $(vname).trigger('play');
     console.log(vname);
   });
 });

</script> -->

<script>
  $(function () {
  'use strict'

  $('[data-toggle="offcanvas"]').on('click', function () {
    $('.row-offcanvas').toggleClass('active')
  })
})
</script>
<script>
  $(document).ready(function(){

    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        
        if(value == "all")
        {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
        }
        else
        {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.'+value).hide('1000');
            $('.filter').filter('.'+value).show('1000');
            
        }
    });
    
    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");

});
</script>
<script>
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
<script>
  $('.carousel').carousel({
  interval: 5000
})
</script>