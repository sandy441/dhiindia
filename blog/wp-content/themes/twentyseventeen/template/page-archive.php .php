<?php
    
/* 
Template Name: Archives
*/
get_header('theme'); ?>
<section class="bg-col-1 commmon-padd">
    <div class="container">
        <div class="row blog">
            
            <div class="col-12 col-md-9">
            <?php if(have_posts()) : while(have_posts()) : the_post();?>
                <div class="media box_w">
                    <img class="align-self-start mr-3 img-fluid" src="image/govinda-hair-transplant.jpg" alt="Generic placeholder image" width="250">
                    <div class="media-body ">
                        <h5 class="mt-0"><a href="stars-who-rocked-the-bollywood-with-a-hair-transplant.php">
                              <?php  the_title();?></a></h5>
                        <?php the_content(); ?>
                        <a href="stars-who-rocked-the-bollywood-with-a-hair-transplant.php" class="btn btn-common my-2 my-sm-0 ">Learn more</a>
                    </div>
                </div>
            
            <?php endwhile; endif;// end of the loop. ?>    
                <!--/row-->
            </div><!--/span-->
                
            <div class="col-6 col-md-3 sidebar-offcanvas about-sidebar" id="sidebar">
                
            </div><!--/span-->
        </div><!--/row-->
            
            
            
    </div>
        
        
</section>
    
 
<?php get_footer('theme'); ?>