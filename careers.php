
<!doctype html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Find latest jobs, current openings and walk-in interviews at DHI India. Click to apply online.">
    <meta name="author" content="">
      <title>Work with DHI - DHI INDIA</title>

<?php include 'header.php';
$countries =  $userinfo->getCountry();
?>
    </head>
    <body>
       

        
        <section class="bg-career">
            
            <div class="container">
                <div class="row ">
                    <div class="heading">
                        <h1 >Work with DHI</h1>
                        <h4>Hair Restoration company with global culture.</h4>
                        <h1 class="text-center"><a href="#explore"  class="btn btn-common btn-pdding m-4">Explore below</a></h1>
                    </div>
                    
                </div>
            </div>    
            
        </section>
        <section class="bg-col-1" id="explore">
            <div class="container">
                
                <div class="content">
                    <div class="card bt-border">
                        <div class="card-body"> 
                            <h2 class="text-center"> DHI is growing.</h2>
                            <h4 class="text-center pt-0">Grow with us.See where you fit in.</h4>
                            <div class="row career_card">
                                
                                <div class="col-sm-4">
                                    <div class="card" style="width: 20rem;">
                                        <img class="card-img-top" src="image/Doctor.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <h4 class="card-title">Are you a <span>Doctor</span></h4>
                                            <h1 class="text-center"><a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal01"  class="btn btn-common m-1">Explore options with DHI</a></h1>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="card" style="width: 20rem;">
                                        <img class="card-img-top" src="image/Franchisee.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <h4 class="card-title">Opportunity as <span>Franchisee</span></h4>
                                            <h1 class="text-center"><a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal00"  class="btn btn-common m-1">See how to become franchisee </a></h1>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="card" style="width: 20rem;">
                                        <img class="card-img-top" src="image/corporate.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <h4 class="card-title"><span>Others</span></h4>
                                            <h1 class="text-center pb-20"><a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal11"  class="btn btn-common m-1">View all Positions</a></h1>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
                
                
                
                
                
            </div>
            
            
            
            
        </div>
        
        
    </section>
    
    
    <div class="clearfix"></div>
    
    
    
    <!-- FOOTER -->
    
<?php include 'footer.php';?>
<div class="modal fade" id="exampleModal11" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLabel">Apply for Job</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="p-3" >
                       <form class="p-5" name="jobapply" id="jobapply" enctype="multipart/form-data" method="post" action="<?php echo $home_url;?>/formrequest.php">
                             <input type="hidden" name="formtype" value="jobapply">
                                   <input type="hidden" name="jobtitle" value="<?php echo $title;?>">
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-lg" name="fname" id="" placeholder="First Name*">
                                </div>
                            </div>
                             <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">Last Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-lg" id="" name="lname" placeholder="Last Name*">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">Email Id</label>
                                <div class="col-sm-8">
                                 <input type="email" class="form-control form-control-lg" id="" name="email" placeholder="Email Id*">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">Contact Number</label>
                                <div class="col-sm-8">
                                  <input type="text" class="form-control form-control-lg" id="" name="phone" placeholder="Contact Number*">
                                </div>
                            </div>
                           <div class="form-row">
    <div class="form-group col-md-6">

   <!--  <label for="exampleFormControlFile1">Example file input</label> -->
     <input type="file" class="form-control-file" name="resume" id="">

      
    </div>
    
</div>
                            
                            <div class="form-group row ">
                                <div class="col-sm-12 m-2">
                                    <h1 class="text-center"><input type="submit" class="btn btn-common btn-pdding m-4" value="Submit" name="sbmitjob"></h1>
                                </div>
                                
                            </div>
                        </form>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal00" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLabel">Become a Franchisee</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="p-3" >
                        <form name="franchiseeform" id="franchiseeform" method="post" action="<?php echo $home_url;?>/formrequest.php">
                            <input type="hidden" name="formtype" value="franchisee">
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" class="form-control" id="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">Email Id</label>
                                <div class="col-sm-8">
                                    <input type="text" name="email" class="form-control" id="" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">Contact Number</label>
                                <div class="col-sm-8">
                                    <input type="text" name="phone" class="form-control" id="" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">Select Country</label>
                                <div class="col-sm-8">
                                    <select name="country" class="form-control" id="">
                                        <option value="">Please Choose Country</option>
                                        <?php foreach ($countries as $val):?>
                                            <option value="<?php echo $val['country_name'];?>"><?php echo $val['country_name'];?></option> 
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">Message</label>
                                <div class="col-sm-8">
                                    <textarea name="message" id=""  rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <div class="col-sm-12 m-2">
                                    <h1 class="text-center"><button type="submit" class="btn btn-common padd-box2 btn-pdding ">Submit</button></h1>
                                </div>
                                
                            </div>
                        </form>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="exampleModal01" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLabel">Associate with DHI Medical Group</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="p-3 custom_form" >
                        <form name="drassociationform" id="drassociationform" method="post" action="<?php echo $home_url;?>/formrequest.php">
                            <input type="hidden" name="formtype" value="drassociationform">
                            <!-- <div class="form-group row">
                              <label for="staticEmail" class="col-sm-4 col-form-label">Type of Training</label>
                              <div class="col-sm-8">
                                <select class="form-control" id="exampleFormControlSelect1">
                                <option>Basic</option>
                                <option>Advance</option>
                                
                              </select>
                              </div>
                            </div> -->
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">I am Interested in</label>
                                <div class="col-sm-8">
                                    <label class="check_custom">Hair Transplant Training at DHI Academy
                                        <input type="checkbox" name="Interested[]" value="Hair Transplant Training at DHI Academy">
                                        <span class="checkmark"></span>
                                    </label>
                                    
                                    <label class="check_custom">Work with DHI as Consulting/Performing Doctor
                                        <input type="checkbox" name="Interested[]" value="Work with DHI as Consulting/Performing Doctor">
                                        <span class="checkmark"></span>
                                    </label>
                                    <!-- <div class="form-check">
                                      <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="">
                                        
                                      </label>
                                    </div> -->
                                    <label class="check_custom"> Exploring Franchisee Options with DHI
                                        <input type="checkbox" name="Interested[]" value="Exploring Franchisee Options with DHI">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="check_custom"> Mutually Beneficial Association with DHI
                                        <input type="checkbox" name="Interested[]" value=" Mutually Beneficial Association with DHI">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label id="Interested[]-error" class="error " for="Interested[]" style="display: none;">Please Select at least one</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="name"class="form-control" id="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">Email Id</label>
                                <div class="col-sm-8">
                                    <input type="text" name="email"class="form-control" id="" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">Contact Number</label>
                                <div class="col-sm-8">
                                    <input type="text" name="phone"class="form-control" id="" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">Select Country</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="country"id="">
                                        <option value="">Please Choose Country</option>
                                        <?php foreach ($countries as $val):?>
                                            <option value="<?php echo $val['country_name'];?>"><?php echo $val['country_name'];?></option> 
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-4 col-form-label">Message</label>
                                <div class="col-sm-8">
                                    <textarea name="message" id=""  rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <div class="col-sm-12 m-2">
                                    <h1 class="text-center"><button type="submit" class="btn btn-common padd-box2 btn-pdding ">Submit</button></h1>
                                </div>
                                
                            </div>
                        </form>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    
</body>
</html>
