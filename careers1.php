
<!doctype html>
<html lang="en">

  <body>
<?php include 'header.php';?>

<section class="career">

  <div class="container">
    <div class="row ">
      <div class="heading">
        <h1 >Careers at DHI</h1>
        <h4><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </strong> </h4>
      </div>
      
    </div>
  </div>
 
</section>
<section class="bg-col-1">
<div class="container">
  
            <div class="content">
  <div class="card bt-border">
            <div class="card-body"> 
<h2>Featured jobs</h2>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Job title</th>
      <th scope="col">Department</th>
      <th scope="col">Team</th>
      <th scope="col">Location</th>
    </tr>
  </thead>
  <tbody>
    <tr>
     <td><a href="job-detail.php">Senior UX Designer</a></td>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
     <td><a href="#">Senior UX Designer</a></td>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <td><a href="#">Senior UX Designer</a></td>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>
            </div>
          </div>
        </div>


        


        
            </div>

            
  

                  </div>


</section>

     
<div class="clearfix"></div>



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  



  </body>
</html>
