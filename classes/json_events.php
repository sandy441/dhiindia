<?php
  include 'userinfo.php';
        
    $userinfo = new userinfo();
/* Calendar View Display */
if(isset($_POST['calDate1']))
{
	$current_date=$_POST['calDate1'];
	$setting_id=$_POST['setting_id1'];
	$city_id=$_POST['city_id1'];
	$timestamp = strtotime($current_date);
	$day = date("l", $timestamp);
	$day = strtolower($day);

      // Prepare and execute query
        $week_blocked_record = "SELECT * FROM wp_ba_weekrecord where (day ='".$day."')&&(setting_id='".$setting_id."')";
	$week_record = $wpdb->get_results($week_blocked_record);
	$app_record = "SELECT * FROM wp_ba_appointmentrecord where (city_id = '".$city_id."')&&(app_date='".$current_date."')&&(status = 1)&&(status_main = 1)";
	$appointment_record = $wpdb->get_results($app_record);
	$app_record_blocked = "SELECT * FROM wp_ba_appointmentrecord where (city_id = '".$city_id."')&&(app_date='".$current_date."')&&(status = 3)";
	$appointment_record_blocked = $wpdb->get_results($app_record_blocked);
	$app_record_processing = "SELECT * FROM wp_ba_appointmentrecord where (city_id = '".$city_id."')&&(app_date='".$current_date."')&&(status = 4)";
	$app_record_processing = $wpdb->get_results($app_record_processing);
	$events = array();

    // results blocked record
    foreach($week_record as $row)
	{
		$e = array();
		$e['id'] = $row->id;
		$e['title'] = "Blocked Default";
		$e['start'] = $row->start_time;
		$e['end'] = $row->end_time;
		$e['backgroundColor'] = 'red';
		// Merge the event array into the return array
		array_push($events, $e);
	}
		 // results booked record
	foreach($appointment_record as $row)
	{
		$e = array();
		$e['id'] = $row->id;
		$e['title'] = "Booked";
		$e['start'] = $row->start_time;
		$e['end'] = $row->end_time;
		$e['backgroundColor'] = 'blue';
		array_push($events, $e);
	}
	foreach($appointment_record_blocked as $row)
	{
		$e = array();
		$e['id'] = $row->id;
		$e['title'] = "Forcefully Blocked";
		$e['start'] = $row->start_time;
		$e['end'] = $row->end_time;
		$e['backgroundColor'] = 'orange';
		$e['status'] = 3;
		array_push($events, $e);
	}
	foreach($app_record_processing as $row)
	{
		$e = array();
		$e['id'] = $row->id;
		$e['title'] = "Processing";
		$e['start'] = $row->start_time;
		$e['end'] = $row->end_time;
		$e['backgroundColor'] = 'pink';
		$e['status'] = 4;
		array_push($events, $e);
	}

    // Output json for our calendar
    echo json_encode($events);
}	
/* Form front removing block slots */	
if(isset($_REQUEST['cityId']) && isset($_REQUEST['selectedDate']))
{
   echo $data = $userinfo->getJsonTimeSlotFromSelectedDateAndCity($_REQUEST['cityId'], $_REQUEST['selectedDate']);
//    getJsonTimeSlotFromSelectedDateAndCity
}

/*Admin Calendar view blocking of paticular time slot */
$forceBlockedjson = array();
if(isset($_POST['blocking']))
{
	echo json_encode($_REQUEST);
}


/* Calendar view blocking forcefully */

if(isset($_POST['blocking_forcefully']))
{
    echo json_encode($_REQUEST);
//	$timingstart=$_POST['timestart'];
//	$timingend=$_POST['timeend'];
//	$dateblocking=$_POST['todaysdate1'];
//	$forceBlocked['city_id']=$_POST['city_id_blocked'];
//	$dateblocked = strtotime($dateblocking);
//	$dateCheck1 = date('Y-m-d',$dateblocked);
//	$status = 3;
//	global $today_date1;
//	$date_today = $today_date1;
//	$SQL = "INSERT INTO wp_ba_appointmentrecord (city_id,app_date,start_time,end_time,consultation_fees,name,email,phone,message,status,date) VALUES (".$forceBlocked['city_id'].",'".$dateCheck1."'	,'".$timingstart."','".$timingend."','-','-','-','-','-',".$status.",'".$date_today."')";
//	$wpdb->query($SQL);
//	echo json_encode($SQL);
}

/* calendar view removing forcefully blocked slots */
if(isset($_POST['deleteblocking']))
{
    echo json_encode($_REQUEST);
//	$blocking_id=$_POST['blocked_id'];
//	$SQL = "Delete from wp_ba_appointmentrecord where id=".$blocking_id;
//	$wpdb->query($SQL);
//	echo json_encode('success');
}
/* calendar view number of days calendar open */
if(isset($_POST['calendarlimit']))
{
    $userinfo->getCalendarlimit($_POST['cityget'],$_POST['calendarlimit']);
   
//	
}
?>