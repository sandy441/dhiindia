<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//include '/classes/connections.php';

class userinfo{
    
    public function __construct() {
        include 'connnections.php';
        $this->conn = $conn = new PDO("mysql:host=$dbhost;dbname=$DBName", $DBUser, $DBpwd);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       // $this->setTimeJone();
    }
    public function setTimeJone(){
        $days = array("monday", "tuesday", "wednesday","thursday","friday","saturday","sunday");
        date_default_timezone_set('Asia/Kolkata');
        $timestamp = time();
        $today_date = date("d-m-Y", $timestamp);
        $today121= strtotime($today_date);
        $today_time_current = date("H:i:s", $timestamp);
        $today_time_current = strtotime($today_time_current);
        $today_date1 = date("Y-m-d H:i:s", $timestamp);
        $todays_day_name = strtotime($today_date1);
        $todays_day_name = date("l", $todays_day_name);
        $todays_day_name = strtolower($todays_day_name);
       
    }

    public function getCityNameById($city_id){
        $stmt = $this->conn->prepare("select city,consultation,discount from wp_ba_cities where id=".$city_id); 
        $stmt->execute();
        $result =  $stmt->fetch();
        return $result['city'];
    }
    
    public function getClinicDetails($city_id){
        $stmt = $this->conn->prepare("select city,consultation,discount from wp_ba_cities where id=".$city_id); 
        $stmt->execute();
        return  $stmt->fetch();
    }
    
    public function getDiscountAmt($city_record){
        return $amount = $city_record->consultation - ($city_record->consultation * ($city_record->discount / 100));
    }
    public function getDiscountAmtFromArr($city_record){
        return $amount = $city_record['consultation'] - ($city_record['consultation'] * ($city_record['discount'] / 100));
    }

    public function saveAppointmentrecord($city_id,$appdate,$start_time,$end_time,$consultation_fee,$name,$email,$phone,$message,$txn_id,$status,$paymentType,$date_today,$source,$campaign,$ref_id){
        $ip = $_SERVER['REMOTE_ADDR'] ;
        $stmt = $this->conn->prepare("INSERT INTO wp_ba_appointmentrecord (city_id,app_date,start_time,end_time,consultation_fees,name,email,phone,message,txn_id,status,status_main,date,ip,campaign,source,ref_id,payment_company) VALUES ($city_id,'$appdate','$start_time','$end_time','$consultation_fee','$name','$email','$phone','$message','$txn_id','$status','$paymentType','$date_today','$ip','$campaign','$source','$ref_id','$paymenttype')"); 
        $stmt->execute();   
        return $this->conn->lastInsertId();
    }
    
    public function paymentSuccess($txnid){
         $stmt = $this->conn->prepare("update wp_ba_appointmentrecord set status=1 where txn_id='$txnid'"); 
        return  $stmt->execute();   
    }
    
    public function getcity(){
        //WHERE city not in ('test')
        $stmt =  $this->conn->prepare("Select id,city from wp_ba_cities where status = 1 order by city ASC"); 
        $stmt->execute();
        return $result =  $stmt->fetchAll();
    }
    public function getCountry(){
        //WHERE city not in ('test')
        $stmt =  $this->conn->prepare("Select * FROM countries order by country_name ASC"); 
        $stmt->execute();
        return $result =  $stmt->fetchAll();
    }
    
    public function getSelectStatment($sql,$all){
        $stmt =  $this->conn->prepare($sql); 
        $stmt->execute();
        return $result =  $all?$stmt->fetchAll(PDO::FETCH_OBJ):$stmt->fetch(PDO::FETCH_OBJ);
    }

    public function fetchUserdetails($txnid){
        $stmt = $this->conn->prepare("SELECT "
				." `appointment`.`id` 					as `id`,"
				." `appointment`.`city_id` 				as `city_id`,"
				." `appointment`.`app_date` 			as `app_date`,"
				." `appointment`.`start_time` 			as `start_time`,"
				." `appointment`.`end_time` 			as `end_time`,"
				." `appointment`.`consultation_fees` 	as `consultation_fees`,"
				." `appointment`.`name` 				as `name`,"
				." `appointment`.`email` 				as `email`,"
				." `appointment`.`phone` 				as `phone`,"
				." `appointment`.`message` 				as `message`,"
				." `appointment`.`status` 				as `status`,"
				." `city`.`city` 						as `city_name`,"
				." `city`.`mobile` 					    as `city_mobile`,"
				." `city`.`address` 					as `city_address`,"
				." `city`.`email` 						as `city_email`"
				." FROM "
				." `wp_ba_appointmentrecord` AS  `appointment` ,"
				." `wp_ba_cities` AS  `city`"
				." WHERE "
				." `appointment`.`city_id` =  `city`.`id` "
				." AND"
				." `appointment`.`txn_id` =  '".$txnid."'"
				.""); 
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_OBJ);
       
    }
    
    public function callApi($api_type='', $api_activity='', $api_input='') {
        $data = array();
		$result = $this->http_post_form("http://api.falconide.com/falconapi/web.send.rest", $api_input);
        return $result;
    }
    public function callMailApi($api_type='', $api_activity='', $data='') {
        //$data = array();
       // echo '<pre>';print_r($api_input);die;
        $to = $data['recipients'];
        $subject = $data['subject'];
        $from = $data['from'];

        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Create email headers
        $headers .= 'From: '.$from."\r\n".
            'Reply-To: '.$from."\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $message = $data['content'];
        
        return mail($to, $subject, $message, $headers);
//	$result = $this->http_post_form("http://api.falconide.com/falconapi/web.send.rest", $api_input);
//        return $result;
    }
    public function http_post_form($url,$data,$timeout=20) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url); 
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_RANGE,"1-2000000");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
        curl_setopt($ch, CURLOPT_REFERER, @$_SERVER['REQUEST_URI']);
        $result = curl_exec($ch); 
        $result = curl_error($ch) ? curl_error($ch) : $result;
        curl_close($ch);
        return $result;
} 
    public function mail_with_attached($data) {
        $to = $data['recipients'];
        $subject = $data['subject'];
 	$content = $data['content'];
        $from = $data['from'];
	$filename = $data['filename'];
        $headers = "From: $from\r\n";
        $headers .= "MIME-Version: 1.0\r\n"
          ."Content-Type: multipart/mixed; boundary=\"1a2a3a\"";

  	$message .= "If you can see this MIME than your client doesn't accept MIME types!\r\n"
          ."--1a2a3a\r\n";

        $message .= "Content-Type: text/html; charset=\"iso-8859-1\"\r\n"
          ."Content-Transfer-Encoding: 7bit\r\n\r\n"
          .$content
          ." \r\n\r\n--1a2a3a\r\n";

        $file = file_get_contents($data['attachments']);

        $message .= "Content-Type: image/jpg; name=\"$filename\"\r\n"
          ."Content-Transfer-Encoding: base64\r\n"
          ."Content-disposition: attachment; file=\"$filename\"\r\n"
          ."\r\n"
          .chunk_split(base64_encode($file))
          ."--1a2a3a--";

        // Send email

        return mail($to,$subject,$message,$headers);
          
    } 

    public function callurl($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
}
    public function callSmsApi($phone_no,$msg){
        $url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=357548&username=8826006713&password=agpjp&To=".$phone_no."&Text=".$msg;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
    
    public function getBaseUrl($env = 'test'){
        if($env == 'test'){
            return 'http://'.$_SERVER['HTTP_HOST'];
        }else{
            return 'https://'.$_SERVER['HTTP_HOST'];
        }
    }
    
    public function sendTxnIdToobserveLabs($txnid){
        $ch = curl_init();
		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, "http://www.observelabs.com/dhi_leads_hairimplantindia/submit.php/?&query=update&txnid=$txnid&status=1");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		// grab URL and pass it to the browser
		$res_response = curl_exec($ch);
		if($res_response === false) { echo 'Curl error: ' . curl_error($ch); }
		curl_close($ch);
    }
    
    
    //FROM JSON FILE 
    public function getCalendarlimit($cityget,$calendarlimit){
        $city_id_1 = $cityget;
	$nos_days1 = "SELECT nos_days FROM  wp_ba_setting where (city_id ='".$city_id_1."')";
	$nos_days1 = $this->getSelectStatment($nos_days1, false);
       
	$a = $nos_days1->nos_days;
	
	$city_record = "SELECT *  FROM  wp_ba_cities where (id ='".$city_id_1."')";
	$city_record = $this->getSelectStatment($city_record, false);

	echo json_encode(array('nos_days' => $nos_days1->nos_days, 'address' => $city_record->address, 'consultation_fees' => ($city_record->consultation - ($city_record->consultation * ($city_record->discount / 100))), 'actual_fees' => $city_record->consultation, 'discountr' => $city_record->discount));
    }
    public function getJsonTimeSlotFromSelectedDateAndCity($city_id,$date_selected){
        $Minutes = 30; 
        $Minutes2 = 0;
	$days = array("monday", "tuesday", "wednesday","thursday","friday","saturday","sunday");
        date_default_timezone_set('Asia/Kolkata');
        $timestamp = time();
        $today_date = date("d-m-Y", $timestamp);
        $today121= strtotime($today_date);
        $today_time_current = date("H:i:s", $timestamp);
        $today_time_current = strtotime($today_time_current);
        $today_date1 = date("Y-m-d H:i:s", $timestamp);
        $todays_day_name = strtotime($today_date1);
        $todays_day_name = date("l", $todays_day_name);
        $todays_day_name = strtolower($todays_day_name);
//	$city_id=$_POST['cityId'];
//	$date_selected=$_POST['selectedDate'];
	$timestamp1 = strtotime($date_selected);
	$dateCheck = date('Y-m-d',$timestamp1);
	$tdo_check = strtotime($dateCheck);
	$day1 = date("l", $timestamp1);
	$day1 = strtolower($day1);
        $day_booked_record = "SELECT * FROM  wp_ba_appointmentrecord where (city_id ='".$city_id."')&&(status = 3)&&(app_date = '".$dateCheck. "')";
	$week_record = $this->getSelectStatment($day_booked_record, true);
	$setting_id = "SELECT id FROM  wp_ba_setting where (city_id ='".$city_id."')";
	$setting_id = $this->getSelectStatment($setting_id, false);
        $week_blocked_record = "SELECT * FROM  wp_ba_weekrecord where (setting_id ='".$setting_id->id."')&&(day ='".$day1."')";
	$week_blocked_record = $this->getSelectStatment($week_blocked_record, true);
	$city_record = "SELECT * FROM  wp_ba_cities where (id ='".$city_id."')";
	$city_record = $this->getSelectStatment($city_record, true);
	$time_slotss = array();
	$time_slotss1 = array();
    // results blocked record
	foreach($city_record as $rows)
	{
		$consultation_fees = $rows->consultation;
		$discount = $rows->discount;
		$address = $rows->address;
		$start_time = strtotime($rows->start_time);
		$end_time = strtotime($rows->end_time);
		$time_duration1 = $Minutes * 60; 
		$time_duration2 = $Minutes2 * 60; 
	   
	 }
	foreach($week_blocked_record as $week_blocked_record1)
	{
		$f = array();
		$f['value'] = $week_blocked_record1->start_time;
		$f['end_time'] = $week_blocked_record1->end_time;
		
		array_push($time_slotss1, $f);
	   
	 }
	foreach($week_record as $week_record_list)
	{
		$g = array();
		$g['value'] = $week_record_list->start_time;
		$g['end_time'] = $week_record_list->end_time;
		array_push($time_slotss1, $g);
	   
	 }
	  // results blocked record
	  //Finally removing all blocked slots from current time slots
		for ( $i = $start_time; $i < $end_time; $i = $i + $time_duration1 ) 
		{  
			 $start_time_slots = $i;
			 $end_time_slots = $i+$time_duration1;
			 $k = 0;
			 $exclarray = array();
			 if( empty( $time_slotss1 ) )
				{
					 $time_slotss1['value']=0;
				}
			foreach($time_slotss1 as $slotss) 
				{  
					$start_time_booked = strtotime($slotss['value']);
					$end_time_booked = strtotime($slotss['end_time'])-1;
					for ( $j = $start_time_booked; $j <= $end_time_booked; $j = $j + $time_duration1 ) 
						{
							$ab = $j;
							$abc = $j+ $time_duration1 -1 ;
							if((($start_time_slots <= $ab && $ab < $end_time_slots)||($start_time_slots < $abc && $abc < $end_time_slots)))
								{
									$exclarray['text'] = date('H:i', $i ).'-'.date('H:i', $i + $time_duration1 );
									$exclarray['value'] = date('H:i:s', $i );
									$exclarray['consultation_fees'] = $consultation_fees - ($consultation_fees * ($discount / 100));
									$exclarray['actual_fees'] = $consultation_fees;
									$exclarray['tdo_check'] = $tdo_check; 
									$exclarray['today121'] = $today121;
									$exclarray['discountr'] = $discount;
									$exclarray['today_time_current'] = $today_time_current;
									$exclarray['start_time_booked'] = $start_time_booked;
									$exclarray['address'] = $address;
									$exclarray['slotcheck'] = 'disable'; 
									// Merge the event array into the return array
									array_push($time_slotss, $exclarray);
								
								
								$k=1;}
							
						}
				}
				if(($tdo_check==$today121)&&($today_time_current>$start_time_slots))
				{
						$exclarray['text'] = date('H:i', $i ).'-'.date('H:i', $i + $time_duration1 );
						$exclarray['value'] = date('H:i:s', $i );
						$exclarray['consultation_fees'] = $consultation_fees - ($consultation_fees * ($discount / 100));
						$exclarray['actual_fees'] = $consultation_fees;
						$exclarray['tdo_check'] = $tdo_check; 
						$exclarray['today121'] = $today121;
						$exclarray['discountr'] = $discount;
						$exclarray['today_time_current'] = $today_time_current;
						$exclarray['start_time_booked'] = $start_time_booked;
						$exclarray['address'] = $address;
						$exclarray['slotcheck'] = 'disable'; 
						// Merge the event array into the return array
						array_push($time_slotss, $exclarray);
					
					
					$k=1;
				
				
				}
			if($k == 0)
				{
					
					$exclarray['text'] = date('H:i', $i ).'-'.date('H:i', $i + $time_duration1 );
					$exclarray['value'] = date('H:i:s', $i );
					$exclarray['consultation_fees'] = $consultation_fees - ($consultation_fees * ($discount / 100));
					$exclarray['actual_fees'] = $consultation_fees;
					$exclarray['discountr'] = $discount;
					$exclarray['address'] = $address;
					$exclarray['tdo_check'] = $tdo_check;
									$exclarray['today121'] = $today121;
									$exclarray['today_time_current'] = $today_time_current;
									$exclarray['start_time_booked'] = $start_time_booked;
					$exclarray['slotcheck'] = 'enable';
					// Merge the event array into the return array
					array_push($time_slotss, $exclarray);
				}
		}
		  //Finally removing all blocked slots from current time slots
		echo json_encode($time_slotss);
    }
    
    public function getBlocking(){
        $forceBlocked = array();
	$forceBlocked['starttiming']=$_POST['timestart'];
	$forceBlocked['endtiming']=$_POST['timeend'];
	$start_tims = strtotime($forceBlocked['starttiming']);
	$end_tims = strtotime($forceBlocked['endtiming']);
	$forceBlocked['dateblocked']=$_POST['todaysdate1'];
	$forceBlocked['city_id']=$_POST['city_id_blocked'];
	$forceBlocked['setting_id']=$_POST['setting_id_blocked'];
	$dateblocked = strtotime($forceBlocked['dateblocked']);
	$dateCheck1 = date('Y-m-d',$dateblocked);
	$day1 = date("l", $dateblocked);
	$day1 = strtolower($day1);
	$week_blocked_record1 = "SELECT start_time,end_time FROM  wp_ba_weekrecord where (setting_id ='".$forceBlocked['setting_id']."')&&(day ='".$day1."')";
	$week_blocked_record1 = $wpdb->get_results($week_blocked_record1);
	$k=0;
	foreach($week_blocked_record1 as $week_blocked_record2)
		{
			$time_week_start = $week_blocked_record2->start_time;
			$time_week_end = $week_blocked_record2->end_time;
			$time_week_start = strtotime($time_week_start);
			$time_week_end = strtotime($time_week_end);
			if(($start_tims <= $time_week_start && $time_week_start < $end_tims)||($start_tims < $time_week_end && $time_week_end < $end_tims))
				{
				$forceBlocked['start_tims']=$_POST['start_tims'];
				$forceBlocked['time_week_start']=$_POST['time_week_start'];
				$forceBlocked['end_tims']=$_POST['end_tims'];
				$forceBlocked['time_week_end']=$_POST['time_week_end'];
				$k=1;}
           
		}
	$day_booked_record1 = "SELECT start_time,end_time FROM  wp_ba_appointmentrecord where (city_id ='".$forceBlocked['city_id']."')&&(status = 1 OR status = 3 OR status = 4)&&(app_date = '".$dateCheck1. "')";
	$week_record1 = $wpdb->get_results($day_booked_record1);
	foreach($week_record1 as $week_record_single)
		{
			$time_week_start1 = $week_record_single->start_time;
			$time_week_end1 = $week_record_single->end_time;
			$time_week_start1 = strtotime($time_week_start1);
			$time_week_end1 = strtotime($time_week_end1);
			if(($start_tims <= $time_week_start1 && $time_week_start1 < $end_tims)||($start_tims < $time_week_end1 && $time_week_end1 < $end_tims))
			{$k=1;}
           
		}
	if($k==1)
	{
	echo json_encode('NoData');
	}else{array_push($forceBlockedjson, $forceBlocked);
	echo json_encode($forceBlockedjson);}
    }
    
    public function TestEnv($env = 'test'){
       if($env == 'test'){
           return true;
       }else{
           return false;
       }
    }
    public function getRecipient($type){
        
       if($this->TestEnv()){
           $stmt = $this->conn->prepare("SELECT email FROM officeemails where type = 'test' "); 
            $stmt->execute();
            return $stmt->fetch()['email'];
       }else{
            $stmt = $this->conn->prepare("SELECT email FROM officeemails where type ='$type' "); 
            $stmt->execute();
          return $stmt->fetch()['email'];
       }
    }
    public function getfromaddr(){
       return "info@dhiindia.com";
    }

    public function refralData($refralName,$refralPh,$refralMobile,$refralRelation){
        session_start();
        $api_key = "d1942cc8edb1bbd654af61c9c6ece16f";
        $from = "info@dhiindia.com";
        $fromname = "DHI-India";
        $txnid = $_SESSION['txnid'];
        $appointmentDetails = $this->fetchUserdetails($txnid);
        $sql = "INSERT INTO wp_refral (appointmentid,rname,phone,mobile,relation,timecreated) values";
        foreach ($refralName as $k=>$v){
            $name = $refralName[$k];
            $Ph = $refralPh[$k];
            $Mobile = $refralMobile[$k];
            $Relation = $refralRelation[$k];
            $time = time();
            $appid = isset($_SESSION['txnid'])?$appointmentDetails->id:0;
            $sql.= "('$appid','$name','$Ph','$Mobile','$Relation','$time'),";
        }
        $sql = rtrim($sql,",");
        $stmt =  $this->conn->prepare($sql); 
       if($stmt->execute()){
            $message1 = "<html><body>";
            $message1 .= "<h2>Referral lead submitted by $appointmentDetails->name</h2>";
            $message1 .= "<table  cellpadding='10' border='1'>";
            $message1 .= "<tr><th><strong>Name</strong> </th><th><strong>Email No.</strong> </th><th><strong>Mobile No.</strong> </th><th><strong>Relation with you</strong> </th></tr>";
            foreach ($refralName as $k=>$v){   
                $name = $refralName[$k];
                $Ph = $refralPh[$k];
                $Mobile = $refralMobile[$k];
                $Relation = $refralRelation[$k];
                $message1 .= "<tr><td>$name</td><td>$Ph</td><td>$Mobile</td><td>$Relation</td></tr>";
            }    
            $message1 .= "</table>";
            $message1 .= "<br>Reffered By :";
            $message1 .= "<br>Name :" .$appointmentDetails->name;
            $message1 .= "<br>Mobile No :". $appointmentDetails->phone;
            $message1 .= "<br>Email :".$appointmentDetails->email;
            $message1 .= "</body></html>";
            $subject1 = 'New Referral Leads';

           
            $subject = $subject1;
            $content = $message1;
            $data=array();
            $data1['subject']= $subject;                                                                       
            $data1['fromname']= $fromname;                                                             
            $data1['api_key'] = $api_key;
            $data1['from'] = $from;
            $data1['content']= $content;
            $data1['recipients']= $this->getRecipient('enquiry');
            $this->callMailApi(@$api_type,@$action,$data1);
       }
    }
    
    public function contactForm($name,$email,$phone,$message,$city){
        $api_key = "d1942cc8edb1bbd654af61c9c6ece16f";
        $from = $this->getfromaddr();
        $fromname = "DHI-India";
        $to = $this->getRecipient('contact');
        $time = time();
        $sql = "INSERT INTO wp_contact_us (name,email,phone,city,message,timecreated) values('$name','$email','$phone','$city','$message','$time')";
        $stmt =  $this->conn->prepare($sql); 
       if($stmt->execute()){
            $data=array();
            $data['subject']= 'Cuntact Us';                                                                       
            $data['fromname']= $fromname;                                                             
            $data['api_key'] = $api_key;
            $data['from'] = $from;
            $data['content']= $message;
            $data['recipients']= $to;
            $this->callMailApi(@$api_type,@$action,$data);
       }      
    }
    
    public function getClientFeddback(){
         $stmt = $this->conn->prepare("SELECT * FROM client_feedbacks WHERE status='1'"); 
        $stmt->execute();
        return $stmt->fetchAll();
    }
    public function saveClientFeddback($service,$name,$email,$contactNo,$rating,$feedbacktext){
        $sql = "INSERT INTO client_feedbacks (service,name,email,mobile,rating,review_text) values('$service','$name','$email','$contactNo','$rating','$feedbacktext')";
        $stmt =  $this->conn->prepare($sql); 
        $stmt->execute();
    }
    public function saveFaqForm($name,$email,$phone,$subject,$question){
        $time = time();
        $sql = "INSERT INTO faq (name,email,phone,subject,question,created_at) values('$name','$email','$phone','$subject','$question','$time')";
        $stmt =  $this->conn->prepare($sql); 
       return $stmt->execute();
    }
    public function saveTraining($name,$email,$phone,$country,$trainingformType){
        $api_key = "d1942cc8edb1bbd654af61c9c6ece16f";
        $from = $this->getfromaddr();
        //$to = "trainning@dhiinternational.com";
        $fromname = "DHI-India";
        $time = time();
        $message = "";
        $sql = "INSERT INTO training (name,email,phone,country,type,created_at) values('$name','$email','$phone','$country','$trainingformType','$time')";
        $stmt =  $this->conn->prepare($sql); 
        if($stmt->execute()){
            $message1 = "<html><body>";
            $message1 .= "<h2>Training Enquiry</h2>";
        
            $message1 .= "<br>Name :" .$name;
            $message1 .= "<br>Mobile No :".$phone;
            $message1 .= "<br>Email :".$email;
            $message1 .= "<br>Country :".$country;
            $message1 .= "<br>Trainning Type:".$trainingformType;
            $message1 .= "</body></html>";
            
            $data=array();
            $data['subject']= 'New Training Enquiry';                                                                       
            $data['fromname']= $fromname;                                                             
            $data['api_key'] = $api_key;
            $data['from'] = $from;
            $data['recipients']= $this->getRecipient('trainning');
            $data['content']= $message1;
        return $this->callMailApi(@$api_type,@$action,$data);
       } 
    }
    public function savefranchisee($name,$email,$phone,$country,$message){
        $api_key = "d1942cc8edb1bbd654af61c9c6ece16f";
        $from = $this->getfromaddr();
        //$to = "trainning@dhiinternational.com";
        $fromname = "DHI-India";
        $time = time();
        $message1 = "";
        $sql = "INSERT INTO franchisee (name,email,phone,country,message,created_at) values('$name','$email','$phone','$country','$message','$time')";
        $stmt =  $this->conn->prepare($sql); 
        if($stmt->execute()){
            $message1 = "<html><body>";
            $message1 .= "<h2>Franchisee request</h2>";
        
            $message1 .= "<br>Name :" .$name;
            $message1 .= "<br>Mobile No :".$phone;
            $message1 .= "<br>Email :".$email;
            $message1 .= "<br>Country :".$country;
            $message1 .= "<br>Message Type:".$message;
            $message1 .= "</body></html>";
            
            $data=array();
            $data['subject']= 'New Franchisee Request';                                                                       
            $data['fromname']= $fromname;                                                             
            $data['api_key'] = $api_key;
            $data['from'] = $from;
            $data['recipients']= $this->getRecipient('franchisee');
            $data['content']= $message1;
            return $this->callMailApi(@$api_type,@$action,$data);
       } 
    }
    public function savedrassociation($name,$email,$phone,$country,$message,$Interested){
        $api_key = "d1942cc8edb1bbd654af61c9c6ece16f";
        $from = $this->getfromaddr();
         $InterestedArr = explode(",,", $Interested);
         $str = "";
         foreach ($InterestedArr as $key => $value) {
            $n = ++$key;
            $str .= "($n) $value <br>" ;
         }
        
        //$to = "trainning@dhiinternational.com";
        $fromname = "DHI-India";
        $time = time();
        $message1 = "";
        $sql = "INSERT INTO dr_association (name,email,phone,country,interested,message,created_at) values('$name','$email','$phone','$country','$Interested','$message','$time')";
        $stmt =  $this->conn->prepare($sql); 
        if($stmt->execute()){
            
            $message1 = "<html><body>";
            $message1 .= "<h2>Dr Association Request</h2>";
            $message1 .= "<br>Name :" .$name;
            $message1 .= "<br>Mobile No :".$phone;
            $message1 .= "<br>Email :".$email;
            $message1 .= "<br>Country :".$country;
            $message1 .= "<br>Dr Express interest in: <br>".$str;
            $message1 .= "<br>Message :".$message;
            $message1 .= "</body></html>";
            
            $data=array();
            $data['subject']= 'New Dr Association Request';                                                                       
            $data['fromname']= $fromname;                                                             
            $data['api_key'] = $api_key;
            $data['from'] = $from;
            $data['recipients']= $this->getRecipient('franchisee');
            $data['content']= $message1;
            return $this->callMailApi(@$api_type,@$action,$data);
       } 
    }
    public function saveNeedHelpForm($name,$email,$phone,$city){
        $api_key = "d1942cc8edb1bbd654af61c9c6ece16f";
        $from = $this->getfromaddr();
        $fromname = "DHI-India";
        $time = time();
        $message1 = "";
        $sql = "INSERT INTO needhelp (name,email,phone,city,created_at) values('$name','$email','$phone','$city','$time')";
        $stmt =  $this->conn->prepare($sql); 
        if($stmt->execute()){
            $message1 = "<html><body>";
            $message1 .= "<h2>I need help please call me.Details are below</h2>";
        
            $message1 .= "<br>Name :" .$name;
            $message1 .= "<br>Mobile No :".$phone;
            $message1 .= "<br>Email :".$email;
            $message1 .= "<br>City :".$city;
            $message1 .= "</body></html>";
            
            $data=array();
            $data['subject']= 'New Need Help Request';                                                                       
            $data['fromname']= $fromname;                                                             
            $data['api_key'] = $api_key;
            $data['from'] = $from;
            $data['recipients']= $this->getRecipient('info');
            $data['content']= $message1;
            return $this->callMailApi(@$api_type,@$action,$data);
       } 
    }
    public function savejobapplyForm($fname,$lname,$email,$phone,$jobtitle,$filename){
        $api_key = "d1942cc8edb1bbd654af61c9c6ece16f";
        $from = $this->getfromaddr();
        $fromname = "DHI-India";
        $time = time();
        $message1 = "";
        $sql = "INSERT INTO jobapply (fname,lname,phone,email,jobtitle,resume,created_at) values('$fname','$lname','$phone','$email','$jobtitle','$filename','$time')";
        $stmt =  $this->conn->prepare($sql); 
        if($stmt->execute()){
            $message1 = "<html><body>";
            $message1 .= "<h2>New Job Apply</h2>";
            $message1 .= "<br>First Name :" .$fname;
            $message1 .= "<br>Last Name :" .$lname;
            $message1 .= "<br>Mobile No :".$phone;
            $message1 .= "<br>Email :".$email;
            $message1 .= "</body></html>";
            $data=array();
            $data['subject']= 'New Job Apply from '.$fname;                                                                       
            $data['fromname']= $fromname;                                                             
            $data['api_key'] = $api_key;
            $data['from'] = $from;
            $data['recipients']= $this->getRecipient('hr');
            $data['content']= $message1;
            $data['attachments']= $_SERVER['DOCUMENT_ROOT'].'/image/upload/'.$filename;
            return $this->mail_with_attached($data);
       } 
    }
    
    
	
	
}