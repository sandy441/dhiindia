<?php
include 'classes/userinfo.php';
$userinfo = new userinfo();
$home_url = $userinfo->getBaseUrl();
$ClientFeddback = $userinfo->getClientFeddback();

if(isset($_POST['submit'])){
    
    $rating = isset($_POST['rating'])?$_POST['rating']:0;
    $service = $_POST['services'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $contactNo = $_POST['contactNo'];
    $feedbacktext = $_POST['feedbacktext'];
    $userinfo->saveClientFeddback($service,$name,$email,$contactNo,$rating,$feedbacktext);
}
//echo '<pre>';print_r($ClientFeddback);die;
?>
<?php include 'header.php';?>
        
        <section>
            <img src="image/testimonial-banner.jpg" alt="" class="img-fluid">
        </section>
        
        <section class="commmon-padd bg-col-1">
            <div class="container">
                <div class="row justify-content-center">
                    <h2 class=" pb-3">What clients are saying about us</h2>
                </div>
                <div class="row">
                    <div class="col-sm-12 pb-3 text-center">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal1" class="btn btn-common my-2 my-sm-0">Tell Us Your Story</a>
                    </div>
                    
                    <div id="pinBoot">
                         <?php $count=0; foreach ($ClientFeddback as $client):?>
                        <article class="white-panel">
                            <div class="form-group rating col-sm-12" data-rating="<?php echo $client['rating'];?>">
                                <input type="radio" data-trigger="5" id="star<?php echo $count;?>5" name="rating<?php echo $count;?>" value="5" disabled=""><label for="star<?php echo $count;?>5" title="Rocks!">5 stars</label>
                                 <input type="radio" data-trigger="4" id="star<?php echo $count;?>4" name="rating<?php echo $count;?>" value="4"disabled=""><label for="star<?php echo $count;?>4" title="Pretty good">4 stars</label>
                                 <input type="radio" data-trigger="3" id="star<?php echo $count;?>3" name="rating<?php echo $count;?>" value="3"disabled=""><label for="star<?php echo $count;?>3" title="Meh">3 stars</label>
                                 <input type="radio" data-trigger="2" id="star<?php echo $count;?>2" name="rating<?php echo $count;?>" value="2"disabled=""><label for="star<?php echo $count;?>2" title="Kinda bad">2 stars</label>
                                 <input type="radio" data-trigger="1" id="star<?php echo $count;?>1" name="rating<?php echo $count;?>" value="1"disabled=""><label for="star<?php echo $count;?>1" title="Sucks big time">1 star</label>
                            </div> 
                        <?php if(!empty($client['videolink']) && !empty($client['banner'])):?>    
                            <div class="wrapper">
                                <img src="<?php echo $client['banner'];?>" alt="">
                                <div class="image-pos">
                                    <a href="javascript:void(0);" data-toggle="modal" data-clientname="<?php echo $client['name'];?>" data-youtubeurl="<?php echo $client['videolink'];?>" ><span class="oi oi-play-circle"></span></a> 
                                </div>  
                            </div>   
                         <?php endif;?>        
                         <?php if(!empty($client['image'])):?>
                            <img src="<?php echo $client['image']?>" alt="">
                        <?php endif;?>    
                            <h5 class="pt-3 pb-3">
                                    <span><img src="image/default-avatar-250x250.png" alt="" class="rounded-circle" style="width:40px;"></span>
                                        <span class="pl-2"><?php echo $client['name'];?></span>
                            </h5>
                            
                          <?php if(!empty($client['review_text'])):?>    
                            <p><?php echo $client['review_text']?></p>
                         <?php endif;?>    
                            <!-- <div class="clearfix">
                              <div class="float-left"><span class="oi oi-calendar"></span> 20-11-2017</div>
                              <div class="float-right text-warning"><span class="oi oi-star"></span><span class="oi oi-star"></span><span class="oi oi-star"></span><span class="oi oi-star"></span><span class="oi oi-star"></span></div>
                            </div> -->
                        </article>
                        <?php $count++;endforeach;?>
                   
                    </div>
                </div>
<!--                <div class="row justify-content-center"> 
                    <nav aria-label="Page navigation example ">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>-->
            </div>
        </section>
        
        <div class="clearfix"></div>
        
        
        <section class="bottm_sec">
            <h1>Increase your hair density naturally</h1>
            <a href="javascript:void(0);" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
        </section>
        
        
        <!-- FOOTER -->
        
<?php include 'footer.php';?>
        
        <div class="modal fade" id="YoutubePuppop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="" data-modelname=''> Ramandeep Singh
                            (India)</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" data-modelbody=''>
                        
                    </div>
                    
                </div>
            </div>
        </div>
<!--        <div class="modal fade" id="exampleModal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"> Gaurav Gera
                            (India)</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <iframe width="100%" height="500" src="//www.youtube.com/embed/6Fyc4kXppLY?rel=0" frameborder="0" allowfullscreen=""></iframe>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal-3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"> R. Sreejesh
                            
                            (India)</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <iframe width="100%" height="500" src="https://www.youtube.com/embed/uh6f6lwrX-w" frameborder="0" allowfullscreen=""></iframe>
                    </div>
                    
                </div>
            </div>
        </div>-->
        <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLabel">How do you rate us</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="mx-auto" style="width: 600px">
                            <form name="feedbackForm" method="post" action="">
                            <div class="form-group rating col-sm-12">
                                <strong>Rate Us :</strong> 

                                 <input type="radio" id="star5" name="rating" value="5"><label for="star5" title="Rocks!">5 stars</label>
                                 <input type="radio" id="star4" name="rating" value="4"><label for="star4" title="Pretty good">4 stars</label>
                                 <input type="radio" id="star3" name="rating" value="3"><label for="star3" title="Meh">3 stars</label>
                                 <input type="radio" id="star2" name="rating" value="2"><label for="star2" title="Kinda bad">2 stars</label>
                                 <input type="radio" id="star1" name="rating" value="1"><label for="star1" title="Sucks big time">1 star</label>
                            </div>
                                
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Select Service</label>
                                    <select class="form-control" name="services" id="">
                                        <option value="Platelet Rich Plasma">Platelet Rich Plasma</option>
                                        <option value="Laser Anagen">Laser Anagen</option>
                                        <option value="Direct Hair Fusion">Direct Hair Fusion</option>
                                        <option value="Direct Hair Implantation">Direct Hair Implantation</option>
                                        <option value="Scalp Micropigmentation">Scalp Micropigmentation</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" name="name" placeholder="NAME*">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="" name="email" placeholder="EMAIL*">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" name="contactNo"placeholder="CONTACT NUMBER*">
                                </div>
                                
                                <div class="form-group">
                                    <textarea class="form-control" id="" rows="4" name="feedbacktext" placeholder="REVIEW(Your Review will be posted publicly)"></textarea>
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" name="submit" class="btn btn-common btn-pdding ">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
    </body>
</html>
