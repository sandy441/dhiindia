
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" >
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="DHI™ India -Â  Available at Delhi, Gurgaon, Chennai, Bangalore, Kolkata, Jaipur, Chandigarh, Ludhiana and 20+ Locations. Book Consultation with DHI™ Doctors at your nearest Clinic now!
              ">
        <meta name="author" content="">
        <title>DHI™ Hair Loss Treatment Clinics in India - Ranked No: 1 in Customer Satisfaction by IMRD
            
        </title>
<?php include 'header.php';?>
    </head>
    <body>
        
        <section class="bg-col-1 commmon-padd">
            <div class="container">
                <h1 class="text-center">OUR CLINICS</h1>
                <div class="row mt-4">
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/delhi-clinic.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-delhi.php">Delhi</a></h5>
                                
                                <address>
                                    B – 5/15 Safdarjung Enclave<br />
                                    Opposite DLTA & Deer Park<br />
                                    Delhi 110029<br />
                                    011 – 46103032/36 & +91 8826006714<br />
                                    <a target="_blank" href="https://www.google.com/maps/place/DHI%E2%84%A2+India+-+Best+Hair+Transplant+Clinic+in+Delhi/@28.5603867,77.1874923,17z/data=!3m1!4b1!4m5!3m4!1s0x390ce276f4190a7d:0xf7755f0b8d7a9e7d!8m2!3d28.560382!4d77.189681"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                        
                        
                    </div>
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/gurgaon-clinic.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-gurugram.php">Gurugram</a></h5>
                                <address>
                                    220-221, 2nd Floor<br />
                                    South Point Mall, Sec 53, DLF Golf Course Road<br />
                                    Gurugram 122009<br />
                                    0124 – 4276426/ 40666481 & +91 8800804330<br />
                                    <a target="_blank" href="https://www.google.com/maps/search/DHI+2nd+Floor+South+Point+Mall,+DLF+Golf+Course+Road+Gurgaon+122009/@28.4480192,77.0969281,17z/data=!3m1!4b1"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/mumbai-clinic.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-mumbai.php">Mumbai</a></h5>
                                <address>
                                    CPLSS 3rd Floor<br />
                                    Above Sarla Hospital, Dattataray Road<br />
                                    Santacruz West, Mumbai 400054<br />
                                    022 – 6117888/ 61178880 & +91 9833807002<br />
                                    <a target="_blank" href="https://www.google.com/maps/place/DHI+Hair+Loss+%7C+Baldness+Treatment+Mumbai+Clinic/@19.0856561,72.8338375,17z/data=!4m8!1m2!2m1!1sMumbai+CPLSS+3rd+Floor+Above+Sarla+Hospital,+Dattataray+Road+Santacruz+West,+Mumbai+400054!3m4!1s0x3be7c9a693f7c851:0x696cbc13704a693!8m2!3d19.085692!4d72.835675"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/banglor-clinic.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-bangalore.php">Bangalore</a></h5>
                                <address>
                                    4/1 Walton Road (Above Cafe Coffee Day)<br />
                                    1st Floor, Lavelle Junction<br />
                                    Bangalore 560001<br />
                                    080 – 22270046/ 47 & +91 9686115381<br />
                                    <a target="_blank" href="https://www.google.com/maps/place/DHI+Bangalore+Clinic+-+Hair+Loss+Treatment+%26+Transplant/@12.9714881,77.5957182,17z/data=!3m1!4b1!4m5!3m4!1s0x3bae16799aa46f25:0x1ca246babbadf5d8!8m2!3d12.9714829!4d77.5979122"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/pune-clinic.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-pune.php">Pune</a></h5>
                                <address>
                                    CPLSS, Shree Dattaguru Complex <br />
                                    6th Lane Ashok Chakra Society Koregoan Park <br />
                                    Pune 411001<br />
                                    020 – 69335555 & +91 8446435000<br />
                                    <a target="_blank" href="https://www.google.com/maps/place/CPLSS+Pune-+Cosmetic+Surgeon/@18.5362261,73.896402,17z/data=!3m1!4b1!4m5!3m4!1s0x3bc2c107b28ccd2f:0x5cca2190f663fdc5!8m2!3d18.536221!4d73.898596"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/chennai-clinic.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-chennai.php">Chennai</a></h5>
                                <address>
                                    2nd Floor, Sheriff Towers<br />
                                    No. 28/118, GN Chetty Road, T.Nagar<br />
                                    Chennai 600017<br />
                                    044 – 45497788 & +91 9840844084<br />
                                    <a target="_blank" href="https://www.google.com/maps/place/DHI+Hair+Loss+Treatment+%26+Transplant+Surgery+Clinic+Chennai/@13.0475395,80.2420646,17z/data=!3m1!4b1!4m5!3m4!1s0x3a526652085d52b9:0xd6dd8677d151c617!8m2!3d13.0475343!4d80.2442586"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/kolcata.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-kolkata.php">Kolkata</a></h5>
                                <address>
                                    DHI, 4th Floor, Platinum Mall 31,<br />
                                    Elgin Road,<br />
                                    Kolkata- 700020<br />
                                    +91 9007194854<br />
                                    <a target="_blank" href="https://www.google.co.in/maps/place/DHI+India-+Best+Hair+Transplant+In+Kolkata+%26+Hair+Loss+Treatment+Clinic+In+Kolkata/@22.5382055,88.3513781,15z/data=!4m2!3m1!1s0x0:0x1e85654577e9d0bf?sa=X&ved=0ahUKEwjj8qr-vfnVAhUDL48KHbrrAPoQ_BIIZDAK"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/clinic2.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-kochi.php">Kochi</a></h5>
                                <address>
                                    G189, Panampilly Nagar<br />
                                    Near Panampilly Nagar Post Office<br />
                                    Kochi 682036<br />
                                    0484 – 4033302 & +91 9995776644<br />
                                    <a target="_blank" href="https://www.google.com/maps/place/DHI+Hair+Loss+Treatment+Clinic+Kochi/@9.9630489,76.2951883,17z/data=!3m1!4b1!4m5!3m4!1s0x3b0872c7cd44b899:0x659ebd433af30925!8m2!3d9.9630436!4d76.2973823"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div> 
                    
                    
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/Ahmedabad.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-ahemdabad.php">Ahmedabad</a></h5>
                                <address>
                                    Newtouch Laser Center Satellite<br />
                                    108 Sangrila Arcade, Near Shyaml Char Rasta<br />
                                    Ahmedabad 380015<br />
                                    079 – 40062549 & 8511100962<br />
                                    <a target="_blank" href="https://www.google.com/maps/place/New+Touch/@23.0142589,72.5278725,17z/data=!4m8!1m2!2m1!1sAhmedabad+Newtouch+Laser+Center+Satellite+108+Sangrila+Arcade,+Near+Shyaml+Char+Rasta+Ahmedabad+380015!3m4!1s0x395e84d6512f44ab:0x9bbc51db23fc3032!8m2!3d23.014084!4d72.528829"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/hyderabad-clinic.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-hyderabad.php">Hyderabad</a></h5>
                                <address>
                                    Oliva Hair Transplantation & Surgery center<br />
                                    H.No. 8-2-293/82/A/502, Road No 36<br />
                                    Jubilee Hills, Hyderabad 500034<br />
                                    +91 9550160055 & +91 9000287345<br />
                                    <a target="_blank" href="https://www.google.com/maps/place/Oliva+Hair+Transplantation+Center+%7C+Hair+Transplant+Clinic+Hyderabad/@17.4346931,78.4032506,19z/data=!4m8!1m2!2m1!1sHyderabad+Oliva+Hair+Transplantation+%26+Surgery+center+H.No.+8-2-293%2F82%2FA%2F502,+Road+No+36+Jubilee+Hills,+Hyderabad+500034!3m4!1s0x3bcb91486f9da425:0x42d344a0d3cf9b14!8m2!3d17.4340186!4d78.4051592"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/chandigarh-clinic.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-chandigarh.php">Chandigarh</a></h5>
                                <address>
                                    Headmasters – Madhya Marg
                                    SCO 16-19, Sector 8-C
                                    Chandigarh 160017, India<br />
                                    0172 – 4008553 & +91 8284818844<br />
                                    <a target="_blank" href="https://www.google.com/maps/place/HEADMASTERS+SALON+PVT+LTD/@30.7400891,76.7903684,17z/data=!3m1!4b1!4m5!3m4!1s0x390fed0d815cd345:0x15eebca692f0e91f!8m2!3d30.7400845!4d76.7925624"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div>
                    
                    <!-- <div class="col-sm-4">
                      <div class="media">
                  <img class="align-self-start mr-3" src="./image/office/ludhiyana-clinic.png" alt="Generic placeholder image" width="100">
                  <div class="media-body">
                    <h5 class="mt-0 clinic_name">Ludhiana</h5>
                   <address>
                                  SCO 10E, 2nd Floor, Headmasters Building<br />
                                  Malhar Cinema Road, Sarabha Nagar<br />
                                  Ludhiana 141001<br />
                                  +91 9915470344 & +91 9501612383<br />
                                  <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
                              </address>
                  </div>
                </div>
                    </div> -->
                    
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/Jaipur.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-jaipur.php">Jaipur</a></h5>
                                <address>
                                    201-201 A,2nd floor Ambition tower,<br />
                                    agrasen circle, C-scheme,<br />
                                    Jaipur 302001<br />
                                    08890122218, +91 141-4022218, +91 141-2362217<br />
                                    
                                    <a target="_blank" href="https://www.google.co.in/maps/place/DHI-+Hair+Treatment+Clinic/@26.9132167,75.8006213,17z/data=!3m1!4b1!4m5!3m4!1s0x396db4108d76cbcb:0xedb791d788784023!8m2!3d26.9132167!4d75.8028154"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/calikut-clinic.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-calicut.php">Calicut</a></h5>
                                <address>
                                    Dr. Rafeek’s Skin and Cosmetic Center<br />
                                    Near Malayala Manorama, East Nadakkavu <br />
                                    Wayanad Road<br />
                                    Calicut 673017<br />
                                    0495 6533302<br />
                                    <a target="_blank" href="https://www.google.co.in/maps/place/Dr.+Rafeeq's+Skin+%26+Cosmetic+Surgery+Research+Centre/@11.2697213,75.7757943,17z/data=!3m1!4b1!4m5!3m4!1s0x3ba65ecb5828f561:0xf7cff16fb95fd2a5!8m2!3d11.269716!4d75.7779883"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/Guwahati.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-guwahati.php">Guwahati</a></h5>
                                <address>
                                    Rejuve Skin & Aesthetic Clinic<br />
                                    2nd Floor, Gulshan Grand<br />
                                    Dispur, Guwahati 781006<br />
                                    0361 – 2230778 & +91 8011611002<br />
                                    <a target="_blank" href="https://www.google.com/maps/place/Rejuve+Skin+And+Hair+Aesthetic+Clinic/@26.1419378,91.793553,17z/data=!3m1!4b1!4m5!3m4!1s0x375a58d92fa621c1:0x3d7cb434b6543c9b!8m2!3d26.141933!4d91.795747"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/lakhnow-clinic.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name"><a href="hair-transplant-clinic-lucknow.php">Lucknow</a></h5>
                                <address>
                                    DermaKlinic B-43, J Park <br />
                                    Mahanagar Extension<br />
                                    Lucknow 226006<br />
                                    0522 – 4025505 & +91 7851890077<br />
                                    <a target="_blank" href="https://www.google.com/maps/place/Derma+Klinic/@26.8723038,80.937394,17z/data=!3m1!4b1!4m5!3m4!1s0x399bfd8319ab59e5:0x12010fdb0a3407f7!8m2!3d26.872299!4d80.939588"><span class="oi oi-map-marker"></span> Directions</a><br />
                                </address>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/vijayvada-clinic.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name">Vijayawada</h5>
                                <address>
                                    Opening Soon...
                                    
                                </address>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="media">
                            <img class="align-self-start mr-3" src="./image/office/Coimbatore-Icon.png" alt="Generic placeholder image" width="100">
                            <div class="media-body">
                                <h5 class="mt-0 clinic_name">Coimbatore</h5>
                                <address>
                                    Opening Soon...
                                    
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </section>
        
        
        
        <div class="clearfix"></div>
        
        
        
        
        <!-- FOOTER -->
        
<?php include 'footer.php';?>
        
   </body>
</html>
