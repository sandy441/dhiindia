function ucfirst(str,force){
    str=force ? str.toLowerCase() : str;
    return str.replace(/(\b)([a-zA-Z])/,
        function(firstLetter){
            return   firstLetter.toUpperCase();
        });
}
$(document).ready(function (){
    var form = $(".formMain form").attr("id");
    var filter = /^[0-9 +-]*[0-9][0-9 +-]*$/;

    $.validator.addMethod("valueNotEquals", function(value, element, arg){
        if(form == 'lp-sleek') {
            return 'Select City' != value;
        }else if(form == 'brentwoods-uae-client-lp') {
            return 'Select Your MBA Program' != value;
        }else if(form == 'Btech' || form == 'MBA-LP-2014') {
            return 'Select Course' != value;
        }else if(form == 'askiitians_lp') {
            if('Select Country' == value){
                return 'Select Country' != value;
            }else if('Select Subject' == value){
                return 'Select Subject' != value;
            }
            else if('Select Grade' == value){
                return 'Select Grade' != value;
            }else{
                return arg != value;
            }
        }else if(form == 'hdfc') {
            return 'Select City' != value;
        } else if(form == 'firesafety' || form == 'projectsdivision' || form == 'securitydivision') {
            return 'Select Main Product Category' != value;
        } else if(form == 'ICFAI LP' || form == 'ICFAI-Start Your Application') {
            if('Select Your State' == value) {
                return 'Select Your State' != value;
            } else if('Select City/District' == value) {
                return 'Select City/District' != value;
            } else {
                return arg != value;
            }
        } else {
            return arg != value;
        }
    }, "value must be valid.");

    $.validator.addMethod("valueNotCeaseEquals", function(value, element, arg){
        if(form == 'firesafety' || form == 'projectsdivision' || form == 'securitydivision') {
            return 'Select Sub Product Category' != value;
        } else {
            return arg != value;
        }
    }, "value must be valid.");

    $.validator.addMethod("valuecheck", function(value, element, arg){
        return filter.test(value);
    }, "value must be valid.");


    $.validator.addMethod("checkphone", function(value, element) {
        return this.optional(element)|| value.match(/^[0-9 ,\+-]+$/);
    }, "Please enter a valid number");

    $.validator.addMethod("checkcustomphone", function(value, element) {
        return this.optional(element) || (/^[7-9][0-9]{9}$/).test(value);
    }, "Please enter a valid number");
    
    
    $.validator.addMethod("phonewithcodecheck", function(value, element) {
        return this.optional(element) || value.match(/^[0-9 ,\+-]+$/);
    }, "Please enter a valid number");
    
    
    $.validator.addMethod("phonelengthcheck", function(value, element) {
        return this.optional(element) || value.match(/^[0-9 ,\+-]+$/);
    }, "Please enter a valid number");
    
    $.validator.addMethod("checklanguage", function(value, element) {
        return value.match(/^[@.-_ a-zA-Z0-9]+$/);
    }, "Only english characters are allowed.");

    $.validator.addMethod("namechars", function(value, element) {
        return value.match(/^[a-zA-Z]+$/);
    }, "Only characters are allowed.");

    $.validator.addMethod("namecharsspace", function(value, element) {
        return value.match(/^[a-zA-Z ]+$/);
    }, "Only characters are allowed.");


    if('meritta-lite'== form){
        $("#cmobile").removeClass("number");
        $("#cmobile").attr("maxlength", "20");
        $.validator.addClassRules({
        phone: {
            minlength: 10,
            checkphone: true,
            maxlength: 20
        },
        brentwoodphone: {
            number:true,
            minlength: 9,
            maxlength: 13
        },
        brentwoodTanzania: {
            number:true,
            minlength: 9,
            maxlength: 12
        },
        brentwoodUAE: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        brentwoodNigeria: {
            number:true,
            minlength: 10,
            maxlength: 13
        },
        brentwoodPhilippines: {
            number:true,
            minlength: 9,
            maxlength: 11
        },
        brentwoodOman: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        radiorequired : {
            required :true
        },
        checkrequired : {
            required :true
        },
        comborequired    : {
            required :true,
            valueNotEquals: "Select"
        },
        comboceaserequired    : {
            required :true,
            valueNotCeaseEquals: "Select"
        },
        customcheck : {
            minlength: 10,
            maxlength: 15,
            valuecheck: "Select"
        }
    });
    }else if('Responsive Design 2' == form){
      $("#cphone").removeClass("number");
        $("#cphone").attr("maxlength", "20");
        $.validator.addClassRules({
        phone: {
            minlength: 10,
            checkphone: true,
            maxlength: 20
        },
        brentwoodphone: {
            number:true,
            minlength: 9,
            maxlength: 13
        },
        brentwoodTanzania: {
            number:true,
            minlength: 9,
            maxlength: 12
        },
        brentwoodUAE: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        brentwoodNigeria: {
            number:true,
            minlength: 10,
            maxlength: 13
        },
        brentwoodPhilippines: {
            number:true,
            minlength: 9,
            maxlength: 11
        },
        brentwoodOman: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        radiorequired : {
            required :true
        },
        checkrequired : {
            required :true
        },
        comborequired    : {
            required :true,
            valueNotEquals: "Select"
        },
        comboceaserequired    : {
            required :true,
            valueNotCeaseEquals: "Select"
        },
        customcheck : {
            minlength: 10,
            maxlength: 15,
            valuecheck: "Select"
        }
    });
    }else if('website-generic' == form){
        $("#cmobileno").removeClass("number");
        $("#cmobileno").attr("maxlength", "20");
        $.validator.addClassRules({
        phone: {
            minlength: 10,
            checkphone: true,
            maxlength: 20
        },
        brentwoodphone: {
            number:true,
            minlength: 9,
            maxlength: 13
        },
        brentwoodTanzania: {
            number:true,
            minlength: 9,
            maxlength: 12
        },
        brentwoodUAE: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        brentwoodNigeria: {
            number:true,
            minlength: 10,
            maxlength: 13
        },
        brentwoodPhilippines: {
            number:true,
            minlength: 9,
            maxlength: 11
        },
        brentwoodOman: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        radiorequired : {
            required :true
        },
        checkrequired : {
            required :true
        },
        comborequired    : {
            required :true,
            valueNotEquals: "Select"
        },
        comboceaserequired    : {
            required :true,
            valueNotCeaseEquals: "Select"
        },
        customcheck : {
            minlength: 10,
            maxlength: 15,
            valuecheck: "Select"
        }
    });
    }else if('Frankfinn Institute Dubai' == form){
	$.validator.addClassRules({
        phone: {
            number:true,
            minlength: 7,
            maxlength: 7
        },
dialcode:{
 number:true,
            
            maxlength: 2


},
        brentwoodphone: {
            number:true,
            minlength: 9,
            maxlength: 13
        },
        brentwoodTanzania: {
            number:true,
            minlength: 9,
            maxlength: 12
        },
        brentwoodUAE: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        brentwoodNigeria: {
            number:true,
            minlength: 10,
            maxlength: 13
        },
        brentwoodPhilippines: {
            number:true,
            minlength: 9,
            maxlength: 11
        },
        brentwoodOman: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        icfaiphone: {
            number:true,
            minlength: 10,
            maxlength: 10
        },
        radiorequired : {
            required :true
        },
        checkrequired : {
            required :true
        },
        comborequired    : {
            required :true,
            valueNotEquals: "Select"
        },
        comboceaserequired    : {
            required :true,
            valueNotCeaseEquals: "Select"
        },
        customcheck : {
            minlength: 10,
            maxlength: 15,
            checkphone: true
        },
        checklanguage : {
            checklanguage: true
        },
        namechars : {
            namechars: true
        }
    });
    }else if('FICT Vocational Training' == form){
	$.validator.addClassRules({
        phone: {
            number:true,
            minlength: 10,
            maxlength: 10
        },
        brentwoodphone: {
            number:true,
            minlength: 9,
            maxlength: 13
        },
        brentwoodTanzania: {
            number:true,
            minlength: 9,
            maxlength: 12
        },
        brentwoodUAE: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        brentwoodNigeria: {
            number:true,
            minlength: 10,
            maxlength: 13
        },
        brentwoodPhilippines: {
            number:true,
            minlength: 9,
            maxlength: 11
        },
        brentwoodOman: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        icfaiphone: {
            number:true,
            minlength: 10,
            maxlength: 10
        },
        radiorequired : {
            required :true
        },
        checkrequired : {
            required :true
        },
        comborequired    : {
            required :true,
            valueNotEquals: "Select"
        },
        comboceaserequired    : {
            required :true,
            valueNotCeaseEquals: "Select"
        },
        customcheck : {
            minlength: 10,
            maxlength: 15,
            checkphone: true
        },
        checklanguage : {
            checklanguage: true
        },
        namechars : {
            namechars: true
        }
    });
    }else if('a2p sms service' == form){
       $.validator.addClassRules({
        phone: {
            number:true,
            minlength: 7,
            maxlength: 16
        },
        brentwoodphone: {
            number:true,
            minlength: 9,
            maxlength: 13
        },
        brentwoodTanzania: {
            number:true,
            minlength: 9,
            maxlength: 12
        },
        brentwoodUAE: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        brentwoodNigeria: {
            number:true,
            minlength: 10,
            maxlength: 13
        },
        brentwoodPhilippines: {
            number:true,
            minlength: 9,
            maxlength: 11
        },
        brentwoodOman: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        icfaiphone: {
            number:true,
            minlength: 10,
            maxlength: 10
        },
        radiorequired : {
            required :true
        },
        checkrequired : {
            required :true
        },
        comborequired    : {
            required :true,
            valueNotEquals: "Select"
        },
        comboceaserequired    : {
            required :true,
            valueNotCeaseEquals: "Select"
        },
        customcheck : {
            minlength: 10,
            maxlength: 15,
            checkphone: true
        },
	customphonecheck : {
            minlength: 10,
            maxlength: 10,
            checkcustomphone: true
        },
        checklanguage : {
            checklanguage: true
        },
        namechars : {
            namechars: true
        },
	phonewithcode: {
	    phonewithcodecheck:true,
            minlength: 7,
            maxlength: 16
        },
	phonelengthc: {
	    phonelengthcheck:true,
            minlength: 10,
            maxlength: 13
        }
    });
    
    }else{
    $.validator.addClassRules({
        phone: {
            number:true,
            minlength: 10,
            maxlength: 15
        },
        brentwoodphone: {
            number:true,
            minlength: 9,
            maxlength: 13
        },
        brentwoodTanzania: {
            number:true,
            minlength: 9,
            maxlength: 12
        },
        brentwoodUAE: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        brentwoodNigeria: {
            number:true,
            minlength: 10,
            maxlength: 13
        },
        brentwoodPhilippines: {
            number:true,
            minlength: 9,
            maxlength: 11
        },
        brentwoodOman: {
            number:true,
            minlength: 8,
            maxlength: 11
        },
        icfaiphone: {
            number:true,
            minlength: 10,
            maxlength: 10
        },
        radiorequired : {
            required :true
        },
        checkrequired : {
            required :true
        },
        comborequired    : {
            required :true,
            valueNotEquals: "Select"
        },
        comboceaserequired    : {
            required :true,
            valueNotCeaseEquals: "Select"
        },
        customcheck : {
            minlength: 10,
            maxlength: 15,
            checkphone: true
        },
	customphonecheck : {
            minlength: 10,
            maxlength: 10,
            checkcustomphone: true
        },
        checklanguage : {
            checklanguage: true
        },
        namechars : {
            namechars: true
        },
	phonewithcode: {
	    phonewithcodecheck:true,
            minlength: 7,
            maxlength: 17
        },
	phonelengthc: {
	    phonelengthcheck:true,
            minlength: 10,
            maxlength: 13
        }
	
    });
    }

    var submitted = false;
    $(".common").validate({
        highlight: function(element, errorClass) {
            if(element.type=="radio") {
                $(element).parent().css({
                    border:"1px solid Red"
                });

            } else if(element.type=="checkbox") {
                $(element).parent().css({
                    border:"1px red solid"
                });
            } else {
                $(element).css("border", '1px solid Red');

            }
            return false;
        },
        unhighlight: function(element, errorClass) {
            if(element.type=="radio") {
                $(element).parent().css({
                    border:"none"
                });

            } else if(element.type=="checkbox") {
                $(element).parent().css({
                    border:"none"
                });
            } else {
                $(element).css("border", '1px solid #DBD5DB');

            }
            return false;
        },
        errorPlacement: function(error,element) {
            return true;
        },
        errorClass: "error-text",
        validClass: "valid",
        errorLabelContainer: "#errorList",
        wrapper: "li class='indent error-text'",
        showErrors: function(errorMap, errorList) {
            if (submitted) {
                var i = 0;
                var labelText = new Array(this.numberOfInvalids());
                $.each(errorMap, function(name, value) {
                    labelText[i] = ucfirst(name);
                    i++;
                });
                var summary = "You have the following errors: \n";
                i = 0;
                $.each(errorList, function() {
                    this.message = labelText[i] + " " + this.message + "\n";
                    ;
                    summary += this.message;
                    i++;
                });
                alert(summary);
                submitted = false;
            }
            this.defaultShowErrors();
        },
        invalidHandler: function(form, validator) {
            submitted = true;
        },
        submitHandler: function(form) {
            if(form.captcha.value!='0') {
                var why = "";
                if(form.txtInput.value == ""){
                    why += "Security code should not be empty.\n";
                }
                if(form.txtInput.value != ""){
                    if(ValidCaptcha(form.txtInput.value) == false){
                        why += "Security code did not match.\n";
                    }
                }
                if(why != ""){
                    alert(why);
                    return false;
                }
            }
            $('input[type=submit]').val('submitting...');
            $('input[type=submit]').attr('disabled', 'disabled');
            /* Code Start for 3rd party submit*/
            var formname = $( "form" ).attr( "name");
            if(formname == 'TUI Landing Page')
            {
                var cname = $("#cname").val();
                var cphone = $("#cphone").val();
                var cemail = $("#cemail").val();
                var clocation = $("#clocation").val();
                var urlt = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8';
                $.ajax({
                    url: urlt,
                    type: 'GET',
                    async : false,
                    data : "last_name="+cname+"&mobile="+cphone+"&email="+cemail+"&city="+clocation+"&oid=00D90000000jBzN&company=TUI+India&lead_source=Website&00N90000008752b=Desktop%2FLaptop&00N90000004XzF0=Online+SEM&00N90000004Y1w7=1&00N90000004YCHF=1&00N90000004Y4bN=West+India&00N90000004Y4gs=Goa&00N90000004Y4at=Goa-DOM&00N90000004Y4hq=Domestic+Holidays",
                    crossDomain: true,
                    dataType: 'jsonp',
                    async : false,
                    success: function() {}
                });
            }
            /* Code Ends for 3rd party submit*/
            form.submit();
        }

    });

    //http://jsonip.appspot.com?callback=?
    //data.ip
    //http://smart-ip.net/geoip-json?callback=?
    //data.host
    $.getJSON( "http://jsonip.com/?callback=?",
        function(data){
            $("#ip_address").val(data.ip);
            var ip = $('#ip_address').val();
            var webformid = $('#webform_id').val();
            if(webformid == '1715' || webformid == 1715) {
                $.ajax({
                    type: "POST",
                    dataType: "jsonp",
                    url: "http://agl.adv8.co/Form/add-Ip-Detail",
                    data: "ip=" + ip + "&webformid=" + webformid,
                    success: function () {}
                });
            }
        }
        );

    if(document.getElementById("captcha").value!='0') {
        //Generates the captcha function
        var a = Math.ceil(Math.random() * 9)+ '';
        var b = Math.ceil(Math.random() * 9)+ '';
        var c = Math.ceil(Math.random() * 9)+ '';
        var d = Math.ceil(Math.random() * 9)+ '';
        var e = Math.ceil(Math.random() * 9)+ '';

        var code = a + b + c + d + e;
        document.getElementById("txtCaptcha").value = code;
        document.getElementById("txtCaptchaDiv").innerHTML = code;
    }

    var gclid   = GetURLParameter('gclid');
    var fgclid  = (jQuery.type(gclid) === "undefined" || gclid == '') ? '' : gclid;
    document.getElementById("fb_user_id").value = fgclid;
});
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
$(function() {
    $( "#traveldatejetage" ).datepicker({
      showOn: "button",
      buttonImage: "http://agl.adv8.co/common/images/calender.gif",
      buttonImageOnly: true,
      dateFormat: 'dd-mm-yy'
    });
});
function fromdate(from){
    $("#"+from).trigger('focus');
    if(from == 'bookingchitra') {
        $("#"+from).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: 0,
            onClose: function( selectedDate ) {
                $( "#bookingchitra_" ).datepicker( "option", "minDate", selectedDate );
            }
        });
    } else if(from == 'dateofarrival') {
        $("#"+from).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: 0,
            onClose: function( selectedDate ) {
                $( "#dateofarrival_" ).datepicker( "option", "minDate", selectedDate );
            }
        });
    } else {
        $("#"+from).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: 0
        });
    }
}
function fromdates(from){
    $("#"+from).trigger('focus');
    if(from=='dateoftravel' || from=='traveldate') {
        $.daterangepicker({
            dateFormat: 'dd-mm-yy',
            fromSelector: "#"+from,
            minDate :0

        });
    } else if(from=='dob') {
        $.daterangepicker({
            dateFormat: 'dd-mm-yy',
            fromSelector: "#"+from,
            maxDate :0
        });
    } else if(from=='appointmentdate') {
        $.daterangepicker({
            dateFormat: 'dd-mm-yy',
            fromSelector: "#"+from,
            minDate : '+1D'
        });
    } else {
        $.daterangepicker({
            dateFormat: 'dd-mm-yy',
            fromSelector: "#"+from,
            minDate :0
        });

    }
}

function todate(from) {
    var froms = from.split("_");
    var val = $("#"+froms[0]).val();
    //val = val.split('-');
    //var day1 = val[0];
    //var month1 = val[1];
    //var year1 = val[2];
    //var dtFormatted = day1 + '-'+ month1 + '-'+ year1;
    $("#"+from).trigger('focus');
    if(from == 'bookingchitra_') {
        $("#"+from).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate : val,
            onClose: function( selectedDate ) {
                $( "#bookingchitra" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    } else if(from == 'dateofarrival_') {
        $("#"+from).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate : val,
            onClose: function( selectedDate ) {
                $( "#dateofarrival" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    } else {
        $("#"+from).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate : val
        });
    }	
}

// Validate the Entered input aganist the generated security code function
function ValidCaptcha(){
    var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
    var str2 = removeSpaces(document.getElementById('txtInput').value);
    if (str1 == str2){
        return true;
    }else{
        return false;
    }
}

// Remove the spaces from the entered and generated code
function removeSpaces(string){
    return string.split(' ').join('');

}

$(document).ready(function(){
	$('#ccity').keypress(function(event) {
		var inputValue = event.charCode;
		console.log('Hleooi');
		if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
			event.preventDefault();
		}
	});
	
});
