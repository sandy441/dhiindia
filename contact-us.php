<!doctype html>
<html lang="en">
    
    <body>
<?php include 'header.php';
$home_url = $userinfo->getBaseUrl();
$result =  $userinfo->getcity();
?>
        
        <section class="bg-col-1">
            <div class="container">
                <div class="content">
                    <div class="card contactIcon">
                        <div class="card-body maginExtra"> 
                            <div class="row ">
                                
                                <div class="col-sm-12 text-center"><h2>Get in touch with us</h2></div>
                                
                                <div class="col-sm-4 text-center">
                                    <img src="image/icons/contact-us.png" alt="" width="60" class="pb-3">
                                    <p>(Toll Free)</p>
                                    <p>1800 103 9300</p>
                                </div>
                                <div class="col-sm-4 text-center">
                                    <img src="image/icons/address.png" alt="" width="60" class="pb-3">
                                    <p>(Toll Free)</p>
                                    <p>1800 103 9300</p>
                                </div>
                                <div class="col-sm-4 text-center">
                                    <img src="image/icons/email.png" alt="" width="60" class="pb-3">
                                    <p>(Toll Free)</p>
                                    <p>1800 103 9300</p>
                                </div>
                                
                                
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="content">
                    <div class="card bt-border">
                        <div class="card-body maginExtra"> 
                            <h3 class="text-center">if you got any questions <br> please do not hesitate to send us a message</h3>
                            <h4 class="text-center">Personal Information </h4>
                            <p class="text-center">Field mark with * are required</p>
                            <div class="row ">
                                
                                <div class="mx-auto maxw600" >
                                    <form name="contactform" id="contactform"method="post" action="<?php echo $home_url;?>/formrequest.php">
                                        <input type="hidden" name="formtype" value="contactus">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" id="" placeholder="NAME*">
                                        </div>
                                        <div class="form-group">
                                            
                                            <input type="text" name="email" class="form-control" id="" placeholder="EMAIL*">
                                        </div>
                                        <div class="form-group">
                                            
                                            <input type="text" name="phone" class="form-control" id="" placeholder="CONTACT NUMBER*">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect2">SELECT YOUR CITY</label>
                                            <select  class="form-control" id="" name="city[]">
                                                    <?php foreach ($result as $val):?>
                                                        <option value="<?php echo $val['id'];?>"><?php echo $val['city'];?></option> 
                                                    <?php endforeach;?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            
                                            <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="MESSAGE*"></textarea>
                                        </div>
                                        <div class="form-group text-center">
                                            <input type="submit" name="submit"value="Submit" class="btn btn-outline-dark btn-pdding "> 
                                            <!-- <button type="submit" class="btn btn-outline-dark btn-pdding ">Submit</button> -->
                                        </div>
                                    </form>
                                    
                                    
                                    
                                    
                                </div> 
                                
                                
                                
                                
                            </div>
                            
                            
                        </div>
                    </div>
                    
                </div>
                
               
            </div>
            
            
        </section>
        
        
        <div class="clearfix"></div>
        
        
        
        
        <!-- FOOTER -->
        
<?php include 'footer.php';?>
        
        
        
        
        
    </body>
</html>
