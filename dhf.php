
<!doctype html>
<html lang="en">

  <body>
<?php include 'header.php';?>

<section>
  <a href="book-appoint.php"><img src="image/Banner_prp3.png" alt="" class="img-fluid"></a>
</section>

<section class=" bg-col-1">
  <div class="container">
    
    
    <div class="content">
    <div class="card">
  
  <div class="card-body maginExtra">
    <div class="row ">
        <div class="col-md-12"><img src="image/1.png" alt="" class="img-fluid"/></div>  
   <h2>Direct Hair Fusion (hair replacement)</h2>
           
           
           <p>DHI Group is a European company, which has been at the forefront of hair restoration innovation for the last 47 years, having treated more than 200,000 customers satisfactorily. With around more than 65 clinics worldwide. This write up explains a bit about Direct Hair Fusion (DHF), the most advanced non clinical hair replacement procedure that is best suited for people with high grade alopecia. </p>

<p>The ‘hair replacement system’ is a fine, artificial, skin friendly membrane with breathing pores on which 100% human virgin hair are woven, to match the density, texture, length and color of existing hair keeping in mind age appropriate density desired.  The procedure starts with a measurement of the bald area, and preparation of a mold of the bald area.  We also take a small sample of the client’s own hair, to ensure the system made for you exactly matches the scalp shape and existing hair, to give you 100% natural look. </p>


      
   
  </div>
</div>
           
  
         
        
         
            
        
            </div>
  </div>

  <div class="content">
    <div class="card bt-border">
  
  <div class="card-body maginExtra">
    <div class="row ">
        <div class="col-md-12"><img src="image/dhf-banner.jpg" alt="" class="img-fluid"/></div>  

        <div class="col-sm-12">
            <h2 class="pb-0 pt-4">custom design</h2>
<p class="pb-0 pt-0">For each client, a customized ‘hair replacement system’ is made in Europe based on the order form filled by us with above full details.  Arrival of the system takes around 8-12 weeks. Once it arrives, the system is fixed on the scalp of the client, using special skin friendly materials.  The entire procedure is performed by highly trained and experienced hair stylists, in our world class facilities and takes a little more than an hour only.  </p>
 <h2 class="pb-0 pt-0"">refusion - placement</h2>     
  <p class="pb-0 pt-0">Once fixed, the client may experience a little bit of unease for first 3-4 days, but once he gets used to it, the joy of having totally natural looking permanent hair and a new desirable look is immeasurable.  This procedure is equally beneficial for both men and women.  Once fitted, you enjoy full freedom to wash, dry, comb and style your new hair, as you want.  </p>
  <h2 class="pb-0 pt-0"">post care</h2>     
  <p class="pb-0 pt-0">Once fitted, you need to do a maintenance of the system, either in our facility or on your own, every 20-30 days.  You would need a replacement of your skin in 8-12 months.” </p>
        </div>
       
         
      
  
  </div>
</div>
           
        
        
            </div>
  </div>

              

  </div>
</section>
     
<div class="clearfix"></div>

<section class="bottm_sec">
 <h1>Increase your hair density naturally</h1>
   <a href="book-appoint.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>

      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


  
  </body>
</html>
