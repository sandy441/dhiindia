
<!doctype html>
<html lang="en">
 <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="DHIIndia Hair Transplant Results, Before and After Photos of men and women who undergone Hair Transplant by DHI doctors.">
    <meta name="author" content="">
      <title>dhi- eyebrows-hair-transplant-results-eyebrows</title>
<?php include 'header.php';?>
    </head>
  <body>


    <section class=" bg-col-1">
       <div class="container">
<div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 


<div class="row">
 
   <div class="col-sm-12">
   <h3 class="text-center">Eyebrows</h3>  
  </div> 
   <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/eyebrows/1.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#1)</h5>
   
  </div>
            </div>
             <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
            <img class="card-img-top" src="image/Result_Images/eyebrows/2.jpg" alt="eyebrow restoration before and after image 1
">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#2)</h5>
   
  </div>
            </div>

 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
            <img class="card-img-top" src="image/Result_Images/eyebrows/3.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#3)</h5>
   
  </div>
            </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
            <img class="card-img-top" src="image/Result_Images/eyebrows/4.jpg" alt="eyebrow restoration with micro blading services
">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#4)</h5>
   
  </div>
            </div>

<div class=" col-md-6 col-sm-6 col-xs-6 p-3">
            <img class="card-img-top" src="image/Result_Images/eyebrows/5.jpg" alt="eyebrow restoration with micro blading services
">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#5)</h5>
   
  </div>
            </div>
            <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
            <img class="card-img-top" src="image/Result_Images/eyebrows/6.jpg" alt="eyebrow restoration with micro blading services
">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#6)</h5>
   
  </div>
            </div>
            <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
            <img class="card-img-top" src="image/Result_Images/eyebrows/7.jpg" alt="eyebrow restoration with micro blading services
">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#7)</h5>
   
  </div>
            </div>
            <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
            <img class="card-img-top" src="image/Result_Images/eyebrows/8.jpg" alt="eyebrow restoration with micro blading services
">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#8)</h5>
   
  </div>
            </div>



            <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
            <img class="card-img-top" src="image/Result_Images/eyebrows/9.jpg" alt="eyebrow restoration with micro blading services
">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#9)</h5>
   
  </div>
            </div>
            <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
            <img class="card-img-top" src="image/Result_Images/eyebrows/10.jpg" alt="eyebrow restoration with micro blading services
">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#10)</h5>
   
  </div>
            </div>
            <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
            <img class="card-img-top" src="image/Result_Images/eyebrows/11.jpg" alt="eyebrow restoration with micro blading services
">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#11)</h5>
   
  </div>
            </div>

</div>
          
             </div>
           </div>
</div>
    


    </div>
</section>

   
<div class="clearfix"></div>

     
<section class="bottm_sec">
  <h1>Ready To Regain Your Hair & Confidence</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>


      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


  
  </body>
</html>
