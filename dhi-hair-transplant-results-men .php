
<!doctype html>
<html lang="en">
 <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="DHIIndia Hair Transplant Results, Before and After Photos of men and women who undergone Hair Transplant by DHI doctors.">
    <meta name="author" content="">
      <title>DHI Before and After Hair Transplant Results for men</title>
<?php include 'header.php';?>
    </head>
  <body>


    <section class=" bg-col-1">
       <div class="container">
<div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 


<div class="row">
  <div class="col-sm-12">
   <h3 class="text-center">Men</h3>  
  </div>  
   
            <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
    <img class="card-img-top" src="image/Result_Images/men/1.jpg" alt="virendra sehwag before and after images">
  <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Virender Sehwag (#1)</h5>
   <p class="pt-0"> Norwood 6</p>
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
          <img class="card-img-top" src="image/Result_Images/men/2.jpg" alt="dhi gives natural hairline">
                <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Prabhjot Singh (#2)</h5>
   <p class="pt-0"> Norwood 3</p>
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
         <img class="card-img-top" src="image/Result_Images/men/3.jpg" alt="Card image cap">
              <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Sreejesh Ravindran (#3)</h5>
   <p class="pt-0"> Norwood 4</p>
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/5.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3"> Pankaj Mirchandani (#4)</h5>
   <p class="pt-0"> Norwood 3 </p>
  </div>

  </div>

 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/4.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> S.V. Sunil (#5)</h5>
   
  </div>

  </div>

 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/6.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> Nishank Syal (#6)</h5>
   
  </div>

  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/7.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> Dr Sachin Dhawan (#7)</h5>
   
  </div>

  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/8.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#8)</h5>
   
  </div>

  </div>

  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/9.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#9)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/10.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#10)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/11.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#11)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/12.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#12)</h5>
   
  </div>

  </div>



          <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
    <img class="card-img-top" src="image/Result_Images/men/13.jpg" alt="virendra sehwag before and after images">
  <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Result  (#13)</h5>
  
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
          <img class="card-img-top" src="image/Result_Images/men/14.jpg" alt="dhi gives natural hairline">
                <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Result (#14)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
         <img class="card-img-top" src="image/Result_Images/men/15.jpg" alt="Card image cap">
              <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Result (#15)</h5>
  
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/16.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result  (#16)</h5>
   
  </div>

  </div>

 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/17.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result  (#17)</h5>
   
  </div>

  </div>

 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/18.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#18)</h5>
   
  </div>

  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/19.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result  (#19)</h5>
   
  </div>

  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/20.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result  (#20)</h5>
   
  </div>

  </div>

  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/21.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#21)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/22.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#22)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/24.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#23)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/25.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#24)</h5>
   
  </div>

  </div>





          <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
    <img class="card-img-top" src="image/Result_Images/men/26.jpg" alt="virendra sehwag before and after images">
  <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Result  (#25)</h5>
  
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
          <img class="card-img-top" src="image/Result_Images/men/27.jpg" alt="dhi gives natural hairline">
                <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Result (#26)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
         <img class="card-img-top" src="image/Result_Images/men/28.jpg" alt="Card image cap">
              <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Result (#27)</h5>
  
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/29.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#28)</h5>
   
  </div>

  </div>

 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/30.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#29)</h5>
   
  </div>

  </div>

 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/31.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#30)</h5>
   
  </div>

  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/32.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result  (#31)</h5>
   
  </div>

  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/33.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result  (#32)</h5>
   
  </div>

  </div>

  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/34.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#33)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/35.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#34)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/36.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#35)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/37.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#36)</h5>
   
  </div>

  </div>


<div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/38.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result  (#37)</h5>
   
  </div>

  </div>

  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/39.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#38)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/40.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#39)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/41.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#40)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/42.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#41)</h5>
   
  </div>

  </div>
<div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/43.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#42)</h5>
   
  </div>

  </div>
<div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/44.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#43)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/45.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#44)</h5>
   
  </div>

  </div>

 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/46.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#45)</h5>
   
  </div>

  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/47.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#46)</h5>
   
  </div>

  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/48.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#47)</h5>
   
  </div>

  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/49.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#48)</h5>
   
  </div>

  </div>

   <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/50.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#49)</h5>
   
  </div>

  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/51.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#50)</h5>
   
  </div>

  </div>
   <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/52.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#51)</h5>
   
  </div>

  </div>
   <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/53.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#52)</h5>
   
  </div>

  </div>
   <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/54.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#53)</h5>
   
  </div>

  </div>
   <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/55.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#54)</h5>
   
  </div>

  </div>
    <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/56.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#55)</h5>
   
  </div>

  </div>

  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/57.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#56)</h5>
   
  </div>

  </div>

    <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/58.jpg" alt="Card image cap">
    <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#57)</h5>
   
  </div>

  </div>
    <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/59.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#58)</h5>
   
  </div>

  </div>
    <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/60.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#59)</h5>
   
  </div>

  </div>
    <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/61.jpg" alt="Card image cap">
    <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#60)</h5>
   
  </div>

  </div>
<div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/62.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#61)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/63.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#62)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/64.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#63)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/65.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#64)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/66.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#65)</h5>
   
  </div>

  </div>

<div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/67.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#66)</h5>
   
  </div>

  </div>
<div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/68.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#67)</h5>
   
  </div>

  </div>
<div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/69.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#68)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/70.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#69)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/71.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#70)</h5>
   
  </div>

  </div>

<div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/72.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#71)</h5>
   
  </div>

  </div>

  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/73.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#72)</h5>
   
  </div>

  </div>

  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/74.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#73)</h5>
   
  </div>

  </div>
 
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/75.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#74)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/76.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#75)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/77.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#76)</h5>
   
  </div>

  </div>

  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/78.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#77)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/79.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#78)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/80.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#79)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/81.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#80)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/82.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#81)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/83.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#82)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/84.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#83)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/85.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#84)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/86.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#85)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/87.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#86)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/88.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#87)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/89.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#88)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/90.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#89)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/91.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#90)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
  <img class="card-img-top" src="image/Result_Images/men/92.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#91)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
<img class="card-img-top" src="image/Result_Images/men/93.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#92)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
<img class="card-img-top" src="image/Result_Images/men/94.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#93)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
<img class="card-img-top" src="image/Result_Images/men/95.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#94)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
<img class="card-img-top" src="image/Result_Images/men/96.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#95)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
<img class="card-img-top" src="image/Result_Images/men/97.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#96)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
<img class="card-img-top" src="image/Result_Images/men/98.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#97)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
<img class="card-img-top" src="image/Result_Images/men/99.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#98)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
<img class="card-img-top" src="image/Result_Images/men/100.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#99)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
<img class="card-img-top" src="image/Result_Images/men/101.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#100)</h5>
   
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
<img class="card-img-top" src="image/Result_Images/men/102.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#101)</h5>
   
  </div>

  </div>
<div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
<img class="card-img-top" src="image/Result_Images/men/103.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#102)</h5>
   
  </div>

  </div>
<div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
<img class="card-img-top" src="image/Result_Images/men/104.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3"> DHI Result (#103)</h5>
   
  </div>

  </div>

</div>
          
             </div>
           </div>
</div>
    


    </div>
</section>

   
<div class="clearfix"></div>

     
<section class="bottm_sec">
  <h1>Ready To Regain Your Hair & Confidence</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>


      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


  
  </body>
</html>
