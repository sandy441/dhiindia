
<!doctype html>
<html lang="en">
 <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="DHIIndia Hair Transplant Results, Before and After Photos of men and women who undergone Hair Transplant by DHI doctors.">
    <meta name="author" content="">
      <title>DHI Before and After Hair Transplant Results for Women</title>
<?php include 'header.php';?>
    </head>
  <body>


    <section class=" bg-col-1">
       <div class="container">
<div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 


<div class="row">
 
   <div class="col-sm-12">
   <h3 class="text-center">Women</h3>  
  </div> 
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
                       <img class="card-img-top" src="image/Result_Images/women/1.jpg" alt="Card image cap">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Female Result #1</h5>
   
  </div>

  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/women/2.jpg" alt="Card image cap">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Female Result #2</h5>
   
  </div>
            </div>


             <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/women/3.jpg" alt="dhi before and after image 2 
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Female Result #3</h5>
   
  </div>
            </div>

 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/women/4.jpg" alt="dhi before and after image 3 
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Female Result #4</h5>
   
  </div>
            </div>
 
<div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/women/5.jpg" alt="dhi before and after image 3 
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Female Result #5</h5>
   
  </div>
            </div>

<div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/women/6.jpg" alt="dhi before and after image 3 
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Female Result #6</h5>
   
  </div>
            </div>
</div>
          
             </div>
           </div>
</div>
    


    </div>
</section>

   
<div class="clearfix"></div>

     
<section class="bottm_sec">
  <h1>Ready To Regain Your Hair & Confidence</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>


      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


  
  </body>
</html>
