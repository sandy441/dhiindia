
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Direct Hair Implantation method technique & procedure introduced by DHI, It is an innovative technique to give you healthy and naturally looking hair. 100% Natural Results">
    <meta name="author" content="">
      <title>Advanced Direct Hair Implantation Technique - DHI India

</title>
<?php include 'header.php';?>
    </head>
  <body>

<section class="bg-col-1">
   <div class="clearfix">
           
            	<video  preload="auto" width="100%" loop autoplay>
                        <source type="video/webm" src="video/direct-hair-implantation-delhi-clinic.mp4">
                        <source type="video/mp4" src="video/direct-hair-implantation-delhi-clinic.mp4">
                        <source type="video/webm" src="video/direct-hair-implantation-delhi-clinic.ogg">
                        <source type="video/ogg" src="video/direct-hair-implantation-delhi-clinic.ogg">
                    </video>
           
        </div>
        
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
    <div class="card mar-minus ">
  
  <div class="card-body maginExtra">
    <div class="row ">
     
  <h2>Direct Hair Implantation (DHI)<span> 100% Safe, Natural Results, Permanent Solution!</span></h2>
           
           
           <p>At DHI, we have been treating hair loss since 1970. After having pioneered FUE in 2003, we innovated & launched the Direct Hair Implantation (DHI) technique <sup>TM</sup> in 2005. At DHI International the procedure from start to finish is performed only by MD Dermatologist doctors. All our doctors have been trained and certified by the London Hair Restoration Academy – the one & only training academy in hair restoration. It ensures 100% safety, natural result, maximum viability and lifetime growth.</p>
       <div class="col-md-12"><img src="image/Doctor.png" alt="" class="img-fluid"/></div>    
   
  </div>
</div>
           

            </div>
  </div>
         

        	
          <div class="content">
            <div class="card ">
            <div class="card-body maginExtra"> 
            <div id="demo" class="carousel slide" >

  <!-- Indicators -->
  <!-- <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul> -->

  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row">
        <div class=" col-sm-4 col-xs-12 no-gutters">
          <div class="steps">
             <h4 class="text-center">3 Steps Procedure</h4>
          <div class="clearfix">
            <div class="circle ">
            <p>1</p>
          </div>
          </div>
          <div class="data">
            <h6 class="text-center text-uppercase">Extraction phase </h6>
                                        <p>Hair follicles are extracted one by one from the donor area using a specifically designed disposable tool with the diameter of 1mm or less. The follicles are then kept at a specific temperature and in a solution that enhances their development after placement, without separating, cutting or generally handling the grafts</p>
          </div>
          </div>
         
          
        </div>
     <div class="col-sm-8 col-xs-12 sec-video no-gutters margin-minus-tp">
        <video  preload="auto" width="100%"  loop autoplay>
                        <source type="video/webm" src="image/Hair-Extraction.mp4">
                        <source type="video/mp4" src="image/Hair-Extraction.mp4">
                        <source type="video/webm" src="image/Hair-Extraction.ogg">
                        <source type="video/ogg" src="image/Hair-Extraction.ogg">
                    </video>
     </div>
      </div>
     
    </div>
    <div class="carousel-item">
       <div class="row">
        <div class=" col-sm-4 col-xs-12 steps no-gutters">
          <h4 class="text-center">3 Steps Procedure</h4>
          <div class="clearfix">
            <div class="circle ">
            <p>2</p>
          </div>
          </div>
          <div class="data">
            <h6 class="text-center text-uppercase">Placement phase  </h6>
                                        <p>The hair follicles are implanted directly into the region suffering from
                    hair loss using a patented tool, the DHI Implanter, also with a diameter of 1mm or 
                    less, without prior creation of holes or slits.</p>
          </div>
          
        </div>
     <div class="col-sm-8 col-xs-12 sec-video no-gutters margin-minus-tp">
        <video  preload="auto" width="100%"  loop autoplay>
                        <source type="video/webm" src="image/Hair-Implantation.mp4">
                        <source type="video/mp4" src="image/Hair-Implantation.mp4">
                        <source type="video/webm" src="image/Hair-Implantation.ogg">
                        <source type="video/ogg" src="image/Hair-Implantation.ogg">
                    </video>
     </div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="row">
        <div class=" col-sm-4 col-xs-12 steps no-gutters">
          <h4 class="text-center">3 Steps Procedure</h4>
          <div class="clearfix">
            <div class="circle ">
            <p>3</p>
          </div>
          </div>
          <div class="data">
            <h6 class="text-center text-uppercase">Natural result</h6>
                                        <p>Full control of the depth, direction and angle of the placement of each graft,
                    ensures 100% natural results, maximum viability and that the implanted hair will never fall 
                    out and will grow for a lifetime.</p>
          </div>
          
        </div>
     <div class="col-sm-8 col-xs-12 sec-video   no-gutters margin-minus-tp2">
        <video  preload="auto" width="100%"  loop autoplay>
                        <source type="video/webm" src="image/Natural-results.mp4">
                        <source type="video/mp4" src="image/Natural-results.mp4">
                        <source type="video/webm" src="image/Natural-results.ogg">
                        <source type="video/ogg" src="image/Natural-resultsogg">
                    </video>
     </div>
      </div>
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>

</div>
</div>
            </div>
          </div>  

          


<div class="content">
  <div class="card ">
            <div class="card-body maginExtra"> 
              <div class="bg-img">
             <h2 class="text-center">DHI Direct<sup>TM</sup> is better than FUE & FUT (Strip) techniques</h2>
             <div class="row">
               <div class="col-sm-5">
               <h1 class="text-center"><a href="direct-or-fue.php" class="btn btn-outline-white">Learn Why</a></h1>
             </div>
             <div class="col-sm-7">
               <img src="image/Instrument0d0.png" alt="" class="img-fluid"  />
             </div>
             </div>
             
           </div>
            </div>
          </div>
           
            </div>

          

           


<div class="content">
           
     <div class="card">
            <div class="card-body"> 
               <div class="row">
               <div class="col-sm-6 science">
                 <h4 >The Science of Hair</h4>
                  <p>Today, DHI is considered the Gold Standard in Hair Transplant & Restoration, with the strongest research advisory board, world class facilities and US and EU patents.</p>
                  <h2 class="text-center"><a href="about.php" class="btn btn-outline-dark btn-pdding ">Learn more about DHI</a></h2>
               </div>
               <div class="col-sm-6">
                 <img src="image/science-img.jpg" class="img-fluid" alt="">
               </div>
             </div>


          
             
     
            </div>
            </div>        
            


      
            </div>


              
          </div>




                




</section>
   <section class="bg-col-1">
<div class="container key_features">
 
<div class="content">

    <div class="card bt-border">
  <h1 class="text-center pt-5 pb-0">Key Features</h1>
  <div class="card-body ">
  <div class="row ">
    <div class="col-sm-6 pl-5 pr-4 pt-5 pb-5">
  
    <img src="image/key/safety.jpg" alt="" class="img-fluid">
           <div class=" bg-col-w pb-4">
           <h4 class="card-title">Safety</h4>
    <p class="card-text">Strict protocols are applied to all procedures and at all levels to guarantee 100% safety.</p>
           </div>
    
        
    </div>
    <div class="col-sm-6 pl-4 pr-5 pt-5 pb-5 ">
      
    <img src="image/key/Grafts-Viability.jpg" alt="" class="img-fluid">
      <div class=" bg-col-w pb-4">
             <h4 class="card-title">Grafts Viability</h4>
    <p class="card-text">Grafts viability rate is above 90%, while the industry average is about 50% </p>
      </div>
             
          
    
         
     
        
    </div>
    
  </div>
   <div class="row  ">
     <div class="col-sm-6 pl-5 pr-4  pb-5">
          <img src="image/key/pain-free.jpg" alt="" class="img-fluid">
           <div class=" bg-col-w ">
              <h4 class="card-title">Painfree</h4>
    <p class="card-text">Entire procedure is performed under local anaesthesia making the procedure completely painless</p>
           </div>
             
               
    
         
       
    </div>
     <div class="col-sm-6 pl-4 pr-5  pb-5 ">
      
    <img src="image/key/Hide-scar.jpg" alt="" class="img-fluid">
          
          <div class=" bg-col-w">
                 <h4 class="card-title">No Scars</h4>
    <p class="card-text">Only healthy hair follicles which are genetically designed to grow for lifetime are chosen and implanted.</p>
    
          </div>
            
         
         
       
    </div>
    <div class="col-sm-6 pl-5 pr-4 pb-5">
          <img src="image/key/natural-results.jpg" alt="" class="img-fluid">
              <div class=" bg-col-w pb-4">
     <h4 class="card-title">Natural Results</h4>
      <p class="card-text">Full control of the depth, the direction and the angle of placement ensures 100% natural results. </p>
</div>
             
               
    
         
       
    </div>
    <div class="col-sm-6 pl-4 pr-5 pb-5">
      
     <img src="image/key/Doctors-only.jpg" alt="" class="img-fluid">
          
          <div class=" bg-col-w ">
            <h4 class="card-title">Doctors Only</h4>
     <p class="card-text ">The procedure from start to finish is performed by MD Dermatologists, trained and certified by the London Hair Restoration Academy.</p>
    
          </div>
            
         
         
       
    </div>
    
   </div>
  </div>
</div>
            
  
 
                  </div>


</section>  
<div class="clearfix"></div>

<!--  <?php include 'slick-slider.php';?>  -->   

<section class="bottm_sec">
 <h1>Ready to regain your hair & confidence</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>

      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  




  </body>
</html>
