
<!doctype html>
<html lang="en">
  
  <body>
<?php include 'header.php';?>



<section class=" bg-col-1 eshop pb-5">
  <div class="container">
    <h1 class="text-center pt-4 pb-4 mb-0">LOTIONS AND SHAMPOOS AGAINST HAIR LOSS</h1>
    <div class="row  bg-col-w align-items-center">

     <div class="col-sm-6 no-gutters ">
       <img src="image/lotion-gainst.jpg" alt="" class="img-fluid no-gutters">
     </div>
     <div class="col-sm-6 no-gutter">
     <div class="data">
                   <h3>Hair Loss Lotion</h3>
                   <h4><span class="text-red">Rs. 2,499</span> <span> <del>Rs. 2,999</del></span></h4>
                  <ul>
                    <li>Hair Loss Reduction</li>
                    <li>Stimulation of Hair Growth</li>
                    <li>Tonic of Blood Microcirculation</li>
                    <li>Nourishment & Protection</li>
                  </ul>
                  <button type="button" class="btn btn-info mt-4">Buy Now</button>
                 </div>  
     </div>
      
    </div>
    <div class="row pt-5 pb-5">
      
      <div class="col-sm-6  ">
        <div class="card" >
  <img class="card-img-top" src="image/sampoo-against-hair-loss.jpg" alt="Card image cap">
  <div class="card-body">
   <div class="data">
                   <h3>Dry Hair Shampoo</h3>
                   <h4><span class="text-red">Rs. 1,499</span> </h4>
                  <ul>
                    <li>Moisturization & Elasticity</li>
                    <li>Mild Cleansing Effect</li>
                    <li>Anti Hair Loss Treatment</li>
                    
                  </ul>
                  <button type="button" class="btn btn-info mt-4">Buy Now</button>
                 </div>
    
  </div>
</div>
      </div>
       <div class="col-sm-6 ">
        <div class="card" >
  <img class="card-img-top" src="image/sampoo-extra-mild.jpg" alt="Card image cap">
  <div class="card-body">
   <div class="data">
                   <h3>Extra Mild Shampoo</h3>
                   <h4><span class="text-red">Rs. 1,499</span> </h4>
                  <ul>
                <li>Mild Cleansing Effect</li>
                <li>Anti Irritant &amp; Soothing Action</li>
                <li>Protection &amp; Moisturization</li>
              </ul>
                  <button type="button" class="btn btn-info mt-4">Buy Now</button>
                 </div>
    
  </div>
</div>
      </div>
       
    </div>
    <div class="row  bg-col-w align-items-center">

     <div class="col-sm-6 no-gutters ">
       <img src="image/soothing-lotion.jpg" alt="" class="img-fluid no-gutters">
     </div>
     <div class="col-sm-6 no-gutter">
     <div class="data">
                   <h3>Hair Loss Lotion</h3>
                   <h4><span class="text-red">Rs. 2,499</span> <span> <del>Rs. 2,999</del></span></h4>
                  <ul>
                      <li>Anti Irritatant Action</li>
                      <li>Short &amp; Long Term Immune Protection</li>
                      <li>Antiphlogistic &amp; Anti Inflammatory Effect</li>
                      <li>Moisturization &amp; Protection</li>
                    </ul>
                  <button type="button" class="btn btn-info mt-4">Buy Now</button>
                 </div>  
     </div>
      
    </div>
<div class="row pt-5 pb-5">
      
      
       <div class="col-sm-6 ">
        <div class="card" >
  <img class="card-img-top" src="image/sampoo-against-hair-loss2.jpg" alt="Card image cap">
  <div class="card-body">
   <div class="data">
                   <h3>Normal Hair Shampoo</h3>
                   <h4><span class="text-red">Rs. 1,499</span> </h4>
                  <ul>
                <li>Mild Cleansing Effect</li>
                <li>Anti Irritant &amp; Soothing Action</li>
                <li>Protection &amp; Moisturization</li>
              </ul>
                  <button type="button" class="btn btn-info mt-4">Buy Now</button>
                 </div>
    
  </div>
</div>
      </div>
      <div class="col-sm-6  ">
        <div class="card" >
  <img class="card-img-top" src="image/sampoo-against-hair-loss.jpg" alt="Card image cap">
  <div class="card-body">
   <div class="data">
                   <h3>Oily Hair Shampoo</h3>
                   <h4><span class="text-red">Rs. 1,499</span> </h4>
                  <ul>
                    <li>Moisturization & Elasticity</li>
                    <li>Mild Cleansing Effect</li>
                    <li>Seboregulation & Anti Hair Loss Treatment</li>
                    
                  </ul>
                  <button type="button" class="btn btn-info mt-4">Buy Now</button>
                 </div>
    
  </div>
</div>
      </div> 
    </div>
    <div class="row  bg-col-w description p-5 ">
      <div class="col-sm-12">
        <h4>PRODUCT BENEFITS</h4>
          <ul>
              <li><strong>Reduction of Hair Loss –</strong> Contains latest generation of Oligopeptides that reduce hair loss and contribute to
  the regeneration of hair.</li>
              <li><strong>Tonic and Nourishment – </strong>Contains elements of Copper, Magnesium & Zinc, whose absence leads to fragile,
  thinning hair and hair loss. Also contains Biotin & Vitamin PP which nourish, protect and help reduce hair loss.</li>
              <li><strong>Anti - Irritant & Mild Cleansing –</strong> Contains mild surfactants, amino acid derivatives that clean without causing
  irritation.</li>
              <li><strong>Moisturization & Elasticity –</strong> Contains a natural amino acid (Trimethyl Glycine), derivatives of Xylitol & Lactitol
  that reduces dryness, itching and helps moisturize the scalp. Also contains Keramidia which adds shine,
  elasticity and stimulates dry, dull and lifeless hair.</li>
  <li><strong>Seboregulation – </strong>Contains Natural Tar, rich in Sulphur that reduces the hyper secretion of Seborrhea (Sebum)
  on the scalp & Burdock that regulates sebum secretion and the sebaceous glands.</li>
              <li><strong>Tonic of Blood Microcirculation – </strong>Contains ginseng extracts that stimulate the micro circulation on the scalp</li>
            </ul>
      </div>
      
    </div>
  </div>
</section>
     
<div class="clearfix"></div>

     
<section class="bottm_sec">
  <h1>Increase your hair density naturally</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>


      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title" id="exampleModalLabel">How do you rate us</h1>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="mx-auto" style="width: 600px">
                <form>
                   <div class="form-group">
    <label for="exampleFormControlSelect1">Select Service</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>Platelet Rich Plasma</option>
      <option>Laser Anagen</option>
      <option>Direct Hair Fusion</option>
      <option>Direct Hair Implantation</option>
      <option>Scalp Micropigmentation</option>
    </select>
  </div>
  <div class="form-group">
    
    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="NAME*">
  </div>
   <div class="form-group">
    
    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="EMAIL*">
  </div>
   <div class="form-group">
    
    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="CONTACT NUMBER*">
  </div>
  
  <div class="form-group">
   
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="REVIEW(
Your Review will be posted publicly)"></textarea>
  </div>
  <div class="form-group text-center">
   <button type="submit" class="btn btn-outline-dark btn-pdding ">Submit</button>
 </div>
</form>
              </div>
      </div>
     
    </div>
  </div>
</div>
   


  </body>
</html>
