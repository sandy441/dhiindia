
<!doctype html>
<html lang="en">
 <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Want to restore your eyebrows shape and look? DHI India provides eyebrow hair transplant & treatment to regain your eyebrows looks. Easy eyebrow surgery procedure with 100% results.">
    <meta name="author" content="">
      <title>Eyebrow Hair Transplant, Restoration & Treatment - DHI India

</title>
<?php include 'header.php';?>
    </head>
  <body>

<section class="bg-col-1">
   <div class="clearfix">
           
            	<video  preload="auto" width="100%" loop autoplay>
                        <source type="video/webm" src="video/eyebrow.mp4">
                        <source type="video/mp4" src="video/eyebrowt.mp4">
                        <source type="video/webm" src="video/eyebrow.ogg">
                        <source type="video/ogg" src="video/eyebrow.ogg">
                    </video>
           
        </div>
        
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
           
     <div class="card mar-minus  bt-border">
            <div class="card-body"> 
              <h2>Eyebrow Restoration </h2>
           <!--    <h5>DHI Medical Group has Developed Two Different Eyebrow Restoration Treatments For Every Case And Need.</h5> -->
           
           
           <p>Eyebrows have a functional role when they protect the eyes from the extreme sunlight ,sweat and other debris that fall into the eye socket. There is a second important function that eyebrows serve and this is the role of expression. The eyebrows play a unique role in oral communication. When we talk we move our eyebrows, at the same time creating different human expressions like happiness, anger, pain, sadness and many more. The expression of our emotions dramatically influences our daily relations.</p>

           <p>Poorly – shaped eyebrows do not give a defined, charming face; on the contrary well designed  eyebrows help the cheekbones stand out, making the face look more symmetrical while at the same time making the eyes look brighter. Well-groomed eyebrows balance the features of the face and frame the eyes.</p>
            <p > <strong>Common Causes of Eyebrow Hair Loss:</strong></p>
           <ul>
             <li>Physical trauma</li>
             <li>Systemic diseases</li>
             <li>Local disease</li>
             <li>Birth defects</li>
             <li>Hair pulling disorder (Trichotillomania)</li>
             <li>Medical and surgical treatments i.e. chemotherapy, surgical removal etc. </li>
           </ul>
          <div class="row">
           <div class="col-sm-6 p-4"> <img src="image/key/2.jpg" class="img-fluid" alt=""></div>
                <div class="col-sm-6 p-4"> <img src="image/key/3.jpg" class="img-fluid" alt=""></div>
          </div>
           <h5>DHI Group has two separate procedures for eyebrow restoration: a) Direct Hair Implantation; b) DHI Micro-pigmentation</h5>
          <h5>Option 1</h5>

          <h2>Eyebrow Restoration With DHI Direct Hair Implantation</h2>

          
          <div class="row">
            <div class="col-sm-6">
              <p class="float-left" width="400">With Direct Hair Implantation it is now possible to transplant hair to any part of the body and the face. Transplanted hair follicles are picked one by one to match eyebrows; and thanks to the DHI custom designed implanting tool, there is complete control of the depth and the direction of the implanted hair, achieving a nice and natural looking result.</p>
            </div>
            <div class="col-sm-6 p-4">
            <img src="image/key/4.jpg" class="img-fluid" alt="">
            </div>
           
           
          </div>
 <h2>How Does Hair Implantation work?</h2>
<ol>
  <li>The doctor thoroughly designs the eyebrow area according to the needs of the patient, and the defined symmetry</li>
  <li>The number of  required hairs and the kind of follicle grafts required for a natural result and density are counted</li>
  <li>The required hair follicles are extracted from the donor area (usually the back of the head)</li>
  <li>The extracted hair follicles are placed into eyebrow area</li>
  <li>The implanted hairs grow for a person’s lifetime</li>
</ol>
 <a href="direct-hair-implantation.php" class="btn btn-common my-2 my-sm-0 m-4 ">Read more about Direct Hair Implantation</a>  


   <h5>Option 2</h5>
          <h2>Eyebrow Restoration with DHI Micropigmentation</h2>
          <div class="row">
            <div class="col-sm-6"> <p>DHI Micropigmentation is applied to the eyebrows area, into totally naked skin or between existing hairs, for instant high density eyebrows simulation.</p>
          <p>DHI Eyebrow Micropigmentation is not like the process of getting a eyebrow tattoo, and does not use materials from permanent makeup, even though it is using similar process. DHI Scalp Micropigmentation is using a non toxic pigment, and is performed in such a way to offer an accurate simulation of short hair.</p></div>
            <div class="col-sm-6 p-4"><img src="image/key/1.jpg" class="img-fluid" alt=""></div>
          </div>
         
<a href="scalp-micro-pigmentation.php" class="btn btn-common m-4 ">Read more about Micropigmentation</a>
        
            </div>
          
            <!-- <div class="card-body bg-col-2"> 
               <div class="row align-items-center ">
                <div class="col-sm-6  pt-3 pb-2 pr-1 "> <img src="image/key/1.jpg" class="img-fluid" alt=""></div>
                <div class="col-sm-6 pt-3 pb-2 pl-1"> <img src="image/key/2.jpg" class="img-fluid" alt=""></div>
                <div class="col-sm-6 pr-1"> <img src="image/key/3.jpg" class="img-fluid" alt=""></div>
                <div class="col-sm-6 pl-1"> <img src="image/key/4.jpg" class="img-fluid" alt=""></div>
                
            
             
              
             </div>

            </div> -->
            

           <!-- <div class="card-body bg-col-6">
             <div class="row  align-items-center">
              <div class="col-sm-6 ">
                 <img src="image/key/Placement.jpg" class="img-fluid" alt="">
               </div>
               <div class="col-sm-6  ">
               
                 <div class="eyebrow-data text-center">
                   <h3>Placement</h3>
                  <p>The extracted hair follicles are placed into eyebrow area.</p>
                 </div>
               </div>
               
             </div>
           </div> -->
           <!-- <div class="card-body bg-col-6">
             <div class="row  align-items-center  ">
              
               <div class="col-sm-6  ">
               
                 <div class="eyebrow-data text-center">
                   <h3>Extraction</h3>
                 <p>The required hair follicles are extracted from the donor area (usually the back of the head).</p>
                 </div>
               </div>
               <div class="col-sm-6 ">
                 <img src="image/key/Extraction.jpg" class="img-fluid" alt="">
               </div>
             </div>
           </div> -->

           <!-- <div class="card-body bg-col-6">
             <div class="row  align-items-center  ">
           
              <div class="col-sm-6 ">
                 <img src="image/key/Growing.jpg" class="img-fluid" alt="">
               </div>
               <div class="col-sm-6  ">
               
                 <div class="eyebrow-data text-center">
                   <h3>Growing</h3>
                 <p>The implanted hairs grow for a person's lifetime.</p>
                 </div>
               </div>
               
             </div>

</div> -->
          </div>
        	

                 	</div>



</div>
</section>


     
<div class="clearfix"></div>
<section class="bottm_sec">
  <h1>beautify your eyebrows now</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>
     



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


    




  </body>
</html>
