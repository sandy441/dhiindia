
<!doctype html>
<html lang="en">
 
  <body>
<?php include 'header.php';?>
<section class="bg-col-1">
   <div class="clearfix">
           
            	<video  preload="auto" width="100%" loop autoplay>
                        <source type="video/webm" src="video/eyebrow.mp4">
                        <source type="video/mp4" src="video/eyebrowt.mp4">
                        <source type="video/webm" src="video/eyebrow.ogg">
                        <source type="video/ogg" src="video/eyebrow.ogg">
                    </video>
           
        </div>
        
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
           
     <div class="card mar-minus  bt-border">
            <div class="card-body"> 
              <h2>Eyebrow Restoration </h2>
              <h5>DHI Medical Group has Developed Two Different Eyebrow Restoration Treatments For Every Case And Need.</h5>
           
           
           <p>At DHI Eyebrow Restoration is designed to restore eyebrows that are overly thin, scarred, or completely missing. The absence of hair can be due to genetics, prior electrolysis, removal, over plucking or other hormonal abnormalities.</p>
           <p>Eyebrows have a functional role, as they protect the eyes from the extreme sunlight, sweat and other debris that fall into the eye socket. There is a second important function that eyebrows service ad this is the role of expression. The eyebrow plays a unique role in oral communication. When we talk we move our eyebrows, at the same time creating different human expressions like happiness, anger, pain sadness and many more. The expression of our eyebrows emotion dramatically and influences our daily relations.</p>

           <p>Poorly – shaped eyebrows do not give a defined, charming face; on the contrary well designed eyebrows help the cheekbones stand out, making the face look more symmetrical while at the same time making the yes look brighter. Well-groomed eyebrows balance the features of the face and frame the eyes.</p>
           <p>Eyebrow hairs are lost due to many different reasons; physical trauma, local and systemic diseases, Birth defects, Badly shaped plucking, self-inflicted obsessive plucking (trichotillomania), and Medical or Surgical treatments (chemotherapy, surgical removals, etc.)</p>
           <p>DHI Medical Group has developed two different eyebrow restoration treatments for every case ad need: (1) DHI Direct Hair Implantation, and (2) DHI Micropigmentation.</p>
          
          <h2>Eyebrow Restoration With DHI Direct Hair Implantation</h2>
          <p>With Direct Hair Implantation it is now possible to transplant hair to any part of the body and the face. Transplanted hair follicles are picked one by one to match eyebrows; and thanks to the DHI custom designed implanting tool, there is complete control of the depth and the direction of the implanted hair, achieving a nice and natural looking result.</p>


           
            </div>
          
            <div class="card-body bg-col-2"> 
               <div class="row align-items-center ">
                <div class="col-sm-6  pt-3 pb-2 pr-1 "> <img src="image/key/1.jpg" class="img-fluid" alt=""></div>
                <div class="col-sm-6 pt-3 pb-2 pl-1"> <img src="image/key/2.jpg" class="img-fluid" alt=""></div>
                <div class="col-sm-6 pr-1"> <img src="image/key/3.jpg" class="img-fluid" alt=""></div>
                <div class="col-sm-6 pl-1"> <img src="image/key/4.jpg" class="img-fluid" alt=""></div>
                
            
             
              
             </div>

            </div>
            

           <!-- <div class="card-body bg-col-6">
             <div class="row  align-items-center">
              <div class="col-sm-6 ">
                 <img src="image/key/Placement.jpg" class="img-fluid" alt="">
               </div>
               <div class="col-sm-6  ">
               
                 <div class="eyebrow-data text-center">
                   <h3>Placement</h3>
                  <p>The extracted hair follicles are placed into eyebrow area.</p>
                 </div>
               </div>
               
             </div>
           </div> -->
           <!-- <div class="card-body bg-col-6">
             <div class="row  align-items-center  ">
              
               <div class="col-sm-6  ">
               
                 <div class="eyebrow-data text-center">
                   <h3>Extraction</h3>
                 <p>The required hair follicles are extracted from the donor area (usually the back of the head).</p>
                 </div>
               </div>
               <div class="col-sm-6 ">
                 <img src="image/key/Extraction.jpg" class="img-fluid" alt="">
               </div>
             </div>
           </div> -->

           <!-- <div class="card-body bg-col-6">
             <div class="row  align-items-center  ">
           
              <div class="col-sm-6 ">
                 <img src="image/key/Growing.jpg" class="img-fluid" alt="">
               </div>
               <div class="col-sm-6  ">
               
                 <div class="eyebrow-data text-center">
                   <h3>Growing</h3>
                 <p>The implanted hairs grow for a person's lifetime.</p>
                 </div>
               </div>
               
             </div>

</div> -->
          </div>
        	

                 	</div>



</div>
</section>


     
<div class="clearfix"></div>
<section class="bottm_sec">
  <h1>beautify your eyebrows now</h1>
   <a href="book-appoint.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>
     



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


    




  </body>
</html>
