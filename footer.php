 <footer>
      <div class="container">
      <div class="row">
      <div class="small-logo"><img src="<?php echo $host;?>/image/dhi-small.png" alt="Dhi" width="110"></div>
      </div>
        <div class="row">
        	
          <div class="col-md-3 col-sm-6 col-xs-6 col">
            <h4>About Us</h4>
            <ul>
            	<li><a href="<?php echo $host;?>/achievements.php">Introduction</a></li>
               <!--  <li><a href="about-scientific.php">Our Team</a></li> -->
                <li><a href="<?php echo $host;?>/results.php">Our Results</a></li>
                <li><a href="<?php echo $host;?>/client-feedback.php">Client Feedback</a></li>
                <li><a href="<?php echo $host;?>/clinics.php">Our Clinics</a></li>
                <li><a href="<?php echo $host;?>/hair-transplant-training.php">Training Academy</a></li>
                <li><a href="<?php echo $host;?>/book-an-appointment.php">Book a Consultation</a></li>
                <li><a href="<?php echo $host;?>/contact.php">Contact Us</a></li>
            </ul>
            
          </div>
          <div class="col-md-3 col-sm-6 col-xs-6 col">
            <h4>Treatments</h4>
            <ul>
              <li><a href="<?php echo $host;?>/hair-loss-diagnosis.php">Alopecia Diagnostic Test</a></li>
            	<li><a href="<?php echo $host;?>/direct-hair-implantation.php">Direct Hair Implantation</a></li>
                <li><a href="<?php echo $host;?>/scalp-micro-pigmentation.php">Scalp Micro-Pigmentation (MPG)</a></li>
                <li><a href="<?php echo $host;?>/prp.php">PRP Growth Factors</a></li>
              <!--   <li><a href="laser-anageny.php">Anagen Treatment</a></li> -->
                <li><a href="<?php echo $host;?>/eyebrow-restoration.php">Eyebrow Restoration</a></li>
                <li><a href="<?php echo $host;?>/beard-restoration.php">Beard Restoration</a></li>
                <li><a href="<?php echo $host;?>/direct-hair-fusion.php">Hair Prosthetics</a></li>
                <li><a href="<?php echo $host;?>/scar-repair-treatments.php">Scar Repair</a></li>
                <li><a href="<?php echo $host;?>/e-shop.php">Hair Care Products</a></li>
            </ul>
            
          </div>
          <div class="col-md-3 col-sm-6 col-xs-6 col">
            <h4>Our Services</h4>
            <ul>
            	<li><a href="<?php echo $host;?>/hair-loss.php">Hair Loss</a></li>
                <li><a href="<?php echo $host;?>/hair-transplant.php">Hair Transplant</a></li>
                <li><a href="<?php echo $host;?>/hair-restoration.php">Hair Restoration</a></li>
                <li><a href="<?php echo $host;?>/hair-regrowth.php">Hair Regrowth</a></li>
                <li><a href="<?php echo $host;?>/baldness-treatment.php">Baldness Treatment in India</a></li>
                <li><a href="<?php echo $host;?>/eyebrow-reconstruction.php">Eyebrow Reconstruction</a></li>
                <li><a href="<?php echo $host;?>/treatment-costing.php">Treatment Costing</a></li>
                <li><a href="<?php echo $host;?>/for-women.php">Female Hair Loss</a></li>
                <li><a href="<?php echo $host;?>/hair-transplant-by-dhi-clinic-surgeon.php">Hair Transplant by DHI Clinic & Surgeon</a></li>
            </ul>
            
          </div>
          <div class="col-md-3 col-sm-6 col-xs-6 col">
          	
            <ul >
              <li >
              	<span><img src="<?php echo $host;?>/image/phone-icon.png" alt="phone" ></span><span class="font-weight-bold ">18001039300</span> 
              </li>
              <li>
               	<img class="logo-47" src="<?php echo $host;?>/image/footerlogo3.png" alt="dhi 47 years of excellence
" class="img-fluid" width="150">
              </li>
              <li >
              	<img class="logo-iso" src="<?php echo $host;?>/image/footerlogo2.png" alt="" class="img-fluid" width="40">
              </li>
              <li>
              	<img class="logo-q" src="<?php echo $host;?>/image/footerlogo1.png" alt="" class="img-fluid" width="100">
              </li>
              <li>
              	<span>US Patent #8,801,743 B2 <br />
                EU Patent #EU 2234550</span>
                
              </li>
            </ul>
          </div>
        </div>
      </div>
      
    </footer>

<section class="footer_btm">
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
              <p>&copy; 2017. All rights reserved by dhiindia.com.</p>
            </div>
            <div class="col-sm-8 text-right">
              <ul>
                <li><a href="<?php echo $host;?>/terms-conditions.php">Terms & Conditions</a></li>
                <li><a href="<?php echo $host;?>/privacy.php">Privacy Policy</a></li>
                <li><a href="<?php echo $host;?>/cookie-policy.php">Cookie Policy</a></li>
                <li><a href="<?php echo $host;?>/sitemap.php">Sitemap</a></li>
                <li><a href="<?php echo $host;?>/contact.php">Contact Us</a></li>
              </ul>
            </div>
          </div>
        </div>
      </section>
       <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
    <script src="<?php echo $host;?>/js/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="<?php echo $host;?>/js/jquery.validate.min.js"></script>
    <script src="<?php echo $host;?>/js/jquery-ui.js"></script>
    <script src="<?php echo $host;?>/js/custom.js"></script>
    <script src="<?php echo $host;?>/js/book_custom.js"></script>
    <!-- <script src="js/slick.min.js"></script> -->
  
<!-- <script>
 $(document).ready(function() {
   $('.single-item').slick({
     centerMode: true,
     centerPadding: '300px',
     slidesToShow: 1,
     speed: 1500,
     index: 2,
     focusOnSelect:true,
     responsive: [{
       breakpoint: 768,
       settings: {
         arrows: true,
         centerMode: true,
         centerPadding: '40px',
         slidesToShow: 1
       }
     }, {
       breakpoint: 480,
       settings: {
         arrows: false,
         centerMode: true,
         centerPadding: '40px',
         slidesToShow: 1
       }
     }]
   });
   $('single-item video').trigger('pause');
   $('.slick-center video').trigger('play');
   $('.card').click(function()
   {
     //var $this = $(this);
     $('.single-item video').trigger('pause');
     var vname='#'+$(this)[0].children[0].id;
     $(vname).trigger('play');
     console.log(vname);
   });
 });

</script> -->

<script>
  $(function () {
  'use strict'

  $('[data-toggle="offcanvas"]').on('click', function () {
    $('.row-offcanvas').toggleClass('active')
  })
})
</script>
<script>
  $(document).ready(function(){

    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        
        if(value == "all")
        {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
        }
        else
        {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.'+value).hide('1000');
            $('.filter').filter('.'+value).show('1000');
            
        }
    });
    
    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");

});
</script>
<script>
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
