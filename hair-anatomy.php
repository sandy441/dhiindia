
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="know about types of Alopecia &amp; treatment by DHI - Androgenetic Alopecia, Alopecia Areata, Traction, Trichotillomania, scarring, Triangular, Telogen Effluvium &amp; Loose Anagen Syndrome">
    <meta name="author" content="">
      <title>What are the causes of hair loss? DHI India
</title>
<?php include 'header.php';?>
    </head>
  <body>


<section class="bg-col-1">
  <nav aria-label="breadcrumb" role="navigation">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="./">DHI International</a></li>
    <li class="breadcrumb-item active" aria-current="page">Anatomy </li>
  </ol>
</nav>
<div class="container hair_loss">
 <div class="content">
    <div class="card bt-border">
  
  <div class="card-body ">
<h2 class="text-center">The Hair Growth Cycle </h2>
           
  <div class="row">
    <div class="col-sm-12">
    
     <h3>The Hair Cycle </h3>
     
     <p>The production of hair is not continuous but cyclic. The growth phase (Anagen) is succeeded through a transitional phase of regression (Catagen), by the rest phase (Telogen), and the cycle is preserved. </p>
     <p>
During the relatively long Anagen phase (lasts for 1,000 days, i.e. 2-5 years), the matrix cells divide, become keratinized and hair is produced. The Catagen phase lasts for about 2-3 weeks and is practically unimportant. During the Telogen phase, that lasts for several months, the cell division stops, hair growth ceases, and the attachment of the hair to the base of the follicle becomes progressively weaker. Finally, as a result of ordinary traction, whether that be from combing, from washing, its own weight, or a push by a new, growing hair, the old hair is eventually shed and discarded.
</p>
     

     <h5>The hair cycle consists of three phases, which have the following characteristics: </h5>
<p><img src="image/Hair-Growth-Cycle.jpg" alt="hair" class="img-fluid"></p>


     <h5>The Anagen Phase - Growth Phase</h5>
     <ul>
       <li>Approximately 85-90% of all hair are in the growing phase at any one time</li>
       <li>It can vary from two to six years</li>
       <li>Hair grows approximately 10-15cm per year</li>
     </ul>
     <p>In general, the length of any human being’s hair is unlikely to grow
more than one meter.
</p>
<h5>The Catagen Phase - Transitional Phase</h5>
<ul>
  <li>At the end of the Anagen phase</li>
  <li>It lasts about one or two weeks</li>
  <li>The hair follicle shrinks to about 1/6 of the normal length</li>
  <li>The lower part is destroyed and the dermal papilla breaks away to rest below</li>
</ul>
<h5>The Telogen Phase - Resting Phase</h5>
<ul>
  <li>It follows the Catagen phase and normally lasts about 5-6 weeks</li>
  <li>At any given instance in time, approximately 10-15% of all hair follicles are in this phase</li>
  <li>The hair does not grow but stays attached to the follicle</li>
  <li>The dermal papilla stays in a resting phase below</li>
</ul>


<h5>The Anatomy and Histology of the Hair Follicle </h5>
<p>We divide the hair follicle in the following parts:</p>
<ul>
  <li>The Infundibulum. The area from the opening of the sebaceous duct to the follicular opening of the surface of the skin </li>
  <li>The Isthmus. The area from the opening of the Sebaceous duct and continues
down until it reaches the insertion of the arrector pili muscle
</li>
  <li>The Level of Insertion (bulge area). Bulge area contain stem cells important
for the regeneration of the follicle and epidermal and sebaceous gland
</li>
  <li>The Inferior Segment. The area from the insertion of arrector pili muscle to the base of the hair follicle  </li>
  
</ul>
<h5>The histology of follicle can be summarized by the following points:</h5>
<ul>
  <li>The Dermal Papilla is part of dermis and contains specialized fibroblasts. It is regulating matrix cells division and the caliber of the hair. Androgen receptors exist.</li>
  <li>The Matrix Cells are undifferentiated cells which differentiate to the components of the hair follicle: hair shaft is inner root sheath and outer root sheath.</li>
  <li>The Hair Shaft. Innermost cylinder consisted by: the Medulla, the Hair Shaft cortex and the Hair Shaft cuticle.</li>
  <li>The Inner Root Sheath. Exists in the inferior segment of the hair follicle.</li>
  <li>The Outer Root Sheath. Bottom of the hair bulb to the opening of the sebaceous gland. It contains melanocytes neurosecretory cells and Lan gerhans
(Melanocyts - Ame lanotic).
</li>
</ul>
    </div>
  </div>
         
  
            </div>
  </div><!--/row-->

  
</div>


</section>


     
<div class="clearfix"></div>

<section class="bottm_sec">
 <h1>Ready to regain your hair & confidence</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now
   </a>
</section>

      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


   

  </body>
</html>
