<!DOCTYPE html>
<html>

<head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width, initial-scale=1">
    <link href=css/bootstrap.min.css rel=stylesheet type=text/css>
    <link href=css/style.css rel=stylesheet type=text/css>
    <link rel=stylesheet href=css/jquery-ui.css>
    <Title>Hair Transplant in India | Best Clinic for hair loss treatment</title>
    <script type=text/javascript src=js/client_agltracking_common.js></script>
    <meta name=description content="Hair transplantation is a surgical technique for effective hair restoration. DHI clinic offers best hair transplantation service in India at affordable cost">
    <meta name=keywords content="hair transplant India, hair transplant cost, hair transplant clinics, hair loss treatment, Hair Transplant Doctors in India, Hair Transplant Clinic, hair restoration">
    <!--<script>(function(){var c=window._fbq||(window._fbq=[]);if(!c.loaded){var a=document.createElement("script");a.async=true;a.src="//connect.facebook.net/en_US/fbds.js";var b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b);c.loaded=true}c.push(["addPixelId","424471077707230"])})();window._fbq=window._fbq||[];window._fbq.push(["track","PixelInitialized",{}]);</script><noscript><img alt=fb style=display:none src="https://www.facebook.com/tr?id=424471077707230&amp;ev=PixelInitialized"></noscript>-->
    <!--new Facebook Pixel Code 
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '424471077707230');
fbq('track', "PageView");
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=424471077707230&ev=PageView&noscript=1"

/></noscript>
<!--new End Facebook Pixel Code -->
    <!-- <script>(function(d,e,j,h,f,c,b){d.GoogleAnalyticsObject=f;d[f]=d[f]||function(){(d[f].q=d[f].q||[]).push(arguments)},d[f].l=1*new Date();c=e.createElement(j),b=e.getElementsByTagName(j)[0];c.async=1;c.src=h;b.parentNode.insertBefore(c,b)})(window,document,"script","//www.google-analytics.com/analytics.js","ga");ga("create","UA-26848394-1","auto");ga("require","displayfeatures");ga("send","pageview");</script> -->
    <!-- new analytics code 1-3-2016 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-71179767-1', 'auto');
  ga('send', 'pageview');

</script>-->
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-MSVNF3" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                '//www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MSVNF3');
    </script>
    <!-- End Google Tag Manager -->

</head>

<body>
    <?php $utm_source=isset($_REQUEST['utm_source'])?$_REQUEST['utm_source']:'';$utm_medium=isset($_REQUEST['utm_source'])?$_REQUEST['utm_medium']:'';?>
        <div class="container-fluid navigation-main">
            <div class=container>
                <div class="col-sm-12 col-md-12 navigation">
                    <div class="col-xs-12 col-sm-3 col-md-3"><img src=img/logo.png alt="DHI India" title="DHI India" width="180px" /></div>
                    <div class="col-xs-12 col-sm-5 col-md-6">
                        <h1>Global Leader In Hair Transplant Since 1970</h1></div>
                    <div class="col-xs-12 col-sm-4 col-md-3 text-center"><img src=img/iso_certified_img.png alt="DHI ISO Certification" title="DHI ISO Certification" /></div>
                </div>
            </div>
        </div>
        <div class="container-fluid clr"></div>
        <div class="container-fluid banner-color">
            <div class=container>
                <div class="col-sm-12 col-md-3 before-after text-center"><img src=img/before.png alt="Before Hair Implant" title="Before Hair Implant" />
                    <h3> Worried about loosing your precious hair and going bald? </h3></div>
                <div class="col-sm-12 col-md-1 arrow"><img src=img/arrow.png alt="arrow" /></div>
                <div class="col-sm-12 col-md-4">
                    <div class="col-md-12 form-heading">Book your Appointment Online &
                        <br> <span class="blink">Get 65% OFF</span> on Consultation</div>
                    <div class="col-md-12 form-field">
                        <form role=form class=form-top method=post id=form_id enctype=multipart/form-data action='submit.php'>
                            <input type=hidden name=utm_medium value=<?php echo $utm_medium;?> />
                            <input type=hidden name=utm_source value=<?php echo $utm_source;?> />
                            <input type=hidden name=enquiry_source value=A />
                            <div class=form-group>
                                <input type=text class=form-control name=full_name id=full_name placeholder="Enter Name*">
                            </div>
                            <div class=form-group>
                                <input type=email class=form-control id=email name=email placeholder="Enter Email*">
                            </div>
                            <div class=form-group>
                                <input type=text class=form-control id=phone name=phone maxlength=15 placeholder="Enter Phone No.*">
                            </div>
                            <div class=form-group>
                                <select class=form-control name=city id=city>
                                    <option value>Select City*</option>
                                    <option value="Ahmedabad">Ahmedabad</option>
                                    <option value="Ludhiana">Ludhiana</option>
                                    <option value="Gurgaon">Gurgaon</option>
                                    <option value="Delhi NCR">Delhi NCR</option>
                                    <option value=Bangalore>Bangalore</option>
                                    <option value=Kolkata>Kolkata</option>
                                    <option value=Chennai>Chennai</option>
                                    <option value=Hyderabad>Hyderabad</option>
                                    <option value=Jaipur>Jaipur</option>
                                    <option value=Chandigarh>Chandigarh</option>
                                    <option value=Guwahati>Guwahati</option>
                                    <option value=Other>Other</option>
                                </select>
                            </div>
                            <div class=form-group>
                                <input id=datepicker maxlength=15 class=form-control name=appointmentdate type=text placeholder="Appointment Date*">
                            </div>
                            <div class=form-group>
                                <select id=ctimeslots name=timeslots class=form-control style="border:1px solid #dbd5db">
                                    <option value>Select Time Slots*</option>
                                    <option value="10AM to 12PM">10AM to 12PM</option>
                                    <option value="12PM to 3PM">12PM to 3PM</option>
                                    <option value="3PM to 6PM">3PM to 6PM</option>
                                </select>
                            </div>
                            <div class=form-group>
                                <textarea class=form-control rows=2 name=query_message id=query_message placeholder="Query Message"></textarea>
                            </div>
                            <?php 
$referer = ((!empty($_SERVER["HTTP_REFERER"]))?$_SERVER["HTTP_REFERER"]:''); 
if(isset($_REQUEST['obb_source']))
 {
    $utm_campaign = $_REQUEST['obb_campaign'];
    $gamedium  = $_REQUEST['obb_medium'];
    $gasource2 = $_REQUEST['obb_source'].'/'.$gamedium;
    $referer = ((!empty($_SERVER["HTTP_REFERER"]))?$_SERVER["HTTP_REFERER"]:'');
}
else if(isset($_REQUEST['utm_source']) && isset($_REQUEST['utm_medium']) && $_REQUEST['utm_campaign']){
    $utm_source = isset($_REQUEST['utm_source'])?$_REQUEST['utm_source']:'';
    $utm_medium = isset($_REQUEST['utm_medium'])?$_REQUEST['utm_medium']:'';
    $utm_campaign = isset($_REQUEST['utm_campaign'])?$_REQUEST['utm_campaign']:'';
    $utm_term = isset($_REQUEST['utm_term'])?$_REQUEST['utm_term']:'';
    $utm_content = isset($_REQUEST['utm_content'])?$_REQUEST['utm_content']:'';
    $utm_device = isset($_REQUEST['utm_device'])?$_REQUEST['utm_device']:'';
    $gasource2 = $utm_source.'/'.$utm_medium;
} else if(!empty($_SERVER["HTTP_REFERER"])) {
    $utm_campaign='organic';
    if (strpos($referer, 'yahoo') !== false) {
        $gasource2='yahoo/organic';
    }else if (strpos($referer, 'google') !== false) {
        $gasource2='google/organic';
    }else if (strpos($referer, 'facebook') !== false) {
        $gasource2='facebook/referral';
        $utm_campaign='referral';
    }else{
        $gasource2='Not Set/organic';
    }
} else{
    $utm_campaign='direct';
    $gasource2='direct/none';
}

 ?>
                                <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo $utm_campaign; ?>" />
                                <input type="hidden" id="utm_term" name="utm_term" value="<?php echo $utm_term; ?>" />
                                <input type="hidden" id="utm_device" name="utm_device" value="<?php echo $utm_device; ?>" />
                                <input type="hidden" id="utm_content" name="utm_content" value="<?php echo $utm_content; ?>" />
                                <input type="hidden" id="gasource2" name="gasource2" value="<?php echo $gasource2; ?>" />
                                <input type="hidden" id="referer_url" name="referer_url" value="<?php echo $referer; ?>" />
                                <div class=form-group>
                                    <button type=submit id=sub_mit class="btn btn-default button">PROCEED TO BOOK</button>
                                </div>
                        </form>
                    </div>
                    <div class="col-md-12 hair-back"> More than 2 Lac Procedure Experience. </div>
                </div>
                <div class="col-sm-12 col-md-1 arrow"><img src=img/arrow.png alt="arrow" /></div>
                <div class="col-sm-12 col-md-3 before-after text-center"><img src=img/after.png alt="After Hair Implant" title="After Hair Implant" />
                    <h3> 100% Guarantee on Results.<span>Assured Natural Hair Growth.</span></h3></div>
            </div>
        </div>
        <section class="container banner">
            <div class="col-md-12 result">
                <h2>ADVANCED, REVOLUTIONARY TECHNIQUE TO GIVE YOU NATURAL LOOKING, HEALTHY HAIR.</h2></div>
        </section>
        <section class="container banner mb-10">
            <ul>
                <li>No scalpels, no stitches, no scarring- No sutures or scalpels are involved. Our instruments are used just once and are designed in unequivocally hygienic conditions.</li>
                <li>Natural results- Our technical acumen and insight creates the appropriate density, angle, hairline and design for you, ensuring it looks absolutely natural.</li>
                <li>Safety- Our services, products and treatments provide the most authentic and reliable hair restoration solutions.</li>
                <li>Maximized for life- The DHI hair implantation ensures a lifetime of hair growth for you.</li>
                <li>Other features- Easy going, zero risk of infection, less painful, brilliant results</li>
            </ul>
        </section>
    
        <section >
      
    <a href="results.php"> <img src="img/resultsimage.jpg" alt="dhi client reasult - natural and safe
" class="img-responsive"></a>
   
        </section>
        <section class="container banner">
            <div class="col-md-12 national"> National Presence </div>
            <div class="col-md-12 national-city">Ahmedabad | Bangalore | Calicut | Chandigarh | Chennai | Delhi | Gurgaon | Guwahati | Hyderabad | Jaipur | Kochi | Kolkata | Ludhiana | Lucknow | Mumbai | Pune | Saharanpur | Surat | Vijayawada</div>
        </section>
        <section class="container banner">
            <div class="col-md-12 global"> Global Presence </div>
        </section>
        <section class=container>
            <div class="col-md-12 global-country">Australia | Bahrain | Belgium | Bulgaria | Colombia | Egypt | France | Germany | Greece | Hong Kong | India | Indonesia | Isle of Man | Ireland | Italy | Jordan |Jersey | Montenegro | Morocco | Mauritius | Malaysia | Netherlands | Oman | Panama | Philippines | Portugal | Qatar | Saudi Arabia | Switzerland | Turkey | United Arab Emirates | United Kingdom</div>
        </section>
        <section class="container footer">
            <div class="col-xs-12 col-sm-12 col-md-9"> Copyright @ 2014 DHI, All Rights Reserved </div>
            <div class="col-xs-12 col-sm-12 col-md-3 footer text-right"> Design By: Obbserv </div>
        </section>
        <script type=text/javascript src=js/jquery-1.10.2.js></script>
        <script src=js/validation.js></script>
        <script src=js/jquery-ui.js></script>
        <script>
            $(function() {
                $("#datepicker").datepicker({
                    minDate: 0
                });
            });
        </script>
        <script>
            function AddNewLeadCustomerAPI(LeadsSource, Fname, Lname, Email, City, Phone, comments, source, medium, type, campaign, campaignid, adgroupid, keyword, gclid) {

                try {

                    var datastring = {
                        LeadsSource: LeadsSource,
                        Fname: Fname,
                        Lname: (Lname == null ? 'null' : Lname),
                        Email: Email,
                        City: City,
                        Phone: Phone,
                        comments: comments,
                        source: source,
                        medium: medium,
                        type: type,
                        campaign: campaign,
                        campaignid: campaignid,
                        adgroupid: adgroupid,
                        keyword: keyword,
                        gclid: gclid
                    };

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: 'http://54.254.180.8:8010/service/WebService.asmx/AddNewLeadCustomerAPI',
                        data: datastring,
                        dataType: "jsonp",
                        jsonp: "callback",
                        crossDomain: true,
                        async: false,

                        success: function(data, status, xhr) {

                        },
                        error: function(xhr, status, error) {

                        }

                    });

                } catch (e) {
                    //alert("catch...!!");
                }

            }

            $(function() {
                $('#sub_mit').click(function() {
                    AddNewLeadCustomerAPI('Facebook', $('#full_name').val(), 'null', $('#email').val(), $('#city').val(), $('#phone').val(), $('#query_message').val(), $('#utm_source').val() == null ? 'null' : $('#utm_source').val(), $('#utm_medium').val() == null ? 'null' : $('#utm_medium').val(), 'null', $('#utm_campaign').val() == null ? 'null' : $('#utm_campaign').val(), 'null', 'null', 'null', 'null');
                });
            });
        </script>
</body>

</html>