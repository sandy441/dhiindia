<?php 
$city = $_GET['city'];
if ($city=='Delhi NCR')
{ $dynamic_price =  "At our Delhi clinic, our experts will examine you, provide you with an accurate & personalized diagnosis and the best treatment option available at a consultation fee of INR 750."; }
else if ($city=='Bangalore')
{ $dynamic_price =  "At our Banglore clinic, our experts will examine you, provide you with an accurate & personalized diagnosis and the best treatment option available at a consultation fee of INR 950."; }
else if ($city=='Chennai')
{ $dynamic_price =  "At our Chennai clinic, our experts will examine you, provide you with an accurate & personalized diagnosis and the best treatment option available at a consultation fee of INR 500."; }
else if ($city=='Kolkata')
{ $dynamic_price =  "At our Kolkata clinic, our experts will examine you, provide you with an accurate & personalized diagnosis and the best treatment option available at a consultation fee of INR 500."; }
else if ($city=='Jaipur')
{ $dynamic_price =  "At our Jaipur clinic, our experts will examine you, provide you with an accurate & personalized diagnosis and the best treatment option available at a consultation fee of INR 500."; }
else if ($city=='Chandigarh')
{ $dynamic_price =  "At our Chandigarh clinic, our experts will examine you, provide you with an accurate & personalized diagnosis and the best treatment option available at a consultation fee of INR 500."; }
else
{ $dynamic_price =  "At DHI, our experts will examine you, provide you with an accurate & personalized diagnosis and the best treatment option available at a reasonable consultation fee."; }

 ?>
<html>
<head>
<title>THANK YOU DHI</title>
<link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
<link rel="icon" href="../img/favicon.ico" type="image/x-icon">
<link href="css/bootstrap.css" rel="stylesheet"  type="text/css">
<link href="css/bootstrap.min.css" rel="stylesheet"  type="text/css">
<link href="css/bootstrap-theme.css" rel="stylesheet"  type="text/css">
<link href="css/style.css" rel="stylesheet"  type="text/css">
<!-- Facebook Conversion Code for DHI Facebook -->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6017929570974', {'value':'0.01','currency':'INR'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6017929570974&amp;cd[value]=0.01&amp;cd[currency]=INR&amp;noscript=1" /></noscript>
<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-26848394-1', 'auto');
	  ga('require', 'displayfeatures');
	  ga('send', 'pageview');
</script>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 965186510;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "L0jJCJLRmlgQzqeezAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>

<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/965186510/?label=L0jJCJLRmlgQzqeezAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</head>
<body>
	<section class="container">
		<div class="col-md-12 text-center thanks">
			<h1>Thank you for registering with DHI India</h1>
		</div>
		<div class="col-md-12 text-center">
			<img src="img/thankyou.png" alt="thank you">
		</div>
		<!-- <div class="col-md-12 brochure">
			<a href="http://www.dhiindia.com/hair-implant/dhi-brochure.pdf" style="color:color:#FFFFFF;" target="_blank"/>Download e-Brochure <img src="img/pdf.png" /></a>
		</div> -->
				<div class="col-md-12 text-center price-thankyou">
		<p><?php echo $dynamic_price; ?>
</p>
		</div>
		<div class="col-md-12 text-center msg">
			<p>Our experts will get in touch with you soon. Meanwhile, browse through our website to know.</p>
		</div>
		<div class="col-md-12 text-center dhi-link">
			<span><a href="http://www.dhiindia.com/about.html">ABOUT DHI</a></span>
			<span><a href="http://www.dhiindia.com/services/">OUR SERVICES</a></span>
			<span><a href="http://www.dhiindia.com/testimonials/">TESTIMONIALS</a></span>
			<span><a href="http://www.dhiindia.com/results.html">DHI RESULTS</a></span>
			<span><a href="http://www.dhiindia.com/our-locations.html">OUR CLINICS</a></span>
		</div>

	</section>
</body>
</html>
