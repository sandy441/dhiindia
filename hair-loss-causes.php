
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="know about types of Alopecia &amp; treatment by DHI - Androgenetic Alopecia, Alopecia Areata, Traction, Trichotillomania, scarring, Triangular, Telogen Effluvium &amp; Loose Anagen Syndrome">
    <meta name="author" content="">
      <title>What are the causes of hair loss? DHI India
</title>
<?php include 'header.php';?>
    </head>
  <body>


<section class="bg-col-1">
  <nav aria-label="breadcrumb" role="navigation">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="./">DHI International</a></li>
    <li class="breadcrumb-item active" aria-current="page">Causes of Hair Loss</li>
  </ol>
</nav>
<div class="container hair_loss">
 <div class="content">
    <div class="card bt-border">
  
  <div class="card-body ">
<h2 class="text-center">Causes of Hair Loss</h2>
           
  <div class="row">
    <div class="col-sm-7">
      <p>
        Hair loss is one of the most common problems worldwide, which affects about one third of total world population. There are a variety of reasons behind hair loss including genetic predisposition, stress, dandruff, scalp bacteria, poor nutrition, and hormonal imbalances. However, pattern baldness is by far the major contributing factor affecting more than 95% men and millions of women as well. 
      </p>
      <p>Hamilton for the first time discovered that androgens are responsible for male pattern baldness. Even the normal amount of androgens is enough to trigger the development of this hair loss condition. Male pattern baldness will occur if the gene for hair loss is present in the blood. </p>
      <p>Male pattern baldness is hereditary. The responsible gene is either inherited from the mother or father's side. There is a common myth amongst people that the gene is inherited from the mother's side only. It is untrue. </p>
      <p>In short, male pattern baldness or androgenic alopecia is a hereditary condition triggered by a significant amount of androgens in the blood. The severity of hair loss is unpredictable and it increases with age. </p>
    </div>
    <div class="col-sm-5 p-5">
      <img src="image/tickers/DNA.jpg" alt="hair-loss" class="img-fluid" >
    </div>
  </div>
         
  
            </div>
  </div><!--/row-->

  
</div>


</section>


     
<div class="clearfix"></div>

<section class="bottm_sec">
 <h1>Ready to regain your hair & confidence</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>

      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


   

  </body>
</html>
