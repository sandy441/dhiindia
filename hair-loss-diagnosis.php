<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Hair Loss Diagnosis - Treatment of hair loss symptoms with unique diagnostic system for Alopecia (UDSA) developed by DHI. Call us: 1800-103-9300
">
    <meta name="author" content="">
    <title>Hair Loss Diagnosis - Unique Diagnostic System for Alopecia (UDSA) - DHI India
    </title>
    <?php include 'header.php';?>
</head>

<body>

   <section>
  <img src="image/alopecia-banner.jpg" alt="" class="img-fluid">
</section>

    <section class=" bg-col-1">
        <div class="container">
            <div class="content">
                <div class="card ">
                    <div class="card-body ">
                        <h3 class="text-center">Unique Diagnostic System for Alopecia (UDSA)</h3>
                        <div class="row">
                            <div class="col-sm-8">
                                <h4>The correct diagnosis is the best treatment</h4>
                                <p>At DHI Global Medical Group we believe that diagnosis is the basis for successful and permanent treatment of hair loss. This is why we have developed a very sophisticated diagnostic system for hair loss, the Unique Diagnostic System for Alopecia (UDSA)</p>
                            </div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-12">
                                <h4>Why did we develop the UDSA diagnostic system?</h4>
                                <p>Hair loss cannot be attributed to a single cause. There are multiple kinds of alopecia with each having a different treatment plan. Hair transplants work as a treatment only in few kinds of alopecia. Most clinics across the globe do not diagnose for alopecia, but at a DHI clinic each patient undergoes a thorough dermatological examination and alopecia test before a treatment is suggested. As a result each patient receives a treatment plan that is unique to their needs.</p>
                                <p>UDSA examines each specific case individually, including psychological aspects, dermatological examinations, mathematical aspects and the DHI alopecia test before determining the appropriate treatment plan.</p>
                                <p>UDSA was developed by a team of world renowned experts to provide accurate and personalized diagnosis for all types of alopecia – hair and scalp disorders.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="card ">
                    <div class="card-body bt-border">
                        <div class="col-sm-12">
                            <h4>UDSA has four major components: </h4>
                            <ul>
                                <li>DHI alopecia test</li>
                                <li>Psychological Aspect</li>
                                <li>Dermatological aspect</li>
                                <li>Mathematical aspect</li>
                                
                            </ul>
                            
                            <p>A team of DHI experts that include administrative consultant, dermatologist, plastic surgeon, psychologist and endocrinologist are involved in the diagnostic process, according to each patient’s needs. A scientific board compiled of internationally renowned medical experts supervises and approves every treatment plan.</p>
                        </div>

                        <div class="row ml-lg-5 mr-lg-5 mb-lg-5 m-2 bg-col-w udsa">

                            <div class="col-sm-6 no-gutters">
                                <img src="image/Layer 105.jpg" class="img-fluid " alt="">
                            </div>
                            <div class="col-sm-6 ">
                                <div class="p-0">

                                    <h4 class="text-center">DHI Alopecia Test</h4>
                                    <p>Hair loss has major effects on patient’s lives and strong influence on their psychology. According to scientific researches, hair loss can lead on anxiety disorders or even depression.

                                    </p>
                                    <p>The DHI Hair Psychology Test was developed in collaboration with Professor of Psychiatry in the University of Athens, Charalambos Papageorgiou, during an in-depth study and exploration of psychiatric dimensions of hair diseases, in order to determine the necessity of combinatorial access of these conditions and any underlying psychopathology.</p>

                                </div>

                            </div>

                        </div>
                        <div class="row ml-lg-5 mr-lg-5 mb-lg-5 mt-lg-0 m-2 bg-col-w udsa">

                            <div class="col-sm-6 ">
                                <div class="p-0">

                                    <h4 class="text-center">DHI Hair Psychology Test</h4>
                                    <p>Hair loss has major effects on patient’s lives and strong influence oThe DHI Alopecia Test is performed by a team of DHI Specialists which includes a dermatologist, a plastic surgeon, an endocrinologist, a psychologist and an administrative consultant.

                                    </p>
                                    <p>The DHI Alopecia Test examines each case individually, including dermatological examination, mathematical aspects of available hairs in donor area over a lifetime and the psychological aspects, before determining the appropriate treatment plan.</p>
                                    <p>The whole diagnostic process and the approvement of every treatment plan is supervised by the DHI Scientific Board, compiled by internationally renowned medical experts.</p>

                                </div>

                            </div>
                            <div class="col-sm-6 no-gutters">
                                <img src="image/DSA-step-2.jpg" class="img-fluid " alt="">
                            </div>
                        </div>
                        <div class="row ml-lg-5 mr-lg-5 mb-lg-5 mt-lg-0 m-2 bg-col-w udsa">
                            <div class="col-sm-6 no-gutters">
                                <img src="image/DSA-step-3.jpg" class="img-fluid " alt="">
                            </div>
                            <div class="col-sm-6 ">
                                <div class="p-0">

                                    <h4 class="text-center">DHI Hair Biology Test</h4>
                                    <p>The Hair Biology Test is an extraordinary new aspect which provides answers as to whether oxidative stress has caused damage to the DNA in hair follicles of all those suffering from Androgenetic Alopecia.</p>
                                    <p>Oxidative stress occurs in the body when an imbalance exists between the proliferation of free radicals and the body’s natural ability to respond to and detoxify their detrimental effects by neutralizing them with antioxidants. Oxidative stress is thought to contribute to a number of health problems, including hair loss.</p>
                                    <!--   <p>The new Hair Biology Test was developed by DHI Global Medical Group in cooperation with a team of scientists at the Histology and Embryology department, School of Medicine, National University of Athens (UOA) led by the distinguished Prof. Vasilis Gorgoulis</p> -->

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>

    <section class="bottm_sec">
        <h1>Book Your UDSA Hair Loss Diagnosis Today!</h1>
        <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
    </section>

    <!-- FOOTER -->

    <?php include 'footer.php';?>

</body>

</html>