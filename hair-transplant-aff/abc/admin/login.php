<?php
include('../../../wp-config.php'); 
global $wpdb;
if(isset($_POST['submit_login']) && isset($_POST["user_email"]) && isset($_POST["user_password"]))
{
    $username = trim($_POST["user_email"]);
    $password = trim($_POST["user_password"]);
    $credentials = array('user_login' => $username, 'user_password' => $password);
    $user  = wp_signon($credentials,$secure_cookie);
    if($user->ID!='')
    {
        wp_redirect( 'index.php' );
    }
}

?>
<?php include_once('header.php'); ?>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">
          <form name="login" method="post" id="login" action="">
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input class="form-control" id="user_email" name="user_email" type="email" aria-describedby="emailHelp" placeholder="Enter email">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input class="form-control" id="user_password" name="user_password" type="password" placeholder="Password">
          </div>
<!--          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox"> Remember Password</label>
            </div>
          </div>-->
         <input type="submit" class="submit" name="submit_login" id="submit_login">
        </form>
<!--        <div class="text-center">
          <a class="d-block small mt-3" href="register.html">Register an Account</a>
          <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
        </div>-->
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper/popper.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
