<?php 
#################################################################
## Open Cart Module:  PAY U MONEY PAYMENT GATEWAY		       ##
##-------------------------------------------------------------##
## Copyright © 2014 MB "Programanija" All rights reserved.     ##
## http://www.opencartextensions.eu						       ##
## http://www.extensionsmarket.com 						       ##
##-------------------------------------------------------------##
## Permission is hereby granted, when purchased, to  use this  ##
## mod on one domain. This mod may not be reproduced, copied,  ##
## redistributed, published and/or sold.				       ##
##-------------------------------------------------------------##
## Violation of these rules will cause loss of future mod      ##
## updates and account deletion				      			   ##
#################################################################

class ControllerPaymentPayuMoney extends Controller {
	
	private $error = array(); 

	public function index() {
		
		$this->load->language('payment/payumoney');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('payumoney', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));

		}
		
		$text_array = array('heading_title','advert','text_edit','text_enabled','text_disabled','text_on','text_off','text_all_zones','text_live','entry_merchant_key',
		'entry_salt','entry_test','entry_total','entry_order_status','entry_geo_zone','entry_status','entry_sort_order','help_total',
		'button_save','button_cancel');
		
		foreach($text_array as $key){
			$data[$key] = $this->language->get($key);
		}
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['merchant_key'])) {
			$data['error_merchant_key'] = $this->error['merchant_key'];
		} else {
			$data['error_merchant_key'] = '';
		}

        if (isset($this->error['salt'])) {
			$data['error_salt'] = $this->error['salt'];
		} else {
			$data['error_salt'] = '';
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/payumoney', 'token=' . $this->session->data['token'], 'SSL')
   		);
				
		$data['action'] = $this->url->link('payment/payumoney', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['payumoney_merchant'])) {
			$data['payumoney_merchant_key'] = $this->request->post['payumoney_merchant_key'];
		} else {
			$data['payumoney_merchant_key'] = $this->config->get('payumoney_merchant_key');
		}
		
		if (isset($this->request->post['payumoney_salt'])) {
			$data['payumoney_salt'] = $this->request->post['payumoney_salt'];
		} else {
			$data['payumoney_salt'] = $this->config->get('payumoney_salt');
		}
		
		if (isset($this->request->post['payumoney_test'])) {
			$data['payumoney_test'] = $this->request->post['payumoney_test'];
		} else {
			$data['payumoney_test'] = $this->config->get('payumoney_test');
		}
		
		if (isset($this->request->post['payumoney_total'])) {
			$data['payumoney_total'] = $this->request->post['payumoney_total'];
		} else {
			$data['payumoney_total'] = $this->config->get('payumoney_total'); 
		} 
				
		if (isset($this->request->post['payumoney_order_status_id'])) {
			$data['payumoney_order_status_id'] = $this->request->post['payumoney_order_status_id'];
		} else {
			$data['payumoney_order_status_id'] = $this->config->get('payumoney_order_status_id'); 
		} 

		$this->load->model('localisation/order_status');
		
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		if (isset($this->request->post['payumoney_geo_zone_id'])) {
			$data['payumoney_geo_zone_id'] = $this->request->post['payumoney_geo_zone_id'];
		} else {
			$data['payumoney_geo_zone_id'] = $this->config->get('payumoney_geo_zone_id'); 
		} 
		
		$this->load->model('localisation/geo_zone');
										
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['payumoney_status'])) {
			$data['payumoney_status'] = $this->request->post['payumoney_status'];
		} else {
			$data['payumoney_status'] = $this->config->get('payumoney_status');
		}
		
		if (isset($this->request->post['payumoney_sort_order'])) {
			$data['payumoney_sort_order'] = $this->request->post['payumoney_sort_order'];
		} else {
			$data['payumoney_sort_order'] = $this->config->get('payumoney_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('payment/payumoney.tpl', $data));
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/payumoney')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->request->post['payumoney_merchant_key']) {
			$this->error['merchant_key'] = $this->language->get('error_merchant_key');
		}
			
		if (!$this->request->post['payumoney_salt']) {
			$this->error['salt'] = $this->language->get('error_salt');
		}
		
		return !$this->error;
		
	}	
}
?>