<?php
// Guest Order View
$_['guest_order_view']  		= 'Guest Order View';
$_['email_field']  				= 'Order Email:';
$_['order_field']  				= 'Order ID:';
$_['view_order']   				= 'View Order';
$_['heading_title']             = 'Order History';

// Email
$_['text_subject']				= '%s - Order Acknowledgement';
$_['text_email_1']				= 'Your order id is ';
$_['text_email_2']              = ' Please click on the link below to check your invoice.';
// Success
$_['text_success']				= 'Success: A confirmation email has been sent to %s. Please click the link in the email to view this order.';
// Error
$_['text_error']				= 'Error: Wrong email address or Order ID.';

// Separate Invoice View

// Heading
$_['heading_title']         = 'Order History';

// Text
$_['text_account']          = 'Account';
$_['text_order']            = 'Order Information';
$_['text_order_detail']     = 'Order Details';
$_['text_invoice_no']       = 'Invoice No.:';
$_['text_order_id']         = 'Order ID:';
$_['text_date_added']       = 'Date Added:';
$_['text_shipping_address'] = 'Shipping Address';
$_['text_shipping_method']  = 'Shipping Method:';
$_['text_payment_address']  = 'Payment Address';
$_['text_payment_method']   = 'Payment Method:';
$_['text_comment']          = 'Order Comments';
$_['text_history']          = 'Order History';
$_['text_empty']            = 'You have not made any previous orders!';
$_['text_errors']           = 'The order you requested could not be found!';
$_['text_invoice']          = 'Invoice';
$_['text_order_detail']     = 'Order Details';
$_['text_telephone']        = 'Telephone';
$_['text_emails']            = 'E-Mail';
$_['text_website']          = 'Web Site:';

// Column
$_['column_order_id']       = 'Order ID';
$_['column_product']        = 'No. of Products';
$_['column_customer']       = 'Customer';
$_['column_name']           = 'Product Name';
$_['column_model']          = 'Model';
$_['column_quantity']       = 'Quantity';
$_['column_price']          = 'Price';
$_['column_total']          = 'Total';
$_['column_action']         = 'Action';
$_['column_date_added']     = 'Date Added';
$_['column_status']         = 'Order Status';
$_['column_comment']        = 'Comment';
$_['column_product_name']   = 'Product';
$_['column_unit_price']     = 'Unit Price';
// Error
$_['error_reorder']         = '%s is not currently available to be reordered.';