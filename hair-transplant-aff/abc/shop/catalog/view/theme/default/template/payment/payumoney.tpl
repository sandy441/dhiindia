<?php 
/*
#################################################################
## Open Cart Module:  PAY U MONEY PAYMENT GATEWAY		       ##
##-------------------------------------------------------------##
## Copyright © 2014 MB "Programanija" All rights reserved.     ##
## http://www.opencartextensions.eu						       ##
## http://www.extensionsmarket.com 						       ##
##-------------------------------------------------------------##
## Permission is hereby granted, when purchased, to  use this  ##
## mod on one domain. This mod may not be reproduced, copied,  ##
## redistributed, published and/or sold.				       ##
##-------------------------------------------------------------##
## Violation of these rules will cause loss of future mod      ##
## updates and account deletion				      			   ##
#################################################################
*/ 
?>
<form action="<?php echo $action ?>" method="post">
<input type="hidden" name="key" value="<?php echo $payumoney_merchant_key;?>"/>
<input type="hidden" name="hash" value="<?php echo $hash;?>"/>
<input type="hidden" name="txnid" value="<?php echo $txnid;?>"/>
<input type="hidden" name="amount" value="<?php echo $amount;?>"/>
<input type="hidden" name="furl" value="<?php echo $furl;?>'">
<input type="hidden" name="surl" value="<?php echo $surl;?>">
<input type="hidden" name="curl" value="<?php echo $curl;?>">
<input type="hidden" name="service_provider" value="payu_paisa"/>
<input type="hidden" name="firstname" value="<?php echo $firstname ;?>"/>
<input type="hidden" name="phone" value="<?php echo $phone;?>"/>
<input type="hidden" name="email" value="<?php echo $email;?>"/>
<input type="hidden" name="pg" value="<?php echo $pg;?>"/>
<input type="hidden" name="productinfo" value="<?php echo $productinfo;?>"/>
	<div class="buttons">
        <div class="pull-right">
        	<input type="submit" value="<?php echo $button_confirm; ?>" class="btn btn-primary" />
        </div>
    </div>
</form>