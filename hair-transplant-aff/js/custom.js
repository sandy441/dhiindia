$(document).ready(function(){
	$('.owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		nav:true,
		responsive:{
			0:{
				items:1
			}
		}
	});
	$('.form_date').datetimepicker({
			language:  'fr',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0,
			startDate: '-0m'
		}).on('changeDate', function(e) {
			$('#consultationform').data('bootstrapValidator').revalidateField('appdate');
		});
		$('.form_time').datetimepicker({
			language:  'fr',
			weekStart: 1,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 1,
			minView: 0,
			maxView: 1,
			forceParse: 0,
		}).on('changeDate', function(e) {
			$('#consultationform').data('bootstrapValidator').revalidateField('apptime');
		});;
	$('#consultationform').bootstrapValidator({
			message: 'This value is not valid',
			feedbackIcons: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			fields: {
				name: {
					message: 'The username is not valid',
					validators: {
						notEmpty: {
							message: 'Username is required'
						},
						stringLength: {
							min: 5,
							max: 20,
							message: 'Username must be between 5 and 20 characters'
						},
					}
				},
				email: {
					validators: {
						notEmpty: {
							message: 'Email address is required'
						},
						emailAddress: {
							message: 'Invalid Email address'
						}
					}
				},
				contactnumber: {
					validators: {
						regexp: {
							regexp: /[0-9]{10}/,
							message: 'Valid 10 digit mobile number'
						}
					}
				},
				appdate: {
					validators: {
						notEmpty: {
							message: 'Date is required'
						},
						date: {
							format: 'DD/MM/YYYY'
						}
					}
				},
				apptime: {
					validators: {
						notEmpty: {
							message: 'Time is required'
						},
						time: {
							format: 'hh:ii'
						}
					}
				},
			}
		})
		.on('success.form.bv', function(e) {
			e.preventDefault();
			var $form = $(e.target);
			var bv = $form.data('bootstrapValidator');
			var contactnumber = $('#contactnumber').val();
			var OTPnumber = $('#OTPnumber').val();
			$('#mobilenumberverify').val(contactnumber);
			var consultationform = $('#consultationform').serialize();
			$('#SMSModal').modal('show'); 
			$.post($form.attr('action'), $form.serialize(), function(result) {
				console.log(result);
			}, 'json');
		});
		$('#otpform').bootstrapValidator({
			message: 'This value is not valid',
			feedbackIcons: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			fields: {
				otpnumberverify: {
					validators: {
						callback: {
							message: 'Wrong OTP',
							callback: function (value, validator, $field) {
								// Determine the numbers which are generated in captchaOperation
								var OTPnumbers = $('#OTPnumber').val();
								return value == OTPnumbers;
							}
						},
					}
				},
			}
		})
		.on('success.form.bv', function(e) {
			e.preventDefault();
			var $form = $(e.target);
			var consultationform = $("#consultationform").serialize();
			var bv = $form.data('bootstrapValidator');
			$.post($form.attr('action'), $("#consultationform,#otpform").serialize(), function() {
				console.log(result);
				}, 'json');
			window.location = 'thank-you.html';
		});
});

