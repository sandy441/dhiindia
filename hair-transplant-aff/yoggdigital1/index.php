<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
        <meta name="google-site-verification" content="fJ4vkC5vVXBSwS9M9C3sZ6aU0QgO6IOpqi1WjvicA-4" />
	<title>DHI</title>
	<link rel="icon" type="image/x-icon" href="img/favicon.png"/> 
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/owl.carousel.min.css" rel="stylesheet">
	<link href="css/owl.theme.default.min.css" rel="stylesheet">
	<link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
	<link href="css/landing-page.css" rel="stylesheet">
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="css/bootstrapValidator.css"/>
<!--	<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-71179767-1', 'auto');
	  ga('send', 'pageview');</script>-->
</head>

<body>
	<nav class="header-top">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="logo">
                                            <a href="https://www.dhiindia.com/hair-transplant-aff/abc/"><img src="img/logo.png" alt="logo"></a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
<!--					<div class="contactnos pull-right">
						<a href="tel:180010309300"><i class="fa fa-mobile"></i>1800 103 09300</a>
					</div>-->
				</div>
			</div>
		</div>
	</nav>
	<!--Banner-->
	<section class="banner">
		<div class="containter-fluid">
			<div class="row">
				<div class="bannerbg">
					<img src="img/banner.jpg" alt="dhi-banner">
					<div class="contactform">
						<div class="offer"><img src="img/offer.png" alt="offer"></div>
						<h1>BOOK YOUR CONSULTATION</h1>
                                                <form id="consultationform" name='consultationform' method="post" role="form" data-toggle="validator" action="app-submit.php">
								<div class="input-group col-lg-12">
									<input type="text" class="form-control" id="inputName" name="name" placeholder="Name" required>
								</div>	
								<div class="input-group col-lg-12">
									<input type="text" class="form-control" id="inputEmail" name="email" placeholder="Email" required>
								</div>	
								<div class="input-group col-lg-12">
									<input type="number" pattern="[0-9]{10}" class="form-control" id="contactnumber" name="contactnumber" placeholder="Mobile Number" required>
								</div>	
								<div class="input-group col-lg-12">
									<select  class="form-control" id="inputCity" name="city" required>
										<option value="">City</option>
										<option value="2">Delhi</option>
										<option value="20">Gurugram</option>
										<option value="21">Ludhiana</option>
										<option value="10">Jaipur</option>
										<option value="3">Bangalore</option>
										<option value="7">Kolkata</option>
										<option value="8">Chandigarh</option>
										<option value="9">Chennai</option>
										<option value="17">Guwahati</option>
										<option value="4">Ahmedabad</option>
									</select>
								</div>	
								<div  class="input-group date form_date col-lg-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dtp_input2" data-link-format="dd/mm/yyyy">
									<input class="form-control" size="16" type="text" value="" name="appdate" placeholder="Appointment Date" readonly>
									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
								</div>	
								<div class="input-group input-group date form_time col-lg-12" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
									<input class="form-control" size="16" type="text" name="apptime" value="" placeholder="Appointment Time" readonly>
									<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
								</div>
								<div style="text-align: left;" class="input-group" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3">
                                                                    <span style="text-align: left;"><input  size="5" type="radio" checked="true" name="payType" value="1" >&nbspPay by Credit card/Debit card/Net Banking/Wallet</span>
                                                                </div>
                                                                 <div style="text-align: left;" class="input-group" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3">
                                                                    <span style="text-align: left;"><input  size="5" type="radio" name="payType" value="2" >&nbspPay by Paytm</span>
                                                                </div>
                                                                 <input type="text" class="hidden form-control" id="OTPnumber" name="OTPnumber" placeholder="OTPnumber" value="<?php echo mt_rand(100000,999999); ?>">
								<input type="text" class="hidden form-control" id="formname" name="formnameconsultation" value="abc">
                                                                <input type="text" class="hidden form-control" id="leadsourcsname" name="leadsourcsname"  value="abc">
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/Banner-->
	<!--Services-->
	<section class="services">
		<div class="container">
			<div class="row">
				<h2>World's Safest & Most Advanced Permanent Solution To Hair Loss</h2>
				<div class="col-xs-12 col-sm-6 usplist">
					<ul>
						<li>46 years of excellence</li>
						<li>250000+ delighted clients</li>
						<li>60+ locations in 30 countries</li>
						<li>No cuts. No Pain. No Scar. No Stiches</li>
						<li>EMI option available at zero interest rate</li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-6 locationmap">
					<img src="img/map.png" alt="location">
				</div>
			</div>
			<div class="row">
				<h2>Our Services</h2>
				<div class="servicelist">
					<ul>   
						<li><img alt="service1" src="img/s1.png">Direct Hair Implantation</li>
						<li><img alt="service2" src="img/s2.png">PRP Growth</li>
						<li><img alt="service3" src="img/s3.png">Direct Hair Fusion</li>
						<li><img alt="service4" src="img/s4.png">Factor Beard Restoration </li>
					</ul>
				</div>
				<div class="col-xs-12 testimonial">
					<div class="owl-carousel owl-theme">
						<div class="item">
							<ul>
								<li><img src="img/testimonial1.png" alt="testimonial1"></li>
								<li>
									<blockquoute><i class="fa fa-quote-left"></i>I lost my hair due to extreme heat and sweat while playing & had to hide it behind a bandana. DHI rchanged my life!<i class="fa fa-quote-right"></i></blockquoute>
									<p class="textright">Padma Shree Sreejesh Ravindran</p>
									<p class="textright"><i>(Captain, Indian Hockey Team)</i></p>
								</li>
							</ul>
						</div>
						<div class="item">
							<ul>
								<li><img src="img/testimonial1.png" alt="testimonial2"></li>
								<li>
									<blockquoute><i class="fa fa-quote-left"></i>I lost my hair due to extreme heat and sweat while playing & had to hide it behind a bandana. DHI rchanged my life!<i class="fa fa-quote-right"></i></blockquoute>
									<p class="textright">Padma Shree Sreejesh Ravindran</p>
									<p class="textright"><i>(Captain, Indian Hockey Team)</i></p>
								</li>
							</ul>
						</div>
						<div class="item">
							<ul>
								<li><img src="img/testimonial1.png" alt="testimonial3"></li>
								<li>
									<blockquoute><i class="fa fa-quote-left"></i>I lost my hair due to extreme heat and sweat while playing & had to hide it behind a bandana. DHI rchanged my life!<i class="fa fa-quote-right"></i></blockquoute>
									<p class="textright">Padma Shree Sreejesh Ravindran</p>
									<p class="textright"><i>(Captain, Indian Hockey Team)</i></p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
            <div style="text-align: center;"><input onclick="topFunction()" class="btn backtotop btn-primary" type="button" value="Back to Top"></div>
	</section>
	<!--/Services-->
	<!-- Footer -->
	<section class="footer">
		<div class="container">
			<div class="row pdtb15">
				<div class="citypresence citytype">
					NATIONAL PRESENCE
				</div>
				<div class="citypresence citieslist">
					<p>Ahmedabad</p><p>Banglore</p><p>Calicut</p><p>Chandigarh</p><p>Chennai</p><p>Delhi</p><p>Gurgaon</p><p>Guwahati</p><p>Hyderabad</p><p>Jaipur</p><p>Kochi</p><p>Kolkata</p><p>Ludhiana</p><p>Lucknow</p><p>Mumbai</p><p>Pune</p><p>Sharanpur</p><p>Surat</p><p>Vijayawada</p>
				</div>
			</div>
			<div class="row pdtb15">
				<div class="citypresence citytype">
					GLOBAL PRESENCE
				</div>
				<div class="citypresence citieslist">
					<p>Australia</p><p>Bahrain</p><p>Belgium</p><p>Bulgaria</p><p>Colombia</p><p>Egypt</p><p>France</p><p>Germany</p><p>Greece</p><p>Hong Kong</p><p>India</p><p>Indonesia</p><p>Isle of Man</p><p>Ireland</p><p>Italy</p><p>Jordan |Jersey</p><p>Montenegro</p><p>Morocco</p><p>Mauritius</p><p>Malaysia</p><p>Netherlands</p><p>Oman</p><p>Panama</p><p>Philippines</p><p>Portugal</p><p>Qatar</p><p>Saudi Arabia</p><p>Switzerland</p><p>Turkey</p><p>United Arab Emirates</p><p>United Kingdom</p>
				</div>
			</div>
			<div class="row">
				<div class="footerbottom">
					<div class="copyright">
						*DISCLAIMER : Results might vary from case to case
					</div>
					<div class="copyright">
                                            <a href="https://www.dhiindia.com/terms-conditions/">*Terms & Conditions</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.dhiindia.com/privacy/">Privacy Policy</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.dhiindia.com/cookie-policy/">Cookie Policy</a>
					</div>
					<div class="copyright">
						Copyright &copy; 2014 DHI, All Rights Reserved
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /Footer -->
	<!-- OTP Popup -->
<!--	<div class="modal fade" id="SMSModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">A One-Time Password has been generated and sent to your registered mobile number. Please enter it here to take the process further.</h5>
			</div>
			<div class="modal-body">
                            <form id="otpform" method="post" class="otpform" action="app-submit.php">
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">Mobile Number:</label>
						<input type="text" class="form-control" id="mobilenumberverify" readonly>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">OTP</label>
						<input type="number" class="form-control" id="otpnumberverify" name="otpnumberverify">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-primary">Book Appointment</button>
					</div>
					<input type="text" class="hidden form-control" name="formnameotp" value="otpform">
				</form>
			</div>
		</div>
	  </div>
	</div>-->
	<!-- /OTP Popup -->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/owl.carousel.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
	<script type="text/javascript" src="js/bootstrapValidator.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
        <script>
        function topFunction() {
            jQuery('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
}
        </script>
</body>
</html>
