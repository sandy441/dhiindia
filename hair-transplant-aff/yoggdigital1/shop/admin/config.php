<?php
// HTTP
define('HTTP_SERVER', 'https://www.dhiindia.com/hair-transplant-aff/abc/shop/admin/');
define('HTTP_CATALOG', 'https://www.dhiindia.com/hair-transplant-aff/abc/shop/');

// HTTPS
define('HTTPS_SERVER', 'https://www.dhiindia.com/hair-transplant-aff/abc/shop/admin/');
define('HTTPS_CATALOG', 'https://www.dhiindia.com/hair-transplant-aff/abc/shop/');

// DIR
define('DIR_APPLICATION', 'c:/inetpub/wwwroot/new/hair-transplant-aff/abc/shop/admin/');
define('DIR_SYSTEM', 'c:/inetpub/wwwroot/new/hair-transplant-aff/abc/shop/system/');
define('DIR_LANGUAGE', 'c:/inetpub/wwwroot/new/hair-transplant-aff/abc/shop/admin/language/');
define('DIR_TEMPLATE', 'c:/inetpub/wwwroot/new/hair-transplant-aff/abc/shop/admin/view/template/');
define('DIR_CONFIG', 'c:/inetpub/wwwroot/new/hair-transplant-aff/abc/shop/system/config/');
define('DIR_IMAGE', 'c:/inetpub/wwwroot/new/hair-transplant-aff/abc/shop/image/');
define('DIR_CACHE', 'c:/inetpub/wwwroot/new/hair-transplant-aff/abc/shop/system/cache/');
define('DIR_DOWNLOAD', 'c:/inetpub/wwwroot/new/hair-transplant-aff/abc/shop/system/download/');
define('DIR_UPLOAD', 'c:/inetpub/wwwroot/new/hair-transplant-aff/abc/shop/system/upload/');
define('DIR_LOGS', 'c:/inetpub/wwwroot/new/hair-transplant-aff/abc/shop/system/logs/');
define('DIR_MODIFICATION', 'c:/inetpub/wwwroot/new/hair-transplant-aff/abc/shop/system/modification/');
define('DIR_CATALOG', 'c:/inetpub/wwwroot/new/hair-transplant-aff/abc/shop/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '!123Dhiitdepartment123!');
define('DB_DATABASE', 'shop_aff');
define('DB_PORT', '3306');
define('DB_PREFIX', '');
