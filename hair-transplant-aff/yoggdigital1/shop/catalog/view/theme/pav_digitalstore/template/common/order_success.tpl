<?php echo $header; ?>
<div class="container">
    <?php require( PAVO_THEME_DIR."/template/common/config_layout.tpl" );  ?>
  <?php require( PAVO_THEME_DIR."/template/common/breadcrumb.tpl" );  ?>
  <div class="row"><?php if( $SPAN[0] ): ?>
			<aside id="sidebar-left" class="col-md-<?php echo $SPAN[0];?>">
				<?php echo $column_left; ?>
			</aside>	
		<?php endif; ?> 
  
   <section id="sidebar-main" class="col-md-<?php echo $SPAN[1];?>"><div id="content"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php echo $text_message; ?>
      <div class="buttons">
        <div class="pull-left"><a href="<?php echo $phref; ?>" class="button"><?php echo $button_print; ?></a></div>
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
   </section> 
<?php if( $SPAN[2] ): ?>
	<aside id="sidebar-right" class="col-md-<?php echo $SPAN[2];?>">	
		<?php echo $column_right; ?>
	</aside>
<?php endif; ?></div>
</div>
<!-- Facebook Pixel Code 20-3-2017 -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window,document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	 fbq('init', '424471077707230'); 
	 fbq('track', 'PageView');
	 fbq('track', 'Purchase', {value: '<?php echo $totalamount; ?>', currency: 'INR'});
	</script>
	<noscript>
	 <img height="1" width="1" 
	src="https://www.facebook.com/tr?id=424471077707230&ev=PageView
	&noscript=1"/>
	</noscript>
<?php if(isset($orderDetails) && isset($orderProducts)) { ?>
<?php
$order_id = $orderDetails['order_id'];
$total = $orderDetails['total'];
$order_shipping_total = 0;
?>

<script type="text/javascript">
var google_tag_params = {
    ecomm_pagetype: 'purchase',
    ecomm_totalvalue: "<?php echo $orderDetails['total'] ?>",
};
</script>

<script type="text/javascript">
    ga('require', 'ecommerce');

    ga('ecommerce:addTransaction', {
          'id': "<?php echo $orderDetails['order_id'] ?>",
          'affiliation': 'Shop Dhiindia',
          'revenue': "<?php echo $orderDetails['total'] ?>",
          'shipping': '0',
          'tax': '0',
          'currency': 'INR'
    });
        <?php foreach ( $orderProducts as $item ) { ?>
            ga('ecommerce:addItem', {
                 'id': "<?php echo $item['order_id'] ?>",
                 'name': "<?php echo $item['name'] ?>",
                 'sku': "<?php echo $item['sku'] ?>",
                 'category': '',
                 'price': "<?php echo $item['price']+($item['price']*$item['tax']/100); ?>",
                 'quantity': "<?php echo $item['quantity'] ?>"
});
        <?php } ?>

    ga('ecommerce:send');
</script>
<?php } ?>
<?php echo $footer; ?>