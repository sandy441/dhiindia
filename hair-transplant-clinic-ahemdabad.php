
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Looking for Hair transplant clinic in Ahmadabad? DHI™ India provides hair restoration, hair regrowth &amp; hair loss treatment with advance DHI™ technique. ✓No Cuts ✓No Scars ✓Trained Doctors
              ">
        <meta name="author" content="">
        <title>Hair Transplant Clinic &amp; Hair Loss Treatment in Ahmedabad – DHI™ India
            
        </title>
<?php include 'header.php';?>
    </head>
    <body>
        
        
        <section class="location bgLocation9">
            <div class="container">
                <div class="row ">
                    <div class="col-sm-6 ">
                        
                        
                    </div>
                    <div class="col-sm-6">
                      <?php include 'appointmentForm.php';?>
                    </div>
                </div>
            </div>
        </section>
        <section class="bg-col-1">
            <div class="container">
                <div class="content">
                    <div class="card ">
                        <div class="card-body"> 
                            
                            <div class="row">
                                <div class="col-sm-4 p-5">
                                    <address>
                                        Newtouch Laser Center Satellite<br />
                                        108 Sangrila Arcade, Near Shyaml Char Rasta<br />
                                        Ahmedabad 380015<br />
                                        079 – 40062549 & 8511100962<br />
                                        Email: info@dhiindia.com, enquiry@dhiindia.com
                                    </address>
                                </div>
                                <div class="col-sm-8">
                                    <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3672.2688626184704!2d72.52655791423625!3d23.013898384957418!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e9b27bf6b5845%3A0x12dfcb181412562e!2sDHI+Hair+Transplantation+Ahmedabad!5e0!3m2!1sen!2sin!4v1501068118708" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen" class="__web-inspector-hide-shortcut__"></iframe>
                                </div>
                            </div>
                            
                            
                            
                            
                            
                            
                            
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="card ">
                        <div class="card-body"> 
                            <h3>DHI Hair Transplant Clinic — AHMEDABAD</h3>
                            <ul>
                                <li>DHI Ahmedabad offers a complete range of hair transplant treatments by MCI registered doctors. Our procedures include Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, and Direct Hair Fusion among others..</li>
                                <li>
                                    If you are also suffering from any type of hair loss, then pay us a visit at our hair restoration clinic in Ahmedabad for guaranteed solutions. All consultations are confidential and include hair loss evaluation test to help our clients make an informed decision. As hair loss disorders may vary from person to person, our doctors develop a diagnosis and determine best solutions for you
                                    
                                </li>
                                <li>Ideally located in the heart of Shyaml Char Rasta in Ahmedabad, DHI Ahmedabad is accessible to people residing in any part of the city. Ahmedabad Railway station is 8.5 km from our location in the city via Relief Road, 9.8 km via Vikas Gruh Road, and 11.2 km via NH 228.
                                </li>
                                <li>To book an appointment for a consultation at our DHI Ahmedabad clinic, give us a call now!</li>
                                <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                
                
                <div class="content">
                    <div class="card ">
                        <div class="card-body "> 
                            <h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
                            <div class="row">
                                <div class="col-sm-6 pr-lg-0 pr-md-0">
                                    <img src="image/clinic/DHI-Waiting-Area.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/IMG_20170728_182342.jpg" alt="" class="img-fluid"></div>
                                <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC_0259.jpg" alt="" class="img-fluid"></div>
                                <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/IMG_20170728_182302.jpg " alt="" class="img-fluid"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="content">
                    <div class="card ">
                        <div class="card-body bt-border"> 
                            <h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>
                            
                            <div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="row ">
                                            <div class="col-sm-6">
                                                <h4>Rahul Mishra</h4>
                                                <p>DHI hair transplant is undoubtedly the easiest way to get your self-esteem back after heart-wrenching hair loss. I am very pleased to see DHI flourishing in the heart of ahemdabad.</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4>Kunal Sharma</h4>
                                                <p>DHI Guwahati is helmed by a team of doctors certified by London Hair Restoration Academy. They are very professional and understand exactly what you want out of the procedure.</p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h4>Ajay Dixit</h4>
                                                <p>Baldness leaves you feeling frustrated. But all it takes is just a touch of DHI Guwahati team to restore your confidence back. The new me is the best of me. Thanks DHI Ahemdabad.</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4>Mahim Chaudhary</h4>
                                                <p>The ambiance of Ahemdabad is very soothing. It helped me get rid of all the apprehensions I had about the procedure initially. The staff was also friendly and supportive.</p>
                                            </div>
                                            
                                        </div> 
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h4>Punit Verma</h4>
                                                <p>There DHI Ahemdabad clinic has a very positive vibe. You start feeling it as soon as you step inside the premises.</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4>Vijay Desai</h4>
                                                <p>State of the art facilities, world-class doctors, and professional staff all make DHI Ahemdabad the best hair transplant center in the country. I can’t wait to undergo the DHI procedure.</p>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            
            
            
        </div>
        
        
    </section>
    
    
    <div class="clearfix"></div>
    
    
    
    <!-- FOOTER -->
    
<?php include 'footer.php';?>
    
    
    
    
</body>
</html>
