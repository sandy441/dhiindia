
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Looking for Hair transplant clinic in Bangalore? DHI™ India provides hair restoration, hair regrowth &amp; hair loss treatment with advance DHI™ technique. ✓ No Cuts ✓ No Marks ✓ 100% Natural Results
              ">
        <meta name="author" content="">
        <title>Hair Transplant Clinic &amp; Hair Loss Treatment in Bangalore – DHI™ India
            
        </title>
<?php include 'header.php';?>
    </head>
    <body>
        
        
        <section class="location bgLocation4">
            <div class="container">
                <div class="row ">
                    <div class="col-sm-6 ">
                        
                        
                    </div>
                    <div class="col-sm-6">
                       <?php include 'appointmentForm.php';?>
                    </div>
                </div>
            </div>
        </section>
        <section class="bg-col-1">
            <div class="container">
                <div class="content">
                    <div class="card ">
                        <div class="card-body"> 
                            
                            <div class="row">
                                <div class="col-sm-4 p-5">
                                    <address>
                                        4/1 Walton Road (Above Cafe Coffee Day)<br />
                                        1st Floor, Lavelle Junction<br />
                                        Bangalore 560001<br />
                                        080 – 22270046/ 47 & +91 9686115381<br />
                                        Email: info@dhiindia.com, enquiry@dhiindia.com
                                    </address>
                                </div>
                                <div class="col-sm-8">
                                    <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.005762033987!2d77.59572351482196!3d12.971482890856004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1ca246babbadf5d8!2sDhiindia-+Best+Hair+Transplant+In+Bangalore+%26+Hair+Loss+Treatment+Clinic+In+Bangalore!5e0!3m2!1sen!2sin!4v1501055679425" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                                </div>
                            </div>
                            
                            
                            
                            
                            
                            
                            
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="card ">
                        <div class="card-body"> 
                            <h3>DHI Hair Transplant Clinic — BANGALORE</h3>
                            <ul>
                                <li>Beaming with pride amid corporate vibes of the magnificent Silicon Valley of India, DHI Bangalore continues to transform the lives of people through its advanced hair transplant procedures and infrastructure. Situated above Coffee Café Day on the Walton Road, we are equipped with futuristic facilities that help us innovate and deliver some path-breaking solutions. Restoring your hairs and confidence by adhering to DHI’s safety protocols is our ultimate goal.</li>
                                <li>
                                    Anchored by an unmatched legacy of success, our DHI hair transplant clinic in Bangalore is frequently visited by celebrities in Bollywood, Tollywood, television, and sports. All hair restoration services including Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, and Direct Hair Fusion are the industry gold standard and set us apart from the rest in the field.
                                    
                                </li>
                                <li>Doctors at DHI Bangalore are MCI registered. They provide you with the personalized attention to help you understand the root of your hair loss problems. As every hair loss case differs from another, we have personalized treatment solutions for different individuals. Come and witness a transformation in you from the day one.
                                </li>
                                <li>It takes just 6 minutes to travel from Bangalore City Junction Railway Station to our hair transplant clinic in Lavelle Junction. Total driving distance is 5 km. Parking is available on the premises.</li>
                                <li>To book an appointment for a consultation at our DHI Bangalore clinic, give us a call now!</li>
                                <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                
                
                <div class="content">
                    <div class="card ">
                        <div class="card-body "> 
                            <h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
                            <div class="row">
                                <div class="col-sm-6 pr-lg-0 pr-md-0">
                                    <img src="image/clinic/DSC_0261.jpg" alt="1" class="img-fluid">
                                </div>
                                <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_0262.jpg" alt="2" class="img-fluid"></div>
                                <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC_0259.jpg" alt="3" class="img-fluid"></div>
                                <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_0295.jpg " alt="4" class="img-fluid"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="content">
                    <div class="card ">
                        <div class="card-body bt-border"> 
                            <h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>
                            
                            <div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="row ">
                                            <div class="col-sm-6">
                                                <h4>Vikas KT</h4>
                                                <p>DHI crew was very professional, they were always ready and available to provide the advice needed. Entire procedure was explained in detail prior to the surgery and the procedure was carried out professionally. It has been a month after the transplantation, hairs have started growing naturally, waiting for the final results.</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4>Nikhil Ubhe</h4>
                                                <p>I had very good experience with DHI, very professional in this field. I did my PRP treatment at DHI Bangalore and it really helped to retain and grow my hair. All precautionary measures were taken of highest standards before conducting the treatment, and consultant and doctors were very helpful and knowledgable in guiding me through the treatment and making me understand the cause of hair fall. I recommend people to go to DHI for their hair related issues without any second thought.</p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h4>Mahesh S</h4>
                                                <p>I would personally recommend people with accute hair fall to go with dhi the staff and doctors are extremely good and have good knowledge of hair related aspects. The causes of losses are clearly explained with all the remedial measures to take up. The session conducted are taken up with proper hygiene and intensive care is ensured for clients undergoing procedure. The results are good and appreciable.Hope dhi bangalore would continue to endeavour the services to their client.</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4>Umar Farookh Sardar</h4>
                                                <p>I am very much happy with the results of hair transplant from DHI bangalore. All staff @ DHI is very friendly and the doctor explained very well about the procedure during consultation. Thanks to DHI for changing my life!!!!. I strongly recommend DHI for those with hairloss</p>
                                            </div>
                                            
                                        </div> 
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h4>Prashant Awasthi</h4>
                                                <p>I took hair transplant last year and I am happy with it’s result.no side effects after that. DHI is best among the other hair transplant clinic.</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4>Sumit Dawas</h4>
                                                <p>DHI is a leading name in hair regrowth treatment in Bangalore. One of my friend regain his hair after the DHI’s hair transplant surgery treatment. The staff is also very friendly.</p>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            
            
            
        </div>
        
        
    </section>
    
    
    <div class="clearfix"></div>
    
    
    
    <!-- FOOTER -->
    
<?php include 'footer.php';?>
    
    
    
    
</body>
</html>
