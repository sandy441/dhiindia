
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Looking for Hair transplant clinic in Calicut? DHI™ India provides hair restoration, hair regrowth &amp; hair loss treatment with advance DHI™ technique. ✓No Cuts ✓No Scars ✓Trained Doctors">
        <meta name="author" content="">
        <title>Hair Transplant Clinic &amp; Hair Loss Treatment in Calicut – DHI™ India
        </title>
<?php include 'header.php';?>
    </head>
    <body>
        
        
        <section class="location bgLocation13">
            <div class="container">
                <div class="row ">
                    <div class="col-sm-6 ">
                        
                        
                    </div>
                    <div class="col-sm-6">
                <?php include 'appointmentForm.php';?>
                    </div>
                </div>
            </div>
        </section>
        <section class="bg-col-1">
            <div class="container">
                <div class="content">
                    <div class="card ">
                        <div class="card-body"> 
                            
                            <div class="row">
                                <div class="col-sm-4 p-5">
                                    <address>
                                        Dr. Rafeek’s Skin and Cosmetic Center<br />
                                        Near Malayala Manorama, East Nadakkavu <br />
                                        Wayanad Road<br />
                                        Calicut 673017<br />
                                        0495 6533302<br />
                                        Email: info@dhiindia.com, enquiry@dhiindia.com
                                    </address>
                                </div>
                                <div class="col-sm-8">
                                    <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3912.887002597254!2d75.77579961480518!3d11.269715991987932!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba65ecb5828f561%3A0xf7cff16fb95fd2a5!2sDr.+Rafeeq's+Skin+%26+Cosmetic+Surgery+Research+Centre!5e0!3m2!1sen!2sin!4v1501071871158" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="card bt-border">
                        <div class="card-body "> 
                            <h3>DHI Hair Transplant Clinic — CALICUT</h3>
                            <ul>
                                <li>DHI hair transplant clinic in Calicut is specially designed to offer the best in class hair transplant solutions to clients from all walks of life using the latest technologies. We are anchored by a team of MCI registered doctors seasoned by years of experience.</li>
                                <li>
                                    DHI Calicut operates from Dr. Rafeek’s Skin and Cosmetic Center, which is the only ISO, UKAS, NABCB certified Dermato-cosmetology and Cosmetic Surgery Research Center in South India. Our clinic is equipped with state of the art medical facilities. There are changing rooms and showers along with facilities for relaxation before and after the treatment. We provide clients with a wide range of hair transplant services including Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, and Direct Hair Fusion among others.
                                    
                                </li>
                                <li>DHI Calicut clinic is a 3.7 km drive from the Calicut Railway station via Bank Road and Beypore. It will take less than 13 minutes to cover the distance. Prominently located in the heart of vibrant Calicut, we are easily accessible from all parts of the city.
                                </li>
                                
                                
                                <li>To book an appointment for a consultation at our DHI Mumbai clinic, give us a call now!</li>
                                <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                
                
                <!-- <div class="content">
          <div class="card ">
                    <div class="card-body "> 
        <h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
        <div class="row">
          <div class="col-sm-6 pr-lg-0 pr-md-0">
          <img src="image/clinic/DSC_001.jpg" alt="" class="img-fluid">
        </div>
          <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_002.jpg" alt="" class="img-fluid"></div>
          <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC_003.jpg" alt="" class="img-fluid"></div>
          <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_004.jpg " alt="" class="img-fluid"></div>
        </div>
                    </div>
                  </div>
                </div> -->
                
                
                
            </div>
            
            
            
            
        </div>
        
        
    </section>
    
    
    <div class="clearfix"></div>
    
    
    
    <!-- FOOTER -->
    
<?php include 'footer.php';?>
    
    
    
    
</body>
</html>
