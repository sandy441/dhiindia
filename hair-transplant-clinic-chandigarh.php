
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Looking for Hair transplant clinic in Chandigarh? DHI™ India provides hair restoration, hair regrowth &amp; hair loss treatment with advance DHI™ technique.
              ">
        <meta name="author" content="">
        <title>Hair Transplant Clinic &amp; Hair Loss Treatment in Chandigarh – DHI™ India
            
        </title>
<?php include 'header.php';?>
    </head>
    <body>
        
        
        <section class="location bgLocation11">
            <div class="container">
                <div class="row ">
                    <div class="col-sm-6 ">
                        
                        
                    </div>
                    <div class="col-sm-6">
                        
                    <?php include 'appointmentForm.php';?>
                    </div>
                </div>
            </div>
        </section>
        <section class="bg-col-1">
            <div class="container">
                <div class="content">
                    <div class="card ">
                        <div class="card-body"> 
                            
                            <div class="row">
                                <div class="col-sm-4 p-5">
                                    <address>
                                        Headmasters – Madhya Marg
                                        SCO 16-19, Sector 8-C
                                        Chandigarh 160017, India<br />
                                        0172 – 4008553 & +91 8284818844<br />
                                        Email: info@dhiindia.com, enquiry@dhiindia.com
                                    </address>
                                </div>
                                <div class="col-sm-8">
                                    <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3429.251438467437!2d76.79229931444867!3d30.739437981634822!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390fed7330d438e7%3A0xe4d0e8bc86b1053!2sDHIIndia-+Hair+Transplant+Surgery+Clinic+%7C+Hair+Loss+Treatment!5e0!3m2!1sen!2sin!4v1501069365325" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="card ">
                        <div class="card-body "> 
                            <h3>DHI Hair Transplant Clinic — CHANDIGARH</h3>
                            <ul>
                                <li>DHI India welcomes you on your exotic journey to transformation at our clinic in Chandigarh. Join us to discover permanent solutions for hair loss now.</li>
                                <li>
                                    DHI Chandigarh is located in the heart of the capital city, providing easy access to transport networks for complete convenience to local clients and those coming from outside the city.
                                    
                                </li>
                                <li>We are situated in the award-winning Headmasters Salon building, known for its high-quality services in line with European standards. Headmasters is one of the country’s largest wellness spas—a one stop destination for premium skin, hair, and body services. This place is frequented by Bollywood stars on regular basis.
                                </li>
                                <li>Our clinic is designed to provide you with the highest standards of care and advanced treatment solutions without compromising on your safety. DHI team is trained to ensure you a relaxed ambiance and help you make an informed decision.</li>
                                <li>We utilize most advanced technologies to offer you best results for a lifetime. All consultations with us are confidential including complete hair loss evaluation test.</li>
                                <li>DHI Chandigarh is a 14.3 km drive from Shaheed Bhagat Singh International Airport in Ludhiana via Sukhna Panth. It takes about 34 minutes to reach at our clinic from the airport. The driving distance from the Railway Station to our clinic is just 7.6 km.</li>
                                <li>We understand how depressing hair loss can be for people. This is why we have brought you guaranteed solutions to not only restore hairs but also confidence. Whether you are at the first stage of hair loss or at the advanced stage, our procedures will make you fall in love with yourself once again. We offer Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, and Direct Hair Fusion among others.</li>
                                <li>To book an appointment for a consultation at our DHI Mumbai clinic, give us a call now!</li>
                                <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                
                
                <!-- <div class="content">
          <div class="card ">
                    <div class="card-body "> 
        <h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
        <div class="row">
          <div class="col-sm-6 pr-lg-0 pr-md-0">
          <img src="image/clinic/DSC_001.jpg" alt="" class="img-fluid">
        </div>
          <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_002.jpg" alt="" class="img-fluid"></div>
          <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC_003.jpg" alt="" class="img-fluid"></div>
          <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_004.jpg " alt="" class="img-fluid"></div>
        </div>
                    </div>
                  </div>
                </div> -->
                
                
                <div class="content">
                    <div class="card ">
                        <div class="card-body bt-border"> 
                            <h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>
                            
                            <div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="row ">
                                            <div class="col-sm-6">
                                                <h4>Harpreet Singh</h4>
                                                <p>I have recently got my transplant from DHI 6 months ago and i am really happy with the results, its has boosted me with more confidence,Great job.</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4>Navu Ghuman</h4>
                                                <p>My brother has got his session done from DHI a year back, he has got an got an amazing results, we all are very happy for his decision in choosing DHI for his treatment,it was absolutely painless with no scarring at all, i would recommend all to visit DHI once atleast to keep urself motivated,,,,,KEEP IT UP</p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h4>Kamaljeet Arora</h4>
                                                <p>DHI is a leading hair transplantation clinic in Chandigarh. My uncle got his hair transplanted from DHI clinic the result is good.</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4>Aanchal Kumar</h4>
                                                <p>It’s an amazing clinic. From consultancy to treatment everything was too good. I remember when i came along with my cousin there we got an excellent and convincing consultation which enabled us to go with their clinic; my cousin had his hair transplant session which was painless and the results were too good.</p>
                                            </div>
                                            
                                        </div> 
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h4>Meenal Saini</h4>
                                                <p>My brother got hiss transplant two years back from DHI and we are very happy with his results. DHI has got the most advanced technique without any pain and scars. keep it up!!</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4>Shivani Sharma</h4>
                                                <p>DHI is the best clinic for hair transplant, my father has got it done last year & now his looks has been entirely changed , he has started looking more confident & motivated, thanks to all the DHI family…..Cheers to dhi heroes</p>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            
            
            
        </div>
        
        
    </section>
    
    
    <div class="clearfix"></div>
    
    
    
    <!-- FOOTER -->
    
<?php include 'footer.php';?>
    
    
    
    
</body>
</html>
