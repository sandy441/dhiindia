
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Looking for Hair restoration clinic in Chennai? DHI™ India leaders in hair transplant, hair growth &amp; baldness cure with best Cost/Price of hair transplant.
">
    <meta name="author" content="">
      <title>Hair Transplant Clinic &amp; Hair Loss Treatment in Chennai – DHI™ India

</title>
<?php include 'header.php';?>
    </head>
  <body>


<section class="location bgLocation6">
  <div class="container">
    <div class="row ">
      <div class="col-sm-6 ">
      
       
      </div>
      <div class="col-sm-6">
<?php include 'appointmentForm.php';?>
      </div>
    </div>
  </div>
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card ">
            <div class="card-body"> 
              
<div class="row">
  <div class="col-sm-4 p-5">
  <address>
               2nd Floor, Sheriff Towers<br />
                No. 28/118, GN Chetty Road, T.Nagar<br />
                Chennai 600017<br />
               Email: info@dhiindia.com, enquiry@dhiindia.com
              </address>
  </div>
  <div class="col-sm-8">
    <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3886.8135931841693!2d80.2420699143762!3d13.047534290806084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a526652085d52b9%3A0xd6dd8677d151c617!2sDHIIndia-+Hair+Transplant+In+Chennai+%26+Hair+Loss+Treatment+Clinic+In+Chennai!5e0!3m2!1sen!2s!4v1501064595823" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
  </div>
</div>

  </div>
            </div>
          </div>
            <div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 
<h3>DHI Hair Transplant Clinic — CHENNAI</h3>
<ul>
  <li>DHI Chennai is an epitome of high quality and flawless perfection. With our team of trained and certified doctors, we ensure you quality hair transplant procedures and personalized attention to begin your inspiring journey of personal transformation.</li>
  <li>
DHI Chennai has received multiple recognitions and awards for consistently delivering path-breaking solutions. Our team offers help with Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, and Direct Hair Fusion among other hair related disorders.

</li>
  <li>We are situated on the 2nd floor of Sheriff Towers located prominently in the heart of Chennai’s vibrant city—T. Nagar, which is just 10 km from Chennai International Airport and approximately 8 km from Chennai Central Railway station. Big textile showrooms, plush jewelry outlets, and parks all come together to give a distinct character to this place. This city’s proximity to Saidapet, West Mambalam, and Kodambakkam gives us an opportunity to serve outstation clients as well.
</li>
  <li>We understand how disheartening hair loss can be for you. This is why we have a range of comprehensive hair loss solutions for all types of cases. It is time to restore not only your hairs but also your confidence.</li>

<li>To book an appointment for a consultation at our DHI Mumbai clinic, give us a call now!</li>
  <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
  
</ul>
            </div>
          </div>
        </div>


        <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
<div class="row">
  <div class="col-sm-6 pr-lg-0 pr-md-0">
  <img src="image/clinic/DSC7160.jpg" alt="" class="img-fluid">
</div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC7156.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC7159.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC7166.jpg" alt="" class="img-fluid"></div>
</div>
            </div>
          </div>
        </div>


        <div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 
<h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>

<div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row ">
        <div class="col-sm-6">
          <h4>Hari Hanuman</h4>
          <p>Procedure is painless and staff are very helpful and friendly.. Khizar is giving suggestions and tips even when I am in abroad.. Now waiting results.</p>
        </div>
        <div class="col-sm-6">
          <h4>ARUL SELVA KUMAR.F</h4>
          <p>Very happy with the results of PRP and LASER. My hair fall stopped. density increased. now doing only maintanance treatment.</p>
        </div>
        
      </div>
    </div>
    <div class="carousel-item">
     <div class="row">
      <div class="col-sm-6">
          <h4>Jay M</h4>
          <p>This Centre is very good for the hair care treatments. I got here by my frnds referral. The doc have suggested me for PRP treatment initially and it’s be 2 sittings. I could see the difference. Lots more to go. Compared to other places I really trust them more. The doctor and the team is great.</p>
        </div>
        <div class="col-sm-6">
          <h4>Annu Raj</h4>
          <p>Nice service and easy going procedure. very good work by DHI. Was a time consuming job but the surgeon did his job to the best. Best part was that there was no pain at all. Thumbs up. now waiting for the results to show.</p>
        </div>
       
      </div> 
    </div>
    <div class="carousel-item">
     <div class="row">
         <div class="col-sm-6">
          <h4>SAI PAVAN MULA</h4>
          <p>I am very happy and satisfied with the treatment that I have taken at DHI, Chennai. Price worthy option to choose if you are planning for a hair transplant. Discussion during the Consultation, hospitality during the procedure and the follow up post procedure are very good. Happy to Choose DHI, Chennai.</p>
        </div>
        <div class="col-sm-6">
          <h4>Venkatachalamurthi T</h4>
          <p>I strongly recommend everyone to do the procedure in DHI Chennai. Because DHI provides a painless treatment and the team was excellent.</p>
        </div>
      </div> 
    </div>
  </div>
 
</div>

            </div>
          </div>
        </div>
            </div>

            
  

                  </div>


</section>

     
<div class="clearfix"></div>



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  



  </body>
</html>
