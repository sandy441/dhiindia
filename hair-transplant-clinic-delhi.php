
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="DHI™ India Hair transplant clinic in Delhi provides hair restoration, hair regrowth &amp; hair loss treatment with advance DHI™ technique. 100% Natural Results
              ">
        <meta name="author" content="">
        <title>Hair Loss Treatment &amp; Hair Transplant Clinic in Delhi – DHI™ India
            
        </title>
<?php include 'header.php';?>
    </head>
    <body>
        
        
        <section class="location bgLocation1">
            <div class="container">
                <div class="row ">
                    <div class="col-sm-6 ">
                        
                        
                    </div>
                    <div class="col-sm-6">
                        
                      <?php include 'appointmentForm.php';?>
                    </div>
                </div>
            </div>
        </section>
        <section class="bg-col-1">
            <div class="container">
                <div class="content">
                    <div class="card ">
                        <div class="card-body"> 
                            
                            <div class="row">
                                <div class="col-sm-4 p-5">
                                    <address>
                                        B – 5/15 Safdarjung Enclave<br />
                                        Opposite DLTA & Deer Park<br />
                                        Delhi 110029<br />
                                        011 – 46103032/36 & +91 8826006714<br />
                                        Email: info@dhiindia.com, enquiry@dhiindia.com
                                    </address>
                                </div>
                                <div class="col-sm-8">
                                    <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14017.249385771276!2d77.189681!3d28.560382!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf7755f0b8d7a9e7d!2sDHI%E2%84%A2+India+-+Delhi's+Best+Hair+Transplant+Clinic!5e0!3m2!1sen!2sin!4v1500875495308" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                                </div>
                            </div>
                            
                            
                            
                            
                            
                            
                            
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="card ">
                        <div class="card-body"> 
                            <h3>DHI Hair Transplant Clinic — NEW DELHI</h3>
                            <ul>
                                <li>Our DHI hair transplant clinic in New Delhi is prominently situated amid scenic, tranquil natural surroundings of exotic Safdarjung Enclave. Named after a famous Mughal Emperor from early 18th century, this plush locality is dotted with picturesque parks, historical monuments, and Embassies. Hordes of peacocks dancing in rain and deer roaming around freely are two very common sights.</li>
                                <li>
                                    Owing to the locality’s proximity to the Ring Road, it is easily accessible from any part of the capital. Parking is also available on the premises.s
                                    
                                </li>
                                <li>DHI Delhi features a wide range of hair loss treatments including Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, Hair Replacement and Direct Hair Fusion. All procedures are deliberately planned for our clients after a detailed diagnosis of their cases. This helps our doctors offer unmatched hair loss solutions for a lifetime.
                                </li>
                                <li>To book an appointment for a consultation at our DHI Delhi clinic, give us a call now!</li>
                                <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                
                
                <div class="content">
                    <div class="card ">
                        <div class="card-body "> 
                            <h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
                            <div class="row">
                                <div class="col-sm-6 pr-lg-0 pr-md-0">
                                    <img src="image/IMG_8981_edit.png" alt="" class="img-fluid">
                                </div>
                                <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/IMG_consutation_edit.png" alt="" class="img-fluid"></div>
                                <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/IMG_9099_edit.png" alt="" class="img-fluid"></div>
                                <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/IMG_9114_edit.png " alt="" class="img-fluid"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="content">
                    <div class="card ">
                        <div class="card-body bt-border"> 
                            <h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>
                            
                            <div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="row ">
                                            <div class="col-sm-6">
                                                <h4>Sanjeev Kumar</h4>
                                                <p>Best part about visiting dhi clinic is they do proper diagnosis to eradicate the problem ( hairloss) taken prp and dhi hair transplant and totally satisfied with results. I Highly recommended DHI</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4>Jitendra Tanwar</h4>
                                                <p>Hello everyone, I am from army and got DHI Procedure done yesterday from DHI New Delhi Clinic. I liked the behaviour of staff and procedure was pain free. I would always recommend DHI Procedure of hair transplant to everyone.</p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h4>Jiva Tomar</h4>
                                                <p>I am extremely satisfied with my hair transplantation result in DHI Safdarjung Enclave clinic. Thanks to Dr. Ajay and his team</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4>Sunil Singh</h4>
                                                <p>Very cooperative and professional services,fully satisfied by the transplant and the results thereafter,i will recommend others to go for DHI in case of transplant it may be bit costlier than others but its worth based on safety and results</p>
                                            </div>
                                            
                                        </div> 
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h4>Pankaj Sharma</h4>
                                                <p>Very cooperative staff and doctors,results was satisfactory.</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4>Jiva Tomar</h4>
                                                <p>DHI is the best clinic in Delhi for Hair Transplant surgery. Right from first interaction to hair transplant and hair growth , my experience was very satisfactory . I just want to appreciate the Delhi clinic of DHI for hair transplant work and behaviour of the entire team. You guys did a fantastic job. I am totally satisfied thank you</p>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            
            
            
        </div>
        
        
    </section>
    
    
    <div class="clearfix"></div>
    
    
    
    <!-- FOOTER -->
    
<?php include 'footer.php';?>
    
    
    
    
</body>
</html>
