
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Looking for Hair restoration clinic in gurugram? DHI™ India provides hair restoration, hair regrowth &amp; hair loss treatment with advance DHI™ technique. ✓ No Cuts ✓ No Marks ✓ 100% Natural Results
">
    <meta name="author" content="">
      <title>Hair Transplant Clinic &amp; Hair Loss Treatment in Gurugram – DHI™ India

</title>
<?php include 'header.php';?>
    </head>
  <body>


<section class="location bgLocation2">
  <div class="container">
    <div class="row ">
      <div class="col-sm-6 ">
      
       
      </div>
      <div class="col-sm-6">
<?php include 'appointmentForm.php';?>
      </div>
    </div>
  </div>
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card ">
            <div class="card-body"> 
              
<div class="row">
  <div class="col-sm-4 p-5">
   <address>
                220-221, 2nd Floor<br />
                South Point Mall, Sec 53, DLF Golf Course Road<br />
                Gurugram 122009<br />
                0124 – 4276426/ 40666481 & +91 8800804330<br />
                
              </address>
  </div>
  <div class="col-sm-8">
    <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112198.07012601595!2d77.07463701668931!3d28.50394004637179!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d18b4f5e20d45%3A0xf6195edb997b3244!2sDhiindia-+Best+Hair+Transplant+In+Gurgaon+%26+Hair+Loss+Treatment+Clinic+In+Gurugram!5e0!3m2!1sen!2sin!4v1501061505475" width="100%" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
  </div>
</div>
   

  
    
   
    
   
  </div>
            </div>
          </div>
            <div class="content">
  <div class="card ">
            <div class="card-body"> 
<h3>DHI Hair Transplant Clinic — GURUGRAM</h3>
<ul>
  <li>DHI Gurugram operates from South Point Mall in the heart of the Millennium City. Located right next to the busy Golf Course Road in South Gurugram, South Point Mall is a popular landmark built by the realty giant DLF. It is home to big international brands, cafes, restaurants, supermarkets, grocery stores, beauty centers, spas, and medical institutions.</li>
  <li>
DHI Gurugram offers Gold Standard services in hair restoration services with the help of a strong research advisory board and world class facilities. Our global medical team constantly revolutionizes the industry with new treatments for various stages of hair loss. We host a comprehensive range of best hair loss solutions such as Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, Hair Replacement and Direct Hair Fusion among others.

</li>
  <li>DHI Delhi features a wide range of hair loss treatments including Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, Hair Replacement and Direct Hair Fusion. All procedures are deliberately planned for our clients after a detailed diagnosis of their cases. This helps our doctors offer unmatched hair loss solutions for a lifetime.
</li>
  <li>We are easily accessible from any part of Gurugram and Delhi. Parking is also available on the premises. DHI Gurugram provides detailed consultations to guide our clients through the entire process. To book an appointment for a consultation at our DHI Gurugram clinic, give us a call now</li>
  <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
  
</ul>
            </div>
          </div>
        </div>


        <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
<div class="row">
  <div class="col-sm-6 pr-lg-0 pr-md-0">
  <img src="image/clinic/DSC_001.jpg" alt="" class="img-fluid">
</div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_002.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC_003.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_004.jpg " alt="" class="img-fluid"></div>
</div>
            </div>
          </div>
        </div>


        <div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 
<h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>

<div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row ">
        <div class="col-sm-6">
          <h4>Somind Negi</h4>
          <p>DHI gurugram team is very professional. It helped me learn about my hair transplant options really well. But I’m not sure whether I need their services or not. After some more time may be.</p>
        </div>
        <div class="col-sm-6">
          <h4>Prakash Jha</h4>
          <p>DHI gurgaon is a nice facility, although it is too far from where I live. Spent more than an hour on the driving seat.</p>
        </div>
        
      </div>
    </div>
    <div class="carousel-item">
     <div class="row">
      <div class="col-sm-6">
          <h4>Jiva Tomar</h4>
          <p>I am extremely satisfied with my hair transplantation result in DHI Safdarjung Enclave clinic. Thanks to Dr. Ajay and his team</p>
        </div>
        <div class="col-sm-6">
          <h4>Mahir Jaiswal</h4>
          <p>DHI doctors are friendly and understand what you need from the procedure. My hairs after DHI look even more beautiful than they really were before hair loss.</p>
        </div>
       
      </div> 
    </div>
    <div class="carousel-item">
     <div class="row">
         <div class="col-sm-6">
          <h4>Vandana Sharma</h4>
          <p>DHI is worth all the hype. I look young in a matter of few hours. Thank you DHI!</p>
        </div>
        <div class="col-sm-6">
          <h4>Mahesh Nagpal</h4>
          <p>DHI gurgaon is amazing. I loved their hair Extra Mild Shampoo, although I found it a bit expensive.</p>
        </div>
      </div> 
    </div>
  </div>
 
</div>

            </div>
          </div>
        </div>
            </div>

            
  

                  </div>


</section>

     
<div class="clearfix"></div>



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  



  </body>
</html>
