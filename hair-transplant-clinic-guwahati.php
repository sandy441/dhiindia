
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Looking for Hair transplant clinic in Guwahati? DHI™ India provides hair restoration, hair regrowth &amp; hair loss treatment with advance DHI™ technique. ✓No Cuts ✓No Scars ✓Trained Doctors
">
    <meta name="author" content="">
      <title>Hair Transplant Clinic &amp; Hair Loss Treatment in Guwahati – DHI™ India

</title>
<?php include 'header.php';?>
    </head>
  <body>


<section class="location bgLocation14">
  <div class="container">
    <div class="row ">
      <div class="col-sm-6 ">
      
       
      </div>
      <div class="col-sm-6">

     <?php include 'appointmentForm.php';?>
      </div>
    </div>
  </div>
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card ">
            <div class="card-body"> 
              
<div class="row">
  <div class="col-sm-4 p-5">
  <address>
                Rejuve Skin & Aesthetic Clinic<br />
                2nd Floor, Gulshan Grand<br />
                Dispur, Guwahati 781006<br />
                0361 – 2230778 & +91 8011611002<br />
               Email: info@dhiindia.com, enquiry@dhiindia.com
              </address>
  </div>
  <div class="col-sm-8">
   <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3581.6812820518076!2d91.79355831502876!3d26.141932983465654!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x375a58d92fa621c1%3A0x3d7cb434b6543c9b!2sRejuve+Skin+And+Hair+Aesthetic+Clinic!5e0!3m2!1sen!2s!4v1501074289930" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
  </div>
</div>

  </div>
            </div>
          </div>
            <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3>DHI Hair Transplant Clinic — GUWAHATI</h3>
<ul>
  <li>Why suffer when you can take charge of your destiny? Let us help you find permanent solutions to your hair loss problems.</li>
  <li>
DHI has set up its base in pristine Dispur, Assam, which is not only the state capital but also a potpourri of exquisite cultures. We operate from the world class clinic Rejuve Skin & Aesthetic Clinic whose managing director and founder, Dr. Sharadi Shreemoyee, introduced DHI to the state for the first time.

</li>
  <li>Guwahati Railway station is a 7.8 km drive from our clinic via Mahapurush Shrimanta Shankardev Rd. Due to its proximity to railway station and International airport, DHI Guwahati is an easy commute from all parts of the city. Traveling to our clinic from other parts of the state is also convenient.
</li>

<li>The staff is passionate, friendly, and compassionate. Our team members respect for your privacy and comfort. Doctors are trained, certified, and MCI registered to perform DHI procedures for all types of alopecia and other hair related disorders.</li>
<li>We offer a wide range of hair transplant treatments including Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, and Direct Hair Fusion among others.</li>
<li>To book an appointment for a consultation at our DHI Mumbai clinic, give us a call now!</li>
  <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
  
</ul>
            </div>
          </div>
        </div>


        <!-- <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
<div class="row">
  <div class="col-sm-6 pr-lg-0 pr-md-0">
  <img src="image/clinic/DSC_001.jpg" alt="" class="img-fluid">
</div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_002.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC_003.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_004.jpg " alt="" class="img-fluid"></div>
</div>
            </div>
          </div>
        </div> -->


        <div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 
<h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>

<div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row ">
        <div class="col-sm-6">
          <h4>Bisvajit Chakravarty</h4>
          <p>My wife had skin allergy and hence she went to this clinic. Her experience according her was splendid. I would recommend all to visit this clinic</p>
        </div>
        <div class="col-sm-6">
          <h4>Rekha Deka</h4>
          <p>Nice to see DHI skin and hair aesthetic clinic at Guwahati</p>
        </div>
        
      </div>
    </div>
    <div class="carousel-item">
     <div class="row">
      <div class="col-sm-6">
          <h4>Kamran Khera</h4>
          <p>The ambiance is soothing and doctors are certified to perform DHI procedures. Feels good to know that you are in safe hands. Thank you DHI.</p>
        </div>
        <div class="col-sm-6">
          <h4>Sunil Singh</h4>
          <p>Very cooperative and professional services,fully satisfied by the transplant and the results thereafter,i will recommend others to go for DHI in case of transplant it may be bit costlier than others but its worth based on safety and results</p>
        </div>
       
      </div> 
    </div>
    
  </div>
 
</div>

            </div>
          </div>
        </div>
            </div>

            
  

                  </div>


</section>

     
<div class="clearfix"></div>



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  



  </body>
</html>
