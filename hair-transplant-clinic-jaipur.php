
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="DHI India - Best Hair Transplant and Hair Restoration Surgery in Jaipur done by expert Surgeon with 47 years of experience in Hair Transplant. 100% Natural Results
">
    <meta name="author" content="">
      <title>Hair Transplant &amp; Hair Loss Treatment Surgery Clinic in Jaipur - DHI India

</title>
<?php include 'header.php';?>
    </head>
  <body>


<section class="location bgLocation12">
  <div class="container">
    <div class="row ">
      <div class="col-sm-6 ">
      
       
      </div>
      <div class="col-sm-6">
<?php include 'appointmentForm.php';?>
      </div>
    </div>
  </div>
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card ">
            <div class="card-body"> 
              
<div class="row">
  <div class="col-sm-4 p-5">
  <address>
               201-201 A,2nd floor Ambition tower,<br />
                agrasen circle, C-scheme,<br />
                Jaipur 302001<br />
                08890122218, +91 141-4022218, +91 141-2362217<br />
               Email: info@dhiindia.com, enquiry@dhiindia.com
              </address>
  </div>
  <div class="col-sm-8">
   <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3557.693628758651!2d75.80062671433643!3d26.913216683127423!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396db4108d76cbcb%3A0xedb791d788784023!2sDHI-+Hair+Treatment+Clinic!5e0!3m2!1sen!2sin!4v1501070649882" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
  </div>
</div>

  </div>
            </div>
          </div>
            <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3>DHI Hair Transplant Clinic — JAIPUR</h3>
<ul>
  <li>As Jaipur is considered the heart of India’s royal heritage and tourism, our DHI hair transplant clinic here serves an opportunity for international clients as well to be part of a revolution in hair transplant and discover the brilliance in them. With a driving distance of just 3.7 km from the nearest Railway station, DHI Jaipur is easily accessible from any part the state capital. Parking is also available on our clinic’s premise.</li>
  <li>
Our center is situated on the second floor of Ambition Tower, famous for its high-end spas and salons, gaming zones, restaurants, multipurpose halls, lounges, and gymnasiums.

</li>
  <li>We offer a complete range of hair transplant solutions including Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, and Direct Hair Fusion among others. The clinic is equipped with all the latest facilities, technologies, and single-use imported instruments to ensure best results for a lifetime.
</li>
 <li>Our staff is friendly and passionate about maintaining the highest standards of DHI International. We respect the privacy of our clients, which is why all consultations are confidential. Our doctors are trained, certified, and MCI registered.</li>

<li>To book an appointment for a consultation at our DHI Mumbai clinic, give us a call now!</li>
  <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
  
</ul>
            </div>
          </div>
        </div>


        <!-- <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
<div class="row">
  <div class="col-sm-6 pr-lg-0 pr-md-0">
  <img src="image/clinic/DSC_001.jpg" alt="" class="img-fluid">
</div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_002.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC_003.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_004.jpg " alt="" class="img-fluid"></div>
</div>
            </div>
          </div>
        </div> -->


        <div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 
<h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>

<div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row ">
        <div class="col-sm-6">
          <h4>Rahul Sharma</h4>
          <p>DHI’s hair care range is more effective than popular cosmetic brands I have used in my life. They nourish your hairs and protect them from infection after DHI procedure. I will recommend it to everyone.</p>
        </div>
        <div class="col-sm-6">
          <h4>Deepa Mediratta</h4>
          <p>Wonderful experience… my brother got his all hairs back — Naturally… Simply amazing.</p>
        </div>
        
      </div>
    </div>
    <div class="carousel-item">
     <div class="row">
      <div class="col-sm-6">
          <h4>Vikramsingh Rathod</h4>
          <p>Back to daily routine 3 days after the DHI procedure. Already falling in love with myself. Thank you DHI.</p>
        </div>
        <div class="col-sm-6">
          <h4>Arnab Mukherjee</h4>
          <p>DHI has not only given me hair transplant but also confidence.</p>
        </div>
       
      </div> 
    </div>
    <div class="carousel-item">
     <div class="row">
         <div class="col-sm-6">
          <h4>Pankaj Sharma</h4>
          <p>Very cooperative staff and doctors,results was satisfactory.</p>
        </div>
        <div class="col-sm-6">
          <h4>Pawan Singh</h4>
          <p>People say so many things about hair transplant. But in reality it is so different. I won’t call DHI a hair transplant procedure because it is so natural. It is like regrowing your own hair.</p>
        </div>
      </div> 
    </div>
  </div>
 
</div>

            </div>
          </div>
        </div>
            </div>

            
  

                  </div>


</section>

     
<div class="clearfix"></div>



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  



  </body>
</html>
