
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Looking for Hair transplant clinic in Kochi? DHI™ India provides hair restoration, hair regrowth &amp; hair loss problem with advance DHI™ technique. ✓Trained dermatologist’s ✓No Pain ✓100% Natural Results
">
    <meta name="author" content="">
      <title>Hair Transplantation Clinic &amp; Hair Loss Treatment in Kochi - DHI™ India

</title>
<?php include 'header.php';?>
    </head>
  <body>


<section class="location bgLocation8">
  <div class="container">
    <div class="row ">
      <div class="col-sm-6 ">
      
       
      </div>
      <div class="col-sm-6">
<?php include 'appointmentForm.php';?>
      </div>
    </div>
  </div>
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card ">
            <div class="card-body"> 
              
<div class="row">
  <div class="col-sm-4 p-5">
  <address>
                G189, Panampilly Nagar<br />
                Near Panampilly Nagar Post Office<br />
                Kochi 682036<br />
                0484 – 4033302 & +91 9995776644<br />
               Email: info@dhiindia.com, enquiry@dhiindia.com
              </address>
  </div>
  <div class="col-sm-8">
   <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3929.6500771306823!2d76.2951936140113!3d9.963043592876511!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b0872c7cd44b899%3A0x659ebd433af30925!2sDHI-+Best+Hair+Transplant+In+Kochi+%26+Hair+Loss+Treatment+Clinic+In+Kochi!5e0!3m2!1sen!2sin!4v1501066365970" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
  </div>
</div>

  </div>
            </div>
          </div>
            <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3>DHI Hair Transplant Clinic — KOCHI</h3>
<ul>
  <li>Bustling with indelible imprints of some unforgotten cultures in memories, our DHI hair transplant clinic in Kochi feels blessed to serve diverse people and bring transformation in their lives. We offer unrivaled hair transplant procedures including Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, and Direct Hair Fusion among others.</li>
  <li>
Dotted with historical monuments, Kochi serves as an important location for DHI India group for it provides us an opportunity to treat people from other countries as well. We are conveniently located in the heart of Panampilly Nagar with a driving distance of just 2.8 km from the nearest metro station via Sahodaran Ayyappan Road and 3.2 km via Mahatma Gandhi Road. Our doctors offer consultations on a wide range of hair loss treatments. Personalized attention to every case helps us offer you the best results for a lifetime.

</li>
  <li>All consultations are confidential and include hair loss evaluation test to give our clients an idea of the loss in future if left untreated.
DHI hair restoration clinic in Kochi is unrivalled at hair transplant procedures. Our single-use titanium coated hair transplant instruments are imported from countries such as the United States of America.
</li>
 

<li>To book an appointment for a consultation at our DHI Mumbai clinic, give us a call now!</li>
  <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
  
</ul>
            </div>
          </div>
        </div>


        <!-- <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
<div class="row">
  <div class="col-sm-6 pr-lg-0 pr-md-0">
  <img src="image/clinic/DSC_001.jpg" alt="" class="img-fluid">
</div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_002.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC_003.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_004.jpg " alt="" class="img-fluid"></div>
</div>
            </div>
          </div>
        </div> -->


        <div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 
<h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>

<div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row ">
        <div class="col-sm-6">
          <h4>Sirish Jha</h4>
          <p>DHI restored my hair fully. I can’t thank them enough for this. You are truly wonderful.</p>
        </div>
        <div class="col-sm-6">
          <h4>Shivam Babu</h4>
          <p>I was so scared about hair transplant but DHI and its team helped me overcome it. I am now undergoing a DHI procedure soon.</p>
        </div>
        
      </div>
    </div>
    <div class="carousel-item">
     <div class="row">
      <div class="col-sm-6">
          <h4>Vijay Raj</h4>
          <p>Loved the DHI procedure. The hair care products by DHI are amazing. It protected me from any type of infection.</p>
        </div>
        <div class="col-sm-6">
          <h4>Devendra Chauhan</h4>
          <p>Nothing is more beautiful than the feeling of getting your crowning glory back. Thanks DHI for restoring my confidence.</p>
        </div>
       
      </div> 
    </div>
    <div class="carousel-item">
     <div class="row">
         <div class="col-sm-6">
          <h4>Sarafaras M.S</h4>
          <p>DHI consultations were very helpful. I got to make an informed choice and I am very happy with the results. Would recommend DHI to all of you!</p>
        </div>
        <div class="col-sm-6">
          <h4>Shabbir Pathan</h4>
          <p>I visited DHI kochi center recently and am glad that I did. The staff was wonderful in telling me my root problem for hair loss. DHI procedure is going to give me natural results.</p>
        </div>
      </div> 
    </div>
  </div>
 
</div>

            </div>
          </div>
        </div>
            </div>

            
  

                  </div>


</section>

     
<div class="clearfix"></div>



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  



  </body>
</html>
