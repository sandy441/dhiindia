
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Looking for Hair transplant clinic in Kolkata? DHI™ India provides hair restoration, hair regrowth &amp; hair loss treatment with advance DHI™ technique. ✓No Cuts ✓No Scars ✓Trained Doctors
">
    <meta name="author" content="">
      <title>Hair Transplant Clinic &amp; Hair Loss Treatment in Kolkata – DHI™ India

</title>
<?php include 'header.php';?>
    </head>
  <body>


<section class="location bgLocation7">
  <div class="container">
    <div class="row ">
      <div class="col-sm-6 ">
      
       
      </div>
      <div class="col-sm-6">
<?php include 'appointmentForm.php';?>
      </div>
    </div>
  </div>
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card ">
            <div class="card-body"> 
              
<div class="row">
  <div class="col-sm-4 p-5">
  <address>
                DHI, 4th Floor, Platinum Mall 31,<br />
                Elgin Road,<br />
                Kolkata- 700020<br />
                +91 9007194854<br />
               Email: info@dhiindia.com, enquiry@dhiindia.com
              </address>
  </div>
  <div class="col-sm-8">
    <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7370.200973706554!2d88.35107769260452!3d22.537908217323366!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1e85654577e9d0bf!2sDHI+India-+Best+Hair+Transplant+In+Kolkata+%26+Hair+Loss+Treatment+Clinic+In+Kolkata!5e0!3m2!1sen!2sin!4v1503998943193" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
  </div>
</div>

  </div>
            </div>
          </div>
            <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3>DHI Hair Transplant Clinic — KOLKATA</h3>
<ul>
  <li>Shining bright like a diamond amid the affluence of Ballygunge city in South Kolkata, our DHI clinic is regarded as the symbol of excellence, perfection, and optimism. We offer a wide range of comprehensive hair restoration treatments including Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, and Direct Hair Fusion among others.</li>
  <li>
DHI Kolkata is conveniently located in Ballygunge with a driving distance of just 4 km from Kolkata Railway station. Flanked by Park Circus in the North, Dhakuria and Rabindra Sarobar in the South, Kasba and Eastern Railway in the East, and plush localities of Bhowanipore and Lansdowne in the West, we are pleased to serve people from different walks of life.

</li>
  <li>DHI Kolkata is conveniently located in Ballygunge with a driving distance of just 4 km from Kolkata Railway station. Flanked by Park Circus in the North, Dhakuria and Rabindra Sarobar in the South, Kasba and Eastern Railway in the East, and plush localities of Bhowanipore and Lansdowne in the West, we are pleased to serve people from different walks of life.
</li>
  <li>Doctors at our DHI hair transplant clinic in Kolkata are best at DHI technique and guarantee natural looking solutions for a lifetime.
</li>

<li>To book an appointment for a consultation at our DHI Mumbai clinic, give us a call now!</li>
  <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
  
</ul>
            </div>
          </div>
        </div>


        <!-- <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
<div class="row">
  <div class="col-sm-6 pr-lg-0 pr-md-0">
  <img src="image/clinic/DSC_001.jpg" alt="" class="img-fluid">
</div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_002.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC_003.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_004.jpg " alt="" class="img-fluid"></div>
</div>
            </div>
          </div>
        </div> -->


        <div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 
<h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>

<div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row ">
        <div class="col-sm-6">
          <h4>Subirkumar Sen</h4>
          <p>My procedure made me feel better about myself and my personal appearance. Doc and staff were very professional. I got my procedure done 2 days back which was very simple and easy. I would recommend DHI to everyone.Eagerly waiting for my result.</p>
        </div>
        <div class="col-sm-6">
          <h4>Aadil Khan</h4>
          <p>I did my procedure 22 days back and it feels awesome, specially after the scarring experience of doing a strip transplant 4 years back. Awesome medical and support team, great professionalism. Total value for money experience.</p>
        </div>
        
      </div>
    </div>
    <div class="carousel-item">
     <div class="row">
      <div class="col-sm-6">
          <h4>Aarti Yadav</h4>
          <p>DHI has made transplant easy, just 2 days after the session. I could started up my activities normally, my hair feels and looks great. Thanks DHI.</p>
        </div>
        <div class="col-sm-6">
          <h4>Avishek Dutta</h4>
          <p>I made transplant from DHI & its done very easily. its been 6 months from my plantation & my hair is started to grow now. thank you DHI for all on this.</p>
        </div>
       
      </div> 
    </div>
    <div class="carousel-item">
     <div class="row">
         <div class="col-sm-6">
          <h4>Smita Teresa Gomes</h4>
          <p>Wonderful place ….one stop solutions for all your hair problems.</p>
        </div>
        <div class="col-sm-6">
          <h4>Shubhamsri Gupta</h4>
          <p>Nice place,good consultation , DHI Kolkata has helped me in improving my lifestyle.</p>
        </div>
      </div> 
    </div>
  </div>
 
</div>

            </div>
          </div>
        </div>
            </div>

            
  

                  </div>


</section>

     
<div class="clearfix"></div>



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  



  </body>
</html>
