
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Looking for Hair transplant clinic in Mumbai? DHI™ India provides hair restoration, hair regrowth &amp; hair loss treatment with advance DHI™ technique. ✓ No Cuts ✓ No Marks ✓ 100% Natural Results
">
    <meta name="author" content="">
      <title>Hair Transplant Clinic Mumbai - DHI India

</title>
<?php include 'header.php';?>
    </head>
  <body>


<section class="location bgLocation3">
  <div class="container">
    <div class="row ">
      <div class="col-sm-6 ">
      
       
      </div>
      <div class="col-sm-6">

       <?php include 'appointmentForm.php';?>
      </div>
    </div>
  </div>
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card ">
            <div class="card-body"> 
              
<div class="row">
  <div class="col-sm-4 p-5">
  <address>
                CPLSS 3rd Floor<br />
                Above Sarla Hospital, Dattataray Road<br />
                Santacruz West, Mumbai 400054<br />
                022 – 6117888/ 61178880 & +91 9833807002<br />
               Email: info@dhiindia.com, enquiry@dhiindia.com
              </address>
  </div>
  <div class="col-sm-8">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.499866776113!2d72.83455431415045!3d19.08571478708233!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c9a693f7c851%3A0x696cbc13704a693!2sDHI+Hair+Loss+%7C+Baldness+Treatment+Mumbai+Clinic!5e0!3m2!1sen!2sin!4v1501062527163" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen="allowfullscreen"></iframe>
  </div>
</div>
   

  
    
   
    
   
  </div>
            </div>
          </div>
            <div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 
<h3>DHI Hair Transplant Clinic — MUMBAI</h3>
<ul>
  <li>In the heart of an affluent neighborhood in Santacruz West, DHI Mumbai is situated on the 3rd floor of CPLSS facility above Sarla Hospital & ICU—a Multispecialty Hospital. CPLSS is an award winning plastic and cosmetic surgery institution helmed by Medical Director, DHI India, Dr. Viral Desai, who is also the recipient of National Icon in Plastic and Cosmetic Surgery award.</li>
  <li>
DHI Mumbai is a state of the art clinic equipped with ultra-modern facilities and latest technologies to ensure best solutions to our clients. Our team not only guarantees hair restoration but also personal attention and respect for your privacy. Your well-being is our top most priority.

</li>
  <li>Our hair transplant clinic in Mumbai’s Santacruz is an easy commute for both locals and outsiders. The driving distance is 1.3 km from the nearest railway station, which can be covered in less than 8 minutes.
</li>
  <li>Take Tilak Road from Santacruz railway station. From the HDFC bank roundabout, take a right turn onto Swami Vivekananda Road. Take the first left turn past the Airtel Relationship Center onto Dattataray Road. Sarla Hospital & ICU is just a few seconds drive from here.</li>
  <li>Our doctors offer a complete diagnostic evaluation to help you make an informed decision. Our hair loss treatments such as Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, and Direct Hair Fusion are the best in town.
DHI International has successfully transformed the lives of more than 200,000 clients across the world.</li>
<li>To book an appointment for a consultation at our DHI Mumbai clinic, give us a call now!</li>
  <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
  
</ul>
            </div>
          </div>
        </div>


        <!-- <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
<div class="row">
  <div class="col-sm-6 pr-lg-0 pr-md-0">
  <img src="image/clinic/DSC_001.jpg" alt="" class="img-fluid">
</div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_002.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC_003.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_004.jpg " alt="" class="img-fluid"></div>
</div>
            </div>
          </div>
        </div> -->


        <!-- <div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 
<h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>

<div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row ">
        <div class="col-sm-6">
          <h4>Sanjeev Kumar</h4>
          <p>Best part about visiting dhi clinic is they do proper diagnosis to eradicate the problem ( hairloss) taken prp and dhi hair transplant and totally satisfied with results. I Highly recommended DHI</p>
        </div>
        <div class="col-sm-6">
          <h4>Jitendra Tanwar</h4>
          <p>Hello everyone, I am from army and got DHI Procedure done yesterday from DHI New Delhi Clinic. I liked the behaviour of staff and procedure was pain free. I would always recommend DHI Procedure of hair transplant to everyone.</p>
        </div>
        
      </div>
    </div>
    <div class="carousel-item">
     <div class="row">
      <div class="col-sm-6">
          <h4>Jiva Tomar</h4>
          <p>I am extremely satisfied with my hair transplantation result in DHI Safdarjung Enclave clinic. Thanks to Dr. Ajay and his team</p>
        </div>
        <div class="col-sm-6">
          <h4>Sunil Singh</h4>
          <p>Very cooperative and professional services,fully satisfied by the transplant and the results thereafter,i will recommend others to go for DHI in case of transplant it may be bit costlier than others but its worth based on safety and results</p>
        </div>
       
      </div> 
    </div>
    <div class="carousel-item">
     <div class="row">
         <div class="col-sm-6">
          <h4>Pankaj Sharma</h4>
          <p>Very cooperative staff and doctors,results was satisfactory.</p>
        </div>
        <div class="col-sm-6">
          <h4>Jiva Tomar</h4>
          <p>DHI is the best clinic in Delhi for Hair Transplant surgery. Right from first interaction to hair transplant and hair growth , my experience was very satisfactory . I just want to appreciate the Delhi clinic of DHI for hair transplant work and behaviour of the entire team. You guys did a fantastic job. I am totally satisfied thank you</p>
        </div>
      </div> 
    </div>
  </div>
 
</div>

            </div>
          </div>
        </div> -->
            </div>

            
  

                  </div>


</section>

     
<div class="clearfix"></div>



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  



  </body>
</html>
