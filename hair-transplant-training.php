<!doctype html>
<html lang="en">
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="DHI’s flagship training course offers training in its award winning Direct Hair Implantation (DHI) technique to dermatologists from all over the world.

              ">
        <meta name="author" content="">
        <title>DHI™ Hair Transplant Training Programme - DHI™ India

        </title>
    <?php include 'header.php';?>
    </head>
    
    <body>
    
        <section class="bg-col-1">
            <div class="clearfix">
            
                <video preload="auto" width="100%" loop autoplay>
                    <source type="video/webm" src="video/DHI Hair Restoration Training Academy_01.mp4">
                    <source type="video/mp4" src="video/DHI Hair Restoration Training Academy_01.mp4">
                    <source type="video/webm" src="video/DHI Hair Restoration Training Academy_01.ogg">
                    <source type="video/ogg" src="video/DHI Hair Restoration Training Academy_01.ogg">
                </video>
                
            </div>
            
        </section>
        
        <section class=" bg-col-1 ">
            <div class="container">
                <div class="content">
                    <div class="card ">
                            <div class="card-body ">
                        
                            <div class="row pt-4 pb-2">
                                <div class="col-sm-12">
                                    <h2 class="text-center">What is Direct Hair Implantation?</h2>
                                </div>
                              
                                <p> DHI Medical Group’s flagship training course offers training in its award winning Direct Hair Implantation (DHI) technique to dermatologists from all over the world. DHI is the most advanced technique for hair restoration. It is performed in three steps:</p>
                                
                                
                                <div class="col-sm-4 threeSteps1">
                                    <div class="card" style="width: 20rem;">
                                        <img class="card-img-top" src="image/dhi copy.jpg" alt="DHI hair evaluation test">
                                        <div class="card-body  text-center">
                                            <div class="circle1 mb-3 mt-3">
                                                <span>1</span>
                                            </div>
                                            <h5 class="card-title">Unique Diagnostic System for Alopecia (UDSA)</h5>
                                            <p class="card-text clinic_name"><a href="#" data-toggle="tooltip" data-placement="top" title="UDSA was developed by a team of world
                                                                                renowned experts to provide accurate and
                                                                                personalized diagnosis for all types of
                                                                                alopecia – hair and scalp disorders" >
                                                    UDSA</a>, is a combination of tests which includes Psychological Aspects, Dermatological Examination, Mathematical Aspects and the DHI Alopecia Test .</p>
      
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 threeSteps1">
                                    <div class="card" style="width: 20rem;">
                                        <img class="card-img-top" src="image/Layer 16.jpg" alt="Dhi hair follicle extractor">
                                        <div class="card-body  text-center">
                                            <div class="circle1 mb-3 mt-3">
                                                <span>2</span>
                                            </div>
                                            <h5 class="card-title">Extraction Phase</h5>
                                            <p class="card-text">Hair Follicles are extracted one by one from
                                                the donor area using Titanium coated single use tool with the diameter of 0.7mm or more.</p>
    
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 threeSteps1">
                                    <div class="card" style="width: 20rem;">
                                        <img class="card-img-top" src="image/Layer 17.jpg" alt=" dhi hair transplant implanter
      
                                             ">
                                        <div class="card-body  text-center">
                                            <div class="circle1 mb-3 mt-3">
                                                <span>3</span>
                                            </div>
                                            <h5 class="card-title">Placement Phase</h5>
                                            <p class="card-text">Hair follicles are implanted directly into the region suffering from hair loss using a patented tool  with a perfect control on depth, angle & direction of hair. Using DHI’s Total Care System protocols ensuring adequate density as per ADT (Average Density Target) 
                                            </p>
        
                                        </div>
                                    </div>
                                </div>
        
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content ">
                    <div class="card bg-col-2">
                        <div class="card-body  ">
                            <h2 class="text-center">Why Choose DHI Hair Transplant Training Program?</h2>
                          
                            <div class="col-sm-12 training">
                                <table class="table table-bordered " >
                                    <thead>
                                            <tr>
        
                                            <th scope="col" width="20%"></th>
                                            <th scope="col" class="text-center">DHI Academy</th>
                                            <th scope="col" class="text-center">Others</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Academy</th>
                                            <td><p>Only hair transplant training academy in the world with well-defined training protocols and hair restoration practise on live patients</p></td>
                                            <td><p>There is no other hair transplant academy in the world. Other hair transplant training clinics give more importance  to theoretical aspects than practical aspects</p></td>
          
                                        </tr>
                                        <tr>
                                            <th scope="row">Practical Exposure</th>
                                            <td><p>Observation of live hair restoration sessions performed by Master MD surgeons.</p>
                                                <p>Practise is done on live patients</p></td>
                                            <td><p>Observation of hair restoration sessions are limited. If there are any, they are done on dummies or cadavers.</p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Experience</th>
                                            <td>
                                                <p>DHI has trained more than 150 doctors from 35+ countries</p>
                                            </td>
                                            <td><p>Other clinics don’t have information about this.</p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Technique</th>
                                            <td><p> DHI <sup>TM</sup>Technique. The world’s most advanced technique.</p></td>
                                            <td><p>FUE/FUT</p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Knowledge</th>
                                            <td><p> Comprehensive training on the Anatomy of hair, alopecia diagnosis, medical procedures for different stages of hair loss, design and density for 100% natural results</p></td>
                                            <td><p>Basic training about hair loss and the hair transplant techniques
                                                </p></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Trainers</th>
                                            <td><p>Training given under the supervision of master MD surgeons who have performed 1000+</p></td>
                                            <td><p>Other clinics don’t talk much about the experience of their trainers. In most cases, the trainers are trainees themselves.</p></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="card ">
                        <div class="card-body list_image">
                            <div class="col-sm-12">
                                <h2 class="text-center">Hair Transplant Training</h2>
                            </div>
                            <div class="col-sm-12 pt-4 pb-2 ">
                                
                                <p>There are two types of training programs—basic and advanced. For seven days in basic training program, trainee doctors receive hands-on experience. Advanced training program lasts for two months in which all medical practitioners get to learn about DHI’s signature techniques in detail and various other aspects.</p>
                                
                                <p>The training takes place at its VIP clinic and DHI hair restoration training academy in one of the most vibrant heritage cities in the world—New Delhi. Our training is the most comprehensive training program that covers the following aspects:</p>
                                <ul>
                                    <li>Clinic Management</li>
                                    <li>Marketing</li>
                                    <li>Clinic Management</li>
                                    <li>Medical</li>
                                    <li>Training</li>
                                    <li>Risk Management</li>
                                </ul>

                                <div >
                                  <p>Select your location for hair transplant training: </p>
                                  <div class="row">
                                    <div class="col-sm-6">
                                       <ul>
                                        <li><a href="hair-transplant-training/india/">India</a></li>
                                        <li><a href="hair-transplant-training/canada/">Canada</a></li>
                                        <li><a href="hair-transplant-training/china/">China</a></li>
                                        <li><a href="hair-transplant-training/brazil/">Brazil</a></li>
                                        <li><a href="hair-transplant-training/colombia/">Colombia</a></li>
                                 
                                    
                                </ul>
                                    </div>
                                    <div class="col-sm-6">
                                      <ul>
                                        <li><a href="hair-transplant-training/mexico/">Mexico</a></li>
                                        <li><a href="hair-transplant-training/usa/">United States of America</a></li>
                                        <li><a href="hair-transplant-training/delhi/">Delhi</a></li>
                                        <li><a href="hair-transplant-training/chennai/">Chennai</a></li>
                                        <li><a href="hair-transplant-training/gurgaon/">Gurgaon</a></li>
                                 
                                    
                                </ul>
                                    </div>
                                  </div>
                                 
                                </div>
                            </div>
                            <!--   <div class="col-sm-12">
                                  <h4 class="text-center">DHI offers Two Training Programs:</h4>
                              </div> -->
                            <!-- <div class="row">
                              <div class="col-sm-6">
                                  <h3 class="text-center pb-0 mb-0">Basic </h3>
                                  <p class="text-center">(Course Duration - 7 Days)<p>
                              
                                  <ol>
                                      <li>Onsite training for doctors at the DHI International Academy in New Delhi</li>
                                      <li>Introduction to DHI: History, Present, and Future</li>
                                      <li>Introduction to Total Care System, and DHI techniques & Services</li>
                                      <li>Elements of Marketing</li>
                                      <li>Media Planning & Endorsements</li>
                                      <li>Pricing Strategy</li>
                                      <li>Call Handling</li>
                                      <li>DSA: Pre Work, Psychological, Dermatological & Mathematical Aspects</li>
                                      <li>Mathematical Aspects</li>
                                      <li>Formation of Treatment Plan</li>
                                      <li>Mock/Live Consultations</li>
                                      <li>Diagnosis & Alopecia Test</li>
                                      <li>Introduction to the Power of Follow Ups</li>
                                      <li>Clinic Management</li>
                                      <li>Lead Analysis</li>
                                      <li>Reporting: Business & Medical KPIS</li>
                                      <li>Open Discussion</li>
                                      <li>Evaluation & Certification</li>
                                      <li>Training of extraction of grafts</li>
                                      <li>Complete assistance for 2 months.</li>
                                  
                                    </ol>  
                                
                              </div>
                                <div class="col-sm-6 bd-left dot">
                                  <h3 class="text-center pb-0 mb-0">Advance </h3>
                                  <p class="text-center">(Course Duration - 2 Months)<p>
                              
                                    <ol>
                                      <li>Discussion: Introduction to hair loss industry, DHI history</li>
                                      <li> Introduction to hair transplant basics</li>
                                      <li>Discussion: DHI Principles and Objectives.</li>
                                      <li>Introduction to DHI’s Total Care System</li>
                                      <li> Introduction to DHI Technique 
                                        <ul>
                                          <li>DHI v/s FUE</li>
                                          <li>DHI v/s FUT</li>
                                          <li>Live Consultation Observation</li>
                                        </ul>
                                    
             </li>
                                      <li>Understanding DHI Services in detail
                                        <ul>
                                          <li>Platelet-Rich Plasma (PRP)Therapy</li>
                                          <li> Micro-Pigmentation (MPG)</li>
                                          <li> Direct Hair Fusion (DHI )</li>
                                          <li>Dos and Don’ts - Photo Protocol</li>
                                        </ul>
                                    
             </li>
                                      <li>Introduction to Consultation Process
                                        <ul>
                                          <li>Discussion of 7 steps of a successful consultation</li>
                                          <li>TCCS role in Consultation</li>
                                          <li> Importance of understanding impact of hair loss on client</li>
                                        </ul>
                                    
            </li>
                                      <li> Introduction to Hair System
                                        <ul>
                                          <li>Detailed study of hair systems</li>
                                          <li> DSA – Steps of UDSA</li>
                                          <li>Types of treatment offered</li>
                                          <li>Skill Gate</li>
                                          <li>PRP theory and practical session</li>
                                        </ul>
                                    
             </li>
                                      <li>Live Session, Consultation Observation & PRP Practice Session</li>
                                      <li>Preparation of VIVA for Certification</li>
                                    </ol>
                                
                                
                                
                                </div> 
                            
                            
                            
                            </div> -->
                        
                            <!-- <div class="row">
                                <div class="col-sm-6 text-center pb-5 pt-5">
                                  <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal2" class="btn btn-common btn-pdding">Enquire Now for Basic</a> 
                                </div>
                                 <div class="col-sm-6 text-center pb-5 pt-5 bd-left">
                                  <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal2" class="btn btn-common btn-pdding">Enquire Now for Advance</a> 
                                </div>
                                </div> -->      
                          
                          
                        </div>
                    </div>
                </div>
                
                
                
                
                
                <div class="content train_carousel">
                    <div class="card bt-border ">
                        <div class="card-body  ">
                            <h2 class="text-center">Testimonials by DHI Trained Doctors</h2>
                            <div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <!--  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li> -->
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="row ">
                                            <div class="col-sm-6">
                                                <div class="media">
                                                    <img class="mr-3 circle" src="image/team/test1.png"  alt="Generic placeholder image"  >
                                                    <div class="media-body">
                                                        <h5 class="mt-0">Dr Juan Vlieg, Panama</h5>
                                                        “ I decided to join DHI Academy as a DHI Specialist (Hair Transplant Surgeon) as it is the largest, most advanced and prestigious organization in the world. What is special about DHI is the unique technique, innovative methods and strict quality control mechanisms.“
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="media">
                                                    <img class="mr-3 circle" src="image/team/test2.png"  alt="Generic placeholder image"  >
                                                    <div class="media-body">
                                                        <h5 class="mt-0">Dr Sindy Rodriguez Sosa, Colombia</h5>
                                                        “ It was fruitful and fulfilling learning experience at DHI Academy New Delhi, and I feel fortunate to be able to train in the most advanced form of hair transplant. Training with DHI International was a truly fulfilling experience and I feel proud and privileged to be a part of the DHI International family. Thank you DHI !”
                                                    </div>
                                                </div>
                                            </div>
            
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="media">
                                                    <img class="mr-3 circle" src="image/team/test5.jpg"  alt="Generic placeholder image"  >
                                                    <div class="media-body">
                                                        <h5 class="mt-0">Dr Charlie, Philippines / Spain</h5>
                                                        “Observing the highly skilled and some of the best in the world surgeons at DHI was both a learjing experience and motivating factor at the same time. We were trained to do approx. 5 sessions per week. And care was taken to explain every single minute’s details to us. This In turn helped us build a strong foundation, which ultimately led to us becoming competent hair transplant surgeons at such a short time. ”
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="media">
                                                    <img class="mr-3 circle" src="image/team/test4.jpg"  alt="Generic placeholder image"  >
                                                    <div class="media-body">
                                                        <h5 class="mt-0">Dr Rukshan Senanayake, Brisbane</h5>
                                                        “ I was trained in different hair types, both in male and female cases. Learning how to important across all the grades of Androgenic Alopecia was extremely fulfilling. I was also taught the minute details of implantation, e.g. the Angle and direction, the density and distribution of implanted hair which leads to absolutely natural results. It was enlightening and encouraging to see so many beautiful, natural results in the follow up cases. “
                                                    </div>
                                                </div>
                                            </div>
            
                                        </div> 
                                    </div>
        
                                </div>
      
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </section>
        
        <div class="clearfix"></div>
        
        <section class="bottm_sec">
            <h1>Register for Hair Transplant Training
            </h1>
            <a <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal2" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
        </section>
        
        <!-- FOOTER -->
        
    <?php include 'footer.php';?>
        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLabel">Register for Hair Transplant Training</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="p-5" >
                            <form>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 col-form-label">Type of Training</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="exampleFormControlSelect1">
                                            <option>Basic</option>
                                            <option>Advance</option>
          
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="exampleFormControlInput1">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Email Id</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="exampleFormControlInput1" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Contact Number</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="exampleFormControlInput1" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Select Country</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="exampleFormControlSelect1">
                                            <option>India</option>
                                            <option>India</option>
          
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row ">
                                    <div class="col-sm-12 m-2">
                                        <h1 class="text-center"><button type="submit" class="btn btn-common padd-box2 btn-pdding ">Submit</button></h1>
                                    </div>
        
                                </div>
</form>
    
                        </div>
                    </div>
          
                </div>
            </div>
        </div>
    </body>
    
</html>