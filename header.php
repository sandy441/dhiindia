<?php
    /*Just for your server-side code*/
    header('Content-Type: text/html; charset=utf-8');
    include 'classes/userinfo.php';
    $userinfo = new userinfo();
    $host = $userinfo->getBaseUrl();
	$ruri = $host.$_SERVER['REQUEST_URI'];
   
?>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '424471077707230');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=424471077707230&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-71179767-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-71179767-1');
</script>
<!-- Global site tag (gtag.js) - Google AdWords: 971524401 -->

<script async src="https://www.googletagmanager.com/gtag/js?id=AW-971524401"></script>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
 
 gtag('js', new Date());

  gtag('config', 'AW-971524401');
</script>

 <meta name="google-site-verification" content="fJ4vkC5vVXBSwS9M9C3sZ6aU0QgO6IOpqi1WjvicA-4" />
 <meta name="msvalidate.01" content="415CD5C067BF6533236893C6FE5B62E7" />
<link rel="icon" href="<?php echo $host;?>/image/favicon.png">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<!--  <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
<!-- Custom styles for this template -->
<link href="<?php echo $host;?>/css/carousel.css" rel="stylesheet">
<link rel=”canonical” href=”<?php echo $ruri; ?>” /> 
        <link href="<?php echo $host;?>/css/jquery-ui.css" rel="stylesheet">
<!-- <link href="css/slick.min.css" rel="stylesheet">
<link href="css/slick-theme.css" rel="stylesheet"> -->
<link href="<?php echo $host;?>/css/wpcustom.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo $host;?>/css/open-iconic-bootstrap.css" >
<input type="hidden" name="baseurl" id="baseurl" value="<?php echo $host;?>">		
<section class="box-position">
<div class="container-fluid">
        <div class="col-xs-12 col-md-3 col-sm-4 pull-right message_box">

             <!-- Message box title    --> 
            <div class="panel">
               
                <div class="panel-heading top-bar" role="tab" id="open-message">
                    
                    <!-- <div class=" clearfix">
                        <p class="panel-title float-left"> <strong>Need Help?</strong> </p>
                        <p class="float-right"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#message" aria-expanded="false" aria-controls="message">
                        <span class="glyphicon glyphicon-plus"></span></a></p>
                    </div> -->
      <h5 class="panel-title">
        <a class="collapsed" data-plussign="" data-toggle="collapse" data-parent="#accordion" href="#message" aria-expanded="false" aria-controls="collapseTwo">
         <strong>Need Help?</strong>
        </a>
      </h5>      
        </div>
            </div>
                
             <!-- Message body  --> 
             
                <div id="message" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body p-2">
                        <form  action="#" name="needHelpForm" id="needHelpForm">
                        <p class="text-center">Request a Callback</p>
                        <small id="emailHelp" class="pb-2 form-text text-muted text-center">Speak to Our Hair Loss Experts</small>
                        
                        <div class="form-group ">
                            <input type="text" class="form-control" data-input="name" name="name" id="" placeholder="Name*" required="required">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" data-input="phone" name="phone" maxlength="11" id="" placeholder="Phone*" required="required">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" data-input="email" name="email" id="" placeholder="Email" >
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" data-input="city" name="city" id="" placeholder="City*" required="required">
                        </div>
                        
                        
                        <div class="form-group">
                            <input type="button" data-formsubmitbtn="" class="form-control btn-common" id="" value="Send">
                        </div>
                        
                    </form>
						<div class="alert alert-success" data-successdiv="" style='display:none;'>
							Thanks for contacting us! We will get in touch with you shortly.
						</div>
                    </div>
                </div>
                
        </div>
</div>
</section>
<div class="custom_nav">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a  href="./"><img alt="direct hair implantation 
                           " src="<?php echo $host;?>/image/dhi-small.png" width="110"></a>
        <form class="form-inline text-right mt-md-0 d-block d-sm-none">            
            <a href="<?php echo $host;?>/book-an-appointment.php" class="btn btn-outline-info my-sm-0 ">Book an Appointment</a>
        </form>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto ">
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Hair Loss
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        
                        <a class="dropdown-item" href="hair-anatomy.php">Hair Anatomy</a>  
                        <a class="dropdown-item" href="alopecia-types.php">Type of Alopecia</a>
                        <a class="dropdown-item" href="hair-loss-causes.php">Causes of Hair Loss</a>
                        <a class="dropdown-item" href="hair-loss-treatment.php">Hair Loss Treatment</a>
                        
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        About Us
                    </a>
                    
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo $host;?>/about.php">About DHI</a>
                        <a class="dropdown-item" href="<?php echo $host;?>/achievements.php">Milestone</a>
                        <a class="dropdown-item" href="<?php echo $host;?>/clinics.php">Our Clinics</a>
                        <a class="dropdown-item" href="<?php echo $host;?>/medical-tourisum.php"> Medical Tourism</a>
                        <a class="dropdown-item" href="<?php echo $host;?>/hair-transplant-training.php"> Training Academy</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Treatments
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo $host;?>/hair-loss-diagnosis.php">Alopecia Diagnostic Test</a>
                        <a class="dropdown-item" href="<?php echo $host;?>/direct-hair-implantation.php">Direct Hair Implantation</a>
                        <a class="dropdown-item" href="<?php echo $host;?>/direct-hair-fusion.php">Hair Prosthetics</a>
                        <a class="dropdown-item" href="<?php echo $host;?>/prp.php">Platelet Rich Plasma</a>
                        
                        <!-- <a class="dropdown-item" href="laser-anageny.php">Laser Anagen</a> -->
                        <a  class="dropdown-item"  href="<?php echo $host;?>/scalp-micro-pigmentation.php">Scalp Micropigmentation</a>
                        
                        
                        <a class="dropdown-item" href="<?php echo $host;?>/eyebrow-restoration.php">Eyebrow Restoration</a>
                        <a class="dropdown-item"  href="<?php echo $host;?>/beard-restoration.php">Beard Restoration</a>
                        <a class="dropdown-item"  href="<?php echo $host;?>/scar-repair-treatments.php">Scar Repair</a>
                        <a class="dropdown-item"  href="<?php echo $host;?>/e-shop.php">Hair Care Products</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="<?php echo $host;?>/results.php">results</a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo $host;?>/client-feedback.php">Client's Feedback</a>
                </li>
                
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Resource
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo $host;?>//blog">Blog</a>
                        <a class="dropdown-item" href="<?php echo $host;?>/questions-you-must-ask.php">FAQ</a>
                        <a class="dropdown-item" href="<?php echo $host;?>/careers.php">Work with Us</a>
                        <a class="dropdown-item" href="<?php echo $host;?>/contact.php">Contact Us</a>
                </li>
                
            </ul>
            
            <form class="form-inline mt-2 mt-md-0 d-none d-sm-block clinic_name">
                <a href="tel:18001039300" class="colw my-2 my-sm-0 " style="font-size: 12px;font-weight: bold;">Toll Free :1800 103 9300</a>
                <a href="<?php echo $host;?>/book-an-appointment.php" class="btn btn-outline-info my-2 my-sm-0 ">Book an Appointment</a>
            </form>
        </div>
    </nav>
</div> 
