<?php
    /*Just for your server-side code*/
    header('Content-Type: text/html; charset=ISO-8859-1');
?>
<!doctype html>
<html lang="en">    
        <body>
    
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="http://<?php echo $host;?>/favicon.ico">
        
        <title>DHI-Home</title>
        
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <!--  <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
        <!-- Custom styles for this template -->
        <link href="http://<?php echo $host;?>/css/carousel.css" rel="stylesheet">
        <link href="http://<?php echo $host;?>/css/jquery-ui.css" rel="stylesheet">
        <!--<link href="http://<?php echo $host;?>/css/comment.css" rel="stylesheet">-->
        <link href="http://<?php echo $host;?>/css/wpcustom.css" rel="stylesheet">
        <link rel="stylesheet" href="http://<?php echo $host;?>/css/open-iconic-bootstrap.css" >
        
    </head>
    <div class="custom_nav">
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a  href="./"><img alt="logo" src="http://<?php echo $host;?>/image/dhi-small.png" width="110"></a>
            <form class="form-inline text-right mt-md-0 d-block d-sm-none">
                <a href="http://<?php echo $host;?>/book-appoint.php" class="btn btn-outline-info my-sm-0 " >Book an Appointment</a>
            </form>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto ">
                    <li class="nav-item ">
                        <a href="http://<?php echo $host;?>/about-dhi.php">About Us </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Treatments
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="http://<?php echo $host;?>/direct-hair.php">Direct Hair Implantation</a>
                            <a class="dropdown-item" href="http://<?php echo $host;?>/dhf.php">Direct Hair Fusion</a>
                            <a class="dropdown-item" href="http://<?php echo $host;?>/prp.php">Platelet Rich Plasma</a>
              
                            <!-- <a class="dropdown-item" href="laser-anageny.php">Laser Anagen</a> -->
                            <a  class="dropdown-item"  href="http://<?php echo $host;?>/sclapmicro.php">Scalp Micropigmentation</a>
              
              
                            <a class="dropdown-item" href="http://<?php echo $host;?>/eyebrow.php">Eyebrow Restoration</a>
                            <a class="dropdown-item"  href="http://<?php echo $host;?>/beard-restoration.php">Beard Restoration</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="http://<?php echo $host;?>/result.php">Results</a>
                    </li>
                    <li class="nav-item">
                        <a href="http://<?php echo $host;?>/testimonials.php">Client's Feedback</a>
                    </li>
                
                    <li class="nav-item dropdown">
                        <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Resource
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="http://<?php echo $host;?>/blog">Blog</a>
                            <a class="dropdown-item" href="http://<?php echo $host;?>/faq.php">FAQ</a>
                            <a class="dropdown-item" href="http://<?php echo $host;?>/contact-us.php">Contact Us</a>
                    </li>
          
                </ul>
              
                <form class="form-inline mt-2 mt-md-0 d-none d-sm-block clinic_name">
                    <a href="tel:180010393005" class="colw my-2 my-sm-0 " style="font-size: 12px;font-weight: bold;">Toll Free :1800 103 9300</a>
                    <a href="http://<?php echo $host;?>/book-appointment.php" class="btn btn-outline-info my-2 my-sm-0 " >Book an Appointment</a>
                </form>
            </div>
        </nav>
    </div> 