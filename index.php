
<!doctype html>
<html lang="en">
   <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="DHI India is a leading hair transplantation, implantation & restoration clinic in India, having 47+ years of experience in hair transplant surgery treatment & have more then 200,000 clients with us. DHI is a leading scalp & hair clinic provides hair weaving treatment in India">
   
   


<title>Direct Hair Transplantation Implantation & Restoration Surgery Clinic India - DHI India
</title>
<?php include 'header.php';?>
    </head>

  <body>
  
    <header>
      <div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
   <li data-target="#demo" data-slide-to="3"></li>
    <li data-target="#demo" data-slide-to="4"></li>
    <li data-target="#demo" data-slide-to="5"></li>
    <li data-target="#demo" data-slide-to="6"></li>
   <!--  <li data-target="#demo" data-slide-to="7"></li> -->
  </ul>
  <div class="carousel-inner">
    
   
    <div class="carousel-item active" >
     <video  preload="auto" width="100%" autoplay>
                        <source type="video/webm" src="video/dhf.mp4">
                        <source type="video/mp4" src="video/dhf.mp4">
                        <source type="video/webm" src="video/dhf.ogg">
                        <source type="video/ogg" src="video/dhf.ogg">
                    </video>  
    </div>
     <div class="carousel-item " >
     <a href="direct-hair-implantation.php"><img src="image/Banner_prp4.jpg" alt="art and science of hair
" width="1100" height="500"></a> 
      <!-- <div class="carousel-caption">
        
      </div>  -->  
    </div>
      <div class="carousel-item" >
     <a href="safety-protocol.php"><img src="image/Banner_PRP.jpg" alt="dhi patient safety protocol
" width="1100" height="500"></a> 
     <!--  <div class="carousel-caption">
        
        
      </div>  -->  
    </div>
    <div class="carousel-item" >
      <a href="results.php"><img src="image/banner results.jpg" alt="prabhjot singh indian hockey player team
" width="1100" height="500"></a>
     <!--  <div class="carousel-caption">
       
      </div> -->   
    </div>
  
     <div class="carousel-item" >
    <img src="image/banner_doctors.jpg" alt="Chicago" width="1100" height="500"> 
     <!--  <div class="carousel-caption">
        
        
      </div>  -->  
    </div>
  <!--   <div class="carousel-item">
     <a href="javascript:void(0);"><img src="image/beard banner.jpg" alt="Chicago" width="1100" height="500"></a> 
      <div class="carousel-caption">
        
        
      </div>  
    </div> -->
    <div class="carousel-item" >
     <a href="javascript:void(0);"><img src="image/banner_female.jpg" alt="Chicago" width="1100" height="500"></a> 
     <!--  <div class="carousel-caption">
        
        
      </div>  -->  
    </div>
    <div class="carousel-item">
     <img src="image/banner_map.jpg" alt="New York" width="1100" height="500">
       <div class="carousel-caption d-none d-md-block">
    <h3><a href="about.php" class="btn btn-outline-light btn-lg">Learn more</a></h3>
   
  </div>  
    </div>
  </div>
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>

    </header>
<section class="bg-col-1 pt-5 pb-5 threeSteps">
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      
      <h2 class="text-center  pb-5">How Direct Hair Implantation (DHI) Works</h2>
    </div>
    <div class="col-sm-4">
      <div class="card mb-3" >
  <img class="card-img-top" src="image/dhi copy.jpg" alt="DHI hair evaluation test">
  <div class="card-body  text-center">
    <div class="circle1 mb-3">
       <span>1</span>
      </div>
    <h5 class="card-title">Unique Diagnostic System for Alopecia (UDSA)</h5>
    <p class="card-text clinic_name"><a href="#" data-toggle="tooltip" data-placement="top" title="UDSA was developed by a team of world
renowned experts to provide accurate and
personalized diagnosis for all types of
alopecia – hair and scalp disorders" >
  UDSA</a>, is a combination of tests which includes Psychological Aspects, Dermatological Examination, Mathematical Aspects and the DHI Alopecia Test .</p>

  </div>
</div>
    </div>
    <div class="col-sm-4">
      <div class="card mb-3" >
  <img class="card-img-top" src="image/Layer 16.jpg" alt="Dhi hair follicle extractor">
  <div class="card-body  text-center">
    <div class="circle1 mb-3">
       <span>2</span>
      </div>
    <h5 class="card-title">Extraction Phase</h5>
    <p class="card-text">Hair Follicles are extracted one by one from
the donor area using Titanium coated single use tool with the diameter of 0.7mm or more.</p>

  </div>
</div>
    </div>
    <div class="col-sm-4">
      <div class="card mb-3" >
  <img class="card-img-top" src="image/Layer 17.jpg" alt=" dhi hair transplant implanter">
  <div class="card-body  text-center">
    <div class="circle1 mb-3">
       <span>3</span>
      </div>
    <h5 class="card-title">Placement Phase</h5>
    <p class="card-text">Hair follicles are implanted directly into the region suffering from hair loss using a patented tool with a perfect control on depth, angle & direction of hair. DHI’s Total Care System protocols ensures adequate density as per ADT (Average Density Target). 
    </p>

  </div>
</div>
    </div>
  </div>
</div>
</section>
     
<section>
  <div class="container-fluid">
    <div class="row ">
      <div class="col-sm-6 no-gutters pt-2 pb-2 pr-lg-1">
   <a href="#" data-toggle="modal" data-target="#exampleModaldhf" class="video_img"> <img src="image/home/Rectangle 31.jpg" alt="DHI Direct hair fusion and hair replacement" class="img-fluid"></a>
      </div>
     <div class="col-sm-6 no-gutters pt-lg-2 pb-2 pl-lg-1">
   <a href="prp.php"> <img src="image/home/Rectangle 3 copy.jpg" alt="img" class="img-fluid "></a>
      </div>
      <div class="col-sm-6 no-gutters pr-lg-1">
    <a href="scalp-micro-pigmentation.php">
      <img src="image/home/Rectangle 3 copy 21.jpg" alt="img" class="img-fluid"></a>
   
      </div>
      <div class="col-sm-6 no-gutters pl-lg-1">
    <a href="e-shop.php"><img src="image/home/Rectangle 3 copy 2.jpg" alt="img" class="img-fluid"></a>
      </div>
<div class="col-sm-12 no-gutters  pb-2 pt-2">
    <a href="results.php"> <img src="image/home/resultsimage.jpg" alt="dhi client reasult - natural and safe
" class="img-fluid"></a>
      </div>
      <div class="col-sm-12 no-gutters pb-2">
    <a href="client-feedback.php"><img src="image/home/Layer 39.jpg" alt="dhi ranks no 1 in customer satisfation" class="img-fluid"></a>
      </div>
     
     <div class="col-sm-6 no-gutters  pb-2 pr-1">
    <img src="image/home/Layer 38.jpg" alt="dhi ranks no 1 in customer satisfation" class="img-fluid">
       <div class="icon_2">
        <ul>
          <li><a href="pdf/IMRB.pdf" target="_blank"><img src="image/home/Layer 44.png" alt="imrb" width="70"></a></li>
          <li><a href="pdf/What Clinic.pdf" target="_blank"><img src="image/home/what_clinic_logo1.png" alt="whatclinic" width="160"></a></li>
        </ul>
      </div>
      </div>
      <div class="col-sm-6 no-gutters pl-1 pb-2">
    <a href="hair-transplant-training.php" ><img src="image/home/mp.jpg" alt="dhi training academy
" class="img-fluid"></a>
      </div>
     
    </div>
  </div>
</section>

 



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  
<!-- Modal -->
<div class="modal fade" id="exampleModaldhf" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Direct Hair Fusion (Hair Replacement )</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <video  preload="auto" width="100%" autoplay controls="">
                        <source type="video/webm" src="video/hair-prosthetic.mp4">
                        <source type="video/mp4" src="video/hair-prosthetic.mp4">
                        <source type="video/webm" src="video/hair-prosthetic.ogg">
                        <source type="video/ogg" src="video/hair-prosthetic.ogg">
                    </video>
      </div>
     
    </div>
  </div>
</div>

    
    
  </body>
</html>
