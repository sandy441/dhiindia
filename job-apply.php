
<!doctype html>
<html lang="en">
    
    <body>
<?php
include 'classes/userinfo.php';
$userinfo = new userinfo();
$home_url = $userinfo->getBaseUrl();
$title = "Senior UX Designer";
$shortDesc = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus possimus soluta voluptates sed sit alias mollitia consequuntur, voluptas vitae nobis assumenda inventore eos dolorem consectetur fugiat deleniti beatae placeat explicabo.";


?>        
<?php include 'header.php';?>
        
        
        <section class="bg-col-1">
            <div class="container">
                
                <div class="content">
                    <div class="card bt-border">
                        <div class="card-body"> 
                            <a href="careers.php" class="btn primary-link" style="    margin: 30px 0 0 30px;"><span class="oi oi-arrow-left"></span>  Back to Current Openings</a>
                            
                            <h2 class="primary-link"><?php echo $title;?></h2>
                            <p><?php echo $shortDesc;?></p>
                            <form class="p-5" name="jobapply" id="jobapply" enctype="multipart/form-data" method="post" action="<?php echo $home_url;?>/formrequest.php">
                                   <input type="hidden" name="formtype" value="jobapply">
                                   <input type="hidden" name="jobtitle" value="<?php echo $title;?>">
                                <h3 class="pb-4 pl-0 pr-0">Personal information</h3>
                                
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        
                                        <input type="text" class="form-control form-control-lg" name="fname" id="" placeholder="First Name*">
                                    </div>
                                    <div class="form-group col-md-6">
                                        
                                        <input type="text" class="form-control form-control-lg" id="" name="lname" placeholder="Last Name*">
                                    </div>
                                    
                                </div>
                                
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <input type="email" class="form-control form-control-lg" id="" name="email" placeholder="Email Id*">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" class="form-control form-control-lg" id="" name="phone" placeholder="Phone Number*">
                                    </div>
                                </div>
                             
                                <h3 class="pb-4 pl-0 pr-0">Upload Resume(DOC File allowed)</h3>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <!--  <label for="exampleFormControlFile1">Example file input</label> -->
                                        <input type="file" class="form-control-file" name="resume" id="">
                                    </div>
                                    
                                </div>
                                <div class="text-center">
                                    <input type="submit" class="btn btn-common btn-pdding m-4" value="Submit" name="sbmitjob">
                                </div>
                                
                            </form>
                            
                            
                        </div>
                    </div>
                </div>
                
                
                
                
                
                
            </div>
            
            
            
            
        </div>
        
        
    </section>
    
    
    <div class="clearfix"></div>
    
    
    
    <!-- FOOTER -->
    
<?php include 'footer.php';?>
    
    
    
    
</body>
</html>
