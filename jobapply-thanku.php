<!doctype html>
<html lang="en">
    
    <body>
<?php include 'header.php';?>        
        <section class="bg-col-1">
            <div class="container">
                <div class="content">
                    <div class="card contactIcon">
                        <div class="card-body maginExtra"> 
                            <div class="row maxwid600 pb-4">
                                
                                <div class="col-sm-12 text-center"><h2>Get in touch with us</h2></div>
                                
                                <div class="col-sm-6 text-center">
                                    <img src="image/icons/contact-us.png" alt="" width="50" class="pb-2">
                                    
                                    <p>Toll Free : <a href="tel:18001039300">1800 103 9300</a></p>
                                </div>
                                <!-- <div class="col-sm-4 text-center">
                                  <img src="image/icons/address.png" alt="" width="60" class="pb-3">
                                  <p>(Toll Free)</p>
                                  <p>1800 103 9300</p>
                                </div> -->
                                <div class="col-sm-6 text-center">
                                    <img src="image/icons/email.png" alt="" width="60" class="pb-2">
                                    
                                    <p><a href="mailto:enquiry@dhiindia.com">enquiry@dhiindia.com</a></p>
                                </div>
                                
                                
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="content ">
                    <div class="card bt-border">
                        <div class="card-body maginExtra"> 
                            <div class="row text-center">
                                <div class="col-sm-12 "><h2>Thank You for contacting us. <br>We will get back to you as soon as we can.</h2></div>
                                <ul class="social_icon mx-auto p-0">
                                    <li><a href="https://www.facebook.com/DHIIndia" target="_blank"><img src="image/icons/FB.PNG" alt="image" width="60"></a></li>
                                    <li><a href="https://www.instagram.com/dhiindia/" target="_blank"><img src="image/icons/INSTA.PNG" alt="image" width="60"></a></li>
                                    <li><a href="https://twitter.com/DHIMedGroup" target="_blank"><img src="image/icons/Twitter.png" alt="image" width="60"></a></li>
                                    <li><a href="https://www.youtube.com/channel/UC6ZxzZKS4ivYKMsWjOHNR_A" target="_blank"><img src="image/icons/youtube.png" alt="youtube logo image" width="60"></a></li>
                                </ul>
                                
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
            
        </section>
        
        
        <div class="clearfix"></div>
        
        
        
        
        <!-- FOOTER -->
        
<?php include 'footer.php';?>
        
        
        
        
        
    </body>
</html>
