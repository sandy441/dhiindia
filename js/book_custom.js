( function( $ ) {
$(document).ready(function(){
	var cityId = '#sel-city',
        appDate  = '#appointment_inputnamedate',
        timeSlot  = '#timeForm',
        timeSlot  = '#timeForm';
		$(cityId).change(function()
		{	
			var cityId;
			var appDates;
			var nos_days_f;
			var consultation_fe;
			var actual_fe;
			var discount_fe ;
			var baseurl =$("#baseurl").val();
			var citySelect = $(this),
				city = citySelect.val();
				$(appDate+","+timeSlot).val('');
				$(appDate+","+timeSlot).trigger('blur');
				$('#consultationForm').val('');
				$(appDate).attr('disabled', 'disabled');
				$(appDate).after('<div class="loading">Loading...</div>');
				$(timeSlot).attr('disabled', 'disabled');
				/* Refetch number of days calendar is open */
				
				$.ajax({
				
						type: 'POST',
						url: $("#baseurl").val()+'/classes/json_events.php',
						data: {cityget : city, calendarlimit:'set'},
						error:function(data){
							console.log(data);
						},
						success: function(data){
							var data12 = $.parseJSON(data);
							$(appDate).removeAttr('disabled');
							$('.loading').css('display','none');
							
							nos_days_f = data12.nos_days - 1;
							$("#appointment_inputnamedate").val('');
							$("#appointment_inputnamedate").datepicker( "destroy" );
							$("#appointment_inputnamedate").datepicker({ minDate: -0, maxDate: nos_days_f, dateFormat: 'dd-mm-yy' });
							$('#appointment-consultation-address p').html(data12.address);
							 $('#appointment-paynow').attr('data-ref', data12.consultation_fees);
							 $('#appointment-paylater').attr('data-ref', data12.actual_fees);
							$('.fee-consi').html('INR ' +data12.consultation_fees); 
							$('.appointment-fee-cut').html(+data12.actual_fees);
							$('.consultation-fee-inr12').html('INR ' +data12.actual_fees);
							if(city==1)
							{
								 $('.fee-consi-skype').html('INR ' +data12.consultation_fees); 
							     $('.skype-price-cut').html(+data12.actual_fees); 
								 $('#consultationForm').val(data12.consultation_fees);
							}
						},
					}); 
				
		}).trigger('change');
/* Refetch number of days calendar is open */
			$(appDate).change(function()
				{   $(timeSlot).after('<div class="loading">Loading...</div>');
					appDates = $(this),
					appDateF = appDates.val();
					var timeList = $('#sel-city').parents('form').find(timeSlot);
					var city = $('#sel-city').val();
					if(city != "default") {
						$.ajax({
						type: 'POST',
						url: $("#baseurl").val()+'/classes/json_events.php',
						data: {cityId : city , selectedDate : appDateF},
						success: function(data){
							timeList.empty();
							var options = $.parseJSON(data);
							$(timeSlot).removeAttr('disabled');
							$('.loading').css('display','none');
							if(options.length == 0)
							{
								timeList.append('<option value="null"></option>');
							}
							else{
							timeList.append('<option  value=""></option>');
							for(i=0;i<options.length;i++)
								{
									if(options[i].slotcheck=='enable')
									{timeList.append('<option style="color:#555;font-weight:bold" value="'+options[i].value+'">'+options[i].text+'</option>');}
									if(options[i].slotcheck=='disable')
									{timeList.append('<option disabled value="'+options[i].value+'">'+options[i].text+'</option>');}
									
								}
							
							}
                                                        timeList.removeAttr('disabled');
                                                        
						 }
						});
     				  }	
					});
				$('.cons_pay').on('change', function() {
				   var cons_fess_assing = $("input[name=payForm]:checked").attr("data-ref");
				   $('#consultationForm').val(cons_fess_assing);
				});
				$('#navigation-main #div-appointment-paynow,#navigation-main  #div-appointment-paylater').click(function(){
					$('#navigation-main .appointment-div-btn').css('display','block');
				});
				var change_count = 0
				$('#navigation-main #sel-city').on('change',function(){
					if($(this).val() == "1"){
						$('#navigation-main .appointment-div-btn').css('display','block');
					}
					else{
						$('#navigation-main .appointment-div-btn').css('display','none');
					}
					if(change_count != 0)
					$(this).addClass('selected');
					change_count++;
				});
			}).trigger('change');
			

   

  } )( jQuery );
  


	