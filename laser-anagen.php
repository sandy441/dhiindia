
<!doctype html>
<html lang="en">
 
  <body>
<?php include 'header.php';?>
<section class="bg-col-1">
   <div class="clearfix">
           
            	<video  preload="auto" width="100%" loop autoplay>
                        <source type="video/webm" src="video/angen-laserr.mp4">
                        <source type="video/mp4" src="video/angen-laserrt.mp4">
                        <source type="video/webm" src="video/angen-laserr.ogg">
                        <source type="video/ogg" src="video/angen-laserr.ogg">
                    </video>
           
        </div>
        
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card mar-minus">
            <div class="card-body"> 
               <div class="row ">
     
    <div class="col-sm-6"><h2>WHAT ANAGEN LASER TREATMENT ?</h2>
      <p>US FDA approved, Low Level Laser Light Treatment is a preventive therapy for men and women with Alopecia to arrest hair loss and promote hair growth. This simple, pain-free, non-surgical treatment makes use of laser light at a controlled wavelength to stimulate the follicles and rejuvenate them, thereby resulting in healthy hair. It invigorates hair by increasing production of cellular energy of the cells and increases microcirculation. Each session lasts approximately 20 minutes and gives fuller, thicker, shinier hair in few months. The benefits of this treatment are generally cumulative, and visible only after a period of consistent treatment. Patients are advised to use DHI Anagen Hair Loss Treatment Lotion and Shampoo for the best results.</p>
    </div>
    <div class="col-sm-6">
      <div class="table-box">
        <div class="table-img">
          <img  class="img-fluid" src="image/laser-img.png" alt="" />
        </div>
      </div>
      
    </div>
   
  </div>
            </div>
          </div>
           
            </div>

            <div class="content">
  <div class="card bt-border">
            <div class="card-body"> 
              <h2 class="text-center">BENEFITS OF THIS TREATMENT</h2>
               <div class="row icon_box">
     
  
    
     <div class="col text-center ">
      
     
       <img src="image/benefit-img.png" alt="" width="100">
     <p>Works on men and women,
for all stages of hair loss</p>
     
   
     </div>
     <div class="col text-center"> <img src="image/benefit-img1.png" alt="" width="100">
     <p>No downtime and 
no side effects</p></div>
     <div class="col text-center"> <img src="image/benefit-img2.png" alt="" width="100">
     <p>Gentle, painless and 
fuss-free</p></div>
     <div class="col text-center"> <img src="image/benefit-img3.png" alt="" width="100">
     <p>Suitable for modest budgets</p></div>
     <div class="col text-center"> <img src="image/benefit-img4.png" alt="" width="100">
     <p>Minimal time commitment</p></div>
    


  
  </div>
  

            </div>
          </div>
           
            </div>
	

                 	</div>


</section>


     
<div class="clearfix"></div>
<section class=" slick-sec bg-col-1">
  

  <div class="container">
  <div class="single-item">
    <div class="card">
<video id="one" src="http://182.71.168.117/dhinew/key-feature/painfree.mp4" autobuffer autoloop loop autoplay="autoplay" ></video>

  <div class="card-body">
    <h4 class="card-title">Card title1</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    
  </div>
</div>
   <div class="card" >
  <video id="two" src="http://182.71.168.117/dhinew/key-feature/graft-viability.mp4" autobuffer autoloop loop autoplay="autoplay" ></video>
  <div class="card-body">
    <h4 class="card-title">Card title2</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    
  </div>
</div>
   <div class="card" >
  <video id="three" src="http://182.71.168.117/dhinew/key-feature/natural-results.mp4" autobuffer autoloop loop autoplay="autoplay" ></video>
  <div class="card-body">
    <h4 class="card-title">Card title3</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    
  </div>
</div>
  <div class="card" >
  <video src="http://182.71.168.117/dhinew/key-feature/hide-scars.mp4" autobuffer autoloop loop autoplay="autoplay" ></video>
  <div class="card-body">
    <h4 class="card-title">Card title4</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    
  </div>
</div>
   <div class="card" >
  <video src="http://182.71.168.117/dhinew/key-feature/doctors-only_02.mp4" autobuffer autoloop loop autoplay="autoplay" ></video>
  <div class="card-body">
    <h4 class="card-title">Card title5</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    
  </div>
</div>
   <div class="card" >
  <video src="http://182.71.168.117/dhinew/key-feature/safety.mp4" autobuffer autoloop loop autoplay="autoplay" ></video>
  <div class="card-body">
    <h4 class="card-title">Card title6</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    
  </div>
</div>
  </div>
</div>
  
</section>
     
<section class="bottm_sec">
  <h1>Increase your hair density naturally</h1>
   <a href="javascript:void(0);" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>


      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


  </body>
</html>
