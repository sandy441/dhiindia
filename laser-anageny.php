
<!doctype html>
<html lang="en">

  <body>
<?php include 'header.php';?>
<section class="bg-col-1">
   <div class="clearfix">
           
            	<video  preload="auto" width="100%" loop autoplay>
                        <source type="video/webm" src="video/angen-laserr.mp4">
                        <source type="video/mp4" src="video/angen-laserrt.mp4">
                        <source type="video/webm" src="video/angen-laserr.ogg">
                        <source type="video/ogg" src="video/angen-laserr.ogg">
                    </video>
           
        </div>
        
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card mar-minus">
            <div class="card-body maginExtra"> 
               <div class="row ">
     
    <div class="col-sm-6"><h2> Anagen Laser Treatment </h2>
      <p>US FDA approved, Low Level Laser Light Treatment is a preventive therapy for men and women with Alopecia to arrest hair loss and promote hair growth. This simple, pain-free, non-surgical treatment makes use of laser light at a controlled wavelength to stimulate the follicles and rejuvenate them, thereby resulting in healthy hair. It invigorates hair by increasing production of cellular energy of the cells and increases microcirculation. Each session lasts approximately 20 minutes and gives fuller, thicker, shinier hair in few months. The benefits of this treatment are generally cumulative, and visible only after a period of consistent treatment. Patients are advised to use DHI Anagen Hair Loss Treatment Lotion and Shampoo for the best results.</p>
    </div>
    <div class="col-sm-6">
      <div class="table-box">
        <div class="table-img">
          <img  class="img-fluid" src="image/laser-img.png" alt="" />
        </div>
      </div>
      
    </div>
   
  </div>
            </div>
          </div>
           
            </div>

            <div class="content">
  <div class="card bt-border">
            <div class="card-body maginExtra"> 
              <p class="text-center pt-5 pb-5" style="font-size: 30px;">Benefits of This Treatment</p>
               <div class="row icon_box">
     
  
    
     <div class="col text-center ">
      
     
       <img src="./image/icons/men-women.png" alt="" width="80">
     <p  class="pt-3 pb-3">Works on men and women,
for all stages of hair loss</p>
     
   
     </div>
     <div class="col text-center"> <img src="./image/icons/side-effect.png" alt="" width="80">
     <p class="pt-3 pb-3">No downtime and 
no side effects</p></div>
     <div class="col text-center"> <img src="./image/icons/gentel.png" alt="" width="80">
     <p class="pt-3 pb-3">Gentle, painless and 
fuss-free</p></div>
     <div class="col text-center"> <img src="./image/icons/gentel.png" alt="" width="80">
     <p class="pt-3 pb-3">Suitable for modest budgets</p></div>
     <div class="col text-center"> <img src="./image/icons/minimal-time.png" alt="" width="80">
     <p class="pt-3 pb-3">Minimal time commitment</p></div>
    


  
  </div>
  

            </div>
          </div>
           
            </div>
	
        
                 	</div>


</section>


     
<div class="clearfix"></div>

<section class="bottm_sec">
 <h1>Increase your hair density naturally</h1>
   <a href="book-appoint.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>
      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


   
  </body>
</html>
