
<!doctype html>
<html lang="en">

  <body>
<?php include 'header.php';?>

<section class="location bgLocation9">
  <div class="container">
    <div class="row ">
      <div class="col-sm-6 ">
      
       
      </div>
      <div class="col-sm-6">

        <form class="form-padd">

 <h5>Book your consultation</h5>
  <div class="form-row">
    <div class="form-group col-sm-6">
    
      <input type="email" class="form-control" id="inputEmail4" placeholder="Full name">
    </div>
    <div class="form-group col-sm-6">
     
      <input type="password" class="form-control" id="inputPassword4" placeholder="Phone number">
    </div>
    <div class="form-group col-sm-6">
     
      <input type="password" class="form-control" id="inputPassword4" placeholder="Email Id">
    </div>
    <div class="form-group col-sm-6">
      
       <select id="inputState" class="form-control">
        <option selected>Choose City..</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-sm-12">
     
      <input type="password" class="form-control" id="inputPassword4" placeholder="Preferred Date">
    </div>
   
     <!-- <div class="form-group col-sm-12">
      <div class="form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
             Pay Now & Get 50% of on Consultation
          </label>
        </div>
    </div> -->
    <!-- <div class="form-group col-md-6">
     <div class="form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option2" >
             Pay At Clinic
          </label>
        </div>
    </div> -->
  </div>
  
  <div class="text-center">
   <button type="submit" class="btn btn-dark btn-pdding ">Proceed to book</button>
</form>
      </div>
    </div>
  </div>
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card ">
            <div class="card-body"> 
              
<div class="row">
  <div class="col-sm-4 p-5">
   <address>
               Newtouch Laser Center Satellite<br />
                108 Sangrila Arcade, Near Shyaml Char Rasta<br />
                Ahmedabad 380015<br />
                079 – 40062549 & 8511100962<br />
                  Email: info@dhiindia.com, enquiry@dhiindia.com
              </address>
  </div>
  <div class="col-sm-8">
   <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3672.2688626184704!2d72.52655791423625!3d23.013898384957418!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e9b27bf6b5845%3A0x12dfcb181412562e!2sDHI+Hair+Transplantation+Ahmedabad!5e0!3m2!1sen!2sin!4v1501068118708" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen" class="__web-inspector-hide-shortcut__"></iframe>
  </div>
</div>
   

  
    
   
    
   
  </div>
            </div>
          </div>
            <div class="content">
  <div class="card ">
            <div class="card-body"> 
<h3>DHI Hair Transplant Clinic — AHMEDABAD</h3>
<ul>
  <li>DHI Ahmedabad offers a complete range of hair transplant treatments by MCI registered doctors. Our procedures include Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, and Direct Hair Fusion among others..</li>
  <li>
If you are also suffering from any type of hair loss, then pay us a visit at our hair restoration clinic in Ahmedabad for guaranteed solutions. All consultations are confidential and include hair loss evaluation test to help our clients make an informed decision. As hair loss disorders may vary from person to person, our doctors develop a diagnosis and determine best solutions for you

</li>
  <li>Ideally located in the heart of Shyaml Char Rasta in Ahmedabad, DHI Ahmedabad is accessible to people residing in any part of the city. Ahmedabad Railway station is 8.5 km from our location in the city via Relief Road, 9.8 km via Vikas Gruh Road, and 11.2 km via NH 228.
</li>
  <li>To book an appointment for a consultation at our DHI Ahmedabad clinic, give us a call now!</li>
  <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
  
</ul>
            </div>
          </div>
        </div>


        <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
<div class="row">
  <div class="col-sm-6 pr-lg-0 pr-md-0">
  <img src="image/clinic/DHI-Waiting-Area.jpg" alt="" class="img-fluid">
</div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/IMG_20170728_182342.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC_0259.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/IMG_20170728_182302.jpg " alt="" class="img-fluid"></div>
</div>
            </div>
          </div>
        </div>


        <div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 
<h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>

<div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row ">
        <div class="col-sm-6">
          <h4>Sanjeev Kumar</h4>
          <p>Best part about visiting dhi clinic is they do proper diagnosis to eradicate the problem ( hairloss) taken prp and dhi hair transplant and totally satisfied with results. I Highly recommended DHI</p>
        </div>
        <div class="col-sm-6">
          <h4>Jitendra Tanwar</h4>
          <p>Hello everyone, I am from army and got DHI Procedure done yesterday from DHI New Delhi Clinic. I liked the behaviour of staff and procedure was pain free. I would always recommend DHI Procedure of hair transplant to everyone.</p>
        </div>
        
      </div>
    </div>
    <div class="carousel-item">
     <div class="row">
      <div class="col-sm-6">
          <h4>Jiva Tomar</h4>
          <p>I am extremely satisfied with my hair transplantation result in DHI Safdarjung Enclave clinic. Thanks to Dr. Ajay and his team</p>
        </div>
        <div class="col-sm-6">
          <h4>Sunil Singh</h4>
          <p>Very cooperative and professional services,fully satisfied by the transplant and the results thereafter,i will recommend others to go for DHI in case of transplant it may be bit costlier than others but its worth based on safety and results</p>
        </div>
       
      </div> 
    </div>
    <div class="carousel-item">
     <div class="row">
         <div class="col-sm-6">
          <h4>Pankaj Sharma</h4>
          <p>Very cooperative staff and doctors,results was satisfactory.</p>
        </div>
        <div class="col-sm-6">
          <h4>Jiva Tomar</h4>
          <p>DHI is the best clinic in Delhi for Hair Transplant surgery. Right from first interaction to hair transplant and hair growth , my experience was very satisfactory . I just want to appreciate the Delhi clinic of DHI for hair transplant work and behaviour of the entire team. You guys did a fantastic job. I am totally satisfied thank you</p>
        </div>
      </div> 
    </div>
  </div>
 
</div>

            </div>
          </div>
        </div>
            </div>

            
  

                  </div>


</section>

     
<div class="clearfix"></div>



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  



  </body>
</html>
