
<!doctype html>
<html lang="en">

  <body>
<?php include 'header.php';?>

<section class="location bgLocation13">
  <div class="container">
    <div class="row ">
      <div class="col-sm-6 ">
      
       
      </div>
      <div class="col-sm-6">

        <form class="form-padd">

 <h5>Book your consultation</h5>
  <div class="form-row">
    <div class="form-group col-sm-6">
    
      <input type="email" class="form-control" id="inputEmail4" placeholder="Full name">
    </div>
    <div class="form-group col-sm-6">
     
      <input type="password" class="form-control" id="inputPassword4" placeholder="Phone number">
    </div>
    <div class="form-group col-sm-6">
     
      <input type="password" class="form-control" id="inputPassword4" placeholder="Email Id">
    </div>
    <div class="form-group col-sm-6">
      
       <select id="inputState" class="form-control">
        <option selected>Choose City..</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-sm-12">
     
      <input type="password" class="form-control" id="inputPassword4" placeholder="Preferred Date">
    </div>
   
     <!-- <div class="form-group col-sm-12">
      <div class="form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
             Pay Now & Get 50% of on Consultation
          </label>
        </div>
    </div> -->
    <!-- <div class="form-group col-md-6">
     <div class="form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option2" >
             Pay At Clinic
          </label>
        </div>
    </div> -->
  </div>
  
  <div class="text-center">
   <button type="submit" class="btn btn-dark btn-pdding ">Proceed to book</button>
</form>
      </div>
    </div>
  </div>
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card ">
            <div class="card-body"> 
              
<div class="row">
  <div class="col-sm-4 p-5">
  <address>
               Dr. Rafeek’s Skin and Cosmetic Center<br />
                Near Malayala Manorama, East Nadakkavu <br />
                Wayanad Road<br />
                Calicut 673017<br />
                0495 6533302<br />
               Email: info@dhiindia.com, enquiry@dhiindia.com
              </address>
  </div>
  <div class="col-sm-8">
   <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3912.887002597254!2d75.77579961480518!3d11.269715991987932!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba65ecb5828f561%3A0xf7cff16fb95fd2a5!2sDr.+Rafeeq's+Skin+%26+Cosmetic+Surgery+Research+Centre!5e0!3m2!1sen!2sin!4v1501071871158" width="100%" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
  </div>
</div>

  </div>
            </div>
          </div>
            <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3>DHI Hair Transplant Clinic — CALICUT</h3>
<ul>
  <li>DHI hair transplant clinic in Calicut is specially designed to offer the best in class hair transplant solutions to clients from all walks of life using the latest technologies. We are anchored by a team of MCI registered doctors seasoned by years of experience.</li>
  <li>
DHI Calicut operates from Dr. Rafeek’s Skin and Cosmetic Center, which is the only ISO, UKAS, NABCB certified Dermato-cosmetology and Cosmetic Surgery Research Center in South India. Our clinic is equipped with state of the art medical facilities. There are changing rooms and showers along with facilities for relaxation before and after the treatment. We provide clients with a wide range of hair transplant services including Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, and Direct Hair Fusion among others.

</li>
  <li>DHI Calicut clinic is a 3.7 km drive from the Calicut Railway station via Bank Road and Beypore. It will take less than 13 minutes to cover the distance. Prominently located in the heart of vibrant Calicut, we are easily accessible from all parts of the city.
</li>


<li>To book an appointment for a consultation at our DHI Mumbai clinic, give us a call now!</li>
  <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
  
</ul>
            </div>
          </div>
        </div>


        <!-- <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
<div class="row">
  <div class="col-sm-6 pr-lg-0 pr-md-0">
  <img src="image/clinic/DSC_001.jpg" alt="" class="img-fluid">
</div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_002.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC_003.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_004.jpg " alt="" class="img-fluid"></div>
</div>
            </div>
          </div>
        </div> -->


        <div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 
<h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>

<div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row ">
        <div class="col-sm-6">
          <h4>Sanjeev Kumar</h4>
          <p>Best part about visiting dhi clinic is they do proper diagnosis to eradicate the problem ( hairloss) taken prp and dhi hair transplant and totally satisfied with results. I Highly recommended DHI</p>
        </div>
        <div class="col-sm-6">
          <h4>Jitendra Tanwar</h4>
          <p>Hello everyone, I am from army and got DHI Procedure done yesterday from DHI New Delhi Clinic. I liked the behaviour of staff and procedure was pain free. I would always recommend DHI Procedure of hair transplant to everyone.</p>
        </div>
        
      </div>
    </div>
    <div class="carousel-item">
     <div class="row">
      <div class="col-sm-6">
          <h4>Jiva Tomar</h4>
          <p>I am extremely satisfied with my hair transplantation result in DHI Safdarjung Enclave clinic. Thanks to Dr. Ajay and his team</p>
        </div>
        <div class="col-sm-6">
          <h4>Sunil Singh</h4>
          <p>Very cooperative and professional services,fully satisfied by the transplant and the results thereafter,i will recommend others to go for DHI in case of transplant it may be bit costlier than others but its worth based on safety and results</p>
        </div>
       
      </div> 
    </div>
    <div class="carousel-item">
     <div class="row">
         <div class="col-sm-6">
          <h4>Pankaj Sharma</h4>
          <p>Very cooperative staff and doctors,results was satisfactory.</p>
        </div>
        <div class="col-sm-6">
          <h4>Jiva Tomar</h4>
          <p>DHI is the best clinic in Delhi for Hair Transplant surgery. Right from first interaction to hair transplant and hair growth , my experience was very satisfactory . I just want to appreciate the Delhi clinic of DHI for hair transplant work and behaviour of the entire team. You guys did a fantastic job. I am totally satisfied thank you</p>
        </div>
      </div> 
    </div>
  </div>
 
</div>

            </div>
          </div>
        </div>
            </div>

            
  

                  </div>


</section>

     
<div class="clearfix"></div>



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  



  </body>
</html>
