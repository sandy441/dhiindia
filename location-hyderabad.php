
<!doctype html>
<html lang="en">

  <body>
<?php include 'header.php';?>

<section class="location bgLocation10">
  <div class="container">
    <div class="row ">
      <div class="col-sm-6 ">
      
       
      </div>
      <div class="col-sm-6">

        <form class="form-padd">

 <h5>Book your consultation</h5>
  <div class="form-row">
    <div class="form-group col-sm-6">
    
      <input type="email" class="form-control" id="inputEmail4" placeholder="Full name">
    </div>
    <div class="form-group col-sm-6">
     
      <input type="password" class="form-control" id="inputPassword4" placeholder="Phone number">
    </div>
    <div class="form-group col-sm-6">
     
      <input type="password" class="form-control" id="inputPassword4" placeholder="Email Id">
    </div>
    <div class="form-group col-sm-6">
      
       <select id="inputState" class="form-control">
        <option selected>Choose City..</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-sm-12">
     
      <input type="password" class="form-control" id="inputPassword4" placeholder="Preferred Date">
    </div>
   
     <!-- <div class="form-group col-sm-12">
      <div class="form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
             Pay Now & Get 50% of on Consultation
          </label>
        </div>
    </div> -->
    <!-- <div class="form-group col-md-6">
     <div class="form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option2" >
             Pay At Clinic
          </label>
        </div>
    </div> -->
  </div>
  
  <div class="text-center">
   <button type="submit" class="btn btn-dark btn-pdding ">Proceed to book</button>
</form>
      </div>
    </div>
  </div>
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card ">
            <div class="card-body"> 
              
<div class="row">
  <div class="col-sm-4 p-5">
  <address>
               Oliva Hair Transplantation & Surgery center<br />
                H.No. 8-2-293/82/A/502, Road No 36<br />
                Jubilee Hills, Hyderabad 500034<br />
                +91 9550160055 & +91 9000287345<br />
               Email: info@dhiindia.com, enquiry@dhiindia.com
              </address>
  </div>
  <div class="col-sm-8">
   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30454.118058646694!2d78.4107618265638!3d17.42307348564603!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb973702b10487%3A0x9d6bc7c3841a4cc6!2sDHI+-+Best+Hair+Transplant+In+Hyderabad+%26+Hair+Loss+Treatment+Clinic+In+Hyderabad!5e0!3m2!1sen!2sin!4v1501068767711" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen=""></iframe>
  </div>
</div>

  </div>
            </div>
          </div>
            <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3>DHI Hair Transplant Clinic — HYDERABAD</h3>
<ul>
  <li>Looking for a quality hair loss treatment in Hyderabad? Fret not, we are here to help you restore your confidence.</li>
  <li>
DHI Hyderabad is one of our most special gems in the vast DHI India network, providing easy access to No. 1 hair transplant procedure from the award-winning Olivia Hair Transplantation & Surgery Center. Get a full range of best DHI services including Direct Hair Implantation (DHI), Scalp Micro-pigmentation, Anagen Hair Treatment, Eyebrows Restoration, Beard Hair Restoration, and Direct Hair Fusion among others. Our doctors are seasoned by years of experience in carrying out DHI procedures. All consultations are confidential including a complete hair loss evaluation test.

</li>
  <li>Following a rising demand for DHI procedures in the city, we have opened our clinic to help people transform their lives. We are situated in the prime location of Jubilee Hills making it easier for people to reach us from any part of the city. Our clinic is a 47-minute drive from Rajiv Gandhi International Airport.
</li>
 <li>We offer a relaxed and friendly ambiance for our clients to know everything about hair loss and available treatment options. Our clinic is staffed by experienced professionals who are passionate about maintaining standards set by DHI around the world.s</li>

<li>To book an appointment for a consultation at our DHI Mumbai clinic, give us a call now!</li>
  <li>Ranked No. 1 on Customer Satisfaction by IMRB</li>
  
</ul>
            </div>
          </div>
        </div>


        <!-- <div class="content">
  <div class="card ">
            <div class="card-body "> 
<h3 class="text-center pt-5 pb-5">Clinic Tour</h3>
<div class="row">
  <div class="col-sm-6 pr-lg-0 pr-md-0">
  <img src="image/clinic/DSC_001.jpg" alt="" class="img-fluid">
</div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_002.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pr-lg-0 pr-md-0 "><img src="image/clinic/DSC_003.jpg" alt="" class="img-fluid"></div>
  <div class="col-sm-6 pl-lg-0 pl-md-0"><img src="image/clinic/DSC_004.jpg " alt="" class="img-fluid"></div>
</div>
            </div>
          </div>
        </div> -->


        <div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 
<h3 class="text-center pt-5 pb-5">DHI Global Doctors Testimonials</h3>

<div id="carouselExampleIndicators" class="carousel slide testCarousel" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row ">
        <div class="col-sm-6">
          <h4>Sanjeev Kumar</h4>
          <p>Best part about visiting dhi clinic is they do proper diagnosis to eradicate the problem ( hairloss) taken prp and dhi hair transplant and totally satisfied with results. I Highly recommended DHI</p>
        </div>
        <div class="col-sm-6">
          <h4>Jitendra Tanwar</h4>
          <p>Hello everyone, I am from army and got DHI Procedure done yesterday from DHI New Delhi Clinic. I liked the behaviour of staff and procedure was pain free. I would always recommend DHI Procedure of hair transplant to everyone.</p>
        </div>
        
      </div>
    </div>
    <div class="carousel-item">
     <div class="row">
      <div class="col-sm-6">
          <h4>Jiva Tomar</h4>
          <p>I am extremely satisfied with my hair transplantation result in DHI Safdarjung Enclave clinic. Thanks to Dr. Ajay and his team</p>
        </div>
        <div class="col-sm-6">
          <h4>Sunil Singh</h4>
          <p>Very cooperative and professional services,fully satisfied by the transplant and the results thereafter,i will recommend others to go for DHI in case of transplant it may be bit costlier than others but its worth based on safety and results</p>
        </div>
       
      </div> 
    </div>
    <div class="carousel-item">
     <div class="row">
         <div class="col-sm-6">
          <h4>Pankaj Sharma</h4>
          <p>Very cooperative staff and doctors,results was satisfactory.</p>
        </div>
        <div class="col-sm-6">
          <h4>Jiva Tomar</h4>
          <p>DHI is the best clinic in Delhi for Hair Transplant surgery. Right from first interaction to hair transplant and hair growth , my experience was very satisfactory . I just want to appreciate the Delhi clinic of DHI for hair transplant work and behaviour of the entire team. You guys did a fantastic job. I am totally satisfied thank you</p>
        </div>
      </div> 
    </div>
  </div>
 
</div>

            </div>
          </div>
        </div>
            </div>

            
  

                  </div>


</section>

     
<div class="clearfix"></div>



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  



  </body>
</html>
