
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Find out Comparison Chart for Hair Transplant Cost in India,  DHI vs Others. Get best quality treatment at affordable prices. 100% safety with Natural results.">
    <meta name="author" content="">
  <title>Medical Tourism in India - Comparison Chart for Hair Transplant : DHI vs Others
</title>
<?php include 'header.php';?>
    </head>
  <body>




<section class=" bg-col-1">
  <div class="container">
    <div class="content">
           
     <div class="card  ">
            <div class="card-body maginExtra">
            <div class="row m-lg-4 m-2 bg-col-w">

              <div class="col-sm-7 no-gutters">
                 <div class="clearfix ">
           
              <video  preload="true" width="100%" controls=""  style="margin-bottom: -7px;">
                        <source type="video/webm" src="video/videoplayback.mp4">
                        <source type="video/mp4" src="video/videoplayback.mp4">
                        <source type="video/webm" src="video/videoplayback.ogg">
                        <source type="video/ogg" src="video/videoplayback.ogg">
                    </video>
           
        </div>
               </div>
               <div class="col-sm-5 ">
                <div class="mpg pt-5">
                  
                  <h4 class="text-center">Client's Testimonial</h4>
                 <p>The story of Mr. Sanjay Sharma's hair restoration, all the way of Canada to India</p> 
                 
                </div>
                 
               </div>
               
             </div>

            </div>
          </div>
    
  </div>
    <div class="content">
    <div class="card">
  
  <div class="card-body maginExtra">
   
    <div class="row ">
     
      <div class="col-sm-8">
        <h4 >Key Features</h4>
        <ul>
          <li>
            Unbelievable value.
          </li>
          <li>
            Savings up to 85%.
          </li>
          <li>
            DHI surgeons certified by London Hair Restoration Academy
          </li>
          <li>
            Highest safety standards
          </li>
          <li>
            Guaranteed results
          </li>
          <li>
            100% safety
          </li>
          <li>
            Natural results
          </li>
          <li>
            Hair growth for a lifetime.
          </li>
        </ul>
      </div>
  
<div class="col-sm-4 p-5">
  <img src="image/tickers/7.jpg" alt="dhi" class="img-fluid">
</div>

  </div>
</div>

            </div>
  </div>
  <div class="content">
    <div class="card">
  
  <div class="card-body maginExtra">
   
    <div class="row ">
     <div class="col-sm-12">
      <h3 class="text-center">What We Offer</h3>
      
     </div>
      <div class="col-sm-6 p-4">
        <h4>Cost inclusions:</h4>
        <ul>
          <li>
           Accommodation
          </li>
          <li>
            Treatment care
          </li>
          <li>
           Pre-surgery blood tests
          </li>
          <li>
            Medical charges
          </li>
          <li>
           Surgeon’s fee
          </li>
          <li>
           Last but not the least, Lifetime hair growth with 100% safety
          </li>
         
        </ul>
      </div>
  
<div class="col-sm-6 p-4">
<h4>Cost Exclusions:</h4>
<ul>
  <li>Air Fare</li>
  <li>Personal expenses</li>
</ul>
</div>
<div class="col-sm-12">
  <p>One of our representatives will be at the Airport with your name placard to receive you along with a personalized
SUV cab. He will comfortably bring you to our clinic or hotel in India.</p>
<p><strong>Note:</strong>  There are no hidden charges whatsoever. Cost of hair transplant may slightly vary from people to people
as per a number of sessions, level of baldness and density.</p>
</div>
  </div>
</div>

            </div>
  </div>
    <div class="content">
    <div class="card bt-border">
  
  <div class="card-body maginExtra">
   
    <div class="row ">
     
      <div class="col-sm-12">
        <h4 class="text-center">Price Comparison Chart for Hair Transplant : DHI vs Others</h4>
      </div>
  

  <p><img src="image/tickers/Price-chart-2.jpg" alt="Comparison Chart dhi" class="img-fluid"></p>     
  
 
   
  </div>
</div>
           
  
        
            </div>
  </div>

  
  </div>
</section>
     
<div class="clearfix"></div>

<section class="bottm_sec">
 <h1>Ready to regain your hair & confidence</h1>
  <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>

      <!-- FOOTER -->
       
<?php include 'footer.php';?>
<!--  <script>
document.getElementById("video1").addEventListener("loadedmetadata", function() {
     this.currentTime = 10;
}, false);
 </script>  -->


  
  </body>
</html>
