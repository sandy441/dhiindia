
<!doctype html>
<html lang="en">
 
  <body>
<?php include 'header.php';?>

<section class="bg-col-1 commmon-padd">
<div class="container">
  <h1 class="text-center">OUR CLINICS</h1>
  <div class="row mt-4">
    <div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/delhi-clinic.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location.php">Delhi</a></h5>
    
              <address>
                B – 5/15 Safdarjung Enclave<br />
                Opposite DLTA & Deer Park<br />
                Delhi 110029<br />
                011 – 46103032/36 & +91 8826006714<br />
                <a href=https://www.google.com/maps/place/DHI+Hairloss+%26+Regrowth+Transplantation+Surgery+Treatment+Clinic+Delhi/@28.5604975,77.1875508,17z/data=!3m1!4b1!4m5!3m4!1s0x390ce276f4190a7d:0xf7755f0b8d7a9e7d!8m2!3d28.5604928!4d77.1897448" target="_blank""><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
  
  
    </div>
    <div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/gurgaon-clinic.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-gurugram.php">Gurugram</a></h5>
   <address>
                220-221, 2nd Floor<br />
                South Point Mall, Sec 53, DLF Golf Course Road<br />
                Gurugram 122009<br />
                0124 – 4276426/ 40666481 & +91 8800804330<br />
                <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div>
 <div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/mumbai-clinic.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-mumbai.php">Mumbai</a></h5>
   <address>
                CPLSS 3rd Floor<br />
                Above Sarla Hospital, Dattataray Road<br />
                Santacruz West, Mumbai 400054<br />
                022 – 6117888/ 61178880 & +91 9833807002<br />
                <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div>
<div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/banglor-clinic.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-banglore.php">Bangalore</a></h5>
   <address>
                4/1 Walton Road (Above Cafe Coffee Day)<br />
                1st Floor, Lavelle Junction<br />
                Bangalore 560001<br />
                080 – 22270046/ 47 & +91 9686115381<br />
                <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div>
<div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/pune-clinic.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-pune.php">Pune</a></h5>
   <address>
              CPLSS, Shree Dattaguru Complex <br />
              6th Lane Ashok Chakra Society Koregoan Park <br />
              Pune 411001<br />
              020 – 69335555 & +91 8446435000<br />
              <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div>


<div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/chennai-clinic.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-chennai.php">Chennai</a></h5>
   <address>
                2nd Floor, Sheriff Towers<br />
                No. 28/118, GN Chetty Road, T.Nagar<br />
                Chennai 600017<br />
                044 – 45497788 & +91 9840844084<br />
                <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div>

<div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/kolcata.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-kolkata.php">Kolkata</a></h5>
   <address>
                DHI, 4th Floor, Platinum Mall 31,<br />
                Elgin Road,<br />
                Kolkata- 700020<br />
                +91 9007194854<br />
                <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div>

   <div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/clinic2.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-kochi.php">Kochi</a></h5>
   <address>
                G189, Panampilly Nagar<br />
                Near Panampilly Nagar Post Office<br />
                Kochi 682036<br />
                0484 – 4033302 & +91 9995776644<br />
                <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div> 


<div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/Ahmedabad.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-aehmdabad.php">Ahmedabad</a></h5>
   <address>
                Newtouch Laser Center Satellite<br />
                108 Sangrila Arcade, Near Shyaml Char Rasta<br />
                Ahmedabad 380015<br />
                079 – 40062549 & 8511100962<br />
                <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div>

    <div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/hyderabad-clinic.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-hyderabad.php">Hyderabad</a></h5>
   <address>
                Oliva Hair Transplantation & Surgery center<br />
                H.No. 8-2-293/82/A/502, Road No 36<br />
                Jubilee Hills, Hyderabad 500034<br />
                +91 9550160055 & +91 9000287345<br />
                <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div>

<div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/chandigarh-clinic.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-chandigarh.php">Chandigarh</a></h5>
   <address>
                Headmasters – Madhya Marg
SCO 16-19, Sector 8-C
Chandigarh 160017, India<br />
                 0172 – 4008553 & +91 8284818844<br />
                <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div>

    <!-- <div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/ludhiyana-clinic.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name">Ludhiana</h5>
   <address>
                  SCO 10E, 2nd Floor, Headmasters Building<br />
                  Malhar Cinema Road, Sarabha Nagar<br />
                  Ludhiana 141001<br />
                  +91 9915470344 & +91 9501612383<br />
                  <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div> -->

    <div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/Jaipur.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-jaipur.php">Jaipur</a></h5>
   <address>
                201-201 A,2nd floor Ambition tower,<br />
                agrasen circle, C-scheme,<br />
                Jaipur 302001<br />
                08890122218, +91 141-4022218, +91 141-2362217<br />
                 
                <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div>
<div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/calikut-clinic.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-calicut.php">Calicut</a></h5>
   <address>
                Dr. Rafeek’s Skin and Cosmetic Center<br />
                Near Malayala Manorama, East Nadakkavu <br />
                Wayanad Road<br />
                Calicut 673017<br />
                0495 6533302<br />
                <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div>
    <div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/Guwahati.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-Guwahati.php">Guwahati</a></h5>
   <address>
                Rejuve Skin & Aesthetic Clinic<br />
                2nd Floor, Gulshan Grand<br />
                Dispur, Guwahati 781006<br />
                0361 – 2230778 & +91 8011611002<br />
                <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div>

<div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/lakhnow-clinic.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name"><a href="location-lucknow.php">Lucknow</a></h5>
  <address>
                DermaKlinic B-43, J Park <br />
                Mahanagar Extension<br />
                Lucknow 226006<br />
                0522 – 4025505 & +91 7851890077<br />
                <a href="#"><span class="oi oi-map-marker"></span> Directions</a><br />
              </address>
  </div>
</div>
    </div>
    <div class="col-sm-4">
      <div class="media">
  <img class="align-self-start mr-3" src="./image/office/vijayvada-clinic.png" alt="Generic placeholder image" width="100">
  <div class="media-body">
    <h5 class="mt-0 clinic_name">Vijayawada</h5>
  <address>
                Opening Soon...

              </address>
  </div>
</div>
    </div>
  </div>
</div>


</section>


     
<div class="clearfix"></div>




      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


   



  </body>
</html>
