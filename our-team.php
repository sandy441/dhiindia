<!doctype html>
<html lang="en">

<body>
    <?php include 'header.php';?>

        <section class="commmon-padd bg-col-1 ">
            <div class="container">

                <h2 class="text-center">Our Team</h2>
                <h4 class="text-center">World renowned Experts, University 
Professors and Medical Specialists</h4>
                <div class="content">

                    <div class="card bt-border">
                        <div class="card-body ">
                            <div class="row align-items-center bg-col-11">
                                <div class="col-sm-6 ">
                                    <img src="image/team/team1.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-sm-6 ">
                                    <div class="team-data text-center  ">
                                        <h3><strong>Konstantinos P. Giotis</strong></h3>
                                        <h4 class="pt-0">DHI Medical Group Founder</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="card-body ">
                            <div class="row  align-items-center bg-col-11">

                                <div class="col-sm-6  ">

                                    <div class="inverse text-center">
                                        <h3><strong>Georgia Ligda MD</strong></h3>
                                        <h4 class="pt-0">Plastic Surgeon, PhD,
DHI Global Medical Director</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>
                                <div class="col-sm-6 ">
                                    <img src="image/team/Georgia-Ligda-MD.JPG" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                        <h2 class="text-center">DHI Medical Advisory & Research Board</h2>
                        <div class="card-body ">
                            <div class="row align-items-center bg-col-11">
                                <div class="col-sm-6 ">
                                    <img src="image/team/John-Curran-MD.JPG" class="img-fluid" alt="">
                                </div>
                                <div class="col-sm-6 ">
                                    <div class="team-data text-center  ">
                                        <h3><strong>John Curran MD</strong></h3>
                                        <h4 class="pt-0">DHI Channel Islands Clinic Director</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="card-body ">
                            <div class="row  align-items-center bg-col-11">

                                <div class="col-sm-6  ">

                                    <div class="inverse text-center">
                                        <h3><strong>Ryan Welter MD, PhD</strong></h3>
                                        <h4 class="pt-0">Hair Transplant Surgeon</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>
                                <div class="col-sm-6 ">
                                    <img src="image/team/Ryan-Welter-MD,-PhD.JPG" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>

                        <div class="card-body ">
                            <div class="row align-items-center bg-col-11">
                                <div class="col-sm-6 ">
                                    <img src="image/team/Georgios-Dedoussis.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-sm-6 ">
                                    <div class="team-data text-center  ">
                                        <h3><strong>Georgios Dedoussis</strong></h3>
                                        <h4 class="pt-0">Associate Professor of Biology</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="card-body ">
                            <div class="row  align-items-center bg-col-11">

                                <div class="col-sm-6  ">

                                    <div class="inverse text-center">
                                        <h3><strong>Christian Dubreuil</strong></h3>
                                        <h4 class="pt-0">Professor of Ear Nose Throat</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>
                                <div class="col-sm-6 ">
                                    <img src="image/team/Christian-Dubreuil.jpg" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="card-body ">
                            <div class="row align-items-center bg-col-11">
                                <div class="col-sm-6 ">
                                    <img src="image/team/Jana-Hercogova-MD,-PhD.JPG" class="img-fluid" alt="">
                                </div>
                                <div class="col-sm-6 ">
                                    <div class="team-data text-center  ">
                                        <h3><strong>Jana Hercogova MD, PhD</strong></h3>
                                        <h4 class="pt-0">Professor of Dermatology</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="card-body ">
                            <div class="row  align-items-center bg-col-11">

                                <div class="col-sm-6  ">

                                    <div class="inverse text-center">
                                        <h3><strong>Torello Lotti MD</strong></h3>
                                        <h4 class="pt-0">Professor of Dermatology and 
Director of Dermatology & 
Venereology Dpt of the University 
of Guglielmo Marconi, Rome, Italy</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>
                                <div class="col-sm-6 ">
                                    <img src="image/team/Torello-Lotti-MD.JPG" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="card-body ">
                            <div class="row align-items-center bg-col-11">
                                <div class="col-sm-6 ">
                                    <img src="image/team/Bernd-Michael-Loeffler-MD.JPG" class="img-fluid" alt="">
                                </div>
                                <div class="col-sm-6 ">
                                    <div class="team-data text-center  ">
                                        <h3><strong>Bernd-Michael Loeffler MD</strong></h3>

                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="card-body ">
                            <div class="row  align-items-center bg-col-11">

                                <div class="col-sm-6  ">

                                    <div class="inverse text-center">
                                        <h3><strong>Mary Petroliagki MD</strong></h3>
                                        <h4 class="pt-0">Registrat Endocrinologist, 
 General Hospital of Athens</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>
                                <div class="col-sm-6 ">
                                    <img src="image/team/Mary-Petroliagki-MD.JPG" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="card-body ">
                            <div class="row align-items-center bg-col-11">
                                <div class="col-sm-6 ">
                                    <img src="image/team/Professor-J.C.-Kim,-MD,-PhD.JPG" class="img-fluid" alt="">
                                </div>
                                <div class="col-sm-6 ">
                                    <div class="team-data text-center  ">
                                        <h3><strong>Professor J.C. Kim, MD, PhD</strong></h3>
                                        <h4 class="pt-0">DHI Group Research Director</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <h2 class="text-center">DHI Medical Directors </h2>
                        <div class="card-body ">
                            <div class="row  align-items-center bg-col-11">

                                <div class="col-sm-6  ">

                                    <div class="inverse text-center">
                                        <h3><strong>Jarir Al Khaledi MD Plastic 
Surgeon</strong></h3>
                                        <h4 class="pt-0">DHI Jordan Medical Director</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>
                                <div class="col-sm-6 ">
                                    <img src="image/team/Jarir-Al-Khaledi-MD-Plastic.jpg" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>

                        <div class="card-body ">
                            <div class="row align-items-center bg-col-11">
                                <div class="col-sm-6 ">
                                    <img src="image/team/Viral-Desai-MD.JPG" class="img-fluid" alt="">
                                </div>
                                <div class="col-sm-6 ">
                                    <div class="team-data text-center  ">
                                        <h3><strong>Viral Desai MD</strong></h3>
                                        <h4 class="pt-0">DHI India Clinic Director</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="card-body ">
                            <div class="row align-items-center bg-col-11">

                                <div class="col-sm-6 ">
                                    <div class="inverse text-center  ">
                                        <h3><strong>George Gounaris MD</strong></h3>
                                        <h4 class="pt-0">Plastic Surgeon – DHI France 
Medical Director</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>
                                <div class="col-sm-6 ">
                                    <img src="image/team/George-Gounaris-MD.JPG" class="img-fluid" alt="">
                                </div>
                            </div>

                        </div>
                        <div class="card-body ">
                            <div class="row align-items-center bg-col-11">
                                <div class="col-sm-6 ">
                                    <img src="image/team/Dimitrios-Ziakas-MD.JPG" class="img-fluid" alt="">
                                </div>
                                <div class="col-sm-6 ">
                                    <div class="team-data text-center  ">
                                        <h3><strong>Dimitrios Ziakas MD</strong></h3>
                                        <h4 class="pt-0">DHI Dubai Medical Director</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="card-body ">
                            <div class="row align-items-center bg-col-11">

                                <div class="col-sm-6 ">
                                    <div class="inverse text-center  ">
                                        <h3><strong>Eleftherios Papanikolaou MD</strong></h3>
                                        <h4 class="pt-0">DHI Greece Medical Director</h4>
                                        <button type="button" class="btn btn-light">SHOW CV</button>
                                    </div>
                                </div>
                                <div class="col-sm-6 ">
                                    <img src="image/team/team1.jpg" class="img-fluid" alt="">
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </section>

        <div class="clearfix"></div>

        <section class="bottm_sec">
            <h1>Increase your hair density naturally</h1>
            <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
        </section>

        <!-- FOOTER -->

        <?php include 'footer.php';?>

</body>

</html>