
<!doctype html>
<html lang="en">
 <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="This Privacy Policy governs the manner in which M.M.A. Ltd. collects, uses, maintains and discloses information collected from users of the dhiglobal.com website (site). This privacy policy applies to the site and all products and services offered by M.M.A. Ltd. Personal identification information We may collect personal identification information from users in a variety of&hellip
">
    <meta name="author" content="">
      <title>Privacy Policy - DHI India

</title>
<?php include 'header.php';?>
    </head> 
  <body>


<section class="bg-col-1">
  <nav aria-label="breadcrumb" role="navigation">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="./">DHI International</a></li>
    <li class="breadcrumb-item active" aria-current="page">Privacy Policy</li>
  </ol>
</nav>
<div class="container">
 <div class="content">
    <div class="card bt-border">
  
  <div class="card-body ">
     <h2>Privacy Policy</h2>
    <div class="row ">
        
  <p>This Privacy Policy governs the manner in which M.M.A. Ltd. collects, uses, maintains and discloses information collected from users of the dhiglobal.com website (site). This privacy policy applies to the site and all products and services offered by M.M.A. Ltd.</p>
<p>Personal identification information<br />We may collect personal identification information from users in a variety of ways, including, but not limited to, when users visit our site, register on the site, place an order, subscribe to the newsletter, respond to a survey, fill out a form, and in connection with other activities, services, features or resources we make available on our site. Users may be asked for, as appropriate, name, email address, mailing address and phone number. Users may, however, visit our site anonymously. We will collect personal identification information from users only if they voluntarily submit such information to us. Users can always refuse to supply personal identification information, except that it may prevent them from engaging in certain site related activities.</p>
<p>Non-personal identification information<br />We may collect non-personal identification information of users whenever they interact with our site. Non-personal identification information may include the browser name, the type of computer and other technical information of users through which they connect to our site, such as the operating system, the Internet service provider and other similar information.</p>
<p>Web browser cookies<br />Our site may use cookies to enhance user experience. User&rsquo;s web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert the user when cookies are being sent. If they do so, note that some parts of the site may not function properly.</p>
<p>How we use collected information?<br />M.M.A. Ltd. may collect and use users personal information for the following purposes :<br />&ndash; To improve customer service<br />Information provided by user helps us respond to customer service requests and provide support more efficiently.<br />&ndash; To improve our site<br />We may use feedback which you provide to improve our products and services.<br />&ndash; To run a promotion, contest, survey or other site feature<br />To send users information that they agreed to receive about topics we think will be of interest to them.<br />&ndash; To send periodic emails<br />If user decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the user would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or user may contact us via our site if they wish to opt out of the emailing list.<br />How we protect your information?</p>
<p>We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our site.</p>
<p>Sharing your personal information<br />We do not sell, trade, or rent users personal identification information to others. We may share generic aggregated demographic information of visitors and users that are not linked to any personal identification information with our business partners, trusted affiliates and advertisers for the purposes outlined above. We may use third party service providers to help us operate our business and the site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with your consent with these third parties for those limited purposes.</p>
<p>Third party websites<br />Users may find advertising or other content on our site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our site, is subject to that website&rsquo;s own terms and policies.</p>
<p>Changes to this privacy policy<br />M.M.A. Ltd. has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>
<p>Your acceptance of these terms<br />By using this site, you signify your acceptance of this policy and terms of service. If you do not agree to this policy, please do not use our site. Your continued use of the site following the posting of changes to this policy will be deemed your acceptance of those changes.</p>
<p>Contacting us<br />If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please send us an email or contact us at:<br />M.M.A. Ltd.<br />dhiglobal.com<br />30, Vouliagmenis Avenue, Athens 111743<br />Tel:+30 210 9211630</p>
<p>This document was last updated on November 21, 2013.</p>
       
       
         
           
    </div>     












      
   
  
</div>
           
  
         
        
         
            
        
            </div>
  </div><!--/row-->

  
</div>


</section>


     
<div class="clearfix"></div>

<section class="bottm_sec">
 <h1>Ready to regain your hair & confidence</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>

      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


   

  </body>
</html>
