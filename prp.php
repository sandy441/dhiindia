
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Platelet Rich Plasma (PRP) treatment is a Safe new Hair Restoration program which is performed at DHI by specialized practitioners with no side effects.">
    <meta name="author" content="">
      <title>PRP (Platelet Rich Plasma) Hair Therapy & Treatment in Delhi - DHI India

</title>
<?php include 'header.php';?>
    </head>
  <body>

<section class="bg-col-1">
   <div class="clearfix">
           
            	<video  preload="auto" width="100%" loop autoplay>
                        <source type="video/webm" src="video/prp-treatment-dhi-dekhi-clinic.mp4">
                        <source type="video/mp4" src="video/prp-treatment-dhi-dekhi-clinic.mp4">
                        <source type="video/webm" src="video/prp-treatment-dhi-dekhi-clinic.ogg">
                        <source type="video/ogg" src="video/prp-treatment-dhi-dekhi-clinic.ogg">
                    </video>
           
        </div>
        
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
  <div class="card mar-minus">
            <div class="card-body maginExtra"> 
               <div class="row ">
     
    <div class="col-sm-6"><h2>What is PRP?<span>Stimulates hair growth & thickens hair</span></h2></div>
    <div class="col-sm-6"><p>PRP is a non-surgical treatment which is used treat hair thinning,
          to arrest hair loss and promote hair growth. It takes place by injecting plasma into 
          the scalp, with the objective of stimulating the follicles. The PRP is activated with growth factors 
          to stimulate inactive or newly implanted hair follicles into an active growth phase. The regular use of 
          PRP prolongs the Anagen phase of the hair growth cycle. </p></div>
   
  </div>
            </div>
          </div>
           
            </div>

            <div class="content">
  <div class="card ">
            <div class="card-body maginExtra"> 
               <div class="row ">
     
    <div class="col-sm-12 ">
      <h2 class="mb-0">How PRP Works?</h2>
      <p  class="mb-0">A small amount of blood (less than 50 ml) is taken from the body and centrifuged at a high speed to separate the plasma. The PRP is then activated with DNA activators (thrombin) and enriched with calcium ions (e.g. calcium chloride) and injected into the area which is suffering from hair loss in order to stimulate hair growth.</p>
    


    </div>
    
  
  </div>
  <div class="row icon_box pt-4 pb-4">
     <div class="col-sm-3 col-6 text-center">
     
       <img src="image/prp-img.png" alt="" width="150">
      <h5>Harvest</h5>
      <p>A small amount of blood is taken</p>
    </div>
    <div class="col-sm-3 col-6 text-center">
      <img src="image/prp-img1.png" alt="" width="150">
      <h5>Separate</h5>
      <p>PRP is seprated from the blood</p>
    </div>
    <div class="col-sm-3 col-6 text-center"><img src="image/prp-img2.png" alt="" width="150">
       <h5>Activate</h5>
       <p>PRP is activated</p>
    </div>
    <div class="col-sm-3 col-6 text-center"><img src="image/prp-img3.png" alt="" width="150">
     <h5>Inject</h5> 
     <p>PRP is injected into the scalp</p>
    </div>

  </div>


 
            </div>
          </div>
           
            </div>
            <div class="content">
  <div class="card">
            <div class="card-body maginExtra"> 

               <div class="row ">
    <div class="col-sm-7  pt-4 ">
     
       <p>At DHI, strict protocols are followed to ensure highest quality of results each time. Special attention is given to ensure that the centrifugation of blood is done under a temperature controlled environment. </p>
    
      <p>The quantity of blood extracted, the centrifugation speed, anti-coagulant used, etc, all play an important role in multiplying the plasma to 5x, which is the highest in the industry. This highly concentrated platelet rich plasma (PRP) is thereafter injected into the scalp which promotes hair growth and arrests hair fall.</p>
    
        
    </div>
    <div class="col-sm-5">
      <img src="image/tickers/7.jpg" alt="" class="img-fluid">
    </div>
  </div>
            </div>
          </div>
        </div>
	
<div class="content">
  <div class="card bt-border">
            <div class="card-body maginExtra"> 
               <div class="row ">
     
    <div class="col-sm-12 ">
      <h2 class="p-4 text-center">Features</h2>
      
    


    </div>
    
  
  </div>
  <div class="row icon_box">
     <div class="col-sm-4 col-6 text-center">
     
       <img src="./image/icons/safe.png" alt="" width="80">
      <h6>Safe & quick treatment</h6>
     
    </div>
    <div class="col-sm-4 col-6 text-center">
      <img src="./image/icons/hair-growth.png" alt="" width="80">
      <h6>Promotes hair growth</h6>
     
    </div>
    <div class="col-sm-4 col-6 text-center"><img src="./image/icons/result.png" alt="" width="80">
       <h6>Immediate results</h6>
      
    </div>
    <div class="col-sm-4 col-6 text-center"><img src="./image/icons/strength.png" alt="" width="80">
     <h6>Strengthens existing hair</h6> 
   
    </div>
    <div class="col-sm-4 col-6 text-center"><img src="./image/icons/prolong.png" alt="" width="80">
     <h6>Prolongs Anagen phaset</h6> 
     
    </div>
    <div class="col-sm-4 col-6 text-center"><img src="./image/icons/multiply.png" alt="" width="80">
     <h6>Plasma multiplied to 5x (highest in the industry)</h6> 
    
    </div>
    <div class="col-sm-4 col-6 text-center"><img src="./image/icons/epidermis.png" alt="" width="80">
     <h6>Arrests hair fall</h6> 
    
    </div>
     <div class="col-sm-4 col-6 text-center"><img src="./image/icons/restore.png" alt="" width="80">
     <h6>No side effects</h6>
    
    </div>
    <div class="col-sm-4 col-6 text-center"><img src="./image/icons/injection.png" alt="" width="80">
     <h6>Can be used individually or with
any hair restoration treatments</h6> 
    
    </div>
  </div>
<hr>
  <div class="row">
    <div class="col-sm-12">
      <p>At DHI, strict protocols are followed to ensure highest quality of results each time. Special attention is given to ensure that the centrifugation of blood is done under a temperature controlled environment. The quantity of blood extracted, the centrifugation speed, anti-coagulant used, etc, all play an important role in multiplying the plasma to 5x, which is the highest in the industry. This highly concentrated platelet rich plasma (PRP) is thereafter injected into the scalp which promotes hair growth and arrests hair fall.</p>
    </div>
  </div>
            </div>
          </div>
           
            </div>


                  
                 	</div>


</section>


     
<div class="clearfix"></div>
<section class="bottm_sec">
 <h1>Stimulate your hair growth </h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>


      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


  </body>
</html>
