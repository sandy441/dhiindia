<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Know more: what our clients want to know from us ? What kind of treatments DHI offered at hair clinic? Who is a trichologist? How hair transplant treatment done?">
    <meta name="author" content="">
      <title>DHI India - When to choose Hair Transplant? Ask Your Questions

</title> 
<?php include 'header.php';?>
    </head>
<body>
    

        <section class="bg-col-1">

            <div class="container">
                <div class="content">
                    <div class="card bt-border">

                        <div class="card-body ">
                            <h2 class="text-center">FAQ</h2>
                            <div class="row ">
                              
<div class="col-sm-12 faq_tab">
   <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">In conversation with the Medical Director, DHI International</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">All About Hair</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">About DHI Procedure And Results</a>
                                    </li>
                                </ul>
</div>
                               
<div class="col-sm-12 faq_tabcontent">
  <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                      <div id="accordion" role="tablist">
  <div class="card">
    <div class="card-header" role="tab" id="headingOne">
      <h5 class="mb-0">
        <a data-toggle="collapse" class="accordion-toggle" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" >
         1. Is it safe to do hair transplants?
        </a>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
      Today with modern techniques like DHI Direct Technique <sup>TM</sup>, hair transplant is a very safe, minimalistic procedure which can be finished in a few hours, giving lifetime permanent results. But it is important that it is performed by trained doctors following strict protocols such as pre-screening, medical tests so that we choose the right patient for hair transplant.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingTwo">
      <h5 class="mb-0">
        <a class="collapsed accordion-toggle" data-toggle="collapse" href="#collapseTwo" aria-expanded="false"  aria-controls="collapseTwo">
          2. Which doctors perform hair transplants in India?
        </a>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
       Today, in India, Homeopathic doctors, Dentists, Ayurvedic doctors and even gynaecologists are performing hair transplant procedures without proper training. At DHI, all procedures are performed end-to-end by Dermatologist (MCI Registered) doctors.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingThree">
      <h5 class="mb-0">
        <a class="collapsed accordion-toggle" data-toggle="collapse" href="#collapseThree" aria-expanded="false"  aria-controls="collapseThree">
          3. Are the doctors suitably trained and certified?
        </a>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        Hair Transplant is not taught in any medical college world-wide. Doctors learn it from other doctors on the job, without proper training, assessment or certification, thereby compromising on safety. At DHI, each doctor is extensively trained on DHI Total Care System and certified by London Hair Restoration Academy.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="heading4">
      <h5 class="mb-0">
        <a class="collapsed accordion-toggle" data-toggle="collapse" href="#collapse4" aria-expanded="false"  aria-controls="collapse4">
          4. How can you measure the results?
        </a>
      </h5>
    </div>
    <div id="collapse4" class="collapse" role="tabpanel" aria-labelledby="heading4" data-parent="#accordion">
      <div class="card-body">
       Research shows that on n average, only 1 out of 2 implanted follicles grow at clinics without verifiable results. DHI guarantees 100% growth of the implanted hair using international quality protocols and instruments. One third of DHI patients come for repair after their case has been spoilt by other clinics.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="heading5">
      <h5 class="mb-0">
        <a class="collapsed accordion-toggle" data-toggle="collapse" href="#collapse5" aria-expanded="false"  aria-controls="collapse5">
         5. How do some clinics offer very cheap pricing?
        </a>
      </h5>
    </div>
    <div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="heading5" data-parent="#accordion">
      <div class="card-body">
      Clinics which offer cheap hair transplants have substandard facilities, unqualified doctors and they use cheap instruments which may be reused on multiple patients, compromising safety and causing scars.
DHI’s high quality, imported instruments are ‘single use’ and destroyed after every case, ensure zero scarring or damage.
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" role="tab" id="heading6">
      <h5 class="mb-0">
        <a class="collapsed accordion-toggle" data-toggle="collapse" href="#collapse6" aria-expanded="false"  aria-controls="collapse6">
        6. What is the importance of diagnosis/ alopecia test?
        </a>
      </h5>
    </div>
    <div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="heading6" data-parent="#accordion">
      <div class="card-body">
     There are several types of alopecia and one could be suffering from any of them. A correct treatment of alopecia requires a proper diagnosis to begin with. Unfortunately, most clinics do hair transplants without proper diagnosis. DHI’s Diagnostic System for Alopecia (DSA) follows a dermatological examination, psychological aspects, precise mathematical count of donor and recipient area, and computerized alopecia test. This results in a comprehensive lifetime treatment plan and best results for the hair loss problem.
      </div>
    </div>
  </div>
</div>
                                    </div>

                                    <!--second part-->
                                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                      <div id="accordion2" role="tablist">
  <div class="card">
    <div class="card-header" role="tab" id="heading7">
      <h5 class="mb-0">
        <a data-toggle="collapse" class="accordion-toggle" href="#collapse7" aria-expanded="true" aria-controls="collapse7">
         1. What is hair made up of?
        </a>
      </h5>
    </div>

    <div id="collapse7" class="collapse show" role="tabpanel" aria-labelledby="heading7" data-parent="#accordion2">
      <div class="card-body">
Hair is made up of Keratin – a fibrous structural protein.
Hair also contains natural oils (lipids) and water. These hair “ingredients” are arranged in 3 primary structures: the cuticle (which is the outermost, shingle-like layer); the cortex (the inside of the hair consisting of bundles of protein filaments); and the medulla (a soft spongy-like core in the center of the cortex).
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="heading8">
      <h5 class="mb-0">
        <a class="collapsed accordion-toggle" data-toggle="collapse" href="#collapse8" aria-expanded="false"  aria-controls="collapse8">
        2. How many hairs are there on the human scalp?
        </a>
      </h5>
    </div>
    <div id="collapse8" class="collapse" role="tabpanel" aria-labelledby="heading8" data-parent="#accordion2">
      <div class="card-body">
        On an average there are over 100,000 hairs on a human scalp.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="heading9">
      <h5 class="mb-0">
        <a class="collapsed accordion-toggle" data-toggle="collapse" href="#collapse9" aria-expanded="false"  aria-controls="collapse9">
        3. What are the 3 growth cycles of hair?
        </a>
      </h5>
    </div>
    <div id="collapse9" class="collapse" role="tabpanel" aria-labelledby="heading9" data-parent="#accordion2">
      <div class="card-body">
       The growth cycle of hair consists of 3 stages:
<ul>
  <li>Anagen – It is the growing period of a hair follicle that typically lasts about 3 to 5 years.</li>
  <li>Catagen – At the end of the growth period, hair follicles prepare themselves for the resting phase. This stage of the hair growth cycle usually lasts about 1 to 2 weeks.</li>
  <li>Telogen – This is the resting period of a hair follicle. It is usually 3 to 4 months in length and at the end of this period older hairs that have finished their life will fall out and newer hairs will begin to grow.</li>
</ul>




      </div>
    </div>
  </div>


  <div class="card">
    <div class="card-header" role="tab" id="heading10">
      <h5 class="mb-0">
        <a class="collapsed accordion-toggle" data-toggle="collapse" href="#collapse10" aria-expanded="false"  aria-controls="collapse10">
        4. What are the causes of hair loss in men?
        </a>
      </h5>
    </div>
    <div id="collapse10" class="collapse" role="tabpanel" aria-labelledby="heading10" data-parent="#accordion2">
      <div class="card-body">
       By far the most common cause of hair loss in men is “Androgenetic Alopecia” also referred to as “male pattern hair loss” or “common” baldness. Androgenic Alopecia is due to the male hormone dihydrotestosterone (DHT) acting on genetically-susceptible scalp hair follicles that causes them to become progressively smaller and eventually disappear. This process is called “miniaturization.” The other causes can be Stress, scalp bacteria, poor nutrition, hormonal imbalance, injury and high fever.



      </div>
    </div>
  </div>
<div class="card">
    <div class="card-header" role="tab" id="heading11">
      <h5 class="mb-0">
        <a class="collapsed accordion-toggle" data-toggle="collapse" href="#collapse11" aria-expanded="false"  aria-controls="collapse11">
       5. What are different types of Alopecia?
        </a>
      </h5>
    </div>
    <div id="collapse11" class="collapse" role="tabpanel" aria-labelledby="heading11" data-parent="#accordion2">
      <div class="card-body">
       <p>Alopecia is classified into 2 major groups: Non-Scarring Alopecia &amp; Scarring Alopecia</p>
<p>Types of Non-Scarring Alopecia:</p>
<p>AGA, Alopecia Areata, Telogen Effluvium, Anagen Effluvium, Trichotillomania</p>
<p>Types of Scarring Alopecia:</p>
<p>Lichen Planopilaris, Frontal fibrosing alopecia, Pseudopaladae of Brocq, Folliculitis decalvans, tufted folliculitis, dissecting cellulitis, folliculitiskeloidalis, central centrifugal alopecia, alopecia muconosis, keratosis pilaris atrophicans</p>


      </div>
    </div>
  </div>

<div class="card">
    <div class="card-header" role="tab" id="heading12">
      <h5 class="mb-0">
        <a class="collapsed accordion-toggle" data-toggle="collapse" href="#collapse12" aria-expanded="false"  aria-controls="collapse12">
    6. What are the causes of hair loss in women?
        </a>
      </h5>
    </div>
    <div id="collapse12" class="collapse" role="tabpanel" aria-labelledby="heading12" data-parent="#accordion2">
      <div class="card-body">
       The most common form of hair loss in women, Chronic Telogen Effluvium (long term diffuse hair loss), is an increase in hair loss and decrease in hair thickness over a long period of time.


      </div>
    </div>
  </div>

</div>
                                    </div>



                                    <!--third part-->
                                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                      <div id="accordion3" role="tablist">
  <div class="card">
    <div class="card-header" role="tab" id="heading25">
      <h5 class="mb-0">
        <a data-toggle="collapse" class="accordion-toggle" href="#collapse25" aria-expanded="true" aria-controls="collapse25">
         1. What precautions should be taken pre & post procedure?
        </a>
      </h5>
    </div>

    <div id="collapse25" class="collapse show" role="tabpanel" aria-labelledby="heading25" data-parent="#accordion3">
      <div class="card-body">
        One week prior to procedure the client needs to avoid vitamins, aspirin, consumption of alcohol and smoking.
Also, the TCC (Total Care Consultant) needs to advise the client in case he’s diabetic, suffering from high blood pressure, on cardiac related medication or prescribed any medication by a psychiatrist. The doctor will prescribe antibiotics for 3 days after the procedure. Heavy exercise, weight lifting, swimming and scalp exposure to direct sunlight has to be avoided for 15 days after the procedure. The client can resume light exercise and brisk walks 7 days after the procedure.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="heading26">
      <h5 class="mb-0">
        <a class="collapsed accordion-toggle" data-toggle="collapse" href="#collapse26" aria-expanded="false"  aria-controls="collapse26">
        2. Are there any medications prescribed post session?
        </a>
      </h5>
    </div>
    <div id="collapse26" class="collapse" role="tabpanel" aria-labelledby="heading26" data-parent="#accordion3">
      <div class="card-body">
       The client is prescribed antibiotics and anti-inflammatory course which needs to be followed for 3 days post procedure.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="heading27">
      <h5 class="mb-0">
        <a class="collapsed accordion-toggle" data-toggle="collapse" href="#collapse27" aria-expanded="false"  aria-controls="collapse27">
        3. Does the client need to be on any long-term medication post procedure to prevent transplanted hair from falling?
        </a>
      </h5>
    </div>
  <div id="collapse27" class="collapse" role="tabpanel" aria-labelledby="heading27" data-parent="#accordion3">
      <div class="card-body">
      <p>There’s no requirement of any medication, as the implanted hair will not fall.</p> 
    <p>However, Minoxidil applications are required to prevent thinning and falling of the existing hair.</p>

      </div>
    </div>
  </div>
</div>


                                    </div>
                                </div>
</div>


<div class="col-sm-12">
  <h2 class="text-center">Ask Questions to Your Hair Experts</h2>

  <form class="maxwid600 form_faq " id="faqForm" action="<?php echo $home_url;?>/formrequest.php" method="post">
                                         <input type="hidden" name="formtype" value="faq">
                                        <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <input type="text" name="name" class="form-control" id="" placeholder="Full Name*">
                                            </div>
                                                <div class="form-group col-md-6">
                                                    <input type="email" name="email" class="form-control" id="" placeholder="Your Email*">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                                <div class="form-group col-md-6">
        
                                                    <input type="text" name="phone" maxlength="12" class="form-control" id="" placeholder="Mobile No.">
                                            </div>
                                                <div class="form-group col-md-6">
        
                                                    <input type="text" name="subject" class="form-control" id="" placeholder="Subject">
                                            </div>
                                        </div>
      
                                            <div class="form-group">
      
                                            <textarea name="question" id=""  rows="4" class="form-control" placeholder="Your Question"></textarea>
                                        </div>
      
      
                                        <button type="submit" class="btn btn-common">Submit</button>
                                    </form>
</div>
                                

                            
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </section>

        <div class="clearfix"></div>

        <!-- <section class="bottm_sec">
            <h1>Ready to regain your hair & confidence</h1>
            <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
        </section> -->

        <!-- FOOTER -->

        <?php include 'footer.php';?>

</body>

</html>