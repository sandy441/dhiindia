
<!doctype html>
<html lang="en">
 
  <body>
<?php include 'header.php';?>

<section>
  <img src="image/results-banner.jpg" alt="" class="img-fluid">
</section>

    <section class=" bg-col-1">
       <div class="container">
<div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 
               <div class="row pt-4">
        
<div class="col-sm-12">
  <div align="center ">
            <button class="btn btn-default filter-button" data-filter="all">All</button>
            <button class="btn btn-default filter-button" data-filter="men">Men</button>
            <button class="btn btn-default filter-button" data-filter="women">Women</button>
            <button class="btn btn-default filter-button" data-filter="scalp">Scalp Micropigmentation</button>
            <button class="btn btn-default filter-button" data-filter="eye">Eyebrows</button>
             <button class="btn btn-default filter-button" data-filter="body">Body</button>
        </div>
</div>
        
    

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter women ">
                       <img class="card-img-top" src="image/Result_Images/women/1.jpg" alt="Card image cap">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">DHI Female Result #1</h5>
   <p class="pt-0">Women</p>
  </div>
 
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter women ">
                       <img class="card-img-top" src="image/Result_Images/women/2.jpg" alt="Card image cap">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">DHI Female Result #2</h5>
   <p class="pt-0">Women</p>
  </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter women ">
                       <img class="card-img-top" src="image/Result_Images/women/3.jpg" alt="Card image cap">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">DHI Female Result #3</h5>
   <p class="pt-0">Women</p>
  </div>
            </div>

<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter women ">
                       <img class="card-img-top" src="image/Result_Images/women/4.jpg" alt="Card image cap">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">DHI Female Result #4</h5>
   <p class="pt-0">Women</p>
  </div>
            </div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter women ">
                       <img class="card-img-top" src="image/Result_Images/women/5.jpg" alt="Card image cap">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">DHI Female Result #5</h5>
   <p class="pt-0">Women</p>
  </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter women ">
                       <img class="card-img-top" src="image/Result_Images/women/6.jpg" alt="Card image cap">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">DHI Female Result #6</h5>
   <p class="pt-0">Women</p>
  </div>
            </div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter men">
                     
    <img class="card-img-top" src="image/Result_Images/men/1.jpg" alt="Card image cap">
  <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Virender Sehwag (#1)</h5>
   <p class="pt-0">Men, Norwood 6</p>
  </div>

            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter men">
                       <img class="card-img-top" src="image/Result_Images/men/2.jpg" alt="Card image cap">
                <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Prabhjot Singh (#2)</h5>
   <p class="pt-0">Men, Norwood 3</p>
  </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter men">
              <img class="card-img-top" src="image/Result_Images/men/3.jpg" alt="Card image cap">
              <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Sreejesh Ravindran (#3)</h5>
   <p class="pt-0">Men, Norwood 4</p>
  </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter men ">
               <img class="card-img-top" src="image/Result_Images/men/4.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">SV Sunil (#4)</h5>
   <p class="pt-0">Men</p>
  </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter men ">
               <img class="card-img-top" src="image/Result_Images/men/5.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Pankaj Mirchandani (#5)</h5>
   <p class="pt-0">Men, Norwood 3</p>
  </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter men ">
              <img class="card-img-top" src="image/Result_Images/men/6.jpg" alt="Card image cap">
              <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Nishank Syal (#6)</h5>
   <p class="pt-0">Men</p>
  </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter scalp">
                       <img class="card-img-top" src="image/Result_Images/scalp/1.jpg" alt="Card image cap">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Scalp MPG Result (#1</h5>
   <p class="pt-0">Men</p>
  </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter scalp ">
                       <img class="card-img-top" src="image/Result_Images/scalp/2.jpg" alt="Card image cap">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Scalp MPG Result (#2)</h5>
   <p class="pt-0">Scalp Micropigmentation</p>
  </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter scalp ">
                       <img class="card-img-top" src="image/Result_Images/scalp/3.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Scalp MPG Result (#3)</h5>
   <p class="pt-0">Scalp Micropigmentation</p>
  </div>
            </div>
<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter scalp ">
                       <img class="card-img-top" src="image/Result_Images/scalp/4.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Scalp MPG Result (#4)</h5>
   <p class="pt-0">Scalp Micropigmentation</p>
  </div>
            </div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter scalp ">
                       <img class="card-img-top" src="image/Result_Images/scalp/5.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Scalp MPG Result (#5)</h5>
   <p class="pt-0">Scalp Micropigmentation</p>
  </div>
            </div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter scalp ">
                       <img class="card-img-top" src="image/Result_Images/scalp/6.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Scalp MPG Result (#6)</h5>
   <p class="pt-0">Scalp Micropigmentation</p>
  </div>
            </div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter eye ">
                       <img class="card-img-top" src="image/Result_Images/eyebrows/1.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">DHI Eyebrows Result (#1)</h5>
   <p class="pt-0">Eyebrows, Eyebrows Micropigmentation</p>
  </div>
            </div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter eye ">
            <img class="card-img-top" src="image/Result_Images/eyebrows/2.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">DHI Eyebrows Result (#2)</h5>
   <p class="pt-0">Eyebrows, Eyebrows Micropigmentation</p>
  </div>
            </div>

<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter eye ">
            <img class="card-img-top" src="image/Result_Images/eyebrows/3.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">DHI Eyebrows Result (#3)</h5>
   <p class="pt-0">Eyebrows, Eyebrows Micropigmentation</p>
  </div>
            </div>
<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter eye ">
            <img class="card-img-top" src="image/Result_Images/eyebrows/4.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">DHI Eyebrows Result (#4)</h5>
   <p class="pt-0">Eyebrows, Eyebrows Micropigmentation</p>
  </div>
            </div>
<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter eye ">
            <img class="card-img-top" src="image/Result_Images/eyebrows/5.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">DHI Eyebrows Result (#5)</h5>
   <p class="pt-0">Eyebrows, Eyebrows Micropigmentation</p>
  </div>
            </div>


    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter eye ">
            <img class="card-img-top" src="image/Result_Images/eyebrows/6.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">DHI Eyebrows Result (#6)</h5>
   <p class="pt-0">Eyebrows, Eyebrows Micropigmentation</p>
  </div>
            </div>  

<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter body ">
            <img class="card-img-top" src="image/Result_Images/body/1.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">DHI Body Hair Result (#1)</h5>
   <p class="pt-0">Body</p>
  </div>
            </div>


        </div>
             </div>
           </div>
</div>
        
    </div>
</section>

   
<div class="clearfix"></div>

     
<section class="bottm_sec">
  <h1>Increase your hair density naturally</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>


      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


  
  </body>
</html>
