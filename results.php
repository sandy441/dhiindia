
<!doctype html>
<html lang="en">
 <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="DHIIndia Hair Transplant Results, Before and After Photos of men and women who undergone Hair Transplant by DHI doctors.">
    <meta name="author" content="">
      <title>DHI India Hair Transplant Results - Before and After Photos</title>
<?php include 'header.php';?>
    </head>
  <body>


<section>
  <img src="image/results-banner.jpg" alt="" class="img-fluid">
</section>

    <section class=" bg-col-1">
       <div class="container">
<div class="content">
  <div class="card ">
            <div class="card-body"> 


<div class="row">
  <div class="col-sm-12">
   <h3 class="text-center">Men</h3>  
  </div>  
   
 

    
            <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
    <img class="card-img-top" src="image/Result_Images/men/1.jpg" alt="virendra sehwag before and after images
">
  <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Virender Sehwag (#1)</h5>
   <p class="pt-0"> Norwood 6</p>
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
          <img class="card-img-top" src="image/Result_Images/men/2.jpg" alt="dhi gives natural hairline
">
                <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Prabhjot Singh (#2)</h5>
   <p class="pt-0"> Norwood 3</p>
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
         <img class="card-img-top" src="image/Result_Images/men/3.jpg" alt="Card image cap">
              <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Sreejesh Ravindran (#3)</h5>
   <p class="pt-0"> Norwood 4</p>
  </div>

  </div>
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
        <img class="card-img-top" src="image/Result_Images/men/5.jpg" alt="Card image cap">
               <div class="card-data bg-col-w text-center">
    <h5 class="pb-0 pt-3">Pankaj Mirchandani (#4)</h5>
   <p class="pt-0"> Norwood 3</p>
  </div>

  </div>
 <div class="col-sm-12 text-center pb-5 pt-4">
   <a href="dhi-hair-transplant-results-men .php"  class="btn btn-common btn-pdding">View all Men Results</a>
 </div>
</div>
             </div>
           </div>
</div>
   <div class="content">
  <div class="card ">
            <div class="card-body "> 


<div class="row">
 
   <div class="col-sm-12">
   <h3 class="text-center">Women</h3>  
  </div> 
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
                       <img class="card-img-top" src="image/Result_Images/women/1.jpg" alt="Card image cap">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Female Result #1</h5>
   
  </div>

  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/women/2.jpg" alt="Card image cap">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Female Result #2</h5>
   
  </div>
            </div>


             <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/women/3.jpg" alt="dhi before and after image 2 
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Female Result #3</h5>
   
  </div>
            </div>

 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/women/4.jpg" alt="dhi before and after image 3 
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Female Result #4</h5>
   
  </div>
            </div>
 <div class="col-sm-12 text-center pb-5 pt-4">
   <a href="dhi-hair-transplant-results-women.php"  class="btn btn-common btn-pdding">View all Women Results</a>
 </div>



</div>
             </div>
           </div>
</div> 
<div class="content">
  <div class="card ">
            <div class="card-body "> 


<div class="row">
 
   <div class="col-sm-12">
   <h3 class="text-center">Eyebrows</h3>  
  </div> 
   <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/eyebrows/1.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#1)</h5>
   
  </div>
            </div>
             <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
            <img class="card-img-top" src="image/Result_Images/eyebrows/2.jpg" alt="eyebrow restoration before and after image 1
">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#2)</h5>
   
  </div>
            </div>

 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
            <img class="card-img-top" src="image/Result_Images/eyebrows/3.jpg" alt="Card image cap">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#3)</h5>
   
  </div>
            </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
            <img class="card-img-top" src="image/Result_Images/eyebrows/4.jpg" alt="eyebrow restoration with micro blading services
">
                          <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">DHI Eyebrows Result (#4)</h5>
   
  </div>
            </div>
 <div class="col-sm-12 text-center pb-5 pt-4">
   <a href="dhi- eyebrows-hair-transplant-results-eyebrows.php"  class="btn btn-common btn-pdding">View all Eyebrows Results</a>
 </div>



</div>
             </div>
           </div>
</div>
<div class="content">
  <div class="card ">
            <div class="card-body "> 


<div class="row">
 
   <div class="col-sm-12">
   <h3 class="text-center">Scalp Micropigmentation</h3>  
  </div> 
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
                       <img class="card-img-top" src="image/Result_Images/scalp/1.jpg" alt=" scalp hair transplant results
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">Scalp MPG Result (#1)</h5>
 
  </div>
   


  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/scalp/2.jpg" alt=" scalp hair transplant results
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">Scalp MPG Result (#2)</h5>
   
  </div>
            </div>


             <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/scalp/3.jpg" alt=" scalp hair transplant results
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">Scalp MPG Result (#3)</h5>
   
  </div>
            </div>

 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/scalp/4.jpg" alt=" scalp hair transplant results
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">Scalp MPG Result (#4)</h5>
   
  </div>
            </div>
 <div class="col-sm-12 text-center pb-5 pt-4">
   <a href="dhi-hair-transplant-results-scalp.php"  class="btn btn-common btn-pdding">View all MPG Results</a>
 </div>



</div>
             </div>
           </div>
</div>

<div class="content">
  <div class="card ">
            <div class="card-body bt-border"> 


<div class="row">
 
   <div class="col-sm-12">
   <h3 class="text-center">Beard</h3>  
  </div> 
  <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                     
                       <img class="card-img-top" src="image/Result_Images/beard/1.jpg" alt=" beard hair transplant results
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">Beard Result (#1)</h5>
 
  </div>
   


  </div>
 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/beard/2.jpg" alt=" beard hair transplant results
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">Beard Result (#2)</h5>
   
  </div>
            </div>


             <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/beard/3.jpg" alt=" beard hair transplant results
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">Beard Result (#3)</h5>
   
  </div>
            </div>

 <div class=" col-md-6 col-sm-6 col-xs-6 p-3">
                       <img class="card-img-top" src="image/Result_Images/beard/4.jpg" alt=" beard hair transplant results
">
                       <div class="card-data bg-col-w text-center">
    <h5 class="pb-3 pt-3">Beard Result (#4)</h5>
   
  </div>
            </div>
 <div class="col-sm-12 text-center pb-5 pt-4">
   <a href="dhi-hair-transplant-results-mens-beard.php"  class="btn btn-common btn-pdding">View all Beard Results</a>
 </div>



</div>
             </div>
           </div>
</div>


    </div>
</section>

   
<div class="clearfix"></div>

     
<section class="bottm_sec">
  <h1>Ready To Regain Your Hair & Confidence</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>


      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


  
  </body>
</html>
