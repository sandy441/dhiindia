
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="know about types of Alopecia &amp; treatment by DHI - Androgenetic Alopecia, Alopecia Areata, Traction, Trichotillomania, scarring, Triangular, Telogen Effluvium &amp; Loose Anagen Syndrome">
    <meta name="author" content="">
      <title>What are the causes of hair loss? DHI India
</title>
<?php include 'header.php';?>
    </head>
  <body>


<section class="bg-col-1">
  
<div class="container hair_loss">
 <div class="content">
    <div class="card bt-border">
  
  <div class="card-body pb-3">
<h2 class="text-center">Safety Protocol</h2>
           
  <div class="row">
    <div class="col-sm-6">
      <p>
        1. Rigorous medical test before the procedure to ensure that the patient can receive treatment safely
      </p>
      
      
      <p>2. Administration of local anaesthesia only by qualified & well trained doctors.   </p>
      <div class="row">
         <div class="col-sm-12">
      
     <p>3. Comprehensive risk management protocols across all clinics. </p>
     <p>4. Emergency kit in every single procedure room to deal with any unforeseen situation. </p>
     <p>5. Singe use disposable instruments of highest quality, which completely eliminate the chances of any infection. </p>
     <p> 6. Use of sharpest possible instruments specially developed for DHI to ensure no damage to donor area. </p>
     <p>7. Strict follicle care protocols to ensure maximum graft survival. </p>
     <p>8. Strict protocols for placement of follicles including grid planning, ADT & MET standards etc. for most natural results. </p>
     <p>9. Procedure performed by doctor trained & certified at London Hair Restoration Academy. </p>
     <p>10. Rigorous follow up & post procedure care for optimum results. </p>
      </div>
     <!--  <div class="col-sm-5">
       <p><img src="image/scar/fue.jpg" alt="fue" class="img-fluid"></p> 
      </div> -->
      </div>
     
     </div>
     <div class="col-sm-6">
      <p><img src="image/DHI Volume Book Cover &  Sepretor_1.jpg" alt="fue" class="img-fluid"></p>
     </div>
   </div>
 </div>

  </div><!--/row-->

  
</div>


</section>


     
<div class="clearfix"></div>

<section class="bottm_sec">
 <h1>Ready to Repair Your Scars</h1>
   <a href="book-an-appointment.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>

      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


   

  </body>
</html>
