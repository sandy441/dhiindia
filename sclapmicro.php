
<!doctype html>
<html lang="en">

  <body>
<?php include 'header.php';?>
<section class="bg-col-1">
   <div class="clearfix">
           
            	<video  preload="auto" width="100%" loop autoplay>
                        <source type="video/webm" src="video/Treatment.mp4">
                        <source type="video/mp4" src="video/Treatmentt.mp4">
                        <source type="video/webm" src="video/Treatment.ogg">
                        <source type="video/ogg" src="video/Treatment.ogg">
                    </video>
           
        </div>
        
</section>
<section class="bg-col-1">
<div class="container">
  <div class="content">
           
     <div class="card mar-minus">
            <div class="card-body maginExtra"> 
              <h2>Scalp Micro Pigmentation<span> Instantly increases hair density & covers bald spots!</span></h2>
           
           
           <p>Scalp Micro Pigmentation (MPG) involves the application of specialised hypo allergic medical pigments to the dermal layer of your scalp, to replicate the natural appearance of real hair follicles. Our expert practitioners work with your natural hair pattern and different shades of pigment to achieve the most realistic and natural-looking hairline. This non-surgical treatment is suitable and effective for all types and stages of hair loss, and for men and women of all ages, skin colours and skin types</p>
           <div class="col-md-12">
              <img src="image/key/Instument_png.png" alt="" class="img-fluid"/>
           </div>
          
            </div>
          </div>
        	

                 	</div>



</div>
</section>
<section class=" bg-col-1">
  <div class="container">
    <div class="content">
           
     <div class="card  ">
            <div class="card-body maginExtra">
            <div class="row m-lg-5 m-2 bg-col-w">

              <div class="col-sm-6 no-gutters">
                 <img src="image/treatment-img.jpg" class="img-fluid " alt="">
               </div>
               <div class="col-sm-6 ">
                <div class="mpg">
                  <h1 class="text-center">MPG</h1>
                  <h4 class="text-center">When Will The Results Show?</h4>
                  <p>After 2 or 3 sessions spaced at least one week to one month, the pigments are permanent. The results are visible immediately after your first session with us.</p>
                  <p>We specialise in creating hairlines that look natural and compliment your facial shape, style and look natural. A retouch is needed after around 3-5 years</p>
                 <h1 class="text-center"><a href="result.php" class="btn btn-outline-dark  btn-pdding">view all results</a></h1> 
                </div>
                 
               </div>
               
             </div>

            </div>
          </div>
    
  </div>

  <div class="content">
   
    <div class="card bt-border">
  <h1 class="text-center pt-4 pb-0">Key Features</h1>
  <div class="card-body ">
  <div class="row ">
    <div class="col-sm-6 pl-lg-5 pr-lg-4 pt-lg-5 pb-lg-5 p-2">
  
    <img src="image/key/Eyebrow Density.jpg" alt="" class="img-fluid">
           <div class=" bg-col-w pb-5">
            <h4 class="card-title">Eyebrow Density</h4>
  <p class="card-text">DHI Micropigmentation is applied to the eyebrow area, either into completely hairless skin or between existing hairs. This treatment offers instant high-density eyebrow simulation.</p>
           </div>
    
        
    </div>
    <div class="col-sm-6 pl-lg-4 pr-lg-5 pt-lg-5 pb-lg-5 p-2">
      
    <img src="image/key/Density-effect.jpg" alt="" class="img-fluid">
      <div class=" bg-col-w">
             <h4 class="card-title">Create a Density Effect</h4>
    <p class="card-text">Scalp MPG is a good option when the patient’s hair has a low density and the scalp can be seen too easily. Thanks to its pigments injected under the skin, it “colors” the scalp as a tattoo and decrease the skin/ hair contrast: This gives instant density and visible volume.</p>
      </div>
             
 
    </div>
    
  </div>
   <div class="row  ">
     <div class="col-sm-6 pl-lg-5 pr-lg-4  pb-lg-5 p-2">
          <img src="image/key/Supports Hair Transplant.jpg" alt="" class="img-fluid">
           <div class=" bg-col-w ">
               <h4 class="card-title">Supports Hair Transplant</h4>
    <p class="card-text">Scalp MPG works very well with the DHI Direct method, especially when the donor area is insufficient to create an optimum density. It gives a denser look which looks 100% natural and blends with existing hair.</p>
           </div>
             
               
    
         
       
    </div>
     <div class="col-sm-6 pl-lg-4 pr-lg-5  pb-lg-5 p-2 ">
      
    <img src="image/key/Hide-Scars.jpg" alt="" class="img-fluid">
          
          <div class=" bg-col-w">
                 <h4 class="card-title">Hide Scars</h4>
    <p class="card-text">Many men who have already undergone a hair transplant with older methods such as FUT (Strip) find themselves with a scar on the back of the head that they can’t conceal with short hair. In these cases, and for any type of scar, MPG helps conceal the scars.</p>
    
          </div>
            
         
         
       
    </div>
    
   <div class="clearfix pl-lg-5 pr-lg-5 pb-lg-5 p-2 ">
      
     <img src="image/key/Camouflaging-Baldness.jpg" class="img-fluid" alt="img">
          
          <div class=" bg-col-w ">
             <h4 class="card-title">Camouflaging Baldness</h4>
    <p class="card-text">MPG of the scalp allows to recreate the illusion of the hair follicles on the head and thus to mask the baldness, on a small patch or a completely bald scalp.</p>
    
          </div>
            
         
         
       
    </div>
    
   </div>
  </div>
</div>
            
  
 
                  </div>
</section>

     
<div class="clearfix"></div>
<!--  <?php include 'slick-slider2.php';?>  -->
<section class="bottm_sec">
 <h1>Increase your hair density naturally</h1>
   <a href="book-appoint.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>
     



      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


    
  </body>
</html>
