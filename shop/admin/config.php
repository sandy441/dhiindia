<?php
// HTTP
define('HTTP_SERVER', 'http://shop.dhiindia.com/admin/');
define('HTTP_CATALOG', 'http://shop.dhiindia.com/');

// HTTPS
define('HTTPS_SERVER', 'https://shop.dhiindia.com/admin/');
define('HTTPS_CATALOG', 'https://shop.dhiindia.com/');

// DIR
define('DIR_APPLICATION', 'c:/inetpub/wwwroot/shop/admin/');
define('DIR_SYSTEM', 'c:/inetpub/wwwroot/shop/system/');
define('DIR_LANGUAGE', 'c:/inetpub/wwwroot/shop/admin/language/');
define('DIR_TEMPLATE', 'c:/inetpub/wwwroot/shop/admin/view/template/');
define('DIR_CONFIG', 'c:/inetpub/wwwroot/shop/system/config/');
define('DIR_IMAGE', 'c:/inetpub/wwwroot/shop/image/');
define('DIR_CACHE', 'c:/inetpub/wwwroot/shop/system/cache/');
define('DIR_DOWNLOAD', 'c:/inetpub/wwwroot/shop/system/download/');
define('DIR_UPLOAD', 'c:/inetpub/wwwroot/shop/system/upload/');
define('DIR_LOGS', 'c:/inetpub/wwwroot/shop/system/logs/');
define('DIR_MODIFICATION', 'c:/inetpub/wwwroot/shop/system/modification/');
define('DIR_CATALOG', 'c:/inetpub/wwwroot/shop/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '!123Dhiitdepartment123!');
define('DB_DATABASE', 'shop');
define('DB_PORT', '3306');
define('DB_PREFIX', '');
