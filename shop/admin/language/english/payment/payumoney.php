<?php
// Heading
$_['heading_title']      = 'PayU Money'; 
$_['advert']			 = ' - [<a href="http://www.opencartextensions.eu">www.opencartextensions.eu</a>]';

// Text 
$_['text_edit']			 = 'Edit PayU Money';
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Success: You have modified PayU Money account details!';
$_['text_payumoney']     ='<a href="http://www.payumoney.com target="_blank"><img src="view/image/payment/payumoney_logo.gif" style="border: 1px solid #EEEEEE;" width="94" height="25" alt="PayU Money" title="PayU Money"/></a>';
$_['text_off']           = 'Off';
$_['text_on']            = 'On';

// Entries
$_['entry_merchant_key'] = 'Merchant Key:';
$_['entry_salt']         = 'Salt:';
$_['entry_test']         = 'Test Mode:';
$_['entry_total']        = 'Total:';
$_['entry_order_status'] = 'Order Status:';
$_['entry_geo_zone']     = 'Geo Zone:';
$_['entry_status']       = 'Status:'; 
$_['entry_sort_order']   = 'Sort Order:';

//Help
$_['help_total']         =' The checkout total the order must reach before this payment method becomes active';

// Errors
$_['error_permission']   = 'Warning: You do not have permission to modify PayU Money Payment gateway!';
$_['error_merchant_key'] = 'Merchant Key is Required!';
$_['error_salt']         = 'Salt is Required!';
?>