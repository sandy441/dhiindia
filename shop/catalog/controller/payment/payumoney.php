<?php

#################################################################
## Open Cart Module:  PAY U MONEY PAYMENT GATEWAY		       ##
##-------------------------------------------------------------##
## Copyright © 2014 MB "Programanija" All rights reserved.     ##
## http://www.opencartextensions.eu						       ##
## http://www.extensionsmarket.com 						       ##
##-------------------------------------------------------------##
## Permission is hereby granted, when purchased, to  use this  ##
## mod on one domain. This mod may not be reproduced, copied,  ##
## redistributed, published and/or sold.				       ##
##-------------------------------------------------------------##
## Violation of these rules will cause loss of future mod      ##
## updates and account deletion				      			   ##
#################################################################

class ControllerPaymentPayumoney extends Controller {
	
	public function index() {
		
		$this->load->model('checkout/order');
		$this->language->load('payment/payumoney');
		
		$data['button_confirm'] = $this->language->get('button_confirm');
		
		if($this->config->get('payumoney_test')=='On'){
			$data['action'] = 'https://test.payu.in/_payment';
		} else {
		    $data['action'] = 'https://secure.payu.in/_payment';
		}
			
		$hash = '';
		$products = '';
		foreach ($this->cart->getProducts() as $product) {
			$products .= $product['quantity'] . ' x ' . $product['name'] . ', ';
		}
		
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		$data['payumoney_merchant_key'] = $this->config->get('payumoney_merchant_key');
		//$data['txnid'] = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$data['txnid'] = $this->session->data['order_id'];
		$data['amount'] = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);
		$data['productinfo'] = 'xxx';
		$data['firstname'] = $order_info['payment_firstname'];
		$data['lastname'] = $order_info['payment_lastname'];
		$data['address1'] = $order_info['payment_address_1'];
		$data['address2'] = $order_info['payment_address_2'];
		$data['city'] = $order_info['payment_city'];
		$data['state'] = $order_info['payment_zone'];
		$data['country'] = $order_info['payment_iso_code_3'];
		$data['zipcode'] = $order_info['payment_postcode'];
		$data['email'] = $order_info['email'];
		$data['phone'] = $order_info['telephone'];
		$data['surl'] = $this->url->link('payment/payumoney/callback');
		$data['furl'] = $this->url->link('payment/payumoney/callback');
		$data['curl'] = $this->url->link('checkout/checkout', '', 'SSL');
		$data['pg'] = 'CC';
		$data['service_provider'] = 'payu_paisa';
		$data['salt'] = $this->config->get('payumoney_salt');
		
		//$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		
		$hash_string = $data['payumoney_merchant_key'].'|'.$data['txnid'].'|'.$data['amount'].'|'.$data['productinfo'].'|'.$data['firstname'].'|'.$data['email'].'|||||||||||'.$data['salt'];
		
		$hash = strtolower(hash('sha512', $hash_string));
		
		$data['hash'] = $hash;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/payumoney.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/payumoney.tpl', $data);	
		} else {	
			return $this->load->view('default/template/payment/payumoney.tpl', $data);
		}	

	}
	
	
	public function callback() {
				
		$this->load->language('payment/payumoney');

		if (isset($this->request->post['txnid'])) {
			$order_id = $this->request->post['txnid'];
		} else {
			$order_id = 0;
		}

		$this->load->model('checkout/order');

		$order_info = $this->model_checkout_order->getOrder($order_id);
		
		if ($order_info) {
			
			$hash_string = $this->config->get('payumoney_salt').'|'.$this->request->post['status'].'|||||||||||'.$this->request->post['email'].'|'.$this->request->post['firstname'].'|'.$this->request->post['productinfo'].'|'.$this->request->post['amount'].'|'.$this->request->post['txnid'].'|'.$this->request->post['key'];
	
			$hash = strtolower(hash('sha512', $hash_string));
					
			$error = '';

			if (!isset($this->request->post['key']) || $this->request->post['key'] != $this->config->get('payumoney_merchant_key')) {
				
				$error = $this->language->get('text_unable');
			} 
			
			elseif ($this->request->post['hash'] != $hash) {
				$error = $this->language->get('text_unable');
				
			} elseif ($this->request->post['status'] != 'success') {
				$error = $this->language->get('text_declined');
			}
			
		} else {
			$error = $this->language->get('text_unable');
		}
		
		
		if ($error) {
			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_basket'),
				'href' => $this->url->link('checkout/cart')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_checkout'),
				'href' => $this->url->link('checkout/checkout', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_failed'),
				'href' => $this->url->link('checkout/success')
			);

			$data['heading_title'] = $this->language->get('text_failed');

			$data['text_message'] = sprintf($this->language->get('text_failed_message'), $error, $this->url->link('information/contact'));

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
			}
		} else {
			$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payumoney_order_status_id'));

			$this->response->redirect($this->url->link('checkout/success'));
		}
		
	}
}
?>