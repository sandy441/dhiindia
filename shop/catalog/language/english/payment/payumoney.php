<?php

#################################################################
## Open Cart Module:  PAY U MONEY PAYMENT GATEWAY		       ##
##-------------------------------------------------------------##
## Copyright © 2014 MB "Programanija" All rights reserved.     ##
## http://www.opencartextensions.eu						       ##
## http://www.extensionsmarket.com 						       ##
##-------------------------------------------------------------##
## Permission is hereby granted, when purchased, to  use this  ##
## mod on one domain. This mod may not be reproduced, copied,  ##
## redistributed, published and/or sold.				       ##
##-------------------------------------------------------------##
## Violation of these rules will cause loss of future mod      ##
## updates and account deletion				      			   ##
#################################################################

// Text
$_['text_title']				= 'Credit Card / Debit Card (PayU Money)';
$_['text_unable']				= 'Unable to locate or update your order status';
$_['text_declined']				= 'Payment was declined by PayU Money';
$_['text_failed']				= 'PayU Money Transaction Failed';
$_['text_failed_message']		= '<p>Unfortunately there was an error processing your PayU Money transaction.</p><p><b>Warning: </b>%s</p><p>Please verify your PayU Money account balance before attempting to re-process this order</p><p> If you believe this transaction has completed successfully, or is showing as a deduction in your PayU Money account, please <a href="%s">Contact Us</a> with your order details.</p>';
$_['text_basket']				= 'Basket';
$_['text_checkout']				= 'Checkout';
$_['text_success']				= 'Success';									  
?>