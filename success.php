<?php
/* 
Template Name: Success
Description: Payu
*/

if(!session_id()) {
    session_start();
}

//include 'header.php';
include 'classes/userinfo.php';
$userinfo = new userinfo();
$home_url = $userinfo->getBaseUrl();
 ?>
<!--</div>
</div>
</div>
<div class="thankyou"  style="background-image:url(<?php echo $home_url.'/image/udsa2.png'; ?>)">
<div class="thanksmsg">-->
<?php 
$api_key = "d1942cc8edb1bbd654af61c9c6ece16f";
$from = "info@dhiindia.com";
$fromname = "DHI-India";

//echo '<pre>';print_r($_REQUEST);die;
 if (!empty($_POST['status']) && $_POST['status']== 'success') {
		$status=$_POST['status'];
		$firstname=$_POST["firstname"];
		$amount=$_POST["amount"];
		$txnid=$_POST["txnid"];
		$posted_hash=$_POST["hash"];
		$key=$_POST["key"];
		$productinfo=$_POST["productinfo"];
		$email=$_POST["email"];
		$salt="mZxvMkXhX0";
                $payment_source = isset($_REQUEST['payment_source'])?'PayU':'PayTm';
		/* Update value into db of obbservlabs */
		$userinfo->sendTxnIdToobserveLabs($txnid);
                $userinfo->paymentSuccess($txnid);
                $mail_record = $userinfo->fetchUserdetails($txnid);
                
//				if($mail_record->status_main==1){$status_main='Online';}
//				if($mail_record->status_main==2){$status_main='Skype';}
			
				$city_email = $mail_record->city_email;
				$message = "<html><body>";
				$message .= "<h2>".ucfirst($mail_record->name). "&nbsp; has booked an appointment with DHI. The details are as follows:</h2>";
				$message .= "<table  cellpadding='10'>";
				$message .= "<tr><td><strong>Payment Mode</strong> </td><td>" .$payment_source."</td></tr>";
				$message .= "<tr><td><strong>Txn Id</strong> </td><td>" .$txnid."</td></tr>";
				$message .= "<tr><td><strong>Name</strong> </td><td>" .$mail_record->name."</td></tr>";
				$message .= "<tr><td><strong>Phone</strong> </td><td>" .$mail_record->phone."</td></tr>";
				$message .= "<tr><td><strong>Email</strong> </td><td>" .$mail_record->email."</td></tr>";
				$message .= "<tr><td><strong>Appointment Date</strong> </td><td>" .$mail_record->app_date."</td></tr>";
				$message .= "<tr><td><strong>Appointment Time</strong> </td><td>" .$mail_record->start_time."</td></tr>";
				$message .= "<tr><td><strong>City Name</strong> </td><td>" .$mail_record->city_name."</td></tr>";
				$message .= "</table>";
				$message .= "</body></html>";
				$subject = "New appointment booked by $mail_record->name through $payment_source";  
				
				$to = $city_email; //Recipients list (semicolon separated)
				
				$emailIdCity = explode(',', $city_email); 
				$email_trim=array_map('trim', $emailIdCity);
				$to_email_address = array_unique($email_trim);
				$to = $userinfo->TestEnv()?'sandeep.kumar@dhiindia.com':implode(', ', $to_email_address); //Recipients list (semicolon separated)
				$content = $message;
				$data=array();
				$data['subject']= $subject;                                                                       
				$data['fromname']= $fromname;                                                             
				$data['api_key'] = $api_key;
				$data['from'] = $from;
				$data['content']= $content; 
				$data['recipients']= $to;
                               
				$userinfo->callMailApi(@$api_type,@$action,$data);
				
				$message1 = "<html><body>";
				$message1 .= "<h2>".ucfirst($mail_record->name). " &nbsp; Thank you for booking an appointment with DHI. The details are as follows:</h2>";
				$message1 .= "<table  cellpadding='10'>";
                                $message1 .= "<tr><td><strong>Payment Mode</strong> </td><td>" .$payment_source."</td></tr>";
				$message1 .= "<tr><td><strong>Txn Id</strong> </td><td>" .$txnid."</td></tr>";
				$message1 .= "<tr><td><strong>City Name</strong> </td><td>" .$mail_record->city_name."</td></tr>";
				$message1 .= "<tr><td><strong>Phone</strong> </td><td>" .$mail_record->phone."</td></tr>";
				$message1 .= "<tr><td><strong>Email</strong> </td><td>" .$mail_record->email."</td></tr>";
				$message1 .= "<tr><td><strong>Appointment Date</strong> </td><td>" .$mail_record->app_date."</td></tr>";
				$message1 .= "<tr><td><strong>Appointment Time</strong> </td><td>" .$mail_record->start_time."</td></tr>";
				$message1 .= "<tr><td><strong>Consultation Fees Paid</strong> </td><td> INR " .$amount."</td></tr>";
				$message1 .= "<tr><td><strong>Clinic Address</strong> </td><td>" .$mail_record->city_address."</td></tr>";
				$message1 .= "</table>";
				$message1 .= "</body></html>";
				$subject1 = 'Your DHI Appointment has been scheduled successfully';
							
				
				
				$to = $mail_record->email; //Recipients list (semicolon separated)
				$subject = $subject1;
				$content = $message1;
				$data=array();
				$data1['subject']= $subject;                                                                       
				$data1['fromname']= $fromname;                                                             
				$data1['api_key'] = $api_key;
				$data1['from'] = $from;
				$data1['content']= $content;
				$data1['recipients']= $to;
                               
				$userinfo->callMailApi(@$api_type,@$action,$data1);
				
				
//				echo "<h2>Thank You!</h2>";
//                                echo "<h3>Your appointment has been booked.</h3>";
//                                echo "<h4>We have received the payment of your consultation fee.</h4>";
//				echo "<h4>Your Transaction ID is <b>".$txnid."</b></h4>";
//                                echo "<div class='emailaddress'><h6><span>Clinic Address : </span>".$mail_record->city_address."</h6></div>"; 
//				
                                
                                /* Messages */
				$phone_no_client = $mail_record->phone;
				$phone_no_Clinic = $mail_record->city_mobile;
                               
					
					$msg = urlencode("Greetings from DHI Medical Group! \n
						Your consultation has been confirmed for  ".date('d M', strtotime($mail_record->app_date)).",".date('h:i A', strtotime($mail_record->start_time))." at ".$mail_record->city_address."\n
						Booking ID:".$txnid);
					
					
					$userinfo->callSmsApi($phone_no_client,$msg);
						
					 $msg2 = urlencode("Hello, \n
						New appointment #".$txnid." has been confirmed at DHI ".$mail_record->city_name." at ".date('d M', strtotime($mail_record->app_date))." ".date('h:i A', strtotime($mail_record->start_time))." \n
						Thank you");
						
					$welcome2 = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=357548&username=8826006713&password=agpjp&To=".$phone_no_Clinic."&Text=".$msg2;
					$phclinic =	$userinfo->TestEnv()?'9716176394':$phone_no_Clinic;	
                                        $userinfo->callSmsApi($phclinic,$msg2);
                                       // header('Location:/thanku.php?succ');
		   }else if(!empty($_SESSION['txnid'])){
		    $txnid = $_SESSION['txnid'];
		   	//unset($_SESSION['txnid']);
		   	/* Update value into db of obbservlabs */
			$userinfo->sendTxnIdToobserveLabs($txnid);
                        $userinfo->paymentSuccess($txnid);
                        $mail_record = $userinfo->fetchUserdetails($txnid);
         
				$city_email = $mail_record->city_email;
				$message = "<html><body>";
				$message .= "<h2>".ucfirst($mail_record->name). "&nbsp; has expressed interest in DHI and generated a query. The details are as follows:</h2>";
				$message .= "<table  cellpadding='10'>";
                                $message .= "<tr><td><strong>Payment Mode</strong> </td><td>Pay At Clinic</td></tr>";
				$message .= "<tr><td><strong>Txn Id</strong> </td><td>" .$txnid."</td></tr>";
				$message .= "<tr><td><strong>Name</strong> </td><td>" .$mail_record->name."</td></tr>";
				$message .= "<tr><td><strong>Phone</strong> </td><td>" .$mail_record->phone."</td></tr>";
				$message .= "<tr><td><strong>Email</strong> </td><td>" .$mail_record->email."</td></tr>";
				$message .= "<tr><td><strong>Appointment Date</strong> </td><td>" .$mail_record->app_date."</td></tr>";
				$message .= "<tr><td><strong>Appointment Time</strong> </td><td>" .$mail_record->start_time."</td></tr>";
				$message .= "<tr><td><strong>City Name</strong> </td><td>" .$mail_record->city_name."</td></tr>";
				$message .= "</table>";
				$message .= "</body></html>";
				$subject = 'New appointment request by '.$mail_record->name;
				
				$to = $city_email; //Recipients list (semicolon separated)
				$subject = $subject;
				$emailIdCity = explode(',', $city_email); 
				$email_trim=array_map('trim', $emailIdCity);
				$to_email_address = array_unique($email_trim);
				$to = $userinfo->TestEnv()?'sandeep.kumar@dhiindia.com':implode(', ', $to_email_address); //Recipients list (semicolon separated)
				$content = $message;
				$data=array();
				$data['subject']= $subject;                                                                       
				$data['fromname']= $fromname;                                                             
				$data['api_key'] = $api_key;
				$data['from'] = $from;
				$data['content']= $content;
				$data['recipients']= $to;
//                                echo '<pre>';print_r($data);die;
				$userinfo->callMailApi(@$api_type,@$action,$data);
				
				$message1 = "<html><body>";
				$message1 .= "<h2>".ucfirst($mail_record->name). " &nbsp; We have successfully registered your appointment. Our executive will get back to you shortly with the final confirmation. The details are as follows:</h2>";
				$message1 .= "<table  cellpadding='10'>";
                                $message1 .= "<tr><td><strong>Payment Mode</strong> </td><td>Pay At Clinic</td></tr>";
				$message1 .= "<tr><td><strong>Txn Id</strong> </td><td>" .$txnid."</td></tr>";
				$message1 .= "<tr><td><strong>City Name</strong> </td><td>" .$mail_record->city_name."</td></tr>";
				$message1 .= "<tr><td><strong>Phone</strong> </td><td>" .$mail_record->phone."</td></tr>";
				$message1 .= "<tr><td><strong>Email</strong> </td><td>" .$mail_record->email."</td></tr>";
				$message1 .= "<tr><td><strong>Appointment Date</strong> </td><td>" .$mail_record->app_date."</td></tr>";
				$message1 .= "<tr><td><strong>Appointment Time</strong> </td><td>" .$mail_record->start_time."</td></tr>";
				$message1 .= "<tr><td><strong>Consultation Fees</strong> </td><td> INR " .$mail_record->consultation_fees." (Payable at clinic)</td></tr>";
				$message1 .= "<tr><td><strong>Clinic Address</strong> </td><td>" .$mail_record->city_address."</td></tr>";
				$message1 .= "</table>";
				$message1 .= "</body></html>";
				$subject1 = 'Your DHI Appointment has been registered successfully';
				
				$to = $mail_record->email; 
				$subject = $subject1;
				$content = $message1;
				$data=array();
				$data1['subject']= $subject;                                                                       
				$data1['fromname']= $fromname;                                                             
				$data1['api_key'] = $api_key;
				$data1['from'] = $from;
				$data1['content']= $content;
				$data1['recipients']= $to;
				$userinfo->callMailApi(@$api_type,@$action,$data1);
				
//				echo "<h2>Thank You!</h2>";
//                                echo "<h4>Your consultation has been booked. Our executive will contact you shortly.</h4>"; 
//				echo "<h4>Your Transaction ID is <b>".$txnid."</b></h4>";
//				echo "<div class='emailaddress'><h6><span>Clinic Address : </span>".$mail_record->city_address."</h6></div>"; 
				/* Messages */
				
				$phone_no_client = $mail_record->phone;
				$phone_no_Clinic = $mail_record->city_mobile;
					
                                        $msg = urlencode("Greetings from DHI Medical Group! \n
						Your consultation has been confirmed for  ".date('d M', strtotime($mail_record->app_date)).",".date('h:i A', strtotime($mail_record->start_time))." at ".$mail_record->city_address."\n
						Booking ID:".$txnid);
					
				$userinfo->callSmsApi($phone_no_client,$msg);
					
                                        $msg2 = urlencode("Hello, \n
						#".$txnid." has expressed interest in DHI and generated a query for DHI ".$mail_record->city_name." at ".date('d M', strtotime($mail_record->app_date))." ".date('h:i A', strtotime($mail_record->start_time))." \n
						Thank you");
				$phclinic =	$userinfo->TestEnv()?'9716176394':$phone_no_Clinic;	
				$userinfo->callSmsApi($phclinic,$msg2);
					?>
					
					<?php
        }else {
            echo "Invalid Transaction. Please try again";

        } 
        header('Location:/thanku.php?success');  
?>

<!--</div>
</div>-->

<?php //include 'footer.php'; ?>
