
<!doctype html>
<html lang="en">
  
  <body>
<?php include 'header.php';?>

<section class="bg-col-1">
  <nav aria-label="breadcrumb" role="navigation">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">DHI India</a></li>
    <li class="breadcrumb-item active" aria-current="page">Terms & Conditions</li>
  </ol>
</nav>
<div class="container">
 <div class="content">
    <div class="card bt-border">
  
  <div class="card-body ">
     <h2>Terms & Conditions</h2>
    <div class="row ">
        
  <p>Entering this site constitutes your &ldquo;digital signature&rdquo; and acknowledgement that you are at least 18 years of age and agree to all of the terms and conditions. No information provided on this website shall amount to promoting the use of or advertising the products of the company in any manner whatsoever.</p>
<p><strong>LIMITED RIGHTS:</strong>&nbsp;Subject to the terms and conditions set forth in this agreement, SMPL grants you a non-exclusive, non-transferable, limited right to access, use and display this site and the materials thereon. You agree not to interrupt or attempt to interrupt the operation of the site in any way.</p>
<p><strong>LIMITED PERMISSION TO COPY:</strong>&nbsp;You are permitted to view, print or download extracts from these pages for your personal non-commercial use only. This authorization is not a transfer of title. No part of this site may be reproduced or transmitted to or stored in any other web site, nor may any of its pages or part thereof be disseminated in any electronic or non-electronic form, nor included in any public or private electronic retrieval system or service without prior written permission.</p>
<p><strong>INTELLECTUAL PROPERTY RIGHTS:</strong>Any and every material, content, data, information mentioned on this website is the property of SMPL. No content or any portion of it contained herein may be copied without the express permission of SMPL for any purpose whatsoever. The trademarks whether registered or unregistered, compilation including the bulletin board services, chat areas, news groups, forums, communities, personal web pages, calendars, graphics, logos and service names used on our site and/or other message or communication on this website are the exclusive property of SMPL. The copyright in the content of this website is owned by SMPL. You agree that any material, information, and ideas that you transmit to this site shall also be and always be the property of SMPL. Any infringement shall be vigorously defended and pursued to the fullest extent permitted by law. The information and services provided to you is on &ldquo;as is basis&rdquo;. SMPL and its affiliates, agents and licensors cannot and do not warrant the accuracy, completeness, non-infringement, merchantability or fitness of the information available on the web site and the information should not be relied upon for personal, legal, commercial or financial decisions. You should consult an appropriate professional for specific advice tailored to your situation.</p>
<p><strong>USEFUL LINKS AND INFORMATION:</strong>&nbsp;Links to other sites created and/or maintained by organizations other than SMPL are for information only and are provided as part of the service that we offer. SMPL accepts no responsibility or liability for access to, or the accuracy of, the material on any site that is linked to from this &ldquo;Local Links&rdquo; page, and SMPL does not necessarily endorse any views expressed within them. We have no control over the availability of the linked pages. You acknowledge and agree that SMPL does not endorse the content of any member or any site accessed via links or other means from the site and it is not responsible or liable for such content even though it may be unlawful, harassing, libelous, privacy invading, abusive, threatening, harmful, obscene, or otherwise objectionable, or that it infringes or may infringe the intellectual property or other rights of another person.</p>
<p><strong>DISCLAIMER OF WARRANTIES:</strong>&nbsp;SMPL provides the site and the information on an &ldquo;as is, where is and as available&rdquo; basis. To the fullest extent permitted by law, SMPL does not make any express or implied warranties, representations, endorsements or conditions with respect to the site or the information, including without limitation, warranties as to merchantability, operation, non-infringement, usefulness, completeness, accuracy, correctness, reliability and fitness for a particular purpose. Further, SMPL does not represent or warrant that the site will be available or meet your requirements, that access will be uninterrupted, that there will be no delays, failures, errors or omissions or loss of transmitted information, that no viruses or other contaminating or destructive properties will be transmitted, or that no damage will occur to your computer system. You have sole responsibility for adequate protection and backup of data and/or equipment and to take all precautions to scan for computer viruses or other destructive properties.</p>
<p><strong>NO LIABILITY:</strong>Except where prohibited by law, under no circumstances, including, but not limited to, negligence, shall SMPL be liable for any direct, indirect, special, incidental or consequential other damages, including but not limited to, loss of data or profit, arising out of the use, or the inability to use, the site or the information, whether such claim is based upon breach of contract , breach of warranty, negligence , gross negligence , strict liability in tort or any other theory of relief , even if SMPL has been advised in advance of the possibility of such damages. DISCLOSURE: SMPL reserves the right at all times to disclose any information as necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in the sole discretion of SMPL. The information and content provided on SMPL&rsquo;s website and the user&rsquo;s access of website does not create a client- consultant or any other professional relationship between the user and SMPL.</p>
<p><strong>CHANGES:</strong>&nbsp;The content, compilation, information relating to the contents of the website are subjected to periodical changes, alteration, addition, deletion or amendments at any time. SMPL may terminate, change the site, suspend or discontinue any aspect of the site, including the availability of any features of the site, at any time. SMPL may also impose limits on certain features and services or restrict your access to parts or the entire site without notice or liability. SMPL may terminate the authorization, rights and license given above and, upon such termination; you shall immediately destroy all materials.</p>
<p>If any part of this disclaimer is held to be invalid, the remaining parts will continue to be valid and enforceable.</p>
<p>If the user has any comments or questions regarding the website or would like to obtain permission to use any of the information or content in the website. Please use our query form to contact us.</p>
     
       
       
         
           
    </div>     












      
   
  
</div>
           
  
         
        
         
            
        
            </div>
  </div><!--/row-->

  
</div>


</section>


     
<div class="clearfix"></div>

<section class="bottm_sec">
 <h1>Ready to regain your hair & confidence</h1>
   <a href="book-appoint.php" class="btn btn-outline-action btn-lg mt-4 mb-4">Take action now</a>
</section>

      <!-- FOOTER -->
       
<?php include 'footer.php';?>
  


   

  </body>
</html>
